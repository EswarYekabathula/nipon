<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Services_model extends CI_MODEL{
	
	
	public function check($table,$data)
	{
		$this->db->where($data);
		return $this->db->get($table);
        //echo $this->db->last_query(); exit;
	}
	
	public function update($table,$check,$data)
	{
		$this->db->where($check);
		return $this->db->update($table,$data); 
        //echo $this->db->last_query(); exit;
	}
	
	public function save($table,$data){
		return $this->db->insert($table,$data);
		//echo $this->db->last_insert_id();
		//echo $this->db->last_query(); exit;
	}
	
	
	public function get($table){
		return $this->db->get($table);
	}
	
	public function search_brand($keyword){
		$this->db->select('bid,brand_name');
		$this->db->from('brands');
		$this->db->like('brand_name',$keyword);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function search_colours($keyword){
		$this->db->select('cid,colour_name');
		$this->db->from('colours');
		$this->db->like('colour_name',$keyword);
		$query = $this->db->get();	
		//echo $this->db->last_query();exit;
		return $query->result_array();
	}
	
	public function search_colour_groups($keyword){
		$this->db->select('cgid,colour_group_name');
		$this->db->from('colour_groups');
		$this->db->like('colour_group_name',$keyword);
		$query = $this->db->get();		
		return $query->result_array();
	}
	
	public function search_colour_types($keyword){
		$this->db->select('ctid,colour_type');
		$this->db->from('colour_types');
		$this->db->like('colour_type',$keyword);
		$query = $this->db->get();	
		return $query->result_array();
	}
	
	public function search_products($keyword){
		$this->db->select('*');
		$this->db->from('products');
		$this->db->like('product_name',$keyword);
		$this->db->where('online_consumption < quantity');
		$query = $this->db->get();	
		return $query->result_array();
	}
	
	
	public function get_offers(){
		$date = date('Y-m-d');
		$this->db->select('oid,offer_name,offer_validity_from,offer_validity_to,offer_type,vp,products,offer_on,oimage,offer_valid as offer_valid_for_dealers');
		$this->db->where('offer_validity_from <=', $date);
		$this->db->where('offer_validity_to >=', $date);
		$this->db->from('offers');
		return $this->db->get();
		echo $this->db->last_query();exit;
	}
	          
	
	public function get_products($table){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('online_consumption < quantity');
		return $this->db->get();
		echo $this->db->last_query();exit;
	}
	
	public function get_product_suggestions(){
		$this->db->select('*');
		$this->db->from('products');
		$this->db->where('suggested_for != "0"');
		return $this->db->get();	
		//echo $this->db->last_query();exit;
	}
	
	//  Check offers for products
	public function check_offers_for_products($product_id){
		$this->db->select('*');
		$this->db->from('offers');
		$this->db->where_in('products',$product_id);
		$query = $this->db->get();
		return $query->result_array();
	}
	
}


?>