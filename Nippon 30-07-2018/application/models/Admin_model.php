<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
        parent::__construct();		
		$this->load->library('email');	
    }

	public function get($table){
		if($table == 'brands'){
			$this->db->order_by('bid',desc);
		}else  if($table == 'source_countries'){
			$this->db->order_by('sid',desc);
		}else  if($table == 'colour_groups'){
			$this->db->order_by('cgid',desc);
		}else  if($table == 'colour_types'){
			$this->db->order_by('ctid',desc);
		}else  if($table == 'delivery_types'){
			$this->db->order_by('dltid',desc);
		}else  if($table == 'feedback'){
			$this->db->order_by('fid',desc);
		}else  if($table == 'offers'){
			$this->db->order_by('oid',desc);
		}
		return $this->db->get($table);
	}
	
	public function order_analy_month($where){
		$this->db->select('count(month(added_on)) as orders_count');
		$this->db->from('orders');
		$this->db->where($where);
		return $this->db->get();
		//echo $this->db->last_query();exit;
	}

	public function get_ls($table,$limit, $start){
		$this->db->limit($limit, $start);
		if($table == 'catalysts'){
			$this->db->order_by('cid',desc);
		}else if($table == 'delivery_partners'){
			$this->db->order_by('dlvid',desc);			
		}else if($table == 'dealers'){
			$this->db->order_by('did',desc);			
		}else if($table == 'franchise_partners'){
			$this->db->order_by('fid',desc);			
		}else if($table == 'colours'){
			$this->db->order_by('cid',desc);			
		}else if($table == 'products'){
			$this->db->order_by('pid',desc);			
		}else if($table == 'pincodes'){
			$this->db->order_by('pid',desc);			
		}
		return $this->db->get($table);
	}	

	public function get_colour_suggestions(){
		$this->db->select('*');
		$this->db->from('colours');
		$this->db->where('suggested_for != "0"');
		$this->db->limit($limit, $start);
		return $this->db->get($table);
	}
	
	public function get_colours($table,$limit, $start){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('suggested_for = "0"');	
		$this->db->limit($limit, $start);		
		return $this->db->get();
	}
	
	public function get_products($table){		
		return $this->db->get($table);
	}
	
	
	
	public function check($table,$data)
	{
		$this->db->where($data);
		return $this->db->get($table);
        //echo $this->db->last_query(); exit;
	}
	
	
	public function update($table,$check,$data)
	{
		$this->db->where($check);
		return $this->db->update($table,$data); 
        echo $this->db->last_query(); exit;
	}
	
	
	
	public function save($table,$data){
		return $this->db->insert($table,$data);
		
	}
	
	public function count($table){
	$this->db->select('*');
	$this->db->from($table);
	$query = $this->db->get();
	return $query->num_rows();
	}
	
		
	public function login($email,$password)	
	{
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where('email',$email);
		$this->db->where('password',$password);
        $this->db->where('status','1');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		return $query;		
	}
	
	public function get_states(){
		$this->db->select('State');
		$this->db->from('csci');
		$this->db->where('Country_Code','IN');
		$this->db->group_by('State');
		$query = $this->db->get();		
		return $query->result_array();
	}
	
	public function get_cities(){
		$this->db->select('City');
		$this->db->from('csci');
		$this->db->where('Country_Code','IN');		
		$query = $this->db->get();		
		return $query->result_array();
	}
	
	public function get_cities_b_states($state){
		$this->db->select('City');
		$this->db->from('csci');
		$this->db->where('State',$state);
		$query = $this->db->get();
		return $query->result_array();		
	}
	
	
	public function get_admin_data()
	{
		$id = $this->session->userdata('admin_session')['id'];
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where('sid',$id);
		$query = $this->db->get();
		return $query->row_array();
	}	
	
	public function get_users(){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->order_by('uid','desc');
		return $this->db->get();
	}
	
	
	
	
	
	
	
}