<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalyst_model extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
        parent::__construct();
		
		$this->load->library('email');	
    	}

	public function get($table){
		if($table == 'feedback'){
			$this->db->order_by('fid',desc);
		}else  if($table == 'offers'){
			$this->db->order_by('oid',desc);
		}
		return $this->db->get($table);
	}

	public function get_ls($table,$limit, $start){
		$this->db->limit($limit, $start);
		if($table == 'dealers'){
			$this->db->order_by('did',desc);
		}else if($table == 'franchise_partners'){
			$this->db->order_by('fid',desc);			
		}else if($table == 'delivery_partners'){
			$this->db->order_by('dlvid',desc);			
		}
		return $this->db->get($table);
	}	

	public function condition_and_limit_query($table,$where,$limit, $start){
		$this->db->where($where);
		$this->db->limit($limit, $start);
		$this->db->order_by('oid',desc);
		return $this->db->get($table);
		//echo $this->db->last_query(); exit;
	}
	
	public function check($table,$data)
	{
		$this->db->where($data);
		return $this->db->get($table);
        //echo $this->db->last_query(); exit;
	}
	
	
	public function update($table,$check,$data)
	{
		$this->db->where($check);
		return $this->db->update($table,$data); 
        echo $this->db->last_query(); exit;
	}
	
	
	
	public function save($table,$data){
		return $this->db->insert($table,$data);
		//echo $this->db->last_insert_id();
		//echo $this->db->last_query(); exit;
	}
	
	public function count($table){
		$this->db->select('*');
		$this->db->from($table);
		$query = $this->db->get();
		return $query->num_rows();
	}
	
	public function count_with_condition($table,$where){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		if($table == 'notifications'){
			$this->db->order_by('id',desc);
		}
		$query = $this->db->get();
		return $query->num_rows();
	}  
		
	public function login($email,$password)	
	{
		$this->db->select('*');
		$this->db->from('catalysts');
		$this->db->where('email',$email);
		$this->db->where('password',$password);
        //$this->db->where('cstatus','1');
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		return $query;		
	}
	
	public function get_states(){
		$this->db->select('State');
		$this->db->from('csci');
		$this->db->where('Country_Code','IN');
		$this->db->group_by('State');
		$query = $this->db->get();		
		return $query->result_array();
	}
	
	public function get_cities(){
		$this->db->select('City');
		$this->db->from('csci');
		$this->db->where('Country_Code','IN');		
		$query = $this->db->get();		
		return $query->result_array();
	}
	
	public function get_cities_b_states($state){
		$this->db->select('City');
		$this->db->from('csci');
		$this->db->where('State',$state);
		$query = $this->db->get();
		return $query->result_array();		
	}
	
	
	public function get_admin_data()
	{
		$id = $this->session->userdata('admin_session')['id'];
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where('sid',$id);
		$query = $this->db->get();
		return $query->row_array();
	}	
	
	public function get_users(){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->order_by('uid','desc');
		return $this->db->get();
	}
	
	public function get_fps($lat,$lng){
		$query = $this->db->query("SELECT *, ACOS( SIN( RADIANS( `lat` ) ) * SIN( RADIANS( '".$lat."' ) ) + COS( RADIANS( `lat` ) )
		* COS( RADIANS( '".$lat."' )) * COS( RADIANS( `lng` ) - RADIANS( '".$lng."' )) ) * 6380 AS `dist` FROM `franchise_partners`  WHERE ACOS( SIN( RADIANS( `lat` ) ) * SIN( RADIANS( '".$lat."' ) ) + COS( RADIANS( `lat` ) ) * COS( RADIANS( '".$lat."' )) * COS( RADIANS( `lng` ) - RADIANS( '".$lng."' )) ) * 6380 < 10   and fstatus='1' ORDER BY `dist`");
		//echo $this->db->last_query();exit;
		return $query->result_array();    
	}
	
	
	
	
	
	
}