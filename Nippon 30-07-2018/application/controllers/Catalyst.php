<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalyst extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('email');	
		$this->load->helper('cookie');
		$this->load->library('utility');	
		$this->load->library('pagination');	
		$this->load->library('push_notifications');
		$this->load->model('Catalyst_model','my_model');  				
	}

	
	public function index()
	{
		if(!$this->session->userdata('catalyst_session')){	
			$this->load->view('catalyst/login');
		}else{
			redirect('catalyst/dashboard');
		}	
	}
	
	
	public function login()	
	{
		$email = $this->input->post('email');
		$p = $this->input->post('password');
		$password = md5($this->input->post('password'));		
            $remember = $this->input->post('remember_me');
		
		$query = $this->my_model->login($email,$password);
		$data = $query->row_array();
		$rows = $query->num_rows();
		//print_r($rows);exit;
		if($rows > 0){  
		$cc_rows = $this->my_model->check('catalysts',array('email'=>$email,'password'=>$password,'cstatus'=>'1'))->num_rows();
		//print_r($cc_rows);exit;   
			if($cc_rows > 0){
			if ($remember) {
			       $user_email= array( 'name'   => 'cusername', 'value'  => $email, 'expire' => '86400' ); 		
			       $user_password = array( 'name'   => 'cpassword', 'value'  => $p, 'expire' => '86400' ); 		
			       $this->input->set_cookie($user_email);
			       $this->input->set_cookie($user_password);
			}else{
			delete_cookie('cusername'); 
			delete_cookie('cpassword'); 			
						
			}                     

			$admindata = array(
							'id'  => $data['cid'],
							'name' => $data['name'],
							'email' => $data['email'],
							'image' => $data['profile_pic'],
							//'permissions' => $data['permissions']							
								);
			$this->session->set_userdata('catalyst_session',$admindata);
			redirect('catalyst/dashboard',$data);
			}else{
				$this->session->set_flashdata('message','Inactive');
				redirect('dp');	
			}
		}else{	
			$this->session->set_flashdata('message','Failed');
			redirect('catalyst');			
		}
	}
	
	
	public function dashboard()
	{
		if($this->session->userdata('catalyst_session')){				     	     
		$data['dp'] = $this->my_model->count('delivery_partners');	       	     
		$data['fp'] = $this->my_model->count('franchise_partners');	  	     
		$data['offers'] = $this->my_model->count('offers');	       	     
		$data['feedback'] = $this->my_model->count('feedback');	       	     
		$this->load->view('catalyst/index',$data);
		}else{
		redirect('catalyst');
		}
	}
	
	
	
	public function franchise_partners(){
		if($this->session->userdata('catalyst_session')){		
		$pagination = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->my_model->count('franchise_partners');
		
			$data['fp'] = $this->my_model->get_ls('franchise_partners',$limit_per_page, $start_index)->result_array();
			//print_r(($data['dealers']));	exit;
			$config['base_url'] = base_url() . 'catalyst/franchise_partners';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();	
		
		$this->load->view('catalyst/franchise_partners',$data);
		}else{
		redirect('catalyst');
		}
	}
	
	
	
	public function delivery_partners(){
		if($this->session->userdata('catalyst_session')){		
		$pagination = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->my_model->count('delivery_partners');
		
			$data['fp'] = $this->my_model->get_ls('delivery_partners',$limit_per_page, $start_index)->result_array();
			//print_r(($data['dealers']));	exit;
			$config['base_url'] = base_url() . 'catalyst/delivery_partners';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();	
		
		$this->load->view('catalyst/delivery_partners',$data);
		}else{
		redirect('catalyst');
		}
	}
	
	
	
	public function get_cities(){
		$state = $this->input->post('state');
		$cities = $this->my_model->get_cities_b_states($state);
		//print_r($data);
		$h = '<option>Select City</option>';
		foreach($cities as $cval){
			$h .= '<option value="'.$cval['City'].'">'.$cval['City'].'</option>';
		}
		print_r($h);
	}
	
	public function logout() {
	$this->session->sess_destroy();
	$this->session->set_flashdata("success", LOGOUTSUCCESS);
	redirect("catalyst");
	}
	
	
	
	
	public function edit_profile(){
		if($this->session->userdata('catalyst_session')){	
		$admindata = $this->session->userdata("catalyst_session");
		$id = $admindata['id'];
		$where	= array('cid'=>$id); 		
		//print_r($where);exit;
			if(isset($_POST['submit'])){				
				if (!empty($_FILES['admin_image']['name'])) {
				$config['upload_path'] = 'assets/uploads/users/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['encrypt_name'] = TRUE;
				$config['max_size'] = 2568;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('admin_image')) {
				$error = array(
				'error' => $this->upload->display_errors()
				);
				$this->session->set_flashdata("succuss", $error['error']);
				redirect("catalyst/profile");
				} else {
				$upload_data = $this->upload->data();
				$page_image = $upload_data['file_name'];
				}
				} else {
				$page_image = $this->input->post('old_image');
				}
				
				if($this->input->post('password') != ''){
				$password = md5($this->input->post('password'));
				}else{
				$password = $this->input->post('old_password');
				}
				
				
				
				$data = array(
				'name' => $this->input->post('name'),            			
				'email' => $this->input->post('email'),
			
				'password' => $password
				);
				$this->my_model->update('catalysts',$where,$data);
				$this->session->set_flashdata('message','updated');
				$admin_data = $this->my_model->check('catalysts',$where)->row_array();
				//print_r($admin_data);exit;
				$admindata = array(
							'id'  => $admin_data['cid'],
							'name' => $admin_data['name'],
							'email' => $admin_data['email']
												
								);
				
				$this->session->set_userdata('catalyst_session', $admindata);	
				//echo "<pre>";print_r($this->session->all_userdata());echo "</pre>";		
				redirect('catalyst/edit_profile');
				
			}else{
			$data['userdetails'] = $this->my_model->check('catalysts',$where)->row_array();
			//print_r($data);exit;
			$this->load->view('catalyst/edit_profile',$data);
			}
		}else{
		redirect('catalyst');
		}
	}
	
	public function check_mobile(){
		$phone = $this->input->post('mobile');		
		$table = $this->input->post('table');		
		$where = array('phone'=>$phone);
		$rows = $this->my_model->check($table,$where)->num_rows();		
		if($rows > 0){
			echo '1';
		}else{
			echo '0';
		}
	}
	
	
	public function check_email(){
		$email = $this->input->post('email');		
		$table = $this->input->post('table');		
		$where = array('email'=>$email);
		$rows = $this->my_model->check($table,$where)->num_rows();		
		if($rows > 0){
			echo '1';
		}else{
			echo '0';
		}
	}
	
	
	
	public function forgot_password(){
		$email = $this->input->post('email');
		$pwd = $this->utility->generate_otp();
		$password =  md5($pwd);
		$where  =array('email'=>$email);
		$rows = $this->my_model->check('catalysts',$where)->num_rows();
		if($rows > 0){
		$update = array('password'=>$password);
		$res = $this->my_model->update('catalysts',$where,$update);
		if($res){
			$subject = 'Forgot Password';
			$message = 'Dear Admin,your new password is '.$pwd.'';   
			$this->utility->send_email($email,$message,$subject);
			$this->session->set_flashdata('message','pasword_changed');
		}else{
			$this->session->set_flashdata('message','Mobile_not_exists');
		}
		}else{
		$this->session->set_flashdata('message','Mobile_not_exists');
		}
		redirect('catalyst/index');
	}
	
		


	public function feedbacks(){
		if($this->session->userdata('catalyst_session')){		
		$data['feedbacks'] = $this->my_model->get('feedback')->result_array();			
		$this->load->view('catalyst/feedbacks',$data);
		}else{
		redirect('catalyst');
		}
	}
	
	
	
	public function reply_feedback(){
		$email = $this->input->post('email');
		$message = $this->input->post('message');
		$res = $this->send_email($email,$message,'Reply To Feedback');
		if($res){
			echo '1';
		}else{
			echo '0';
		}		
	}
	
	
	public function send_email($toemail,$message,$subject)
	{
		//print_r($toemail);print_r($message);exit;
		$CI =& get_instance();
	  
		$CI->email->initialize(array(
			'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_user' => 'appcare.testing@gmail.com',
			'smtp_pass' => 'Appcare@123',
			'smtp_port' => 465,
			'mailtype'  => 'html',
			'crlf'      => "\r\n",
			'newline'   => "\r\n",
			'charset'	=> "utf-8"
		));
		
	  $CI->email->from('appcare.testing@gmail.com',strtoupper('Nippon'));
	  $CI->email->to($toemail);
	  $CI->email->subject($subject);
	  $CI->email->message($message);
      
	  $result = $CI->email->send();
	  
	  if($result){
		return true; 
	  }else{
		//echo $CI->email->print_debugger();exit;
		return false; 
	  }	  
	}
	
	public function offers(){
		if($this->session->userdata('catalyst_session')){		
		$data['Offers'] = $this->my_model->get('offers')->result_array();			
		$this->load->view('catalyst/offers',$data);
		}else{
		redirect('catalyst');
		}
	}
	
	public function get_details(){
		$shop_img_path = base_url().'assets/uploads/shops/';
		$id = $this->input->post('id');
		$table = $this->input->post('table');
		if($table == 'franchise_partners'){
		$where = array('fid'=>$id);
		}else if($table == 'dealers'){
		$where = array('did'=>$id);	
		}else{
		$where = array('dlvid'=>$id);	
		}
		$details = $this->my_model->check($table,$where)->row_array();
		if($table != 'dealers'){
		echo '<div class="row"><label  class="col-sm-4 control-label">Name</label><div class="col-sm-6"><p class="mtop">'.$details['name'].'</p></div></div>
		<div class="row"><label  class="col-sm-4 control-label">Address</label><div class="col-sm-6"><p class="mtop">'.$details['address'].'</p></div></div>
		<div class="row"><label  class="col-sm-4 control-label">Designation</label><div class="col-sm-6"><p class="mtop">'.$details['designation'].'</p></div></div>
		<div class="row"><label  class="col-sm-4 control-label">Email</label><div class="col-sm-6"><p class="mtop">'.$details['email'].'</p></div></div>
		<div class="row"><label  class="col-sm-4 control-label">Phone</label><div class="col-sm-6"><p class="mtop">'.$details['phone'].'</p></div></div>
		<div class="row"><label  class="col-sm-4 control-label">State</label><div class="col-sm-6"><p class="mtop">'.$details['state'].'</p></div></div>
		<div class="row"><label  class="col-sm-4 control-label">City</label><div class="col-sm-6"><p class="mtop">'.$details['city'].'</p></div></div>';
		}else{
		echo '<div class="row"><label  class="col-sm-4 control-label">Name</label><div class="col-sm-6"><p class="mtop">'.$details['dealer_name'].'</p></div></div>
		<div class="row"><label  class="col-sm-4 control-label">Shop Name</label><div class="col-sm-6"><p class="mtop">'.$details['store_name'].'</p></div></div>
		<div class="row"><label  class="col-sm-4 control-label">Phone</label><div class="col-sm-6"><p class="mtop">'.$details['mobile'].'</p></div></div>
		<div class="row"><label  class="col-sm-4 control-label">Email</label><div class="col-sm-6"><p class="mtop">'.$details['email'].'</p></div></div>
		<div class="row"><label  class="col-sm-4 control-label">Shop Image</label><div class="col-sm-6"><p class="mtop"><img src="'.$shop_img_path.$details['shop_image'].'" style="width:50px;height:50px" alt=""></p></div></div>
		<div class="row"><label  class="col-sm-4 control-label">State</label><div class="col-sm-6"><p class="mtop">'.$details['state'].'</p></div></div>
		<div class="row"><label  class="col-sm-4 control-label">City</label><div class="col-sm-6"><p class="mtop">'.$details['city'].'</p></div></div>
		<div class="row"><label  class="col-sm-4 control-label">Pancard</label><div class="col-sm-6"><p class="mtop">'.$details['pan_number'].'</p></div></div>
		<div class="row"><label  class="col-sm-4 control-label">GST</label><div class="col-sm-6"><p class="mtop">'.$details['gst_number'].'</p></div></div>
		<div class="row"><label  class="col-sm-4 control-label">Cancelled Cheque</label><div class="col-sm-6"><p class="mtop">'.$details['cancelled_cheque'].'</p></div></div>
		<input type="hidden" name="dealer_id"  id="dealer_id" value='.$details['did'].'>';
		}
	}
	
	public function verify_dealer(){		
		$id = $this->input->post('id');
		$where = array('did'=>$id);
		$update = array('kyc_status'=>'1');	
		$res = $this->my_model->update('dealers',$where,$update);	
		if($res){
			echo '1';
		}else{
			echo '0';
		}
	}
	
	
	
	public function dealers(){
		//echo $this->uri->segment(2);exit;
		if($this->session->userdata('catalyst_session')){		
		$pagination = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->my_model->count('dealers');
		
			$data['dealers'] = $this->my_model->get_ls('dealers',$limit_per_page, $start_index)->result_array();
			//print_r(($data['dealers']));	exit;
			$config['base_url'] = base_url() . 'catalyst/dealers';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();	
			
		$this->load->view('catalyst/dealers',$data);
		}else{
		redirect('catalyst');
		}
	}
	
	public function dealer_action($id=''){
		if($this->session->userdata('catalyst_session')){	
			if(isset($_POST['submit'])){
				if (!empty($_FILES['shop_photo']['name'])) {
				$config['upload_path'] = 'assets/uploads/shops/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['encrypt_name'] = TRUE;
				$config['max_size'] = 2568;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('shop_photo')) {
				$error = array(
				'error' => $this->upload->display_errors()
				);
				$this->session->set_flashdata("succuss", $error['error']);
				print_r($this->upload->display_errors());
				} else {
				$upload_data = $this->upload->data();
				$shop_photo = $upload_data['file_name'];
				}
				} else {
				$shop_photo = $this->input->post('old_shop_image');
				}
							
				if($id != ''){
				$data = array(
				'dealer_name' => $this->input->post('name'),
				'store_name' => $this->input->post('store_name'),				
				'email' => $this->input->post('email'),		
				'mobile' => $this->input->post('mobile'),		
				'shop_image' => $shop_photo,
				'flat_no' => $this->input->post('flatno'), 
				'state' => $this->input->post('state'), 
				'city' => $this->input->post('city'),
				'colony' => $this->input->post('colony'),
				'pincode' => $this->input->post('pincode'),
				'pan_number' => $this->input->post('pancard'), 
				'gst_number' => $this->input->post('gst'), 
				'cancelled_cheque' => $this->input->post('cc')				
				);
				
				$where	= array('did'=>$id);
				$this->my_model->update('dealers',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'dealer_name' => $this->input->post('name'),	
				'store_name' => $this->input->post('store_name'),	
				'email' => $this->input->post('email'),		
				'mobile' => $this->input->post('mobile'),						
				'shop_image' => $shop_photo,
				'flat_no' => $this->input->post('flatno'), 
				'state' => $this->input->post('state'), 
				'city' => $this->input->post('city'),
				'colony' => $this->input->post('colony'),
				'pincode' => $this->input->post('pincode'),
				'pan_number' => $this->input->post('pancard'), 
				'gst_number' => $this->input->post('gst'), 
				'cancelled_cheque' => $this->input->post('cc'),
				'password' => md5($this->input->post('password')),
				'created_on'=>date('Y-m-d')
				);
				
				$this->my_model->save('dealers',$data);
				$this->session->set_flashdata('message','added');				
				}
				redirect('catalyst/dealers');
				
			}else{
				if ($id != "") {
				$where = array('did'=>$id);
				$data['states'] = $this->my_model->get_states();
				$data['cities'] = $this->my_model->get_cities();
				$data["dealer_data"] = $this->my_model->check('dealers',$where)->row_array();				
				$this->load->view("catalyst/add_dealer", $data);
				} else {					
					$data['states'] = $this->my_model->get_states();					
				$this->load->view("catalyst/add_dealer",$data);
				}
			}					
		}else{
		redirect('catalyst');
		}
	}
	
	
	public function dealer_requests(){
		if($this->session->userdata('catalyst_session')){		
		$pagination = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->my_model->count('dealers');
		
			$data['dealers'] = $this->my_model->get_ls('dealers',$limit_per_page, $start_index)->result_array();
			//print_r(($data['dealers']));	exit;
			$config['base_url'] = base_url() . 'catalyst/dealer_requests';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();	
			
		$this->load->view('catalyst/dealer_requests',$data);
		}else{
		redirect('catalyst');
		}			
	}
	
	
	//For Dealer Orders
	public function dealer_orders(){
		if($this->session->userdata('catalyst_session')){
			$pagination = array();
        $limit_per_page = 10;
		//echo $this->uri->segment(3);exit;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->my_model->count_with_condition('orders',array('order_from'=>'dealers'));		
			$data['dorders'] = $this->my_model->condition_and_limit_query('orders',array('order_from'=>'dealers'),$limit_per_page, $start_index)->result_array();			
			for($i=0;$i<count($data['dorders']);$i++){
				$where = "oid=".$data['dorders'][$i]['oid']." and fp_status != '2'";
				$data['dorders'][$i]['assigned_fp'] = $this->my_model->check('fp_assigned_orders',$where)->row()->fp_id;
			}
			$config['base_url'] = base_url() . 'catalyst/dealer_orders';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();
			//print_r($data);exit;
			$this->load->view('catalyst/dorders',$data);
		}else{
		redirect('catalyst');	
		}
	}
	
	//For FP Orders
	public function fp_orders(){
		if($this->session->userdata('catalyst_session')){
			$pagination = array();
			$limit_per_page = 10;
			//echo $this->uri->segment(3);exit;
			$start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$total_records = $this->my_model->count_with_condition('orders',array('order_from'=>'fps'));		
			$data['forders'] = $this->my_model->condition_and_limit_query('orders',array('order_from'=>'fps'),$limit_per_page, $start_index)->result_array();			
			
			$config['base_url'] = base_url() . 'catalyst/fp_orders';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();
			//print_r($data);exit;
			$this->load->view('catalyst/forders',$data);
		}else{
		redirect('catalyst');	
		}
	}
	
	//For Order Details
	public function order_details(){
		$id = $this->input->post('id');
		$where = array('oid'=>$id);			
		$details = $this->my_model->check('orders',$where)->row_array();		
		$idet = str_replace('},', '}&', $details['items']);
		$item_split_details = explode("&",$idet);
		$data['items'] = [];
		for($i=0;$i<count($item_split_details);$i++){
			$item_details[$i] = json_decode($item_split_details[$i],true);
			array_push($data['items'],$item_details[$i]);
		}	
		for($k=0;$k<count($data['items']);$k++){
			$total_items_cost = $data['items'][$k]['count']*$data['items'][$k]['item_cost'];
			$data['total_cost'] += $total_items_cost;
		}
		$data['order_details'] = array('cgst'=>$details['cgst_amount'],'sgst'=>$details['sgst_amount'],'total_gst'=>$details['total_gst_amount'],'total_with_gst'=>$details['total_with_gst']);
		$this->load->view('catalyst/order_details',$data);
	}
	
	
	public function user_details($table,$id){
		if($this->session->userdata('catalyst_session')){	
			if($table == 'catalysts'){
				$where = array('cid'=>$id);
			}else if($table == 'franchise_partners'){
				$where = array('fid'=>$id);
			}else if($table == 'dealers'){
				$where = array('did'=>$id);
			}else if($table == 'delivery_partners'){
				$where = array('dlvid'=>$id);
			}	
			$data['udetails'] = $this->my_model->check($table,$where)->row_array();
			$data['table'] = $table;
			
			$this->load->view('catalyst/details_view',$data);
		}else{
		redirect('catalyst');
		}
	}
	
	public function nearby_fps(){
		$order_id = $this->input->post('id');
		$dealer_id = $this->my_model->check('orders',array('oid'=>$order_id))->row()->uid;
		$dealer_details = $this->my_model->check('dealers',array('did'=>$dealer_id))->row();
		$dealer_lat = $dealer_details->lat;
		$dealer_lng = $dealer_details->lng;
		$fps = $this->my_model->get_fps($dealer_lat,$dealer_lng);
		echo '<option value="">Select Franchise Partner</option>';
		foreach($fps as $nearbyFps){
			echo "<option value=".$nearbyFps['fid'].">".$nearbyFps['name']."</option>";
		}
	}    
	
	public function assign_fps(){
		$cid = $this->session->userdata('catalyst_session')['id'];
		$date_time = date('Y-m-d H:i');
		$order_id = $this->input->post('order_id');
		$fp_id = $this->input->post('fp');
		$where = array('fid'=>$fp_id);
		$fpname = $this->my_model->check('franchise_partners',$where)->row()->name;
		$data = array('oid'=>$order_id,'fp_id'=>$fp_id,'assigned_by'=>$cid,'assigned_date'=>$date_time);
		$res = $this->my_model->save('fp_assigned_orders',$data);
		if($res){
			$message  = 'New order '.$order_id.' assigned';
			$this->my_model->save('notifications',array('message'=>$message,'send_to'=>'FP','uid'=>$fp_id,'date'=>date('Y-m-d')));
		}
		$test = ($res) ? '{"fp_name":"'.$fpname.'","status":"1"}' : '0';
		echo $test;
	}
	
	public function getFpPending(){
		$rows = $this->my_model->check('fp_assigned_orders',array('fp_status'=>'0'))->num_rows();
		print_r(json_encode(array('pending_orders'=>$rows),true));
	}
	
	public function invoice($order_id){
		$where = array('oid'=>$order_id);			
		$details = $this->my_model->check('orders',$where)->row_array();
		//print_r($details['uid']);exit;
		$idet = str_replace('},', '}&', $details['items']);
		$item_split_details = explode("&",$idet);
		$data['items'] = [];
		for($i=0;$i<count($item_split_details);$i++){
			$item_details[$i] = json_decode($item_split_details[$i],true);
			array_push($data['items'],$item_details[$i]);
		}	
		for($k=0;$k<count($data['items']);$k++){
			$total_items_cost = $data['items'][$k]['count']*$data['items'][$k]['item_cost'];
			$data['total_cost'] += $total_items_cost;
		}
		$data['order_details'] = $details;
		
			if($details['order_from'] == 'dealers'){
			$data['user_name'] = $this->my_model->check('dealers',array('did'=>$details['uid']))->row()->dealer_name;
			$data['user_email'] = $this->my_model->check('dealers',array('did'=>$details['uid']))->row()->email;
			$data['user_mobile'] = $this->my_model->check('dealers',array('did'=>$details['uid']))->row()->mobile;
			}else{
			$data['user_name'] = $this->my_model->check('franchise_partners',array('fid'=>$details['uid']))->row()->dealer_name;
			$data['user_email'] = $this->my_model->check('franchise_partners',array('fid'=>$details['uid']))->row()->email;			
			$data['user_mobile'] = $this->my_model->check('franchise_partners',array('fid'=>$details['uid']))->row()->mobile;			
			}
		
		//print_r($data);exit;
		$this->load->view('catalyst/invoice',$data);
	}
	
	
	// TO COnfirm order by Catalyst
	public function confirm_order(){
		$order_id = $this->input->post('order_id');
		$base_url = base_url();
		$invoice_url = $base_url.'catalyst/invoice/'.$order_id;
		$order_status = $this->input->post('order_status');
		$res = $this->my_model->update('orders',array('oid'=>$order_id),array('order_status'=>$order_status));
		if($res){			
			$res =  '<a href="javascript:" class="btn btn-warning btn-xs m-b-10 m-l-5">Order Confirmed</a><a href="javascript:order_details('.$order_id.')" class="btn btn-info btn-xs m-b-10 m-l-5">View Details</a>
<a href="'.$invoice_url.'" class="btn btn-primary btn-xs m-b-10 m-l-5">Invoice</a>
<a href="javascript:nearby_fps('.$order_id.')" class="btn btn-primary btn-xs m-b-10 m-l-5">Assign FP</a>';
		print_r(json_encode(array('status'=>'1','msg'=>$res)));
		}else{
		print_r(json_encode(array('status'=>'0')));
		}
	}
	
	
	
	public function confirm_fp_order(){
		$order_id = $this->input->post('order_id');
		$base_url = base_url();
		$invoice_url = $base_url.'catalyst/invoice/'.$order_id;
		$order_status = $this->input->post('order_status');
		$res = $this->my_model->update('orders',array('oid'=>$order_id),array('order_status'=>$order_status));
		if($res){			
			$res =  '<a href="javascript:" class="btn btn-warning btn-xs m-b-10 m-l-5">Order Confirmed</a><a href="javascript:order_details('.$order_id.')" class="btn btn-info btn-xs m-b-10 m-l-5">View Details</a>
<a href="'.$invoice_url.'" class="btn btn-primary btn-xs m-b-10 m-l-5">Invoice</a>';
		print_r(json_encode(array('status'=>'1','msg'=>$res)));
		}else{
		print_r(json_encode(array('status'=>'0')));
		}
	}
	
	
}
