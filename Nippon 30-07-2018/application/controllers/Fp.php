<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fp extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('email');	
		$this->load->helper('cookie');
		$this->load->library('utility');	
		$this->load->library('pagination');
		$this->load->library('push_notifications');		
		$this->load->model('Fp_model','my_model');  	
$this->load->model('catalyst_model','catalyst_model');  		
	}

	
	public function index()
	{
		if(!$this->session->userdata('fp_session')){	
			$this->load->view('fp/login');
		}else{
			redirect('fp/dashboard');
		}	
	}
	
	
	public function login()	
	{
		$email = $this->input->post('email');
		$p = $this->input->post('password');
		$password = md5($this->input->post('password'));		
            $remember = $this->input->post('remember_me');
		
		$query = $this->my_model->login($email,$password);
		$data = $query->row_array();
		$rows = $query->num_rows();
		//print_r($rows);exit;
		if($rows > 0){  
			$ac_rows = $this->my_model->check('franchise_partners',array('email'=>$email,'password'=>$password,'fstatus'=>'1'))->num_rows();
			if($ac_rows > 0){
				if ($remember) {
					   $user_email= array( 'name'   => 'fpusername', 'value'  => $email, 'expire' => '86400' ); 		
					   $user_password = array( 'name'   => 'fppassword', 'value'  => $p, 'expire' => '86400' ); 		
					   $this->input->set_cookie($user_email);
					   $this->input->set_cookie($user_password);
				}else{
				delete_cookie('fpusername'); 
				delete_cookie('fppassword'); 			
							
				}                     

				$admindata = array(
								'id'  => $data['fid'],
								'name' => $data['name'],
								'email' => $data['email'],
								'image' => $data['profile_pic']						
									);
				$this->session->set_userdata('fp_session',$admindata);
				redirect('fp/dashboard',$data);
			}else{
				$this->session->set_flashdata('message','Inactive');
				redirect('fp');	
			}	
		}else{	
			$this->session->set_flashdata('message','Failed');
			redirect('fp');			
		}
	}
	
	
	public function dashboard()
	{
		if($this->session->userdata('fp_session')){	
		$fpid  =   $this->session->userdata('fp_session')['id'];
		$where = "fp_id=".$fpid." and fp_status != '2'";
		$data['dorders'] = $this->my_model->check('fp_assigned_orders',$where)->num_rows();	
		$data['colours'] = $this->my_model->count('colours');	       	     
		$data['products'] = $this->my_model->count('products');	
		$data['dp'] = $this->my_model->count('delivery_partners');			
		//print_r($data['dorders']);exit;
		$this->load->view('fp/index',$data);
		}else{
		redirect('fp');
		}
	}
	
	
	public function get_cities(){
		$state = $this->input->post('state');
		$cities = $this->my_model->get_cities_b_states($state);
		//print_r($data);
		$h = '<option>Select City</option>';
		foreach($cities as $cval){
			$h .= '<option value="'.$cval['City'].'">'.$cval['City'].'</option>';
		}
		print_r($h);
	}
	
	public function logout() {
		$this->session->sess_destroy();
		$this->session->set_flashdata("success", LOGOUTSUCCESS);
		redirect("fp");
	}
	
	
	
	
	public function edit_profile(){
		if($this->session->userdata('fp_session')){	
		$admindata = $this->session->userdata("fp_session");
		$id = $admindata['id'];
		$where	= array('fid'=>$id); 		
		//print_r($where);exit;
			if(isset($_POST['submit'])){				
				if (!empty($_FILES['admin_image']['name'])) {
				$config['upload_path'] = 'assets/uploads/users/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['encrypt_name'] = TRUE;
				$config['max_size'] = 2568;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('admin_image')) {
				$error = array(
				'error' => $this->upload->display_errors()
				);
				$this->session->set_flashdata("succuss", $error['error']);
				redirect("fp/profile");
				} else {
				$upload_data = $this->upload->data();
				$page_image = $upload_data['file_name'];
				}
				} else {
				$page_image = $this->input->post('old_image');
				}
				
				if($this->input->post('password') != ''){
				$password = md5($this->input->post('password'));
				}else{
				$password = $this->input->post('old_password');
				}
				
				
				
				$data = array(
				'name' => $this->input->post('name'),            			
				'email' => $this->input->post('email'),
				
				'password' => $password
				);
				$this->my_model->update('franchise_partners',$where,$data);
				$this->session->set_flashdata('message','updated');
				$admin_data = $this->my_model->check('franchise_partners',$where)->row_array();
				//print_r($admin_data);exit;
				$admindata = array(
							'id'  => $admin_data['fid'],
							'name' => $admin_data['name'],
							'email' => $admin_data['email']						
								);
				
				$this->session->set_userdata('fp_session', $admindata);	
				//echo "<pre>";print_r($this->session->all_userdata());echo "</pre>";		
				redirect('fp/edit_profile');
				
			}else{
			$data['userdetails'] = $this->my_model->check('franchise_partners',$where)->row_array();
			//print_r($data);exit;
			$this->load->view('fp/edit_profile',$data);
			}
		}else{
		redirect('franchise_partners');
		}
	}
	
	public function check_mobile(){
		$phone = $this->input->post('mobile');		
		$table = $this->input->post('table');		
		$where = array('phone'=>$phone);
		$rows = $this->my_model->check($table,$where)->num_rows();		
		if($rows > 0){
			echo '1';
		}else{
			echo '0';
		}
	}
	
	
	public function check_email(){
		$email = $this->input->post('email');		
		$table = $this->input->post('table');		
		$where = array('email'=>$email);
		$rows = $this->my_model->check($table,$where)->num_rows();		
		if($rows > 0){
			echo '1';
		}else{
			echo '0';
		}
	}
	
	
	
	public function forgot_password(){
		$email = $this->input->post('email');
		$pwd = $this->utility->generate_otp();
		$password =  md5($pwd);
		$where  =array('email'=>$email);
		$rows = $this->my_model->check('franchise_partners',$where)->num_rows();
		if($rows > 0){
		$update = array('password'=>$password);
		$res = $this->my_model->update('franchise_partners',$where,$update);
		if($res){
			$subject = 'Forgot Password';
			$message = 'Dear Franchise Partner,your new password is '.$pwd.'';   
			$this->utility->send_email($email,$message,$subject);
			$this->session->set_flashdata('message','pasword_changed');
		}else{
			$this->session->set_flashdata('message','Mobile_not_exists');
		}
		}else{
		$this->session->set_flashdata('message','Mobile_not_exists');
		}
		redirect('catalyst/index');
	}
	


	
	
	public function send_email($toemail,$message,$subject)
	{
		//print_r($toemail);print_r($message);exit;
		$CI =& get_instance();
	  
		$CI->email->initialize(array(
			'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_user' => 'appcare.testing@gmail.com',
			'smtp_pass' => 'Appcare@123',
			'smtp_port' => 465,
			'mailtype'  => 'html',
			'crlf'      => "\r\n",
			'newline'   => "\r\n",
			'charset'	=> "utf-8"
		));
		
	  $CI->email->from('appcare.testing@gmail.com',strtoupper('Nippon'));
	  $CI->email->to($toemail);
	  $CI->email->subject($subject);
	  $CI->email->message($message);
      
	  $result = $CI->email->send();
	  
	  if($result){
		return true; 
	  }else{
		//echo $CI->email->print_debugger();exit;
		return false; 
	  }	  
	}         
	
	//For Dealer Orders
	public function dealer_orders(){
		if($this->session->userdata('fp_session')){
			$fpid  =   $this->session->userdata('fp_session')['id'];
		$pagination = array();
        $limit_per_page = 10;
		//echo $this->uri->segment(3);exit;
        $start_index = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $total_records = $this->my_model->count_with_condition('fp_assigned_orders',array('fp_id'=>$fpid));		
			$data['dorders'] = $this->my_model->condition_and_limit_query('fp_assigned_orders',array('fp_id'=>$fpid),$limit_per_page, $start_index)->result_array();			
			//print_r($data['dorders']);exit;
			for($i=0;$i<count($data['dorders']);$i++){
				$where = "oid=".$data['dorders'][$i]['oid']." and dp_status != '2'";
				$data['dorders'][$i]['total_cost'] = $this->my_model->check('orders',array('oid'=>$data['dorders'][$i]['oid']))->row()->total_cost;
				$data['dorders'][$i]['assigned_dp'] = $this->my_model->check('dp_assigned_orders',$where)->row()->dp_id;
			}
			
			$config['base_url'] = base_url() . 'fp/dealer_orders';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 4;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();
			//print_r($data);exit;
			$this->load->view('fp/dorders',$data);
		}else{
		redirect('fp');	
		}
	}
	
	//For FP Orders
	public function fp_orders(){
		if($this->session->userdata('fp_session')){
			$pagination = array();
			$limit_per_page = 10;
			//echo $this->uri->segment(3);exit;
			$start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$total_records = $this->catalyst_model->count_with_condition('orders',array('order_from'=>'fps'));		
			$data['forders'] = $this->catalyst_model->condition_and_limit_query('orders',array('order_from'=>'fps'),$limit_per_page, $start_index)->result_array();			
			
			$config['base_url'] = base_url() . 'fp/fp_orders';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();
			//print_r($data);exit;
			$this->load->view('fp/forders',$data);
		}else{
		redirect('fp');	
		}
	}
	
	
	
	//For Order Details
	public function order_details(){
		$id = $this->input->post('id');
		$where = array('oid'=>$id);			
		$details = $this->my_model->check('orders',$where)->row_array();		
		$idet = str_replace('},', '}&', $details['items']);
		$item_split_details = explode("&",$idet);
		$data['items'] = [];
		for($i=0;$i<count($item_split_details);$i++){
			$item_details[$i] = json_decode($item_split_details[$i],true);
			array_push($data['items'],$item_details[$i]);
		}	
		for($k=0;$k<count($data['items']);$k++){
			$data['total_cost'] += $data['items'][$k]['item_cost'];
		}

		$this->load->view('catalyst/order_details',$data);
	}
	
	
	public function change_fporder_status(){		
		$dd = date("Y-m-d",strtotime($this->input->post('delivery_date')));		
		$order_id = $this->input->post('order_id');
		$status = $this->input->post('status');
		$fp_id = $this->input->post('fp_id');
		$where = array('oid'=>$order_id,'fp_id'=>$fp_id);
		$update = array('fp_status'=>$status);
		$res = $this->my_model->update('fp_assigned_orders',$where,$update);
		$admin_id = $this->my_model->check('fp_assigned_orders',$where)->row()->assigned_by;
		if($res){
			$fp_details = $this->my_model->check('franchise_partners',array('fid'=>'1'))->row();			
			if($status == '1'){
				$order_details_id = $this->my_model->check('orders',array('oid'=>$order_id))->row()->order_id;
				$dealer_id = $this->my_model->check('orders',array('oid'=>$order_id))->row()->uid;
				$token = $this->my_model->check('dealers',array('did'=>$dealer_id))->row()->device_token;
			$msg = 'Your Order with id '.$order_details_id.' has been confirmed it will be delivered by '.$dd.'';
			$data = array('message'=>$msg);		
			$title = 'Confirm Order';
			$this->push_notifications->send_notification($token,$data,$title);	
			$this->my_model->save('app_notifications',array('dealer_id'=>$dealer_id,'title'=>'Order','msg'=>$msg,'date'=>date('Y-m-d')));
			
			
				$message  = 'Order '.$order_id.' has been accepted by '.$fp_details->name.'';
				$this->my_model->update('orders',array('oid'=>$order_id),array('fp_status'=>$fp_id,'delivery_date'=>$dd));
				$this->my_model->save('notifications',array('message'=>$message,'send_to'=>'Admin','uid'=>$admin_id,'date'=>date('Y-m-d')));
				$msg = '<a href="#" class="btn btn-primary btn-xs m-b-10 m-l-5">Accepted</a>';
				$response=json_encode(array('msg'=>$msg,'status'=>'accept'),true);				
			}else{
				$message  = 'Order '.$order_id.' has been rejected by '.$fp_details->name.'';
				$this->my_model->update('orders',array('oid'=>$order_id),array('fp_status'=>'0'));
				$this->my_model->save('notifications',array('message'=>$message,'send_to'=>'Admin','uid'=>$admin_id,'date'=>date('Y-m-d')));				
				$msg = '<a href="#" class="btn btn-danger btn-xs m-b-10 m-l-5">Rejected</a>';				
				$response=json_encode(array('msg'=>$msg,'status'=>'reject'),true);				
			}
			print_r($response);
		}else{
			echo '0';
		}
	}
	
	
	public function update_notifications_status(){
		$uid = $this->input->post('uid');
		$send_to = $this->input->post('send_to');
		$update = array('view_status'=>'1');
		$where  = array('send_to'=>$send_to,'uid'=>$uid);
		$res = $this->my_model->update('notifications',$where,$update);
		$result = ($res)? '1' : '0';
		echo $result;    
	}
	
	
	public function colours(){
		if($this->session->userdata('fp_session')){	

		$pagination = array();
		$limit_per_page = 10;
		//echo $this->uri->segment(3);exit;
		$start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$total_records = $this->my_model->count('colours');	

		$data['colours'] = $this->my_model->get_ls('colours',$limit_per_page, $start_index)->result_array();
		for($i=0;$i<count($data['colours']);$i++){
			$data['colours'][$i]['colour_type'] = $this->my_model->check('colour_types',array('ctid'=>$data['colours'][$i]['colour_type']))->row()->colour_type;
			$data['colours'][$i]['colour_brand'] = $this->my_model->check('brands',array('bid'=>$data['colours'][$i]['colour_brand']))->row()->brand_name;
			$data['colours'][$i]['colour_group_name'] = $this->my_model->check('colour_groups',array('cgid'=>$data['colours'][$i]['colour_group']))->row()->colour_group_name;
		}
		
		$config['base_url'] = base_url() . 'fp/colours';
		$config['total_rows'] = $total_records;
		$config['per_page'] = $limit_per_page;
		$config["uri_segment"] = 3;       
		$config['next_link'] = 'Next';		
		$config['prev_link'] = 'Previous';			
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();

		$this->load->view('fp/colours',$data);
		}else{
		redirect('fp');
		}
	}
	  
	  
	  
	
	public function colour_details(){
		$clr_img_path = base_url().'assets/uploads/colours/';
		$id = $this->input->post('id');
		$table = $this->input->post('table');	
		$where = array('cid'=>$id);
		$details = $this->my_model->check($table,$where)->row_array();
		$csource = $this->my_model->check('source_countries',array('sid'=>$details['source']))->row()->source;
		$cgroup = $this->my_model->check('colour_groups',array('cgid'=>$details['colour_group']))->row()->colour_group_name;
		$ctype = $this->my_model->check('colour_types',array('ctid'=>$details['colour_type']))->row()->colour_type;
		$cbrand = $this->my_model->check('brands',array('bid'=>$details['colour_brand']))->row()->brand_name;
		$cimage = $clr_img_path.$details['colour_image'];
		$moq = $details['moq'];
		if($moq == 0){
			$moq = '1';
		}else{
			$moq = $details['moq'];
		}
		echo	'<div class="col-md-6 pull-left"><div class="form-inline form-group"><label class="label_field">Colour Name </label><span class="colon">:</span><p class="dval">'.$details['colour_name'].'</p></div></div>
				<div class="col-md-6 pull-right"><div class="form-inline form-group"><label class="label_field">Colour Code </label><span class="colon">:</span><p class="dval">'.$details['colour_code'].'</p></div></div>
				<div class="col-md-6 pull-left"><div class="form-inline form-group"><label class="label_field">Source </label><span class="colon">:</span><p class="dval">'.$csource.'</p></div></div>
				<div class="col-md-6 pull-right"><div class="form-inline form-group"><label class="label_field">Colour Group </label><span class="colon">:</span><p class="dval">'.$cgroup.'</p></div></div>
				<div class="col-md-6 pull-left"><div class="form-inline form-group"><label class="label_field">Colour Type </label><span class="colon">:</span><p class="dval">'.$ctype.'</p></div></div>
				<div class="col-md-6 pull-right"><div class="form-inline form-group"><label class="label_field">Colour Brand </label><span class="colon">:</span><p class="dval">'.$cbrand.'</p></div></div>
				<div class="col-md-6 pull-left"><div class="form-inline form-group"><label class="label_field">Price </label><span class="colon">:</span><p class="dval">'.$details['price'].'</p></div></div>
				<div class="col-md-6 pull-right"><div class="form-inline form-group"><label class="label_field">Description </label><span class="colon">:</span><p class="dval">'.strip_tags($details['description']).'</p></div></div>
				<div class="col-md-6 pull-left"><div class="form-inline form-group"><label class="label_field">Features </label><span class="colon">:</span><p class="dval">'.strip_tags($details['features']).'</p></div></div>
				<div class="col-md-6 pull-right"><div class="form-inline form-group"><label class="label_field">Benefits </label><span class="colon">:</span><p class="dval">'.strip_tags($details['benefits']).'</p></div></div>
				<div class="col-md-6 pull-left"><div class="form-inline form-group"><label class="label_field">Image </label><span class="colon">:</span><img src='.$cimage.' style="width:50px;height:50px"  class="dval"></div></div>
				<input type="hidden" id="item_id" value='.$details['cid'].'><input type="hidden" id="item_count" value='.$moq.'><input type="hidden" id="item_cost" value='.$details['price'].'><input type="hidden" id="item_type" value="Colours">';
	}
	
	
	public function product_details(){
		$clr_img_path = base_url().'assets/uploads/products/';
		$id = $this->input->post('id');
		$table = $this->input->post('table');	
		$where = array('pid'=>$id);
		$details = $this->my_model->check($table,$where)->row_array();		
		$pbrand = $this->my_model->check('brands',array('bid'=>$details['brand']))->row()->brand_name;
		$pimage = $clr_img_path.$details['product_image'];
		$moq = $details['moq'];
		//print_r($moq);exit;
		if($moq == 0){
			$moq = '1';
		}else{
			$moq = $details['moq'];
		}
		echo	'<div class="row col-md-12"><div class="col-md-3 pull-left">Description</div><div class="col-md-9 pull-right"><p style="word-wrap: break-word !important;">'.strip_tags($details['description']).'</p></div></div><div class="clear-fix"></div></br>
				<div class="row col-md-12"><div class="col-md-3 pull-left">Features</div><div class="col-md-9 pull-right"><p style="word-wrap: break-word !important;">'.strip_tags($details['features']).'</p></div></div></br>
				<div class="row col-md-12"><div class="col-md-3 pull-left">Benefits</div><div class="col-md-9 pull-right"><p style="word-wrap: break-word !important;">'.strip_tags($details['benefits']).'</p></div></div></br>
				<div class="col-md-6 pull-left"><div class="form-inline form-group"><label class="label_field">Product Name </label><span class="colon">:</span><p class="dval">'.$details['product_name'].'</p></div></div>
				<div class="col-md-6 pull-right"><div class="form-inline form-group"><label class="label_field">Brand </label><span class="colon">:</span><p class="dval">'.$pbrand.'</p></div></div>
				<div class="col-md-6 pull-left"><div class="form-inline form-group"><label class="label_field">Price </label><span class="colon">:</span><p class="dval">'.$details['product_price'].'</p></div></div>
				<div class="col-md-6 pull-left"><div class="form-inline form-group"><label class="label_field">Image </label><span class="colon">:</span><img src='.$pimage.' style="width:50px;height:50px"  class="dval"></div></div>
				<input type="hidden" id="item_id" value='.$details['pid'].'><input type="hidden" id="item_count" value='.$moq.'><input type="hidden" id="item_cost" value='.$details['product_price'].'><input type="hidden" id="item_type" value="Products">';
	}
   
	

	public function delivery_partners(){
		if($this->session->userdata('fp_session')){		
		$pagination = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->my_model->count('delivery_partners');
		
			$data['fp'] = $this->my_model->get_ls('delivery_partners',$limit_per_page, $start_index)->result_array();
			//print_r(($data['dealers']));	exit;
			$config['base_url'] = base_url() . 'fp/delivery_partners';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();	
		
		$this->load->view('fp/delivery_partners',$data);
		}else{
		redirect('fp');
		}
	}   


	public function products(){
		if($this->session->userdata('fp_session')){
		$pagination = array();
		$limit_per_page = 10;
		//echo $this->uri->segment(3);exit;
		$start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$total_records = $this->my_model->count('products');	
			
		$data['products'] = $this->my_model->get_ls('products',$limit_per_page, $start_index)->result_array();
		for($i=0;$i<count($data['products']);$i++){
			$data['products'][$i]['brand'] = $this->my_model->check('brands',array('bid'=>$data['products'][$i]['brand']))->row()->brand_name;
		}
		
		$config['base_url'] = base_url() . 'fp/products';
		$config['total_rows'] = $total_records;
		$config['per_page'] = $limit_per_page;
		$config["uri_segment"] = 3;       
		$config['next_link'] = 'Next';		
		$config['prev_link'] = 'Previous';			
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();
		//print_r($data);exit;
		$this->load->view('fp/products',$data);
		}else{
		redirect('fp');
		}
	}
	
	
	public function invoice($order_id){
		$where = array('oid'=>$order_id);			
		$details = $this->my_model->check('orders',$where)->row_array();
		
		$idet = str_replace('},', '}&', $details['items']);
		$item_split_details = explode("&",$idet);
		$data['order_product_details'] = [];
		for($i=0;$i<count($item_split_details);$i++){
			$item_details[$i] = json_decode($item_split_details[$i],true);
			array_push($data['order_product_details'],$item_details[$i]);
		}	
		for($k=0;$k<count($data['order_product_details']);$k++){
			$total_items_cost = $data['order_product_details'][$k]['count']*$data['order_product_details'][$k]['item_cost'];
			$data['total_cost'] += $total_items_cost;
		}
		$data['order_details'] = $details;
		
			if($details['order_from'] == 'dealers'){
			$data['user_name'] = $this->my_model->check('dealers',array('did'=>$details['uid']))->row()->dealer_name;
			$data['user_email'] = $this->my_model->check('dealers',array('did'=>$details['uid']))->row()->email;
			$data['user_mobile'] = $this->my_model->check('dealers',array('did'=>$details['uid']))->row()->mobile;
			}else{
			$data['user_name'] = $this->my_model->check('franchise_partners',array('fid'=>$details['uid']))->row()->dealer_name;
			$data['user_email'] = $this->my_model->check('franchise_partners',array('fid'=>$details['uid']))->row()->email;			
			$data['user_mobile'] = $this->my_model->check('franchise_partners',array('fid'=>$details['uid']))->row()->mobile;			
			}
		
		//print_r($data);exit;
		$this->load->view('fp/checkinvoice',$data);
	}
	
	public function cart_list(){
		$user_id = $this->session->userdata('fp_session')['id'];
		$bu = base_url();
	
				$uwhere = array('uid'=>$user_id);
				$cart_count = $this->my_model->check_cart('cart',$uwhere)->num_rows();
				$items = $this->my_model->check('cart',array('uid'=>$user_id))->result_array();
			for($i=0;$i<count($items);$i++){		
				$total_items_cost += $items[$i]['total_cost'];
				if($items[$i]['item_type'] == 'Products'){
					$items[$i]['item_name'] = $this->my_model->check('products',array('pid'=>$items[$i]['item_id']))->row()->product_name;
					$item_image = $this->my_model->check('products',array('pid'=>$items[$i]['item_id']))->row()->product_image;
					$items[$i]['item_image'] = $bu.'assets/uploads/products/'.$item_image;
				}else{
					$items[$i]['item_name'] = $this->my_model->check('colours',array('cid'=>$items[$i]['item_id']))->row()->colour_name;
					$item_image = $this->my_model->check('colours',array('cid'=>$items[$i]['item_id']))->row()->colour_image;
					$items[$i]['item_image'] = $bu.'assets/uploads/colours/'.$item_image;	
				}
			}
			$data['cart_count'] = $cart_count;
			$data['total_items_cost'] = $total_items_cost;
			$data['items'] = $items;
			print_r($data);
	}
		                    
	public function addtoCart(){
		$user_id = $this->session->userdata('fp_session')['id'];
		$item_id = $this->input->post('item_id');
		$item_type = $this->input->post('item_type');
		$item_cost = $this->input->post('item_cost');
		$count = $this->input->post('count');
		$total_cost = $count *($item_cost); 
		$base_url = base_url();
		$item_check = $this->my_model->check('cart',array('uid'=>$user_id,'item_id'=>$item_id,'item_type'=>$item_type,'added_by'=>'FP'))->num_rows();
		if($item_check == 0){
			$data = array('uid'=>$user_id,'item_id'=>$item_id,'item_type'=>$item_type,'item_cost'=>$item_cost,'total_cost'=>$total_cost,'count'=>$count,'added_by'=>'FP','added_on'=>date('Y-m-d H:i'));
			$res = $this->my_model->save('cart',$data);
		}else{
			$user_cart_where = array('uid'=>$user_id,'item_id'=>$item_id,'item_type'=>$item_type,'added_by'=>'FP');
			$user_cart_count = $this->my_model->check('cart',$user_cart_where)->row()->count;
			$UpdateData = array('count'=>$count);
			$res = $this->my_model->update('cart',$user_cart_where,$UpdateData);
		}
		$cdetails = [];
		if($res){
			$count =  $this->my_model->check('cart',array('uid'=>$user_id,'added_by'=>'FP'))->num_rows();
			$cart_list = $this->my_model->check('cart',array('uid'=>$user_id,'added_by'=>'FP'))->result_array();
			for($i=0;$i<count($cart_list);$i++){
				if($cart_list[$i]['item_type'] == 'Colours'){
				$cart_list[$i]['item_name'] = $this->my_model->check('colours',array('cid'=>$cart_list[$i]['item_id']))->row()->colour_name;
				$cart_list[$i]['item_image'] = $this->my_model->check('colours',array('cid'=>$cart_list[$i]['item_id']))->row()->colour_image;
				$imgbase_url = $base_url.'assets/uploads/colours/';
				}else{
				$cart_list[$i]['item_name'] = $this->my_model->check('products',array('pid'=>$cart_list[$i]['item_id']))->row()->product_name;
				$cart_list[$i]['item_image'] = $this->my_model->check('products',array('pid'=>$cart_list[$i]['item_id']))->row()->product_image;
				$imgbase_url = $base_url.'assets/uploads/products/';
				}
			$cart_details = '<a href="#" id="removeditem'.$cart_list[$i]['cid'].'"><img src="'.$imgbase_url.''.$cart_list[$i]['item_image'].'" style="width:50px;height:50px" />
                <div class="mail-contnet"><h5>'.$cart_list[$i]['item_name'].'</h5> <span class="mail-desc">Rs. '.$cart_list[$i]['item_cost'].'</span> 
				<i class="fa fa-trash pull-right" onclick="removefromCart('.$cart_list[$i]['cid'].');"></i></div></a>';
			array_push($cdetails, $cart_details);
			}
		}
		$cart_details = array('cart_count'=>$count,'cart_details'=>$cdetails);
		print_r(json_encode($cart_details));
	}
	
	
	
	public function remove_cart(){
		if($this->session->userdata('fp_session')){	
		$cid = $this->input->post('cart_id');	
		$cart_check = $this->my_model->check('cart',array('cid'=>$cid))->num_rows();		
		$res = $this->db->delete('cart',array('cid'=>$cid));
		if($res){
			$user_id = $this->session->userdata('fp_session')['id'];
			$count = $this->my_model->check('cart',array('uid'=>$user_id,'added_by'=>'FP'))->num_rows();	
			$total = $this->my_model->check('cart',array('uid'=>$user_id,'added_by'=>'FP'))->result_array();
			for($n=0;$n<count($total);$n++){
				$total_cost += $total[$n]['total_cost'];
			}
			$total_cost =  '&#x20b9; '.$total_cost;
			print_r(json_encode(array('status'=>'1','count'=>$count,'total_cost'=>$total_cost),true));
		}else{
			print_r(json_encode(array('status'=>'0'),true));
		}
		}else{
		redirect('fp');
		}
	}
	
	
	public function checkout(){
		if($this->session->userdata('fp_session')){	
		$base_url = base_url();
		$user_id = $this->session->userdata('fp_session')['id'];
		$cart_list = $this->my_model->check('cart',array('uid'=>$user_id,'added_by'=>'FP'))->result_array();
			$data['cdetails'] = [];
			for($i=0;$i<count($cart_list);$i++){
				if($cart_list[$i]['item_type'] == 'Colours'){
				$cart_list_details[$i] = $this->my_model->check('colours',array('cid'=>$cart_list[$i]['item_id']))->row_array();
				$imgbase_url = $base_url.'assets/uploads/colours/';
				$cart_list[$i]['item_name'] = $cart_list_details[$i]['colour_name'];
				$cart_list[$i]['moq'] = $cart_list_details[$i]['moq'];
				$cart_list[$i]['item_image'] = $imgbase_url.$cart_list_details[$i]['colour_image'];				
				}else{
				$imgbase_url = $base_url.'assets/uploads/products/';
				$cart_list_details[$i] = $this->my_model->check('products',array('pid'=>$cart_list[$i]['item_id']))->row_array();
				$cart_list[$i]['item_name'] = $cart_list_details[$i]['product_name'];
				$cart_list[$i]['moq'] = $cart_list_details[$i]['moq'];
				$cart_list[$i]['item_image'] = $imgbase_url.$cart_list_details[$i]['product_image'];
				}
				array_push($data['cdetails'], $cart_list[$i]);
				$data['total_cost'] += $cart_list[$i]['total_cost'];
			}
			
			//Total items for orders
			$cart_list = $this->my_model->check('cart',array('uid'=>$user_id,'added_by'=>'FP'))->result_array();
			$total_items = [];
			for($j=0;$j<count($cart_list);$j++){
				if($cart_list[$j]['item_type'] == 'Colours'){
				$item_list_details[$j] = $this->my_model->check('colours',array('cid'=>$cart_list[$j]['item_id']))->row_array();
				$jmgbase_url = $base_url.'assets/uploads/colours/';
				$item_list[$j]['item_id'] = $item_list_details[$j]['cid'];				
				$item_list[$j]['item_type'] = $cart_list[$j]['item_type'];				
				$item_list[$j]['item_cost'] = $cart_list[$j]['item_cost'];				
				$item_list[$j]['count'] = $cart_list[$j]['count'];				
				$item_list[$j]['item_name'] = $item_list_details[$j]['colour_name'];				
				$item_list[$j]['item_image'] = $jmgbase_url.$item_list_details[$j]['colour_image'];				
				}else{
				$jmgbase_url = $base_url.'assets/uploads/products/';
				$item_list_details[$j] = $this->my_model->check('products',array('pid'=>$cart_list[$j]['item_id']))->row_array();
				$item_list[$j]['item_id'] = $item_list_details[$j]['pid'];	
				$item_list[$j]['item_type'] = $cart_list[$j]['item_type'];	
				$item_list[$j]['item_cost'] = $cart_list[$j]['item_cost'];	
				$item_list[$j]['count'] = $cart_list[$j]['count'];								
				$item_list[$j]['item_name'] = $item_list_details[$j]['product_name'];				
				$item_list[$j]['item_image'] = $jmgbase_url.$item_list_details[$j]['product_image'];
				}
				array_push($total_items, $item_list[$j]);
				$data['total_cost'] += $item_list[$j]['total_cost'];
			}
			
			$data['delivery_types'] = $this->my_model->get('delivery_types')->result_array();
			//print_r(json_encode($total_items,true));exit;
			
		$this->load->view('fp/checkout',$data);
		}else{
		redirect('fp');
		}
	}
	
	
	public function update_cart(){
		if($this->session->userdata('fp_session')){	
			$fpid = $this->session->userdata('fp_session')['id'];
			$cart_id = $this->input->post('id');
			$count = $this->input->post('count');
			$item_cost = $this->input->post('item_cost');
			$total_cost = $count*$item_cost;
			$res = $this->my_model->update('cart',array('cid'=>$cart_id),array('count'=>$count,'total_cost'=>$total_cost));
			if($res){
				$cdetails = $this->my_model->check('cart',array('cid'=>$cart_id))->row_array();
				$item_total_cost = $cdetails['total_cost'];
				$item_total_count = $cdetails['count'];
				$total_cost = $this->my_model->check('cart',array('uid'=>$fpid,'added_by'=>'FP'))->result_array();
				for($i=0;$i<count($total_cost);$i++){
					$items_total_cost += $total_cost[$i]['total_cost']; 
				}
				$item_total_cost = '&#x20b9; '.$item_total_cost;
				$items_total_cost = '&#x20b9; '.$items_total_cost;
				print_r(json_encode(array('status'=>'1','count'=>$item_total_count,'item_total_cost'=>$item_total_cost,'total_cost'=>$items_total_cost),true));
			}else{
				print_r(json_encode(array('status'=>'0'),false));
			}
		}else{
		redirect('fp');
		}
	}
	
	
	public function stocks(){
		if($this->session->userdata('fp_session')){	
		$pagination = array();
		$limit_per_page = 15;
		//echo $this->uri->segment(3);exit;
		$start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$total_records = $this->my_model->count('products');	
			
		$data['products'] = $this->my_model->get_ls('products',$limit_per_page, $start_index)->result_array();
		for($i=0;$i<count($data['products']);$i++){
			$data['products'][$i]['brand'] = $this->my_model->check('brands',array('bid'=>$data['products'][$i]['brand']))->row()->brand_name;
		}
		
		$config['base_url'] = base_url() . 'fp/stocks';
		$config['total_rows'] = $total_records;
		$config['per_page'] = $limit_per_page;
		$config["uri_segment"] = 3;       
		$config['next_link'] = 'Next';		
		$config['prev_link'] = 'Previous';			
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();
		$this->load->view('fp/stocks',$data);
		}else{
		redirect('fp');
		}
	}
	
	
	public function update_product_offline_consumption(){
		$product_id = $this->input->post('product_id');
		$offline_consumption = $this->input->post('offline_consumption');
		$res = $this->my_model->update('products',array('pid'=>$product_id),array('offline_consumption'=>$offline_consumption));
		if($res){
			$poc = $this->my_model->check('products',array('pid'=>$product_id))->row();
			$closing = $poc->quantity-($poc->online_consumption+$poc->offline_consumption);
			$details = json_encode(array('status'=>'1','offline_consumption'=>$poc->offline_consumption,'closing'=>$closing),true);
			print_r($details);
		}else{
			$details = json_encode(array('status'=>'0'),true);
			print_r($details);
		}
	}
	
	
	public function nearby_dps(){
		$order_id = $this->input->post('id');		
		$dps = $this->my_model->get('delivery_partners')->result_array();
		echo '<option value="">Select Delivery Partner</option>';
		foreach($dps as $nearbyDps){
			echo "<option value=".$nearbyDps['dlvid'].">".$nearbyDps['name']."</option>";
		}
	}    
	
	public function assign_dps(){
		$fid = $this->session->userdata('fp_session')['id'];
		$date_time = date('Y-m-d H:i');
		$order_id = $this->input->post('order_id');
		$dp_id = $this->input->post('dp');
		$where = array('dlvid'=>$dp_id);
		$dpname = $this->my_model->check('delivery_partners',$where)->row()->name;
		$data = array('oid'=>$order_id,'dp_id'=>$dp_id,'assigned_by_fp'=>$fid,'assigned_date'=>$date_time);
		$res = $this->my_model->save('dp_assigned_orders',$data);
		if($res){
			$message  = 'New order '.$order_id.' assigned';
			$this->my_model->save('notifications',array('message'=>$message,'send_to'=>'DP','uid'=>$dp_id,'date'=>date('Y-m-d')));
		}
		$test = ($res) ? '{"dp_name":"'.$dpname.'","status":"1"}' : '0';
		echo $test;
	}
	
	
	public function payment_response(){
		$user_id = $this->session->userdata('fp_session')['id'];
		//print_r($this->session->userdata('order_data'));exit;
		$orderId = $_GET['order_id'];
		$merchantId = "nippon_test";
		$ch = curl_init('https://axisbank.juspay.in/order_status');
		curl_setopt($ch, CURLOPT_POSTFIELDS ,array('orderId' => $orderId , 'merchantId' => $merchantId ));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_USERPWD, 'A18FC27CF1A24FAD8316B015A4F81C68:');
		$jsonResponse =  json_decode( curl_exec($ch) );
		
		$orderId = $jsonResponse->{'orderId'};
		$status = $jsonResponse->{'status'};
		$statusId = $jsonResponse->{'statusId'};
		$txnId = $jsonResponse->{'txnId'};
		//if($orderId != "" && $status != "" && $statusId != "" && $txnId != ""){
			$payment_response = 
				array(
					'orderId' => $orderId,
					'status'  => $status,
					'statusId'=> $statusId,
					'transaction_id'=>$txnId,
					'url' => 'http://nipponpaint-autorefinishes.com'
				);
				
				$order_Data = $this->session->userdata('order_data');
				$payment_status = array('payment_status'=>$statusId);
				$transaction_id = array('transaction_id'=>$txnId);   
				$order_Data['payment_status'] = $statusId;
				$order_Data['transaction_id'] = $txnId;
				//print_r($order_Data);exit;
				$res = $this->my_model->save('orders',$order_Data);
				$this->db->delete('cart',array('uid'=>$user_id,'added_by'=>'FP'));
				$this->session->unset_userdata('order_data');
				redirect('fp/payment_status/'.$orderId);
				
		//}
		//$this->load->view('fp/handleResponse');
	}
	
	
	public function payment_status($orderId){
		$data['order_data'] = $this->my_model->check('orders',array('order_id'=>$orderId))->row_array();
		$this->load->view('fp/handleResponse',$data);
	}
	
	public function payment(){
		$base_url = base_url();
		$user_id = $this->session->userdata('fp_session')['id'];
		$cart_list = $this->my_model->check('cart',array('uid'=>$user_id,'added_by'=>'FP'))->result_array();
			for($i=0;$i<count($cart_list);$i++){
				$total_items_cost += $cart_list[$i]['total_cost'];
			}
			
		// GST Addition Ended	
		$gst_details = $this->my_model->check('gst',array('id'=>1))->row();
		$cgst = $gst_details->cgst;
		$sgst = $gst_details->sgst;
		$cgst_added_amount = $total_items_cost*($cgst/100);
		$sgst_added_amount = $total_items_cost*($sgst/100);
		$gst_amount = $cgst_added_amount+$sgst_added_amount;
		$total_with_gst = round($total_items_cost+$gst_amount);
		// GST Addition Ended
		
			// For storing order data in session
		$total_items = [];
		for($j=0;$j<count($cart_list);$j++){
			if($cart_list[$j]['item_type'] == 'Colours'){
			$item_list_details[$j] = $this->my_model->check('colours',array('cid'=>$cart_list[$j]['item_id']))->row_array();
			$jmgbase_url = $base_url.'assets/uploads/colours/';
			$item_list[$j]['item_id'] = $item_list_details[$j]['cid'];				
			$item_list[$j]['item_type'] = $cart_list[$j]['item_type'];				
			$item_list[$j]['item_cost'] = $cart_list[$j]['item_cost'];				
			$item_list[$j]['count'] = $cart_list[$j]['count'];				
			$item_list[$j]['item_name'] = $item_list_details[$j]['colour_name'];				
			$item_list[$j]['item_image'] = $jmgbase_url.$item_list_details[$j]['colour_image'];				
			}else{
			$jmgbase_url = $base_url.'assets/uploads/products/';
			$item_list_details[$j] = $this->my_model->check('products',array('pid'=>$cart_list[$j]['item_id']))->row_array();
			$item_list[$j]['item_id'] = $item_list_details[$j]['pid'];	
			$item_list[$j]['item_type'] = $cart_list[$j]['item_type'];	
			$item_list[$j]['item_cost'] = $cart_list[$j]['item_cost'];	
			$item_list[$j]['count'] = $cart_list[$j]['count'];								
			$item_list[$j]['item_name'] = $item_list_details[$j]['product_name'];				
			$item_list[$j]['item_image'] = $jmgbase_url.$item_list_details[$j]['product_image'];
			}
			array_push($total_items, $item_list[$j]);
			$data['total_cost'] += $item_list[$j]['total_cost'];
		}
		$tcitems = json_encode($total_items,true);
		$ltcitems = str_replace("[","",$tcitems);
		$rtcitems = str_replace("]","",$ltcitems);
		
			$order_id = $this->utility->getenerate_uid();
			$order_id_check = $this->my_model->check('orders',array('order_id'=>$order_id))->num_rows();
			if($order_id_check > 0){
			$order_id = $this->utility->getenerate_uid();
			}else{
			$order_id = $order_id;
			}
		$order_data = 
			array(
				'order_id' => $order_id,
				'uid'  => $user_id,
				'items' => $rtcitems,
				'total_cost' => $total_items_cost,
				'cgst_amount' => $cgst_added_amount,
				'sgst_amount' => $sgst_added_amount,
				'total_gst_amount' => $gst_amount,
				'total_with_gst' => $total_with_gst,
				//'delivery_type' => $this->input->post('delivery_type'),
				'order_from' => 'fps',
				'added_on' => date('Y-m-d H:i:s')
			);
		$this->session->set_userdata('order_data',$order_data);
		// Storing order data in session ends //
		
		
			$data['gstDetails'] = array('total_cost'=>$total_items_cost,'cgst'=>$cgst,'sgst'=>$sgst,'cgst_added_amount'=>$cgst_added_amount,'sgst_added_amount'=>$sgst_added_amount,'total_gst_amount'=>$gst_amount,'total_with_gst'=>$total_with_gst);
			$data['user_details'] = $this->my_model->check('franchise_partners',array('fid'=>$user_id))->row_array();
		$this->load->view('fp/axis',$data);
	}
	
	
	public function payment_process(){
		$this->load->view('fp/payment_process');
	}
	
	
		
	
}