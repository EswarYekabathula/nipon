<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dp extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('email');	
		$this->load->helper('cookie');
		$this->load->library('utility');	
		$this->load->library('pagination');	
		$this->load->model('Dp_model','my_model');  				
	}

	
	public function index()
	{
		if(!$this->session->userdata('dp_session')){	
			$this->load->view('dp/login');
		}else{
			redirect('dp/dashboard');
		}	
	}
	
	
	public function login()	
	{
		$email = $this->input->post('email');
		$p = $this->input->post('password');
		$password = md5($this->input->post('password'));		
            $remember = $this->input->post('remember_me');
		
		$query = $this->my_model->login($email,$password);
		$data = $query->row_array();
		$rows = $query->num_rows();
		//print_r($rows);exit;
		if($rows > 0){  
			$ac_rows = $this->my_model->check('delivery_partners',array('email'=>$email,'password'=>$password,'dstatus'=>'1'))->num_rows();
			if($ac_rows > 0){
				if ($remember) {
					   $user_email= array( 'name'   => 'dpusername', 'value'  => $email, 'expire' => '86400' ); 		
					   $user_password = array( 'name'   => 'dppassword', 'value'  => $p, 'expire' => '86400' ); 		
					   $this->input->set_cookie($user_email);
					   $this->input->set_cookie($user_password);
				}else{
				delete_cookie('dpusername'); 
				delete_cookie('dppassword'); 
				}                     

				$admindata = array(
								'id'  => $data['dlvid'],
								'name' => $data['name'],
								'email' => $data['email']					
									);
				$this->session->set_userdata('dp_session',$admindata);
				redirect('dp/dashboard',$data);
			}else{
				$this->session->set_flashdata('message','Inactive');
				redirect('dp');	
			}	
		}else{	
			$this->session->set_flashdata('message','Failed');
			redirect('dp');			
		}
	}
	
	
	public function dashboard()
	{
		if($this->session->userdata('dp_session')){	
		$dpid = $this->session->userdata('dp_session')['id'];
		$where = "dp_id=".$dpid." and dp_status != '2'";
		$data['orders'] = $this->my_model->check('dp_assigned_orders',$where)->num_rows();
		$data['routes'] = $this->my_model->count('routes');
		$data['riders'] = $this->my_model->count('riders');
		$data['fp'] = $this->my_model->count('franchise_partners');	  
		$this->load->view('dp/index',$data);
		}else{
		redirect('dp');
		}
	}
	
	
	public function get_cities(){
		$state = $this->input->post('state');
		$cities = $this->my_model->get_cities_b_states($state);
		//print_r($data);
		$h = '<option>Select City</option>';
		foreach($cities as $cval){
			$h .= '<option value="'.$cval['City'].'">'.$cval['City'].'</option>';
		}
		print_r($h);
	}
	
	public function logout() {
		$this->session->sess_destroy();
		$this->session->set_flashdata("success", LOGOUTSUCCESS);
		redirect("dp");
	}
	
	
	
	
	public function edit_profile(){
		if($this->session->userdata('dp_session')){	
		$admindata = $this->session->userdata("dp_session");
		$id = $admindata['id'];
		$where	= array('dlvid'=>$id); 		
		//print_r($where);exit;
			if(isset($_POST['submit'])){				
			
				
				if($this->input->post('password') != ''){
				$password = md5($this->input->post('password'));
				}else{
				$password = $this->input->post('old_password');
				}
				
				
				
				$data = array(
				'name' => $this->input->post('name'),            			
				'email' => $this->input->post('email'),
				'password' => $password
				);
				$this->my_model->update('delivery_partners',$where,$data);
				$this->session->set_flashdata('message','updated');
				$admin_data = $this->my_model->check('delivery_partners',$where)->row_array();
				//print_r($admin_data);exit;
				$admindata = array(
							'id'  => $admin_data['dlvid'],
							'name' => $admin_data['name'],
							'email' => $admin_data['email']						
								);
				
				$this->session->set_userdata('dp_session', $admindata);	
				//echo "<pre>";print_r($this->session->all_userdata());echo "</pre>";		
				redirect('dp/edit_profile');
				
			}else{
			$data['userdetails'] = $this->my_model->check('delivery_partners',$where)->row_array();
			//print_r($data);exit;
			$this->load->view('dp/edit_profile',$data);
			}
		}else{
		redirect('dp');
		}
	}
	
	public function check_mobile(){
		$phone = $this->input->post('mobile');		
		$table = $this->input->post('table');		
		$where = array('phone'=>$phone);
		$rows = $this->my_model->check($table,$where)->num_rows();		
		if($rows > 0){
			echo '1';
		}else{
			echo '0';
		}
	}
	
	
	public function check_email(){
		$email = $this->input->post('email');		
		$table = $this->input->post('table');		
		$where = array('email'=>$email);
		$rows = $this->my_model->check($table,$where)->num_rows();		
		if($rows > 0){
			echo '1';
		}else{
			echo '0';
		}
	}
	
	
	
	public function forgot_password(){
		$email = $this->input->post('email');
		$pwd = $this->utility->generate_otp();
		$password =  md5($pwd);
		$where  =array('email'=>$email);
		$rows = $this->my_model->check('delivery_partners',$where)->num_rows();
		if($rows > 0){
		$update = array('password'=>$password);
		$res = $this->my_model->update('delivery_partners',$where,$update);
		if($res){
			$subject = 'Forgot Password';
			$message = 'Dear Delivery Partner,your new password is '.$pwd.'';   
			$this->utility->send_email($email,$message,$subject);
			$this->session->set_flashdata('message','pasword_changed');
		}else{
			$this->session->set_flashdata('message','Mobile_not_exists');
		}
		}else{
		$this->session->set_flashdata('message','Mobile_not_exists');
		}
		redirect('catalyst/index');
	}
	


	
	
	public function send_email($toemail,$message,$subject)
	{
		//print_r($toemail);print_r($message);exit;
		$CI =& get_instance();
	  
		$CI->email->initialize(array(
			'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_user' => 'appcare.testing@gmail.com',
			'smtp_pass' => 'Appcare@123',
			'smtp_port' => 465,
			'mailtype'  => 'html',
			'crlf'      => "\r\n",
			'newline'   => "\r\n",
			'charset'	=> "utf-8"
		));
		
	  $CI->email->from('appcare.testing@gmail.com',strtoupper('Nippon'));
	  $CI->email->to($toemail);
	  $CI->email->subject($subject);
	  $CI->email->message($message);
      
	  $result = $CI->email->send();
	  
	  if($result){
		return true; 
	  }else{
		//echo $CI->email->print_debugger();exit;
		return false; 
	  }	  
	}    


	public function franchise_partners(){
		if($this->session->userdata('dp_session')){		
		$pagination = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->my_model->count('franchise_partners');
		
			$data['fp'] = $this->my_model->get_ls('franchise_partners',$limit_per_page, $start_index)->result_array();
			//print_r(($data['dealers']));	exit;
			$config['base_url'] = base_url() . 'dp/franchise_partners';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();	
		
		$this->load->view('dp/franchise_partners',$data);
		}else{
		redirect('dp');
		}
	}


	public function routes(){
		if($this->session->userdata('dp_session')){		
		$pagination = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->my_model->count('routes');
		
			$data['fp'] = $this->my_model->get_ls('routes',$limit_per_page, $start_index)->result_array();
			//print_r(($data['dealers']));	exit;
			$config['base_url'] = base_url() . 'dp/routes';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();	
		
		$this->load->view('dp/routes',$data);
		}else{
		redirect('dp');
		}
	}	
	
	public function route_action($id=''){
		if($this->session->userdata('dp_session')){	
			if(isset($_POST['submit'])){
							
				if($id != ''){
				$data = array(
				'route_name' => $this->input->post('route_name'),				
				'slatitude' => $this->input->post('slattitude'), 
				'slongitude' => $this->input->post('slongitude'), 
				'start_location' => $this->input->post('start_location'), 
				'end_location' => $this->input->post('end_location'), 
				'end_latitude' => $this->input->post('end_lattitude'), 
				'end_longitude' => $this->input->post('end_longitude'), 
				'pickup_location' => $this->input->post('pickup_location')
				);
				
				$where	= array('id'=>$id);
				$this->my_model->update('routes',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'route_name' => $this->input->post('route_name'),				
				'slatitude' => $this->input->post('slattitude'), 
				'slongitude' => $this->input->post('slongitude'), 
				'start_location' => $this->input->post('start_location'), 
				'end_location' => $this->input->post('end_location'), 
				'end_latitude' => $this->input->post('end_lattitude'), 
				'end_longitude' => $this->input->post('end_longitude'), 
				'pickup_location' => $this->input->post('pickup_location'),
				'created' => date('Y-m-d')
				);
				//print_r($data);exit;
				$this->my_model->save('routes',$data);
				$this->session->set_flashdata('message','added');				
				}
				redirect('dp/routes');
				
			}else{
				if ($id != "") {
				$data["fpdata"] = $this->my_model->get('franchise_partners')->result_array();	
				$data['route_data'] = $this->my_model->check('routes',array('id'=>$id))->row_array();
				$this->load->view("dp/add_route", $data);
				} else {					
				$data["fpdata"] = $this->my_model->get('franchise_partners')->result_array();				
				$this->load->view("dp/add_route",$data);
				}
			}					
		}else{
		redirect('dp');
		}
	}
	
	
	
	public function riders(){
		if($this->session->userdata('dp_session')){		
		$pagination = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->my_model->count('riders');
		
			$data['riders'] = $this->my_model->get_ls('riders',$limit_per_page, $start_index)->result_array();
			//print_r(($data['dealers']));	exit;
			$config['base_url'] = base_url() . 'dp/riders';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();	
		
		$this->load->view('dp/riders',$data);
		}else{
		redirect('dp');
		}
	}
	
	public function rider_action($id=''){
		if($this->session->userdata('dp_session')){	
			if(isset($_POST['submit'])){
							
				if($id != ''){
				$data = array(
				'rider_name' => $this->input->post('rider_name'),	
				'email' => $this->input->post('email'),	
				'address' => $this->input->post('address'), 
				'route' => $this->input->post('route'), 
				'type_of_vehicle' => $this->input->post('vehicle_type')
				);
				
				$where	= array('rid'=>$id);
				$this->my_model->update('riders',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'rider_name' => $this->input->post('rider_name'),	
				'email' => $this->input->post('email'),	
				'password' => md5($this->input->post('password')),	
				'address' => $this->input->post('address'), 
				'route' => $this->input->post('route'), 
				'type_of_vehicle' => $this->input->post('vehicle_type'),
				'created' => date('Y-m-d')
				);
				//print_r($data);exit;
				$this->my_model->save('riders',$data);

				//To Send an email
				$subject = 'Registration';
				$message = 'Dear Rider,Your registration done successfully.Your password is '.$this->input->post('password').'.Thanks for registering with Xpress Delivery';   
				$this->utility->send_email($this->input->post('email'),$message,$subject);
				
				$this->session->set_flashdata('message','added');				
				}
				redirect('dp/riders');
				
			}else{
				if ($id != "") {
				$data["route_data"] = $this->my_model->get('routes')->result_array();	
				$data['rider_data'] = $this->my_model->check('riders',array('rid'=>$id))->row_array();
				$this->load->view("dp/add_rider", $data);
				} else {					
				$data["route_data"] = $this->my_model->get('routes')->result_array();				
				$this->load->view("dp/add_rider",$data);
				}
			}					
		}else{
		redirect('dp');
		}
	}
	
	
	public function orders(){
		if($this->session->userdata('dp_session')){
			$dpid  =   $this->session->userdata('dp_session')['id'];
		$pagination = array();
        $limit_per_page = 10;
		//echo $this->uri->segment(3);exit;
        $start_index = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $total_records = $this->my_model->count_with_condition('dp_assigned_orders',array('dp_id'=>$dpid));		
			$data['dorders'] = $this->my_model->condition_and_limit_query('dp_assigned_orders',array('dp_id'=>$dpid),$limit_per_page, $start_index)->result_array();			
			//print_r($data['dorders']);exit;
			for($i=0;$i<count($data['dorders']);$i++){
				$where = "oid=".$data['dorders'][$i]['oid']." and dp_status != '2'";
				$data['dorders'][$i]['total_cost'] = $this->my_model->check('orders',array('oid'=>$data['dorders'][$i]['oid']))->row()->total_cost;
				$data['dorders'][$i]['order_id'] = $this->my_model->check('orders',array('oid'=>$data['dorders'][$i]['oid']))->row()->order_id;
				$data['dorders'][$i]['route_name'] = $this->my_model->check('routes',array('id'=>$data['dorders'][$i]['route']))->row()->route_name;
			}
			
			$config['base_url'] = base_url() . 'fp/dealer_orders';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 4;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();
			//print_r($data);exit;
			$data['riders'] = $this->my_model->check('riders',array('rstatus'=>'1'))->result_array();
			$this->load->view('dp/dorders',$data);
		}else{
		redirect('fp');	
		}
	}
	
	public function update_notifications_status(){
		$uid = $this->input->post('uid');
		$send_to = $this->input->post('send_to');
		$update = array('view_status'=>'1');
		$where  = array('send_to'=>$send_to,'uid'=>$uid);
		$res = $this->my_model->update('notifications',$where,$update);
		$result = ($res)? '1' : '0';
		echo $result;    
	}
	
	
	public function get_routes(){
		$order_id = $this->input->post('id');		
		$routes = $this->my_model->get('routes')->result_array();
		echo '<option value="">Select Route</option>';
		foreach($routes as $routesvalues){
			echo "<option value=".$routesvalues['id'].">".$routesvalues['route_name']."</option>";
		}
	}   
	
	
	public function assign_route(){
		
		$date_time = date('Y-m-d H:i');
		$order_id = $this->input->post('order_id');
		$route = $this->input->post('fp');
	
		$data = array('route'=>$route);
		$res = $this->my_model->update('dp_assigned_orders',array('oid'=>$order_id),$data);
		$route = $this->my_model->check('dp_assigned_orders',array('oid'=>$order_id,'dp_status'=>'1'))->row()->route;
		$route_name = $this->my_model->check('routes',array('id'=>$route))->row()->route_name;
		if($res){
			$message  = 'Route is assigned by delivery partner '.$order_id.'';
			//$this->my_model->save('notifications',array('message'=>$message,'send_to'=>'FP','uid'=>$fid,'date'=>date('Y-m-d')));
		}
		$test = ($res) ? '{"route_name":"'.$route_name.'","status":"1"}' : '0';
		echo $test;
	}
	
	
	//For Order Details
	public function order_details($id){
		//$id = $this->input->post('id');
		$where = array('oid'=>$id);			
		$details = $this->my_model->check('orders',$where)->row_array();		
		$idet = str_replace('},', '}&', $details['items']);
		$item_split_details = explode("&",$idet);
		$data['items'] = [];
		for($i=0;$i<count($item_split_details);$i++){
			$item_details[$i] = json_decode($item_split_details[$i],true);
			array_push($data['items'],$item_details[$i]);
		}	
		$data['total_cost'] = $details['total_cost'];
		$data['route_details'] = $this->my_model->check('dp_assigned_orders',$where)->row_array();
		//print_r($data);exit;
		$this->load->view('dp/order_details',$data);
	}
	
	
	// To Assign Rider
	public function assign_rider(){
		$orders = $this->input->post('orders');
		$rider_id = $this->input->post('rider_id');
		$timeslot = $this->input->post('timeslot');
		$multiple_orders = explode(',',$orders);		
		for($i=0;$i<count($multiple_orders);$i++){
			$res = $this->my_model->update('dp_assigned_orders',array('oid'=>$multiple_orders[$i],'dp_status'=>'1'),array('rider'=>$rider_id,'timeslot'=>$timeslot));
		}
		if($res){
			print_r(json_encode(array('status'=>'1','order_ids'=>$multiple_orders),true));
		}else{
			print_r(json_encode(array('status'=>'0'),true));
		}
	}
	
	
	//To Accept Order By Delivery Partner
	public function change_dporder_status(){
		$order_id = $this->input->post('order_id');
		$status = $this->input->post('status');
		$dp_id = $this->input->post('dp_id');
		$where = array('oid'=>$order_id,'dp_id'=>$dp_id);
		$update = array('dp_status'=>$status);
		$res = $this->my_model->update('dp_assigned_orders',$where,$update);
		$fp_id = $this->my_model->check('dp_assigned_orders',$where)->row()->assigned_by_fp;
		if($res){
			$dp_details = $this->my_model->check('delivery_partners',array('dlvid'=>'1'))->row();			
			if($status == '1'){
				$message  = 'Order '.$order_id.' has been accepted by '.$dp_details->name.'';
				$this->my_model->update('orders',array('oid'=>$order_id),array('dp_status'=>$dp_id));
				$this->my_model->save('notifications',array('message'=>$message,'send_to'=>'FP','uid'=>$fp_id,'date'=>date('Y-m-d')));
				$msg = '<a href="#" class="btn btn-primary btn-xs m-b-10 m-l-5">Accepted</a>';
				$response=json_encode(array('msg'=>$msg,'status'=>'accept'),true);		
			}else{
				$message  = 'Order '.$order_id.' has been rejected by '.$dp_details->name.'';
				$this->my_model->update('orders',array('oid'=>$order_id),array('dp_status'=>'0'));
				$this->my_model->save('notifications',array('message'=>$message,'send_to'=>'FP','uid'=>$fp_id,'date'=>date('Y-m-d')));				
				$msg = '<a href="#" class="btn btn-danger btn-xs m-b-10 m-l-5">Rejected</a>';				
				$response=json_encode(array('msg'=>$msg,'status'=>'reject'),true);		
			}
			print_r($response);  
		}else{
			echo '0';
		}
	}
	
	public function accept_order_dp(){
		$order_id = $this->input->post('order_id');
		$status = $this->input->post('status');
		$dp_id = $this->input->post('dp_id');
		$delivery_date = date('Y-m-d',strtotime($this->input->post('delivery_date')));
		$where = array('oid'=>$order_id,'dp_id'=>$dp_id);
		$update = array('dp_status'=>$status);
		$res = $this->my_model->update('dp_assigned_orders',$where,$update);
		$fp_id = $this->my_model->check('dp_assigned_orders',$where)->row()->assigned_by_fp;
		if($res){
			$dp_details = $this->my_model->check('delivery_partners',array('dlvid'=>'1'))->row();			
			
				$message  = 'Order '.$order_id.' has been accepted by '.$dp_details->name.'';
				$this->my_model->update('orders',array('oid'=>$order_id),array('dp_status'=>$dp_id,'delivery_date'=>$delivery_date));
				$this->my_model->save('notifications',array('message'=>$message,'send_to'=>'FP','uid'=>$fp_id,'date'=>date('Y-m-d')));
				$msg = '<a href="#" class="btn btn-primary btn-xs m-b-10 m-l-5">Accepted</a>';
				$response=json_encode(array('msg'=>$msg,'status'=>'accept'),true);		
			
			print_r($response);  
		}else{
			echo '0';
		}
	}
	
	
	public function get_order_delivery_date(){
		$order_id = $this->input->post('order_id');
		$odetails = $this->my_model->check('orders',array('oid'=>$order_id))->row();
		$delivery_date = $odetails->delivery_date;
		$response = array('status'=>'1','delivery_date'=>$delivery_date);
		print_r(json_encode($response));
	}
	
	public function get_rider_lat_long(){
		$rider_id = $this->input->post('rider_id');
		$rider_details = $this->my_model->check('riders',array('rid'=>$rider_id))->row_array();
		$data = array('lattitude'=>$rider_details['lat'],'longitude'=>$rider_details['lng']);
		print_r(json_encode($data));
	}
	
	
	
	
	
}