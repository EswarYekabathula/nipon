<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Rider extends REST_Controller {
	
	function __construct()
    {
        // Construct the parent class
        parent::__construct();
		$this->load->model('services_model');
		$this->load->helper('string');
		$this->load->library('utility');
		$this->load->library('push_notifications');
		$this->load->library('email');
    }


	
	
	public function login_post(){
		$rider_name = $this->post('username');	
		$lat = $this->post('lat');
		$lng = $this->post('lng');
		$password = md5($this->post('password')); 
		//$update_array = array('device_token'=>$device_token,'device_name'=>$device_name);
		
		if($rider_name != '' && $password != '' && $lat != "" && $lng != ""){
			$check = array('rider_name'=>$rider_name);
			$user_rows = $this->services_model->check('riders',$check)->num_rows(); 
			//print_r($user_rows);exit;
			if($user_rows > 0){					
					$check2 = array('rider_name'=>$rider_name,'password'=>$password);
					$user_valid = $this->services_model->check('riders',$check2)->num_rows(); 
						if($user_valid > 0){
							$update=array('lat'=>$lat,'lng'=>$lng);		
							$this->services_model->update('riders',$check2,$update); 
							$user_status_check = $this->services_model->check('riders',array('rider_name'=>$rider_name,'password'=>$password,'rstatus'=>'1'))->num_rows();
							if($user_status_check > 0){
							$base_path = base_url().'assets/uploads/users/';					
							$user_details = $this->services_model->check('riders',$check)->row();
							
							$userdetails = 	array('rider_id'=>$user_details->rid,'rider_name'=>$user_details->rider_name,'password'=>md5($user_details->password),'email'=>$user_details->email);
							$this->response(['status'=>TRUE,'message'=>'Login success','user_details'=>$userdetails],200);	
							}else{
								$this->response(['status'=>FALSE,'message'=>'Your account will be activated once it is approved by admin'],400);	
							}
						}else{
							$this->response(['status'=>FALSE,'message'=>'Invalid password/password doesnot match'],400);	
						}					
			}else{
				$this->response(['status'=>FALSE,'message'=>'rider does not exists'],400);
			}
		}else{
			$this->response(['status'=>FALSE,'message'=>'Please enter username,password,lat and lng'],400);
		}
	}
	
	
	public function orders_get(){
		$rider_id = $this->get('rider_id');
		if($rider_id != ""){
		$orders = $this->services_model->check('dp_assigned_orders',array('rider'=>$rider_id))->result_array();
			if(count($orders) > 0){
				$PendingOrders = array();
				for($i=0;$i<count($orders);$i++){
					$order_details = $this->services_model->check('orders',array('oid'=>$orders[$i]['oid']))->row_array();
					if($order_details['order_from'] == 'dealers'){
					$userDetails = $this->services_model->check('dealers',array('did'=>$order_details['uid']))->row_array();
					}else{
					$userDetails = $this->services_model->check('franchise_partners',array('fid'=>$order_details['uid']))->row_array();	
					}
					if($orders[$i]['rider_status'] == '0'){
					$order_status	= 'Pending';
					}else if($orders[$i]['rider_status'] == '1'){
					$order_status	= 'Accepted';
					}else if($orders[$i]['rider_status'] == '2'){
					$order_status	= 'Rejected';
					}
					$delivery_address = array('flat_no'=>$userDetails['flat_no'],'city'=>$userDetails['city'],'state'=>$userDetails['state'],'colony'=>$userDetails['colony'],'pincode'=>$userDetails['pincode']);
					$orderDetails = array('dpid'=>$orders[$i]['id'],'order_status'=>$order_status,'order_id'=>$order_details['order_id'],'delivery_address'=>$delivery_address);
					array_push($PendingOrders,$orderDetails);
				}
					$this->response(['status'=>TRUE,'message'=>'success','orders'=>$PendingOrders],200);  
			}else{
				$this->response(['status'=>FALSE,'message'=>'No orders found'],400);  	
			}
		}else{
			$this->response(['status'=>FALSE,'message'=>'Please enter rider id'],400);  
		}
	}   
	
	
	public function accept_order_get(){
		$dpid = $this->get('dpid');
		if($dpid != ""){
			$dprows = $this->services_model->check('dp_assigned_orders',array('id'=>$dpid))->num_rows();
			if($dprows > 0){
				$res = $this->services_model->update('dp_assigned_orders',array('id'=>$dpid),array('rider_status'=>'1'));
				if($res){
					$this->response(['status'=>TRUE,'message'=>'Order accepted successfully'],200); 
				}else{
					$this->response(['status'=>FALSE,'message'=>'Please try later'],400); 
				}
			}else{
				$this->response(['status'=>FALSE,'message'=>'Invalid rider assigned id'],400); 
			}	
		}else{
			$this->response(['status'=>FALSE,'message'=>'Please enter rider assigned order id(dpid)'],400);  
		}
	}
	
	public function reject_order_get(){
		$dpid = $this->get('dpid');
		if($dpid != ""){
			$dprows = $this->services_model->check('dp_assigned_orders',array('id'=>$dpid))->num_rows();
			if($dprows > 0){
				$res = $this->services_model->update('dp_assigned_orders',array('id'=>$dpid),array('rider_status'=>'2'));
				if($res){
					$this->response(['status'=>TRUE,'message'=>'Order rejected successfully'],200); 
				}else{
					$this->response(['status'=>FALSE,'message'=>'Please try later'],400); 
				}
			}else{
				$this->response(['status'=>FALSE,'message'=>'Invalid rider assigned id'],400); 
			}	
		}else{
			$this->response(['status'=>FALSE,'message'=>'Please enter rider assigned order id(dpid)'],400);  
		}
	}
	  
  
	
	





}