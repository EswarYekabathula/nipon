<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Services extends REST_Controller {
	
	function __construct()
    {
        // Construct the parent class
        parent::__construct();
		$this->load->model('services_model');
		$this->load->helper('string');
		$this->load->library('utility');
		$this->load->library('push_notifications');
		$this->load->library('email');
    }

	public function index_get(){
		$this->load->view('api/services');
	}
	
	public function generatekey($mobile_number,$token){
		$salt = hash('sha256', time() . mt_rand().base64_encode($mobile_number.$token));
		$key = substr($salt, 0, config_item('rest_key_length'));
		return $key;
	}


	public function register_post(){                
       				
		$dealer_name =  $this->post('dealername');		
		$store_name =  $this->post('storename');		
		$email =  $this->post('email');	
		$password =  base64_encode($this->post('password'));
		$reg_on = date('Y-m-d');
		//$store_address =  $this->post('store_address');		
		$mobile =  $this->post('phonenumber');
		
		
		if($dealer_name != ""  && $store_name != "" && $email != "" && $password != ""  && $mobile != ""){
			if(filter_var($email, FILTER_VALIDATE_EMAIL)){
			$checkuser = array('dealer_name'=>$dealer_name);
			$check_user_rows = $this->services_model->check('dealers',$checkuser)->num_rows();
			if($check_user_rows == 0){
				$checkemail = array('email'=>$email);
				$check_email_rows = $this->services_model->check('dealers',$checkemail)->num_rows();
				if($check_email_rows == 0){
						$checkmobile = array('mobile'=>$mobile);
						$check_mobile_rows = $this->services_model->check('dealers',$checkmobile)->num_rows();
						if($check_mobile_rows == 0){
						$data = array('dealer_name'=>$dealer_name,'store_name'=>$store_name,'email'=>$email,'password'=>$password,'mobile'=>$mobile,'created_on'=>$reg_on);
						$res = $this->services_model->save('dealers',$data);
						if($res){				
						$user_details = $this->services_model->check('dealers',$checkuser)->row();
						$userdetails = 	array('userid'=>$user_details->did,'username'=>$user_details->dealer_name,'password'=>base64_decode($user_details->password),'phonenumber'=>$user_details->mobile,'email'=>$user_details->email,'storename'=>$user_details->store_name);
						$this->response(['status'=>TRUE,'message'=>'user registered succesfully','user_details'=>$userdetails],200);
						}else{
						$this->response(['status'=>FALSE,'message'=>'Please try again'],400);
						}
						}else{
							$this->response(['status'=>FALSE,'message'=>'Mobile number already exists'],400);
						}
				}else{
					$this->response(['status'=>FALSE,'message'=>'Email already exists'],400);
				}
			}else{
				$this->response(['status'=>FALSE,'message'=>'User name already exists'],400);
			}
			}else{
				$this->response(['status'=>FALSE,'message'=>'Please enter valid email id'],400);
			}
		}else{
			$this->response(['status'=>FALSE,'message'=>'Please enter dealername,storename,email,phonenumber and password'],400);
		}
				
	}
	
	
	public function login_post(){
		$dealer_name = $this->post('username');	
		$device_token = $this->post('device_token');	
		$device_name = $this->post('device_name');
		$lat = $this->post('lat');
		$lng = $this->post('lng');
		$password = base64_encode($this->post('password')); 
		//$update_array = array('device_token'=>$device_token,'device_name'=>$device_name);
		
		if($dealer_name != '' && $password != '' && $device_name != "" && $device_token != "" && $lat != "" && $lng != ""){
			$check = array('dealer_name'=>$dealer_name);
			$user_rows = $this->services_model->check('dealers',$check)->num_rows(); 
			//print_r($user_rows);exit;
			if($user_rows > 0){					
					$check2 = array('dealer_name'=>$dealer_name,'password'=>$password);
					$user_valid = $this->services_model->check('dealers',$check2)->num_rows(); 
						if($user_valid > 0){
							$update=array('device_token'=>$device_token,'device_name'=>$device_name,'lat'=>$lat,'lng'=>$lng);		
							$this->services_model->update('dealers',$check2,$update); 
							$user_status_check = $this->services_model->check('dealers',array('dealer_name'=>$dealer_name,'password'=>$password,'dstatus'=>'1'))->num_rows();
							if($user_status_check > 0){
							$base_path = base_url().'assets/uploads/users/';					
							$user_details = $this->services_model->check('dealers',$check)->row();
							$cart_count =  $this->services_model->check('cart',array('uid'=>$user_details->did,'added_by'=>'Dealer'))->num_rows();
							$userdetails = 	array('userid'=>$user_details->did,'username'=>$user_details->dealer_name,'password'=>base64_decode($user_details->password),'phonenumber'=>$user_details->mobile,'email'=>$user_details->email,'storename'=>$user_details->store_name);
							$this->response(['status'=>TRUE,'message'=>'Login success','cart_count'=>$cart_count,'user_details'=>$userdetails],200);	
							}else{
								$this->response(['status'=>FALSE,'message'=>'Your account will be activated once it is approved by admin'],400);	
							}
						}else{
							$this->response(['status'=>FALSE,'message'=>'Invalid password/password doesnot match'],400);	
						}					
			}else{
				$this->response(['status'=>FALSE,'message'=>'user does not exists'],400);
			}
		}else{
			$this->response(['status'=>FALSE,'message'=>'Please enter username,password,device_name,device_token,lat and lng'],400);
		}
	}
	
  	
	
	
	public function update_profile_image_post(){
		$user_id = $this->post('uid');
		$datetime = date('his');		
		if($user_id != "" && !empty($_FILES['profile_image']['name'])){
			$cart_count =  $this->services_model->check('cart',array('uid'=>$user_id,'added_by'=>'Dealer'))->num_rows();
			$check = array('did'=>$user_id);
			$auth_rows = $this->services_model->check('dealers',$check)->num_rows();
			if($auth_rows > 0){
			if (!empty($_FILES['profile_image']['name'])) {
				$config['upload_path'] = 'assets/uploads/dealers';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['encrypt_name'] = TRUE;
				$config['max_size'] = 10000;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('profile_image')) {
					$error = array(
						'error' => $this->upload->display_errors()
					);
					print_r($error);
					exit;
				} else {
					$upload_data = $this->upload->data();
					$profile_image = $upload_data['file_name'];
				}
			}
			
			$where = array('did'=>$user_id);
			$updatearray = array('profile_image'=>$profile_image);
			$res = $this->services_model->update('dealers',$where,$updatearray);
			if($res){
				$base_path = base_url().'assets/uploads/dealers/';
				$user_details = $this->services_model->check('dealers',$where)->row();
				$userdetails = 	array('uid'=>$user_details->did,'name'=>$user_details->dealer_name,'email'=>$user_details->email,'mobile'=>$user_details->mobile,'store_name'=>$user_details->store_name,'store_address'=>$user_details->store_address,'profile_image'=>$base_path.$user_details->profile_image,'kyc_status'=>$user_details->kyc_status);
				$this->response(['status'=>TRUE,'message'=>'Profile updated successfully','cart_count'=>$cart_count,'user_details'=>$userdetails],200);
			}else{
				$this->response(['status'=>FALSE,'message'=>'Please try again'],400);
			}
			}else{
					$this->response(['status'=>FALSE,'message'=>'Invalid User id'],400);	
			}
		}else{
        $this->response(['status'=>FALSE,'message'=>'Please enter uid and upload profile_image'],400);   
        } 
		
	}
	
	
	
	public function kyc_process_post(){
		$user_id = $this->post('userid');
		$pan_no = $this->post('pan_no');
		$gst = $this->post('gst_no');		
		$flat_no =  $this->post('flat_no');
		$colony =  $this->post('colony');
		$city =  $this->post('city');
		$state =  $this->post('state');
		$pincode =  $this->post('pincode');	
		if($user_id != "" && $pan_no != "" && $gst != "" && $flat_no && $city != "" && $state != "" && $colony != "" && $pincode != ""){
			$check = array('did'=>$user_id);
			$auth_rows = $this->services_model->check('dealers',$check)->num_rows();
			if($auth_rows > 0){
				$shop_base_path = base_url().'assets/uploads/shops/';  
				$pan_check = array('did'=>$user_id,'pan_number'=>$pan_no);
				$pan_rows = $this->services_model->check('dealers',$pan_check)->num_rows();
				if($pan_rows == 0){
					$pan_check = array('did'=>$user_id,'pan_number'=>$pan_no);
					$pan_rows = $this->services_model->check('dealers',$pan_check)->num_rows();
				if (!empty($_FILES['shop_image']['name'])) {
					$config['upload_path'] = 'assets/uploads/shops';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['encrypt_name'] = TRUE;
					$config['max_size'] = 10000;
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('shop_image')) {
					$error = array(
						'error' => $this->upload->display_errors()
					);
					print_r($error);
					exit;
					} else {
					$upload_data = $this->upload->data();
					$store_image = $upload_data['file_name'];
					}
				}
						
						$where = array('did'=>$user_id);
						$updatearray = array('shop_image'=>$store_image,'pan_number'=>$pan_no,'gst_number'=>$gst,'kyc_status'=>'1','flat_no'=>$flat_no,'city'=>$city,'state'=>$state,'colony'=>$colony,'pincode'=>$pincode);
						$res = $this->services_model->update('dealers',$where,$updatearray);
						if($res){
							$base_path = base_url().'assets/uploads/dealers/';
							$shop_base_path = base_url().'assets/uploads/shops/';
							$user_details = $this->services_model->check('dealers',$where)->row();
							$userdetails = 	array('userid'=>$user_details->did,'username'=>$user_details->dealer_name,'password'=>base64_decode($user_details->password),'email'=>$user_details->email,'phonenumber'=>$user_details->mobile,'storename'=>$user_details->store_name,'flat_no'=>$user_details->flat_no,'colony'=>$user_details->colony,'city'=>$user_details->city,'state'=>$user_details->state,'pincode'=>$user_details->pincode,'shop_image'=>$shop_base_path.$user_details->shop_image);
							$this->response(['status'=>TRUE,'message'=>'Kyc process done successfully','user_details'=>$userdetails],200);
						}else{
							$this->response(['status'=>FALSE,'message'=>'Please try again'],400);
						}
				}else{
					$this->response(['status'=>FALSE,'message'=>'Pan number already exists'],400);
				}		
			}else{
					$this->response(['status'=>FALSE,'message'=>'Invalid User id'],400);	
			}
		}else{
        $this->response(['status'=>FALSE,'message'=>'userid,pan_no,gst_no and store_address are required'],400);   
        } 
		
	}
	
	
	
	public function update_shop_image_post(){
		$user_id = $this->post('userid');  
		$datetime = date('his');		
		if($user_id != "" && !empty($_FILES['shop_image']['name'])){
			$cart_count =  $this->services_model->check('cart',array('uid'=>$user_id,'added_by'=>'Dealer'))->num_rows();
			$check = array('did'=>$user_id);
			$auth_rows = $this->services_model->check('dealers',$check)->num_rows();
			if($auth_rows > 0){
				if (!empty($_FILES['shop_image']['name'])) {
					$config['upload_path'] = 'assets/uploads/shops';
					$config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPEG|JPG';
					$config['encrypt_name'] = TRUE;
					$config['max_size'] = 80000;
					$this->load->library('upload', $config);
					$this->upload->initialize($config);  
					if (!$this->upload->do_upload('shop_image')) {
					$error = array(
						'error' => $this->upload->display_errors()
					);
					print_r($error);
					exit;
					} else {
					$upload_data = $this->upload->data();
					$shop_image = $upload_data['file_name'];
					}
				}
			
			$where = array('did'=>$user_id);
			$updatearray = array('shop_image'=>$shop_image);
			$res = $this->services_model->update('dealers',$where,$updatearray);
			if($res){
				$shop_base_path = base_url().'assets/uploads/shops/';
				$base_path = base_url().'assets/uploads/dealers/';
				$user_details = $this->services_model->check('dealers',$where)->row();
				$userdetails = 	array('uid'=>$user_details->did,'name'=>$user_details->dealer_name,'email'=>$user_details->email,'mobile'=>$user_details->mobile,'store_name'=>$user_details->store_name,'kyc_status'=>$user_details->kyc_status,'shop_image'=>$shop_base_path.$user_details->shop_image);
				$this->response(['status'=>TRUE,'message'=>'Shop Image updated successfully','cart_count'=>$cart_count,'user_details'=>$userdetails],200);
			}else{
				$this->response(['status'=>FALSE,'message'=>'Please try again'],400);
			}
			}else{
					$this->response(['status'=>FALSE,'message'=>'Invalid User id'],400);	
			}
		}else{
        $this->response(['status'=>FALSE,'message'=>'Please enter uid and upload shop_image'],400);   
        } 
		
	}
	
	
	
	public function forgot_password_post(){
		//$uid  = $this->post('userid');
		$email  = $this->post('email');
		$pwd = $this->utility->generate_otp();
		if( $email != ''){
			//$uid_check = array('did'=>$uid);				
			$check_email = array('email'=>$email);			
			$email_rows = $this->services_model->check('dealers',$check_email)->num_rows();
			
				if($email_rows > 0){		
					//$uid_check = array('did'=>$uid);
					$udetails = $this->services_model->check('dealers',$check_email)->row();
					$password = base64_decode($udetails->password);	
					$update = array('password'=>base64_encode($pwd));
					$res=$this->services_model->update('dealers',$check_email,$update);						
					if($res){
						$uid = $udetails->did;
					$message = 'Your password is '.$pwd.'';
					$toemail = $email;
					$subject = "Forgot Password";
					$this->send_email($toemail,$message,$subject);					
					$this->response(['status'=>TRUE,'message'=>'Password sent to your email','uid'=>$uid],200);		
					}else{
					$this->response(['status'=>TRUE,'message'=>'please try later'],400);			
					}
				}else{
					$this->response(['status'=>FALSE,'message'=>'Email does not exist'],400);		
				}
			
		}else{
		$this->response(['status'=>FALSE,'message'=>'Email is required'],400);		
		}
	
	}
	
	 
	public function viewprofile_post(){
		$uid = $this->post(userid);
		if($uid != ''){
			$uid_check = array('did'=>$uid);
			$uid_rows = $this->services_model->check('dealers',$uid_check)->num_rows();		
			if($uid_rows > 0){	
			$user_details = $this->services_model->check('dealers',$uid_check)->row();
			$userdetails = 	array('userid'=>$user_details->did,'dealername'=>$user_details->dealer_name,'email'=>$user_details->email,'phonenumber'=>$user_details->mobile,'storename'=>$user_details->store_name,'flat_no'=>$user_details->flat_no,'colony'=>$user_details->colony,'city'=>$user_details->city,'state'=>$user_details->state,'pincode'=>$user_details->pincode,password=>base64_decode($user_details->password));
			$this->response(['status'=>TRUE,'message'=>'user profile view','user_details'=>$userdetails],200);			
			}else{
			$this->response(['status'=>FALSE,'message'=>'userid does not exist'],400);			
			}
		}else{
		$this->response(['status'=>FALSE,'message'=>'userid is required'],400);			
		}
	}


	public function get_location_details($lat,$lng){		
		$geolocation = $lat.','.$lng;
		$url = 'http://maps.googleapis.com/maps/api/geocode/json?language=en&latlng='.$geolocation.'&sensor=false';
		$file_contents = file_get_contents($url);
		$output = json_decode($file_contents);
		for($j=0;$j<count($output->results[0]->address_components);$j++){
			$cn=array($output->results[0]->address_components[$j]->types[0]);			   
			if(in_array("administrative_area_level_1", $cn))
			{
			$state = $output->results[0]->address_components[$j]->long_name;
			}
			
			if(in_array("administrative_area_level_2", $cn))
			{
			$city = $output->results[0]->address_components[$j]->long_name;
			}
			
			if(in_array("locality", $cn))
			{
			$place = $output->results[0]->address_components[$j]->long_name;
			}
			
			if(in_array("political", $cn))
			{
			$street = $output->results[0]->address_components[$j]->long_name;
			}
		}
		return $array = array(				
			'place' => $place,
			'street' => $street,	
			);
			// echo json_encode($array); 
	}



	
	
	public function edit_profile_post(){
		$username = $this->post('username');
		$storename = $this->post('storename');
		$mobile = $this->post('mobile');
		$email = $this->post('email');
		$user_id = $this->post('userid');
		$flat_no =  $this->post('flat_no');
		$colony =  $this->post('colony');
		$city =  $this->post('city');
		$state =  $this->post('state');
		$pincode =  $this->post('pincode');	
		$password = base64_encode($this->post('password'));
		
		if($username != '' && $storename != '' && $user_id != '' && $mobile != '' && $flat_no && $city != "" && $state != "" && $colony != "" && $pincode != "" && $email != "" && $password != ""){
			if(filter_var($email, FILTER_VALIDATE_EMAIL)){
			
			$uwhere  = array('did'=>$user_id);
			$rows  = $this->services_model->check('dealers',$uwhere);			
			
			if($rows->num_rows() > 0){				
				$data = array('dealer_name'=>$username,'mobile'=>$mobile,'store_name'=>$storename,'flat_no'=>$flat_no,'city'=>$city,'state'=>$state,'colony'=>$colony,'pincode'=>$pincode,'email'=>$email,'password'=>$password);
				$this->services_model->update('dealers',$uwhere,$data);
				$user_details = $this->services_model->check('dealers',$uwhere)->row();
				$userdetails = 	array('userid'=>$user_details->did,'dealername'=>$user_details->dealer_name,'email'=>$user_details->email,'phonenumber'=>$user_details->mobile,'storename'=>$user_details->store_name,'flat_no'=>$user_details->flat_no,'colony'=>$user_details->colony,'city'=>$user_details->city,'state'=>$user_details->state,'pincode'=>$user_details->pincode,password=>base64_decode($user_details->password));
				$this->response(['status'=>TRUE,'message'=>'Profile updated succesfully','user_details'=>$userdetails],200);								
			}else{
				$this->response(['status'=>FALSE,'message'=>'Invalid user id'],400);					
			}
			
			}else{
					$this->response(['status'=>FALSE,'message'=>'Please enter valid email id'],400);					
			}
		}else{
		$this->response(['status'=>FALSE,'message'=>'username,storename,mobile,email,userid,password and store_address fields are required'],400);						
		}
	}


	
	public function change_password_post(){		
		$user_id = $this->post('userid');
		$oldpassword = $this->post('oldpassword');
		$newpassword = $this->post('newpassword');
		
		if($user_id != '' && $oldpassword != '' && $newpassword != '' ){
			
			$uarray = array('did'=>$user_id); 
			$urows = $this->services_model->check('dealers',$uarray)->num_rows();
			if($urows > 0){
			
					$parray = array('did'=>$user_id,'password'=>base64_encode($oldpassword));
					$user_rows = $this->services_model->check('dealers',$parray)->num_rows();
					if($user_rows > 0){
						$update_array = array('password'=>base64_encode($newpassword));
						$res = $this->services_model->update('dealers',$uarray,$update_array);	
							$subject = "Change Password";
							$toemail = $this->services_model->check('dealers',$uarray)->row()->email;							
							$message = 'Your password has been changed succesfully,your new password is '.$newpassword.'';
						if($res){						
							//print_r($toemail);print_r($message);exit;
							$this->send_email($toemail,$message,$subject);
							$this->response(['status'=>TRUE,'message'=>'Password reset done succesfully'],200);									
						}else{
							$this->response(['status'=>FALSE,'message'=>'Please try later'],400);															
						}
					}else{
						$this->response(['status'=>FALSE,'message'=>'Old password does not match with database'],400);																					
					}
					
			}else{
			$this->response(['status'=>FALSE,'message'=>'Invalid user id'],400);									
			}
		}else{
			$this->response(['status'=>FALSE,'message'=>'Please enter userid ,oldpassword and newpassword'],200);									
		}
	}
	
	
	public function feedback_post(){
		$user_id = $this->post('userid');
		$username = $this->post('username');
		$email = $this->post('email');
		$phonenumber = $this->post('phonenumber');
		$querytype = $this->post('querytype');
		$message = $this->post('message');
		if($user_id != "" && $username != "" && $email != "" && $phonenumber != "" && $querytype != "" && $message != ""){
			$data = array(
			'uid' => $user_id,
			'username' => $username,
			'email' => $email,
			'phonenumber' => $phonenumber,
			'querytype' => $querytype,
			'message' => $message
			);
			$usercheck = array('did'=>$user_id);
			$userrows = $this->services_model->check('dealers',$usercheck)->num_rows();
			if($userrows > 0){
				$res = $this->services_model->save('feedback',$data);
				if($res){
				$this->response(['status'=>TRUE,'message'=>'your feedback sent successfully'],200);
				}else{
				$this->response(['status'=>FALSE,'message'=>'please try later'],400);
				}
			}else{
				$this->response(['status'=>FALSE,'message'=>'Userid does not exist'],400);
			}	
		}else{
			$this->response(['status'=>FALSE,'message'=>'All fields are required'],400);
		}
	}




	
	public function send_email($toemail,$message,$subject)
	{
		//print_r($toemail);print_r($message);exit;
		$CI =& get_instance();
	  
		$CI->email->initialize(array(
			'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_user' => 'noreply@nipponpaint-autorefinishes.com',
			'smtp_pass' => 'welcome@123',
			'smtp_port' => 465,
			'mailtype'  => 'html',
			'crlf'      => "\r\n",
			'newline'   => "\r\n",
			'charset'	=> "utf-8"
		));
		
	  $CI->email->from('appcare.testing@gmail.com',strtoupper('Nippon'));
	  $CI->email->to($toemail);
	  $CI->email->subject($subject);
	  $CI->email->message($message);
      
	  $result = $CI->email->send();
	  
	  if($result){
		return true; 
	  }else{
		// echo $CI->email->print_debugger();exit;
		return false; 
	  }	  
	}

	public function querytypes_get(){
		$query_types = $this->services_model->get('queries')->result_array();
		if(count($query_types) > 0){
			$this->response(['status'=>TRUE,'message'=>'success','querydetails'=>$query_types],200);
		}else{
			$this->response(['status'=>FALSE,'message'=>'No query types found'],400);
		}
	}
	
	public function brands_get(){
		$user_id	= $this->get('user_id');		
		$base_url = base_url();
		$notifications_count = $this->services_model->check('app_notifications',array('dealer_id'=>$user_id))->num_rows();
		$brands = $this->services_model->get('brands')->result_array();
		if(count($brands) > 0){
			$cart_count =  $this->services_model->check('cart',array('uid'=>$user_id,'added_by'=>'Dealer'))->num_rows();				
			for($i=0;$i<count($brands);$i++){
				$brands[$i]['brand_image'] = $base_url.'assets/uploads/brands/'.$brands[$i]['brand_image'];
				$brands[$i]['description'] = strip_tags($brands[$i]['description']);
			}
			 
			$this->response(['status'=>TRUE,'message'=>'success','notifications_count'=>$notifications_count,'cart_count'=>$cart_count,'brands'=>$brands],200);
		}else{
			$this->response(['status'=>FALSE,'message'=>'No brands found'],400);
		}
	}
	     
	public function colours_get(){		
		$base_url = base_url();
		$user_id	= $this->get('user_id');
		$ctid = $this->get('ctid');
		$cart_count =  $this->services_model->check('cart',array('uid'=>$user_id,'added_by'=>'Dealer'))->num_rows();			
		$data['colours'] = $this->services_model->check('colours',array('colour_type'=>$ctid))->result_array();		
		if(count($data['colours']) > 0){
		for($i=0;$i<count($data['colours']);$i++){
			$data['colours'][$i]['source'] = $this->services_model->check('source_countries',array('sid'=>$data['colours'][$i]['source']))->row()->source;
			$data['colours'][$i]['colour_type'] = $this->services_model->check('colour_types',array('ctid'=>$data['colours'][$i]['colour_type']))->row()->colour_type;
			$data['colours'][$i]['colour_brand'] = $this->services_model->check('brands',array('bid'=>$data['colours'][$i]['colour_brand']))->row()->brand_name;
			$data['colours'][$i]['colour_group'] = $this->services_model->check('colour_groups',array('cgid'=>$data['colours'][$i]['colour_group']))->row()->colour_group_name;
			$data['colours'][$i]['colour_image'] = $base_url.'assets/uploads/colours/'.$data['colours'][$i]['colour_image'];
			
			$data['colours'][$i]['description'] = strip_tags($data['colours'][$i]['description']);
			$data['colours'][$i]['features'] = strip_tags($data['colours'][$i]['features']);
			$data['colours'][$i]['benefits'] = strip_tags($data['colours'][$i]['benefits']);
			$data['colours'][$i]['item_type'] = 'Colours';
			$data['colours'][$i]['item_count'] = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$data['colours'][$i]['cid'],'item_type'=>'Colours'))->row()->count;
			/* $data['colours'][$i]['cart_count'] =  $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$data['colours'][$i]['cid'],'item_type'=>'Colours'))->num_rows(); */
		}
		$this->response(['status'=>TRUE,'message'=>'success','cart_count'=>$cart_count,'colours'=>$data['colours']],200);
		}else{
		$this->response(['status'=>FALSE,'message'=>'No colours available'],400);	
		}
	}
	
	public function colour_details_get(){
		$base_url = base_url();
		$colour_id = $this->get('colour_id');
		$user_id = $this->get('user_id');
		if($colour_id != "" ){
			$colour_rows = $this->services_model->check('colours',array('cid'=>$colour_id))->num_rows();
			if($colour_rows > 0){
				$cart_count =  $this->services_model->check('cart',array('uid'=>$user_id,'added_by'=>'Dealer'))->num_rows();				
				$item_count = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$colour_id,'item_type'=>'Colours'))->row()->count;	
				
				$colour_details = $this->services_model->check('colours',array('cid'=>$colour_id))->row();
				$colour_details->source = $this->services_model->check('source_countries',array('sid'=>$colour_details->source))->row()->source;
				$colour_details->colour_type = $this->services_model->check('colour_types',array('ctid'=>$colour_details->colour_type))->row()->colour_type;
				$colour_details->colour_brand = $this->services_model->check('brands',array('bid'=>$colour_details->colour_brand))->row()->brand_name;
				$colour_details->colour_group = $this->services_model->check('colour_groups',array('cgid'=>$colour_details->colour_group))->row()->colour_group_name;
				$colour_details->features = strip_tags($colour_details->features);
				$colour_details->benefits = strip_tags($colour_details->benefits);
				$colour_details->description = strip_tags($colour_details->description);
				$colour_details->colour_image = $base_url.'assets/uploads/colours/'.$colour_details->colour_image;
				
				
				
			$this->response(['status'=>TRUE,'message'=>'success','cart_count'=>$cart_count,'item_count'=>$item_count,'colour_details'=>$colour_details],200);
			}else{
			$this->response(['status'=>FALSE,'message'=>'Invalid Colour ID'],400);	
			}
		}else{
			$this->response(['status'=>FALSE,'message'=>'Please enter colour_id'],400);	
		}
	}


	
	public function products_get(){	
		$base_url = base_url();	
		$user_id	= $this->get('user_id');
		$pcid = $this->get('pcid');
			$cart_count =  $this->services_model->check('cart',array('uid'=>$user_id,'added_by'=>'Dealer'))->num_rows();
			$data['products'] = $this->services_model->check('products',array('product_category'=>$pcid))->result_array();			
			if(count($data['products']) > 0){
			for($i=0;$i<count($data['products']);$i++){
				$data['products'][$i]['brand'] = $this->services_model->check('brands',array('bid'=>$data['products'][$i]['brand']))->row()->brand_name;
				$data['products'][$i]['product_image'] = $base_url.'assets/uploads/products/'.$data['products'][$i]['product_image'];
				$data['products'][$i]['tds'] = $base_url.'assets/uploads/products/'.$data['products'][$i]['tds'];
				$data['products'][$i]['sds'] = $base_url.'assets/uploads/products/'.$data['products'][$i]['sds'];
				$data['products'][$i]['description'] = strip_tags($data['products'][$i]['description']);
				$data['products'][$i]['features'] = strip_tags($data['products'][$i]['features']);
				$data['products'][$i]['benefits'] = strip_tags($data['products'][$i]['benefits']);
				$data['products'][$i]['item_type'] = 'Products';
				$data['products'][$i]['item_count'] = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$data['products'][$i]['pid'],'item_type'=>'Products'))->row()->count;
				/* $data['products'][$i]['cart_count'] =  $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$data['products'][$i]['pid'],'item_type'=>'Products'))->num_rows(); */
			}
			$this->response(['status'=>TRUE,'message'=>'success','cart_count'=>$cart_count,'products'=>$data['products']],200);
			}else{
				$this->response(['status'=>FALSE,'message'=>'No products available'],400);
			}
	}
	
	
	public function product_details_get(){
		$base_url = base_url();
		$product_id	= $this->get('product_id');
		$user_id	= $this->get('user_id');
		if($product_id != ""){
			$where	= array('pid'=>$product_id);
			$pdetails = $this->services_model->check('products',array('pid'=>$product_id))->row();
			$used = $pdetails->online_consumption+$pdetails->offline_consumption;
			$availability = $pdetails->quantity-$used;
			
			$product_rows = $this->services_model->check('products',$where)->num_rows();
			if($product_rows > 0){
				$item_count = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$product_id,'item_type'=>'Products'))->row()->count;
				$cart_count = $this->services_model->check('cart',array('uid'=>$user_id,'added_by'=>'Dealer'))->num_rows();
				$data['products'] = $this->services_model->check('products',$where)->row_array();
				$data['products']['brand'] = $this->services_model->check('brands',array('bid'=>$data['products']['brand']))->row()->brand_name;
				$data['products']['product_image'] = $base_url.'assets/uploads/products/'.$data['products']['product_image'];
				$data['products']['tds'] = $base_url.'assets/uploads/products/'.$data['products']['tds'];
				$data['products']['sds'] = $base_url.'assets/uploads/products/'.$data['products']['sds'];
				$data['products']['features'] = strip_tags($data['products']['features']);
				$data['products']['benefits'] = strip_tags($data['products']['benefits']);
				$data['products']['description'] = strip_tags($data['products']['description']);
				$data['products']['available_quantity'] = $availability;
				
				// product Suggestions
				$total_product_suggestions = $this->services_model->get_product_suggestions()->result_array();
				$product_suggestions = [];
				for($i=0;$i<count($total_product_suggestions);$i++){
					if(strpos($total_product_suggestions[$i]['suggested_for'],$product_id) !== false){
						$total_product_suggestions[$i]['item_count'] = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$total_product_suggestions[$i]['pid'],'item_type'=>'Products'))->row()->count;
						
					
					$total_product_suggestions[$i]['brand'] = $this->services_model->check('brands',array('bid'=>$total_product_suggestions[$i]['brand']))->row()->brand_name;
					$total_product_suggestions[$i]['product_image'] = $base_url.'assets/uploads/products/'.$total_product_suggestions[$i]['product_image'];
					$total_product_suggestions[$i]['tds'] = $base_url.'assets/uploads/products/'.$total_product_suggestions[$i]['tds'];
					$total_product_suggestions[$i]['sds'] = $base_url.'assets/uploads/products/'.$total_product_suggestions[$i]['sds'];
					$total_product_suggestions[$i]['features'] = strip_tags($total_product_suggestions[$i]['features']);
					$total_product_suggestions[$i]['benefits'] = strip_tags($total_product_suggestions[$i]['benefits']);
					$total_product_suggestions[$i]['description'] = strip_tags($total_product_suggestions[$i]['description']);
					$total_product_suggestions[$i]['available_quantity'] = $availability;
					array_push($product_suggestions,$total_product_suggestions[$i]);
					}
				}
				//echo '<pre>';print_r($product_suggestions);exit;
				$this->response(['status'=>TRUE,'message'=>'success','cart_count'=>$cart_count,'item_count'=>$item_count,'product_details'=>$data['products'],'suggestions'=>$product_suggestions],200);
			}else{
				$this->response(['status'=>FALSE,'message'=>'Invalid Product ID'],400);
			}
		}else{
			$this->response(['status'=>FALSE,'message'=>'Please enter Product ID'],400);
		}
	}
	
	
	// 03-07-2018
	public function colours_products_old_get(){
		$base_url = base_url();	
		$brand_id = $this->get('brand_id');
		$user_id = $this->get('user_id');
		
		if($brand_id != ""){
			$cart_count =  $this->services_model->check('cart',array('uid'=>$user_id,'added_by'=>'Dealer'))->num_rows();
			$bcheck = $this->services_model->check('brands',array('bid'=>$brand_id))->num_rows();
			if($bcheck > 0){
		$data['colours'] = $this->services_model->check('colours',array('colour_brand'=>$brand_id))->result_array();
		if(count($data['colours'] > 0)){
		for($i=0;$i<count($data['colours']);$i++){
			$data['colours'][$i]['source'] = $this->services_model->check('source_countries',array('sid'=>$data['colours'][$i]['source']))->row()->source;
			$data['colours'][$i]['colour_type'] = $this->services_model->check('colour_types',array('ctid'=>$data['colours'][$i]['colour_type']))->row()->colour_type;
			$data['colours'][$i]['colour_brand'] = $this->services_model->check('brands',array('bid'=>$data['colours'][$i]['colour_brand']))->row()->brand_name;
			$data['colours'][$i]['colour_group'] = $this->services_model->check('colour_groups',array('cgid'=>$data['colours'][$i]['colour_group']))->row()->colour_group_name;
			$data['colours'][$i]['colour_image'] = $base_url.'assets/uploads/colours/'.$data['colours'][$i]['colour_image'];
			
			$data['colours'][$i]['features'] = strip_tags($data['colours'][$i]['features']);
			$data['colours'][$i]['benefits'] = strip_tags($data['colours'][$i]['benefits']);
			$data['colours'][$i]['description'] = strip_tags($data['colours'][$i]['description']);
		}
		$data['colours'] = $data['colours'];
		}else{
			$data['colours'] = '0';
		}
		
		$data['products'] = $this->services_model->check('products',array('brand'=>$brand_id))->result_array();	
	if(count($data['products'] > 0)){		
		for($i=0;$i<count($data['products']);$i++){
			$data['products'][$i]['brand'] = $this->services_model->check('brands',array('bid'=>$data['products'][$i]['brand']))->row()->brand_name;
			$data['products'][$i]['product_image'] = $base_url.'assets/uploads/products/'.$data['products'][$i]['product_image'];
			
			$data['products'][$i]['features'] = strip_tags($data['products'][$i]['features']);
			$data['products'][$i]['benefits'] = strip_tags($data['products'][$i]['benefits']);
			$data['products'][$i]['description'] = strip_tags($data['products'][$i]['description']);
		}		
		$data['products'] = $data['products'];
	}else{
		$data['products'] = '0';
	}
		$this->response(['status'=>TRUE,'message'=>'success','cart_count'=>$cart_count,'colours'=>$data['colours'],'products'=>$data['products']],200);
		}else{
			$this->response(['status'=>FALSE,'message'=>'Invalid Brandid'],400);
		}
		}else{
			$this->response(['status'=>FALSE,'message'=>'Please enter brand id'],400);
		}
	}
	
	
	public function colours_products_get(){
		$base_url = base_url();	
		$user_id = $this->get('user_id');
		$cart_count =  $this->services_model->check('cart',array('uid'=>$user_id,'added_by'=>'Dealer'))->num_rows();
		$data['colour_types'] = $this->services_model->get('colour_types')->result_array();
		for($i=0;$i<count($data['colour_types']);$i++){
			$data['colour_types'][$i]['ct_image'] = base_url().'assets/uploads/colours/'.$data['colour_types'][$i]['ct_image'];
		}
		$data['product_categories'] = $this->services_model->get('product_categories')->result_array();
		for($i=0;$i<count($data['product_categories']);$i++){
			$data['product_categories'][$i]['pc_img'] = base_url().'assets/uploads/products/'.$data['product_categories'][$i]['pc_img'];
		}
		$this->response(['status'=>TRUE,'message'=>'success','cart_count'=>$cart_count,'colours'=>$data['colour_types'],'products'=>$data['product_categories']],200);
	}
	
	
	public function search_get(){		
		$keyword = $this->get('keyword');
		$user_id = $this->get('user_id');
		if($keyword != ""){
			$notifications_count = $this->services_model->check('app_notifications',array('dealer_id'=>$user_id))->num_rows();
		$base_url = base_url();
		$brands = $this->services_model->search_brand($keyword);
		$colours = $this->services_model->search_colours($keyword);	
		$colour_groups = $this->services_model->search_colour_groups($keyword);	
		$colour_types = $this->services_model->search_colour_types($keyword);	
		$data['products'] = $this->services_model->search_products($keyword);	
			$items = [];			
			for($i=0;$i<count($data['products']);$i++){
				$used[$i] = $data['products'][$i]['online_consumption']+$data['products'][$i]['offline_consumption'];
				$availability[$i] = $data['products'][$i]['quantity']-$used[$i];
			$data['products'][$i]['item_type'] = 'Products';
			$data['products'][$i]['brand'] = $this->services_model->check('brands',array('bid'=>$data['products'][$i]['brand']))->row()->brand_name;
			$data['products'][$i]['product_image'] = $base_url.'assets/uploads/products/'.$data['products'][$i]['product_image'];
			$data['products'][$i]['tds'] = $base_url.'assets/uploads/products/'.$data['products'][$i]['tds'];
			$data['products'][$i]['sds'] = $base_url.'assets/uploads/products/'.$data['products'][$i]['sds'];
			$data['products'][$i]['features'] = strip_tags($data['products'][$i]['features']);
			$data['products'][$i]['benefits'] = strip_tags($data['products'][$i]['benefits']);
			$data['products'][$i]['description'] = strip_tags($data['products'][$i]['description']);
			$data['products'][$i]['item_count'] = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$data['products'][$i]['pid'],'item_type'=>'Products'))->row()->count;
			$data['products'][$i]['availability'] = $availability[$i];
			array_push($items,$data['products'][$i]);
			}
			
			  
			for($i=0;$i<count($brands);$i++){
				$products = $this->services_model->check('products',array('brand'=>$brands[$i]['bid']))->result_array();				
				if(count($products)>0){
					for($j=0;$j<count($products);$j++){
						$used[$j] = $data['products'][$j]['online_consumption']+$data['products'][$j]['offline_consumption'];
						$availability[$j] = $data['products'][$j]['quantity']-$used[$j];
						$products[$j]['item_type'] = 'Products';
						$products[$j]['brand'] = $this->services_model->check('brands',array('bid'=>$products[$j]['brand']))->row()->brand_name;
						$products[$j]['product_image'] = $base_url.'assets/uploads/products/'.$products[$j]['product_image'];
						$products[$j]['tds'] = $base_url.'assets/uploads/products/'.$products[$j]['tds'];
						$products[$j]['sds'] = $base_url.'assets/uploads/products/'.$products[$j]['sds'];
						$products[$j]['features'] = strip_tags($data['products'][$j]['features']);
						$products[$j]['benefits'] = strip_tags($data['products'][$j]['benefits']);
						$products[$j]['description'] = strip_tags($data['products'][$j]['description']);
						$products[$j]['item_count'] = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$products[$j]['pid'],'item_type'=>'Products'))->row()->count;
						$data['products'][$j]['availability'] = $availability[$j];
						if(in_array($products[$j], $items)){
						array_push($items,$products[$j]);
						}
					}	
				}
				$colours = $this->services_model->check('colours',array('colour_brand'=>$brands[$i]['bid']))->result_array();
				if(count($colours)>0){
					for($j=0;$j<count($colours);$j++){
						$colours[$j]['item_type'] = 'Colours';
					$colours[$j]['source'] = $this->services_model->check('source_countries',array('sid'=>$colours[$j]['source']))->row()->source;
					$colours[$j]['colour_type'] = $this->services_model->check('colour_types',array('ctid'=>$colours[$j]['colour_type']))->row()->colour_type;
					$colours[$j]['colour_brand'] = $this->services_model->check('brands',array('bid'=>$colours[$j]['colour_brand']))->row()->brand_name;
					$colours[$j]['colour_group'] = $this->services_model->check('colour_groups',array('cgid'=>$colours[$j]['colour_group']))->row()->colour_group_name;
					$colours[$j]['features'] = strip_tags($colours[$j]['features']);
					$colours[$j]['benefits'] = strip_tags($colours[$j]['benefits']);
					$colours[$j]['colour_image'] = $base_url.'assets/uploads/colours/'.$colours[$j]['colour_image'];
				
					$colours[$j]['item_count'] = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$colours[$j]['cid'],'item_type'=>'Colours'))->row()->count;
					$colours[$j]['availability'] = '0'; 
					if(!in_array($colours[$j], $items)){
					array_push($items,$colours[$j]);
					}
					}
				}
			}
			
			
			for($i=0;$i<count($colours);$i++){
				$colours = $this->services_model->check('colours',array('cid'=>$colours[$i]['cid']))->result_array();
				//print_r($colours);exit;
				if(count($colours)>0){
					for($j=0;$j<count($colours);$j++){
						$colours[$j]['item_type'] = 'Colours';
					$colours[$j]['source'] = $this->services_model->check('source_countries',array('sid'=>$colours[$j]['source']))->row()->source;
					$colours[$j]['colour_type'] = $this->services_model->check('colour_types',array('ctid'=>$colours[$j]['colour_type']))->row()->colour_type;
					$colours[$j]['colour_brand'] = $this->services_model->check('brands',array('bid'=>$colours[$j]['colour_brand']))->row()->brand_name;
					$colours[$j]['colour_group'] = $this->services_model->check('colour_groups',array('cgid'=>$colours[$j]['colour_group']))->row()->colour_group_name;
					$colours[$j]['features'] = strip_tags($colours[$j]['features']);
					$colours[$j]['benefits'] = strip_tags($colours[$j]['benefits']);
					$colours[$j]['colour_image'] = $base_url.'assets/uploads/colours/'.$colours[$j]['colour_image'];
			
					$colours[$j]['item_count'] = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$colours[$j]['cid'],'item_type'=>'Colours'))->row()->count;
					$colours[$j]['availability']  = '0';
					if(!in_array($colours[$j], $items)){
					array_push($items,$colours[$j]);
					}
					}
				}
			}
			
			for($i=0;$i<count($colour_groups);$i++){
				$colour_groups = $this->services_model->check('colours',array('colour_group'=>$colour_groups[$i]['cgid']))->result_array();
				if(count($colour_groups)>0){
					for($j=0;$j<count($colour_groups);$j++){
						$colour_groups[$j]['item_type'] = 'Colours';
					$colour_groups[$j]['source'] = $this->services_model->check('source_countries',array('sid'=>$colour_groups[$j]['source']))->row()->source;
					$colour_groups[$j]['colour_type'] = $this->services_model->check('colour_types',array('ctid'=>$colour_groups[$j]['colour_type']))->row()->colour_type;
					$colour_groups[$j]['colour_brand'] = $this->services_model->check('brands',array('bid'=>$colour_groups[$j]['colour_brand']))->row()->brand_name;
					$colour_groups[$j]['colour_group'] = $this->services_model->check('colour_groups',array('cgid'=>$colour_groups[$j]['colour_group']))->row()->colour_group_name;
					$colour_groups[$j]['features'] = strip_tags($colour_groups[$j]['features']);
					$colour_groups[$j]['benefits'] = strip_tags($colour_groups[$j]['benefits']);
					$colour_groups[$j]['colour_image'] = $base_url.'assets/uploads/colours/'.$colour_groups[$j]['colour_image'];
					
					$colour_groups[$j]['availability'] = '0';
					if(!in_array($colour_groups[$j], $items)){
					array_push($items,$colour_groups[$j]);
					}
					}
				}
			}
			for($i=0;$i<count($colour_types);$i++){
				$colour_types = $this->services_model->check('colours',array('colour_type'=>$colour_types[$i]['ctid']))->result_array();
				if(count($colour_types)>0){
					for($j=0;$j<count($colour_groups);$j++){
						$colour_types[$j]['item_type'] = 'Colours';
					$colour_types[$j]['source'] = $this->services_model->check('source_countries',array('sid'=>$colour_types[$j]['source']))->row()->source;
					$colour_types[$j]['colour_type'] = $this->services_model->check('colour_types',array('ctid'=>$colour_types[$j]['colour_type']))->row()->colour_type;
					$colour_types[$j]['colour_brand'] = $this->services_model->check('brands',array('bid'=>$colour_types[$j]['colour_brand']))->row()->brand_name;
					$colour_types[$j]['colour_group'] = $this->services_model->check('colour_groups',array('cgid'=>$colour_types[$j]['colour_group']))->row()->colour_group_name;
					$colour_types[$j]['features'] = strip_tags($colour_types[$j]['features']);
					$colour_types[$j]['benefits'] = strip_tags($colour_types[$j]['benefits']);
					$colour_types[$j]['colour_image'] = $base_url.'assets/uploads/colours/'.$colour_types[$j]['colour_image'];
					
						$colour_types[$j]['availability']  = '0';
					if(!in_array($colour_types[$j], $items)){
					array_push($items,$colour_types[$j]);
					}
					}
				}
			}
			for($k=0;$k<count($items);$k++){
				$items[$k]['item_id'] = $k+1;
			}
			$cart_count =  $this->services_model->check('cart',array('uid'=>$user_id,'added_by'=>'Dealer'))->num_rows();
			if(count($items)>0){
			$this->response(['status'=>TRUE,'message'=>'success','notifications_count'=>$notifications_count,'cart_count'=>$cart_count,'items'=>$items],200);
			}else{
			$this->response(['status'=>FALSE,'message'=>'No items available'],400);	
			}
		}else{
			$base_url = base_url();
			$notifications_count = $this->services_model->check('app_notifications',array('dealer_id'=>$user_id))->num_rows();
			$items = [];
			$data['products'] = $this->services_model->get_products('products')->result_array();	
			for($i=0;$i<count($data['products']);$i++){
				$used[$i] = $data['products'][$i]['online_consumption']+$data['products'][$i]['offline_consumption'];
				$availability[$i] = $data['products'][$i]['quantity']-$used[$i];
				$data['products'][$i]['item_type'] = 'Products';
			$data['products'][$i]['brand'] = $this->services_model->check('brands',array('bid'=>$data['products'][$i]['brand']))->row()->brand_name;
			$data['products'][$i]['product_image'] = $base_url.'assets/uploads/products/'.$data['products'][$i]['product_image'];
			$data['products'][$i]['tds'] = $base_url.'assets/uploads/products/'.$data['products'][$i]['tds'];
			$data['products'][$i]['sds'] = $base_url.'assets/uploads/products/'.$data['products'][$i]['sds'];
			$data['products'][$i]['features'] = strip_tags($data['products'][$i]['features']);
			$data['products'][$i]['benefits'] = strip_tags($data['products'][$i]['benefits']);
			$data['products'][$i]['description'] = strip_tags($data['products'][$i]['description']);
			$data['products'][$i]['item_count'] = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$data['products'][$i]['pid'],'item_type'=>'Products'))->row()->count;
			$data['products'][$i]['availability'] = $availability[$i];
			array_push($items,$data['products'][$i]);
			}
			
			$data['colours'] = $this->services_model->get('colours')->result_array();
			for($i=0;$i<count($data['colours']);$i++){
				$data['colours'][$i]['item_type'] = 'Colours';
			$data['colours'][$i]['source'] = $this->services_model->check('source_countries',array('sid'=>$data['colours'][$i]['source']))->row()->source;
			$data['colours'][$i]['colour_type'] = $this->services_model->check('colour_types',array('ctid'=>$data['colours'][$i]['colour_type']))->row()->colour_type;
			$data['colours'][$i]['colour_brand'] = $this->services_model->check('brands',array('bid'=>$data['colours'][$i]['colour_brand']))->row()->brand_name;
			$data['colours'][$i]['colour_group'] = $this->services_model->check('colour_groups',array('cgid'=>$data['colours'][$i]['colour_group']))->row()->colour_group_name;
			$data['colours'][$i]['colour_image'] = $base_url.'assets/uploads/colours/'.$data['colours'][$i]['colour_image'];
			
			$data['colours'][$i]['features'] = strip_tags($data['colours'][$i]['features']);
			$data['colours'][$i]['benefits'] = strip_tags($data['colours'][$i]['benefits']);
			$data['colours'][$i]['item_count'] = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$data['colours'][$i]['cid'],'item_type'=>'Colours'))->row()->count;
			$data['colours'][$i]['availability'] = '0';
			array_push($items,$data['colours'][$i]);
			}
			  
			for($k=0;$k<count($items);$k++){
				$items[$k]['item_id'] = $k+1;
			}
			$cart_count =  $this->services_model->check('cart',array('uid'=>$user_id,'added_by'=>'Dealer'))->num_rows();
			if(count($items)>0){
			$this->response(['status'=>TRUE,'message'=>'success','notifications_count'=>$notifications_count,'cart_count'=>$cart_count,'items'=>$items],200);
			}else{
			$this->response(['status'=>FALSE,'message'=>'No items available'],400);	
			}
			
		}	
			
	}


	public function check_pincode_get(){
		$pincode = $this->get('pincode');
		if($pincode  != ""){
			$pinrows = $this->services_model->check('pincodes',array('pincode'=>$pincode))->num_rows();			
			if($pinrows > 0){
				$this->response(['status'=>TRUE,'message'=>'Delivery available in this location'],200);
			}else{
				$this->response(['status'=>FALSE,'message'=>'Delivery not available in this location'],400);
			}
		}else{
			$this->response(['status'=>FALSE,'message'=>'Please enter pincode'],400);	
		}
	}
	
	
	public function add_address_post(){
		$user_id = $this->post('user_id');
		$billing_flatno = $this->post('billing_flatno');
		$billing_colony = $this->post('billing_colony');
		$billing_city = $this->post('billing_city');
		$billing_state = $this->post('billing_state');
		$billing_pincode = $this->post('billing_pincode');
		$shipping_flatno = $this->post('shipping_flatno');
		$shipping_colony = $this->post('shipping_colony');
		$shipping_city = $this->post('shipping_city');
		$shipping_state = $this->post('shipping_state');
		$shipping_pincode = $this->post('shipping_pincode');
		if($user_id != "" && $billing_flatno != "" && $billing_colony != "" && $billing_state != "" && $billing_city != "" && $billing_pincode != "" && 
		$shipping_flatno != "" && $shipping_colony != "" && $shipping_city != "" && $shipping_state != "" && $shipping_pincode != ""){
			$user_rows = $this->services_model->check('dealers',array('did'=>$user_id))->num_rows();
			if($user_rows > 0){
				$uwhere = array('did'=>$user_id);
				$barray = array('flat_no'=>$billing_flatno,'city'=>$billing_city,'state'=>$billing_state,'colony'=>$billing_colony,'pincode'=>$billing_pincode);
				$this->services_model->update('dealers',$uwhere,$barray);
				$check_dealers_sadd = $this->services_model->check('dealers_shipping_address',array('dealer_id'=>$user_id))->num_rows();
				$sarray = array('dealer_id'=>$user_id,'shipping_flatno'=>$shipping_flatno,'shipping_colony'=>$shipping_colony,'shipping_city'=>$shipping_city,'shipping_state'=>$shipping_state,'shipping_pincode'=>$shipping_pincode);
				if($check_dealers_sadd == 0){				
				$res = $this->services_model->save('dealers_shipping_address',$sarray);
				}else{
				$res = $this->services_model->update('dealers_shipping_address',array('dealer_id'=>$user_id),$sarray);	
				}
				if($res){
					$address_response = array_merge($barray,$sarray);
					$this->response(['status'=>TRUE,'message'=>'Address added succesfully','response'=>$address_response],200);
				}else{
					$this->response(['status'=>FALSE,'message'=>'Error saving address please try later'],400);
				}
			}else{
				$this->response(['status'=>FALSE,'message'=>'Invalid UserID'],400);
			}	
		}else{
			$this->response(['status'=>FALSE,'message'=>'All fields are required'],400);
		}
	}
	    
	
	public function place_order_post(){
		$user_id = $this->post('user_id');
		$items = $this->post('items');		
		$total_cost = $this->post('total_cost');		
		$delivery_type = $this->post('delivery_type');
		$cgst_amount = $this->post('cgst_amount');
		$sgst_amount = $this->post('sgst_amount');
		$total_gst_amount = $this->post('total_gst_amount');
		$total_with_gst = $this->post('total_with_gst');
		if($user_id != "" && $items != "" && $total_cost != "" && $delivery_type != "" && $cgst_amount != "" && $sgst_amount != "" && $total_gst_amount !="" && $total_with_gst != ""){
			$user_check_rows = $this->services_model->check('dealers',array('did'=>$user_id))->num_rows();
			if($user_check_rows > 0){
				$order_id = $this->utility->getenerate_uid();
				$order_id_check = $this->services_model->check('orders',array('order_id'=>$order_id))->num_rows();
				if($order_id_check > 0){
					$order_id = $this->utility->getenerate_uid();
				}else{
					$order_id = $order_id;
				}
				$idet = str_replace('},', '}&', $items);
				$item_split_details = explode("&",$idet);
				for($i=0;$i<count($item_split_details);$i++){
					$item_details[$i] = json_decode($item_split_details[$i],true);
					if($item_details[$i]['item_type'] == 'Products'){
						$pid = $item_details[$i]['item_id'];					
						$count = $item_details[$i]['count'];
						$fpd = $this->services_model->check('products',array('pid'=>$pid))->row()->online_consumption;
						$online_consumption = $fpd+$count;
						$update = $this->services_model->update('products',array('pid'=>$pid),array('online_consumption'=>$online_consumption));
					}
				}			
				
				$this->db->delete('cart',array('uid'=>$user_id,'added_by'=>'Dealer'));
				
				$data = array('order_id'=>$order_id,'uid'=>$user_id,'items'=>$items,'total_cost'=>$total_cost,'cgst_amount'=>$cgst_amount,'sgst_amount'=>$sgst_amount,'total_gst_amount'=>$total_gst_amount,'total_with_gst'=>$total_with_gst,'delivery_type'=>$delivery_type,'order_from'=>'dealers','added_on'=>date('Y-m-d H:i:s'));
				$res = $this->services_model->save('orders',$data);				
				if($res){
					$order_amount = $total_cost;
					$this->response(['status'=>TRUE,'message'=>'Order placed succesfully','order_amount'=>$order_amount,'order_id'=>$order_id],200);
				}else{
					$this->response(['status'=>FALSE,'message'=>'Please try again'],400);
				}
			}else{
				$this->response(['status'=>FALSE,'message'=>'Invalid user id'],400);
			}	
		}else{
			$this->response(['status'=>FALSE,'message'=>'All fields are required'],400);
		}
	}
	
	public function delivery_types_get(){
		$delivery_types = $this->services_model->get('delivery_types')->result_array();
		$this->response(['status'=>TRUE,'message'=>'success','delivery_types'=>$delivery_types],200);
	}
	
	
	public function remove_cart_post(){
		$cid = $this->post('cart_id');
		$uid = $this->post('user_id');
		if($cid != "" && $uid != ""){
			$cart_check = $this->services_model->check('cart',array('cid'=>$cid))->num_rows();
			
			if($cart_check > 0){
				$res = $this->db->delete('cart',array('cid'=>$cid));
				if($res){
					$cart_count = $this->services_model->check('cart',array('uid'=>$uid,'added_by'=>'Dealer'))->num_rows();
				$this->response(['status'=>TRUE,'message'=>'Items removed from cart succesfully','cart_count'=>$cart_count],200);
				}else{
					$this->response(['status'=>FALSE,'message'=>'Please try later'],400);
				}
			}else{
				$this->response(['status'=>FALSE,'message'=>'invalid Cart id'],400);
			}
		}else{
			$this->response(['status'=>FALSE,'message'=>'All fields are required'],400);
		}
	}
	
	
	public function cart_list_get(){
		$uid = $this->get('user_id');
		$bu = base_url();
		if($uid != ""){
			$items = $this->services_model->check('cart',array('uid'=>$uid,'added_by'=>'Dealer'))->result_array();
			if(count($items)>0){
				$uwhere = array('uid'=>$uid,'added_by'=>'Dealer');
				$cart_count = $this->services_model->check('cart',$uwhere)->num_rows();
				
			for($i=0;$i<count($items);$i++){
				$total_items_cost += $items[$i]['total_cost'];
				if($items[$i]['item_type'] == 'Products'){
					$items[$i]['item_name'] = $this->services_model->check('products',array('pid'=>$items[$i]['item_id']))->row()->product_name;
					$item_image = $this->services_model->check('products',array('pid'=>$items[$i]['item_id']))->row()->product_image;
					$items[$i]['item_image'] = $bu.'assets/uploads/products/'.$item_image;
				}else{
					$items[$i]['item_name'] = $this->services_model->check('colours',array('cid'=>$items[$i]['item_id']))->row()->colour_name;
					$item_image = $this->services_model->check('colours',array('cid'=>$items[$i]['item_id']))->row()->colour_image;
					$items[$i]['item_image'] = $bu.'assets/uploads/colours/'.$item_image;	
				}
				
			}
			//print_r($product_offers);
			
		$gst_details = $this->services_model->check('gst',array('id'=>1))->row();
		$cgst = $gst_details->cgst;
		$sgst = $gst_details->sgst;
		$cgst_added_amount = $total_items_cost*($cgst/100);
		$sgst_added_amount = $total_items_cost*($sgst/100);
		$gst_amount = $cgst_added_amount+$sgst_added_amount;
		$total_with_gst = $total_items_cost+$gst_amount;
		$gst_details = array('cgst_perc'=>$cgst,'sgst_perc'=>$sgst,'cgst_amount'=>$cgst_added_amount,'sgst_amount'=>$sgst_added_amount,'total_gst_amount'=>$gst_amount);
			$this->response(['status'=>TRUE,'message'=>'success','cart_count'=>$cart_count,'gst_details'=>$gst_details,'total_with_gst'=>$total_with_gst,'total_items_cost'=>$total_items_cost,'items'=>$items],200);
			}else{
			$this->response(['status'=>FALSE,'message'=>'Your cart is empty'],400);	
			}
		}else{
			$this->response(['status'=>FALSE,'message'=>'UserID is required'],400);
		}
	}
	
	
	public function check_coupon_get(){
		$offer_id = $this->get('oid');
		if($offer_id != ""){
			$offer_rows = $this->services_model->check('offers',array('oid'=>$offer_id))->num_rows();
			if($offer_rows > 0){
				$offer_details = $this->services_model->check('offers',array('oid'=>$offer_id))->row_array();
				$ovalidity = date('Y-m-d',strtotime($offer_details['offer_validity']));
				if($ovalidity >= date('Y-m-d')){
					$this->response(['status'=>TRUE,'message'=>'success'],200);
				}else{
					$this->response(['status'=>FALSE,'message'=>'offer is expired'],400);
				}
			}else{
				$this->response(['status'=>FALSE,'message'=>'Invalid offer id'],400);
			}	
		}else{
			$this->response(['status'=>FALSE,'message'=>'Please enter offer id'],400);
		}
	}
	                    
	public function addtoCart_post(){
		$user_id = $this->post('user_id');
		$item_id = $this->post('item_id');
		$item_type = $this->post('item_type');
		$item_cost = $this->post('item_cost');
		$offer_id = $this->post('oid');
		$count = $this->post('count');
		$total_cost = $count *($item_cost); 
		$uwhere = array('uid'=>$user_id,'added_by'=>'Dealer');
		if($user_id != "" && $item_id != "" && $item_type != "" && $item_cost != "" && $count != ""){
			
			$user_check_rows = $this->services_model->check('dealers',array('did'=>$user_id))->num_rows();
			if($user_check_rows > 0){
				if($item_type == 'Colours'){
					$min_order_count = $this->services_model->check('colours',array('cid'=>$item_id))->row()->moq;
				}else{
					$min_order_count = $this->services_model->check('products',array('pid'=>$item_id))->row()->moq;
				}
				if($count >= $min_order_count){
				
				$item_check = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$item_id,'item_type'=>$item_type,'added_by'=>'Dealer'))->num_rows();
				
				if($item_check == 0){
				$data = array('uid'=>$user_id,'item_id'=>$item_id,'item_type'=>$item_type,'item_cost'=>$item_cost,'total_cost'=>$total_cost,'count'=>$count,'added_by'=>'Dealer','added_on'=>date('Y-m-d H:i:s'));
				$res = $this->services_model->save('cart',$data);
				}else{
					$user_cart_where = array('uid'=>$user_id,'item_id'=>$item_id,'item_type'=>$item_type,'added_by'=>'Dealer');
					$user_cart_count = $this->services_model->check('cart',$user_cart_where)->row()->count;					
					$UpdateData = array('count'=>$count,'total_cost'=>$count*$item_cost);
					$res = $this->services_model->update('cart',$user_cart_where,$UpdateData);					
				}
				$cart_count = $this->services_model->check('cart',$uwhere)->num_rows();
				$item_count = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$item_id,'item_type'=>$item_type))->row()->count;
				if($res){
					$items = $this->services_model->check('cart',array('uid'=>$user_id))->result_array();	
					for($i=0;$i<count($items);$i++){		
						$total_items_cost += $items[$i]['total_cost'];
						if($items[$i]['item_type'] == 'Products'){
							$items[$i]['item_name'] = $this->services_model->check('products',array('pid'=>$items[$i]['item_id']))->row()->product_name;
							$item_image = $this->services_model->check('products',array('pid'=>$items[$i]['item_id']))->row()->product_image;
							$items[$i]['item_image'] = $bu.'assets/uploads/products/'.$item_image;
							}else{
							$items[$i]['item_name'] = $this->services_model->check('colours',array('cid'=>$items[$i]['item_id']))->row()->colour_name;
							$item_image = $this->services_model->check('colours',array('cid'=>$items[$i]['item_id']))->row()->colour_image;
							$items[$i]['item_image'] = $bu.'assets/uploads/colours/'.$item_image;	
						}
						
					}
					$gst_details = $this->services_model->check('gst',array('id'=>1))->row();
					$cgst = $gst_details->cgst;
					$sgst = $gst_details->sgst;
					$cgst_added_amount = $total_items_cost*($cgst/100);
					$sgst_added_amount = $total_items_cost*($sgst/100);
					$gst_amount = $cgst_added_amount+$sgst_added_amount;
					$total_with_gst = $total_items_cost+$gst_amount;
					$gst_details = array('cgst_perc'=>$cgst,'sgst_perc'=>$sgst,'cgst_amount'=>$cgst_added_amount,'sgst_amount'=>$sgst_added_amount,'total_gst_amount'=>$gst_amount);
					$this->response(['status'=>TRUE,'message'=>'Items added to cart succesfully','gst_details'=>$gst_details,'cart_count'=>$cart_count,'item_count'=>$item_count,'total_items_cost'=>$total_items_cost,'total_cost_with_gst'=>$total_with_gst,'items'=>$items],200);
				}else{
					$this->response(['status'=>FALSE,'message'=>'Please try again'],400);
				}
				
				}else{
					$this->response(['status'=>FALSE,'message'=>'Minimum order quantity should be '.$min_order_count.''],400);
				}				
			}else{
				$this->response(['status'=>FALSE,'message'=>'Invalid user id'],400);
			}
		}else{
			$this->response(['status'=>FALSE,'message'=>'All fields are required'],400);
		}
	}
	
	
	
	public function incdec_get(){
		$num = $this->get('num');
		$type = $this->get('type');
		if($num != "" && $type != ""){
		if($type == 'inc'){
			$count = $num+1;
		}else if($type == 'dec'){
			$count = $num-1;
		}
		$this->response(['status'=>TRUE,'message'=>'success','count'=>$count],200);
		}else{
			$this->response(['status'=>FALSE,'message'=>'please enter number and type'],400);
		}
	}
	
	  
	public function offers_get(){       
		$bu = base_url();
		$user_id = $this->get('user_id');
		if($user_id != ""){
			$offers = $this->services_model->get_offers()->result_array();
			$user_offers = [];
			if(count($offers) > 0){
				for($i=0;$i<count($offers);$i++){				
					$var = explode(",",$offers[$i]['offer_valid_for_dealers']);				
					//print_r($var);exit;
					if(in_array($user_id,$var)){
					$offers[$i]['offer_image'] = $bu.'assets/uploads/offers/'.$offers[$i]['oimage'];
					array_push($user_offers,$offers[$i]);
					} 
				}
				$this->response(['status'=>TRUE,'message'=>'success','offers'=>$user_offers],200);
			}else{
				$this->response(['status'=>FALSE,'message'=>'No offers found'],400);
			}	
		}else{
			$this->response(['status'=>FALSE,'message'=>'user_id is required'],400);
		}
		
	}
	
	public function offer_details_get(){
		$offer_id = $this->get('oid');
		$user_id = $this->get('user_id');
		$odetails = $this->services_model->check('offers',array('oid'=>$offer_id))->row_array();
		$cart_count = $this->services_model->check('cart',array('uid'=>$user_id,'added_by'=>'Dealer'))->num_rows();
		//$items = [];
		if($odetails['offer_on'] == 'Brands'){
			$colours = $this->services_model->get('colours')->result_array();
			$products = $this->services_model->get('products')->result_array();
			for($i=0;$i<count($colours);$i++){
				$colours[$i]['source'] = $this->services_model->check('source_countries',array('sid'=>$colours[$i]['source']))->row()->source;
				$colours[$i]['colour_type'] = $this->services_model->check('colour_types',array('ctid'=>$colours[$i]['colour_type']))->row()->colour_type;
				$colours[$i]['colour_brand'] = $this->services_model->check('brands',array('bid'=>$colours[$i]['colour_brand']))->row()->brand_name;
				$colours[$i]['colour_group'] = $this->services_model->check('colour_groups',array('cgid'=>$colours[$i]['colour_group']))->row()->colour_group_name;
				$colours[$i]['colour_image'] = $base_url.'assets/uploads/colours/'.$colours[$i]['colour_image'];
				
				$colours[$i]['features'] = strip_tags($colours[$i]['features']);
				$colours[$i]['benefits'] = strip_tags($colours[$i]['benefits']);
				$colours[$i]['description'] = strip_tags($colours[$i]['description']);
				$colours[$i]['item_type'] = 'Colours';
				$colours[$i]['item_count'] = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$colours[$i]['pid'],'item_type'=>'Colours'))->row()->count;
			
				//array_push($items,$colours[$i]);
			}	      
			for($i=0;$i<count($products);$i++){
				$products[$i]['brand'] = $this->services_model->check('brands',array('bid'=>$products[$i]['brand']))->row()->brand_name;
				$products[$i]['product_image'] = $base_url.'assets/uploads/products/'.$products[$i]['product_image'];
				$products[$i]['tds'] = $base_url.'assets/uploads/products/'.$products[$i]['tds'];
				$products[$i]['sds'] = $base_url.'assets/uploads/products/'.$products[$i]['sds'];
				$products[$i]['description'] = strip_tags($products[$i]['description']);
				$products[$i]['features'] = strip_tags($products[$i]['features']);
				$products[$i]['benefits'] = strip_tags($products[$i]['benefits']);
				$products[$i]['item_type'] = 'Products';
				$products[$i]['item_count'] = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$products[$i]['pid'],'item_type'=>'Products'))->row()->count;
			
				//array_push($items,$products[$i]);
			}			
		}else if($odetails['offer_on'] == 'Colours'){
			$colours = $this->services_model->get('colours')->result_array();
			for($i=0;$i<count($colours);$i++){
				$colours[$i]['source'] = $this->services_model->check('source_countries',array('sid'=>$colours[$i]['source']))->row()->source;
				$colours[$i]['colour_type'] = $this->services_model->check('colour_types',array('ctid'=>$colours[$i]['colour_type']))->row()->colour_type;
				$colours[$i]['colour_brand'] = $this->services_model->check('brands',array('bid'=>$colours[$i]['colour_brand']))->row()->brand_name;
				$colours[$i]['colour_group'] = $this->services_model->check('colour_groups',array('cgid'=>$colours[$i]['colour_group']))->row()->colour_group_name;
				$colours[$i]['colour_image'] = $base_url.'assets/uploads/colours/'.$colours[$i]['colour_image'];
			
				$colours[$i]['features'] = strip_tags($colours[$i]['features']);
				$colours[$i]['benefits'] = strip_tags($colours[$i]['benefits']);
				$colours[$i]['description'] = strip_tags($colours[$i]['description']);
				$colours[$i]['item_type'] = 'Colours';
				$colours[$i]['item_count'] = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$colours[$i]['pid'],'item_type'=>'Colours'))->row()->count;
			
				//array_push($items,$colours[$i]);
			}	
		}else if($odetails['offer_on'] == 'Products'){
			$products = $this->services_model->get('products')->result_array();
			for($i=0;$i<count($products);$i++){
				$products[$i]['brand'] = $this->services_model->check('brands',array('bid'=>$products[$i]['brand']))->row()->brand_name;
				$products[$i]['product_image'] = $base_url.'assets/uploads/products/'.$products[$i]['product_image'];
				$products[$i]['tds'] = $base_url.'assets/uploads/products/'.$products[$i]['tds'];
				$products[$i]['sds'] = $base_url.'assets/uploads/products/'.$products[$i]['sds'];
				$products[$i]['description'] = strip_tags($products[$i]['description']);
				$products[$i]['features'] = strip_tags($products[$i]['features']);
				$products[$i]['benefits'] = strip_tags($products[$i]['benefits']);
				$products[$i]['item_type'] = 'Products';
				$products[$i]['item_count'] = $this->services_model->check('cart',array('uid'=>$user_id,'item_id'=>$products['pid'],'item_type'=>'Products'))->row()->count;
			
				//array_push($items,$products[$i]);
			}		
		}
		if(count($colours)>0){
			$colours = $colours;
		}else{
			$colours = [];
		}  
		if(count($products)>0){
			$products = $products;
		}else{
			$products = [];
		}
		$this->response(['status'=>TRUE,'message'=>'success','cart_count'=>$cart_count,'colours'=>$colours,'products'=>$products],200);
	}
	
	public function my_orders_get(){
		$user_id = $this->get('user_id');
		if($user_id != ""){
			$usercheck = array('did'=>$user_id);
			$userrows = $this->services_model->check('dealers',$usercheck)->num_rows();
			if($userrows > 0){
				$no_of_orders = $this->services_model->check('orders',array('uid'=>$user_id,'order_from'=>'dealers'))->num_rows();
				$orders = $this->services_model->check('orders',array('uid'=>$user_id,'order_from'=>'dealers'))->result_array();
				if(count($orders)>0){
					for($i=0;$i<count($orders);$i++){
						$Orders[$i]['order_id'] = $orders[$i]['oid'];
						$Orders[$i]['order_date'] = $orders[$i]['added_on'];
						$Orders[$i]['delivery_status'] = $orders[$i]['delivery_status'];
					}
				$this->response(['status'=>TRUE,'message'=>'success','total_orders'=>count($orders),'orders_list'=>$Orders],200);
				}else{
				$this->response(['status'=>FALSE,'message'=>'No orders'],400);	
				}
			}else{
				$this->response(['status'=>FALSE,'message'=>'Userid does not exist'],400);
			}	
		}else{
			$this->response(['status'=>FALSE,'message'=>'Please enter user_id'],400);
		}		
	}
	
	public function order_details_get(){
		$user_id = $this->get('user_id');
		$order_id = $this->get('order_id');
		if($user_id != "" && $order_id != ""){
			$usercheck = array('did'=>$user_id);
			$userrows = $this->services_model->check('dealers',$usercheck)->num_rows();
			if($userrows > 0){			
				$where = array('uid'=>$user_id,'oid'=>$order_id);
				$details = $this->services_model->check('orders',$where)->row_array();	
				$delivery_type_details = $this->services_model->check('delivery_types',array('dltid'=>$details['delivery_type']))->result_array();
				$idet = str_replace('},', '}&', $details['items']);
				$item_split_details = explode("&",$idet);
				$items = [];
				for($i=0;$i<count($item_split_details);$i++){
					$item_details[$i] = json_decode($item_split_details[$i],true);
					$item_details[$i]['total_cost'] = $item_details[$i]['count']*$item_details[$i]['item_cost'];
					array_push($items,$item_details[$i]);
				}	
				//$gst = $this->services_model->check('gst',array('id'=>'1'))->row()->gst;
				//$add_gst = $details['total_cost']*($gst/100);
				//$details['total_cost'] = $add_gst+$details['total_cost'];
				//print_r($details['total_cost']);exit;
			
				//$delivery_details = array('gst'=>$gst,'gst_added'=>$add_gst,'ordered_date'=>date('Y-m-d',strtotime($details['added_on'])),'total_price'=>$details['total_cost'],'order_status'=>'pending','estimated_delivery_date'=>$details['delivery_date']);
				
				$delivery_details = array('ordered_date'=>date('Y-m-d',strtotime($details['added_on'])),'total_price'=>$details['total_cost'],'order_status'=>'pending','estimated_delivery_date'=>$details['delivery_date']);
				$delivery_address = $this->services_model->check('dealers_shipping_address',array('dealer_id'=>$user_id))->row_array();
				$this->response(['status'=>TRUE,'message'=>'success','delivery_details'=>$delivery_details,'orders_list'=>$items,'delivery_address'=>$delivery_address,'delivery_type_details'=>$delivery_type_details],200);
			}else{
				$this->response(['status'=>FALSE,'message'=>'Userid does not exist'],400);
			}	
		}else{
			$this->response(['status'=>FALSE,'message'=>'Please enter user_id and order_id'],400);
		}	
	}
	            
	
	public function edit_cart_post(){
		$user_id = $this->input->post('user_id');
		$items = $this->input->post('items');
		if($user_id != "" && $items != ""){
			$cart_count = $this->services_model->check('cart',array('uid'=>$user_id,'added_by'=>'Dealer'))->num_rows();
			$this->db->delete('cart',array('uid'=>$user_id));
			$idet = str_replace('},', '}&', $items);
			$item_split_details = explode("&",$idet);
			//print_r($item_split_details);
			for($i=0;$i<count($item_split_details);$i++){				
				$idetails[$i] = json_decode($item_split_details[$i],true);					
				$item_total_cost[$i] = $idetails[$i]['count']*$idetails[$i]['item_cost'];
				$total_items_cost += $item_total_cost[$i];
				$cart_data[$i] = array('uid'=>$user_id,'item_id'=>$idetails[$i]['item_id'],'item_type'=>$idetails[$i]['item_type'],'item_cost'=>$idetails[$i]['item_cost'],'total_cost'=>$item_total_cost[$i],'count'=>$idetails[$i]['count'],'added_on'=>date('Y-m-d H:i'));
				$this->services_model->save('cart',$cart_data[$i]);
			}
			$this->response(['status'=>TRUE,'message'=>'Cart updated succesfully','cart_count'=>$cart_count,'total_items_cost'=>$total_items_cost],200);
		}else{
			$this->response(['status'=>FALSE,'message'=>'Please enter user_id and items'],400);
		}
	}
	
	public function send_notification_post(){
		$data = array('message'=>'Let me know if you got this');
		$er = 'fYaT-G7F5WE:APA91bFCS8D9QqupO-aaCtalSaNIhZGT_k8BYnaQieWj4B-BoH_8BTKSvDIDgVSzjgDEFgrjBf8cfC6YeM9P948nSzcOevWG2vGFdf4p8NhCyY5e6m90NTdJvv4bWD__yQbCeI4o136CLXZZuvGwj7LNTC6RCRB4qg';
		$title = 'Test Notification';
		$this->push_notifications->send_notification($er,$data,$title);	
	}
	
	public function notifications_get(){
		$user_id = $this->get('user_id');
		if($user_id  != ""){
			$user_rows = $this->services_model->check('dealers',array('did'=>$user_id))->num_rows();
			if($user_rows > 0){
				$notifications = $this->services_model->check('app_notifications',array('dealer_id'=>$user_id));
				if($notifications->num_rows() > 0){
					$ndetails = $notifications->result_array();
					for($j=0;$j<count($ndetails);$j++){
						$ndetails[$j]['date'] = date("F j, Y", strtotime($ndetails[$j]['date']));
					}
					
					$this->response(['status'=>TRUE,'message'=>'success','notifications_count'=>$notifications->num_rows(),'notifications'=>$ndetails],200);
				}else{
					$this->response(['status'=>FALSE,'message'=>'No new notifications'],400);
				}
			}else{
				$this->response(['status'=>FALSE,'message'=>'Invalid user_id'],400);
			}
		}else{
				$this->response(['status'=>FALSE,'message'=>'Please enter user_id and items'],400);
		}
	}
	
	
	
	public function apply_coupon_post(){
		$user_id = $this->input->post('user_id');
		$cart_id = $this->input->post('cart_id');
		$oid = $this->input->post('oid');
		if($user_id != "" && $cart_id != "" && $oid != ""){
			$offer_rows = $this->services_model->check('cart',array('cid'=>$cart_id,'offer_id'=>$oid))->num_rows();
			//echo $offer_rows;exit;
			if($offer_rows == 0){
				$cart_details = $this->services_model->check('cart',array('cid'=>$cart_id))->row_array();
				$offer_details = $this->services_model->check('offers',array('oid'=>$oid))->row_array();
				$item_type = $cart_details['item_type'];
				
				// Update Total cost before applying coupon
				$cart_total_cost = $cart_details['count']*$cart_details['item_cost'];       
				$this->services_model->update('cart',array('cid'=>$cart_id),array('total_cost'=>$cart_total_cost));
				
				$updated_cart_details = $this->services_model->check('cart',array('cid'=>$cart_id))->row_array();
				
				$product_offers = [];
				$product_id = $updated_cart_details['item_id'];
				$offer_products = $offer_details['products'];
				$var = explode(",",$offer_products);				
				if(in_array($updated_cart_details['item_id'],$var)){
					if($offer_details['offer_type'] == 'by_value'){
						$offer_price = $offer_details['vp'];
					}else{
						$offer_price = round($updated_cart_details['total_cost']*($offer_details['vp']/100));
					}
					$total_after_offer  = $updated_cart_details['total_cost']-$offer_price;
					$this->services_model->update('cart',array('cid'=>$cart_id),array('offer_id'=>$oid,'offer_price'=>$offer_price,'total_cost'=>$total_after_offer));
					$this->call_cart_list($user_id);
					//$this->response(['status'=>TRUE,'message'=>'success','cart_list'=>$cart_list],200);
				}else{
					$this->response(['status'=>FALSE,'message'=>'Offer is not applicable on this product'],400);
				} 
			}else{
				$this->response(['status'=>FALSE,'message'=>'Offer already applied'],400);
			}
		}else{
			$this->response(['status'=>FALSE,'message'=>'Please enter user_id,cart_id and offer id(oid)'],400);
		}
	}
	
	
	public function call_cart_list($user_id){
		$base_url = base_url();
		$service_url = $base_url."api/services/cart_list?user_id=".$user_id."&API-KEY=1491615";
		//echo $service_url;exit;
		$curl = curl_init($service_url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$curl_response = curl_exec($curl);
		print_r($curl_response);
	}
	
	public function payment_response_get(){
		$orderId = $this->get('order_id');
		$merchantId = "nippon_test";
		$ch = curl_init('https://axisbank.juspay.in/order_status');
		curl_setopt($ch, CURLOPT_POSTFIELDS ,array('orderId' => $orderId , 'merchantId' => $merchantId ));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_USERPWD, 'A18FC27CF1A24FAD8316B015A4F81C68:');
		$jsonResponse =  json_decode( curl_exec($ch) );
		
		$orderId = $jsonResponse->{'orderId'};
		$status = $jsonResponse->{'status'};
		$statusId = $jsonResponse->{'statusId'};
		$txnId = $jsonResponse->{'txnId'};
		if($orderId != "" && $status != "" && $statusId != "" && $txnId != ""){
			$payment_response = 
				array(
					'orderId' => $orderId,
					'status'  => $status,
					'statusId'=> $statusId,
					'transaction_id'=>$txnId,
					'url' => 'http://nipponpaint-autorefinishes.com'
				);
				$url = "http://nipponpaint-autorefinishes.com";
				$update_data = array('payment_status'=>$statusId,'transaction_id'=>$txnId);   
				$this->services_model->update('orders',array('order_id'=>$orderId),$update_data);
				$this->response(['status'=>TRUE,'message'=>'success','payment_response'=>$url],200);
		}
	} 
	               
	   
	
	

}