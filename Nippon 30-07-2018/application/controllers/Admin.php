<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('email');	
		$this->load->helper('cookie');
		$this->load->library('utility');	
		$this->load->library('pagination');	
		$this->load->library('push_notifications');
		$this->load->model('admin_model','my_model');  				
		$this->load->model('catalyst_model','catalyst_model');  				
	}

	
	public function index()
	{
		if(!$this->session->userdata('admin_session')){	
			$this->load->view('admin/login');
		}else{
			redirect('admin/dashboard');
		}	
	}
	
	   
	public function orders_analy(){
		$orderManaly = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		for ($i = 0; $i < 12; $i++) {
			$where = "year(added_on) = ".date('Y')." and month(added_on) = " . ($i + 1);
			$orderManaly[$i] = $this->my_model->check("orders", $where)->num_rows();
		}
		$data['orderManaly'] = implode(",", $orderManaly);		
		//print_r($data);exit;
		$this->load->view('admin/orders_analy',$data);
	}         
	
	
	public function login()	
	{
		$email = $this->input->post('email');
		$p = $this->input->post('password');
		$password = md5($this->input->post('password'));		
            $remember = $this->input->post('remember_me');
		
		$query = $this->my_model->login($email,$password);
		$data = $query->row_array();
		$rows = $query->num_rows();
		//print_r($rows);exit;
		if($rows > 0){  
			if ($remember) {
			       $user_email= array( 'name'   => 'ausername', 'value'  => $email, 'expire' => '86400' ); 		
			       $user_password = array( 'name'   => 'apassword', 'value'  => $p, 'expire' => '86400' ); 		
			       $this->input->set_cookie($user_email);
			       $this->input->set_cookie($user_password);
			}else{
			delete_cookie('ausername'); 
			delete_cookie('apassword'); 			
						
			}                     

			$admindata = array(
							'id'  => $data['id'],
							'name' => $data['name'],
							'email' => $data['email'],
							'image' => $data['profile_pic'],
							//'permissions' => $data['permissions']							
								);
			$this->session->set_userdata('admin_session',$admindata);
			redirect('admin/dashboard',$data);
		}else{	
			$this->session->set_flashdata('message','Failed');
			redirect('admin');			
		}
	}
	
	
	public function dashboard()
	{
		if($this->session->userdata('admin_session')){		
		$data['dealers'] = $this->my_model->count('dealers');	       	     
		$data['dp'] = $this->my_model->count('delivery_partners');	       	     
		$data['fp'] = $this->my_model->count('franchise_partners');	       	     
		$data['admins'] = $this->my_model->count('catalysts');	       	     
		$data['brands'] = $this->my_model->count('brands');	       	     
		$data['colours'] = $this->my_model->count('colours');	       	     
		$data['products'] = $this->my_model->count('products');	       	     
		$data['feedback'] = $this->my_model->count('feedback');	       	     
		$this->load->view('admin/index',$data);
		}else{
		redirect('admin');
		}
	}
	
	
	public function catalysts(){
		if($this->session->userdata('admin_session')){		
		$pagination = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->my_model->count('catalysts');
		
			$data['catalysts'] = $this->my_model->get_ls('catalysts',$limit_per_page, $start_index)->result_array();	
			$config['base_url'] = base_url() . 'admin/catalysts';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();	
			
		//print_r($data);exit;
		$this->load->view('admin/catalysts',$data);
		}else{
		redirect('admin');
		}
	}
	
	public function franchise_partners(){
		if($this->session->userdata('admin_session')){		
		$pagination = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->my_model->count('franchise_partners');
		
			$data['fp'] = $this->my_model->get_ls('franchise_partners',$limit_per_page, $start_index)->result_array();
			//print_r(($data['dealers']));	exit;
			$config['base_url'] = base_url() . 'admin/franchise_partners';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();	
		
		$this->load->view('admin/franchise_partners',$data);
		}else{
		redirect('admin');
		}
	}
	
	public function catalyst_action($id=''){
		if($this->session->userdata('admin_session')){	
			if(isset($_POST['submit'])){
							
				if($id != ''){
				$data = array(
				'name' => $this->input->post('name'),				
				'address' => $this->input->post('address'), 
				'designation' => $this->input->post('designation'), 
				'email' => $this->input->post('email'), 
				'phone' => $this->input->post('mobile'), 
				'password' => md5($this->input->post('password')), 
				'state' => $this->input->post('state'), 
				'city' => $this->input->post('city')
				);
				
				$where	= array('cid'=>$id);
				$this->my_model->update('catalysts',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'name' => $this->input->post('name'),				
				'address' => $this->input->post('address'), 
				'designation' => $this->input->post('designation'), 
				'email' => $this->input->post('email'), 
				'phone' => $this->input->post('mobile'), 
				'password' => md5($this->input->post('password')), 
				'state' => $this->input->post('state'), 
				'city' => $this->input->post('city'),
				'created_date' => date('Y-m-d')
				);
				
				$this->my_model->save('catalysts',$data);
				$this->session->set_flashdata('message','added');				
				}
				redirect('admin/catalysts');
				
			}else{
				if ($id != "") {
				$where = array('cid'=>$id);
				$data['states'] = $this->my_model->get_states();
				$data['cities'] = $this->my_model->get_cities();
				$data["Catalyst_data"] = $this->my_model->check('catalysts',$where)->row_array();				
				$this->load->view("admin/add_catalyst", $data);
				} else {					
					$data['states'] = $this->my_model->get_states();					
				$this->load->view("admin/add_catalyst",$data);
				}
			}					
		}else{
		redirect('admin');
		}
	}
	      
	   
	public function fp_action($id=''){
		if($this->session->userdata('admin_session')){	
			if(isset($_POST['submit'])){
							
				if($id != ''){
				$data = array(
				'name' => $this->input->post('name'),				
				'address' => $this->input->post('address'), 
				'designation' => $this->input->post('designation'), 
				'email' => $this->input->post('email'), 
				'phone' => $this->input->post('mobile'), 
				'state' => $this->input->post('state'), 
				'city' => $this->input->post('city'), 
				'password' => md5($this->input->post('password')),
				'lat' => $this->input->post('lattitude'), 
				'lng' => $this->input->post('longitude')
				);
				
				$where	= array('fid'=>$id);
				$this->my_model->update('franchise_partners',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'name' => $this->input->post('name'),				
				'address' => $this->input->post('address'), 
				'designation' => $this->input->post('designation'), 
				'email' => $this->input->post('email'), 
				'phone' => $this->input->post('mobile'), 
				'state' => $this->input->post('state'), 
				'city' => $this->input->post('city'), 
				'password' => md5($this->input->post('password')), 	
				'lat' => $this->input->post('lattitude'), 
				'lng' => $this->input->post('longitude'),				
				'created_date' => date('Y-m-d')
				);
				//print_r($data);exit;
				$this->my_model->save('franchise_partners',$data);
				$this->session->set_flashdata('message','added');				
				}
				redirect('admin/franchise_partners');
				
			}else{
				if ($id != "") {
					$data['states'] = $this->my_model->get_states();
				$data['cities'] = $this->my_model->get_cities();
				$where = array('fid'=>$id);
				$data["fp_data"] = $this->my_model->check('franchise_partners',$where)->row_array();				
				$this->load->view("admin/add_fp", $data);
				} else {					
					$data['states'] = $this->my_model->get_states();					
				$this->load->view("admin/add_fp",$data);
				}
			}					
		}else{
		redirect('admin');
		}
	}
	
	
	
	public function delivery_partners(){
		if($this->session->userdata('admin_session')){		
		$pagination = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->my_model->count('delivery_partners');
		
			$data['fp'] = $this->my_model->get_ls('delivery_partners',$limit_per_page, $start_index)->result_array();
			//print_r(($data['dealers']));	exit;
			$config['base_url'] = base_url() . 'admin/delivery_partners';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();	
		
		$this->load->view('admin/delivery_partners',$data);
		}else{
		redirect('admin');
		}
	}
	
	public function dp_action($id=''){
		if($this->session->userdata('admin_session')){	
			if(isset($_POST['submit'])){
							
				if($id != ''){
				$data = array(
				'name' => $this->input->post('name'),				
				'address' => $this->input->post('address'), 
				'designation' => $this->input->post('designation'), 
				'email' => $this->input->post('email'), 
				'phone' => $this->input->post('mobile'), 
				'state' => $this->input->post('state'), 
				'city' => $this->input->post('city'), 
				'password' => md5($this->input->post('password'))
				);
				
				$where	= array('dlvid'=>$id);
				$this->my_model->update('delivery_partners',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'name' => $this->input->post('name'),				
				'address' => $this->input->post('address'), 
				'designation' => $this->input->post('designation'), 
				'email' => $this->input->post('email'), 
				'phone' => $this->input->post('mobile'), 
				'state' => $this->input->post('state'), 
				'city' => $this->input->post('city'), 
				'password' => md5($this->input->post('password')), 				
				'created_date' => date('Y-m-d')
				);
				//print_r($data);exit;
				$this->my_model->save('delivery_partners',$data);
				$this->session->set_flashdata('message','added');				
				}
				redirect('admin/delivery_partners');
				
			}else{
				if ($id != "") {
					$data['states'] = $this->my_model->get_states();
				$data['cities'] = $this->my_model->get_cities();
				$where = array('dlvid'=>$id);
				$data["dp_data"] = $this->my_model->check('delivery_partners',$where)->row_array();				
				$this->load->view("admin/add_dp", $data);
				} else {					
					$data['states'] = $this->my_model->get_states();					
				$this->load->view("admin/add_dp",$data);
				}
			}					
		}else{
		redirect('admin');
		}
	}
	
	public function get_cities(){
		$state = $this->input->post('state');
		$cities = $this->my_model->get_cities_b_states($state);
		//print_r($data);
		$h = '<option>Select City</option>';
		foreach($cities as $cval){
			$h .= '<option value="'.$cval['City'].'">'.$cval['City'].'</option>';
		}
		print_r($h);
	}
	
	public function logout() {
	$this->session->sess_destroy();
	$this->session->set_flashdata("success", LOGOUTSUCCESS);
	redirect("admin");
	}
	
	
	
	
	public function edit_profile(){
		if($this->session->userdata('admin_session')){	
		$admindata = $this->session->userdata("admin_session");
		$id = $admindata['id'];
		$where	= array('id'=>$id); 		
		//print_r($where);exit;
			if(isset($_POST['submit'])){				
				if (!empty($_FILES['admin_image']['name'])) {
				$config['upload_path'] = 'assets/uploads/users/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPG';
				$config['encrypt_name'] = TRUE;
				$config['max_size'] = 10000;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('admin_image')) {
				$error = array(
				'error' => $this->upload->display_errors()
				);
				$this->session->set_flashdata("succuss", $error['error']);
				redirect("admin/profile");
				} else {
				$upload_data = $this->upload->data();
				$page_image = $upload_data['file_name'];
				}
				} else {
				$page_image = $this->input->post('old_image');
				}
				
				if($this->input->post('password') != ''){
				$password = md5($this->input->post('password'));
				}else{
				$password = $this->input->post('old_password');
				}
				
				
				
				$data = array(
				'name' => $this->input->post('name'),            			
				'email' => $this->input->post('email'),
				'profile_pic' => $page_image,
				'password' => $password
				);
				$this->my_model->update('admin',$where,$data);
				$this->session->set_flashdata('message','updated');
				$admin_data = $this->my_model->check('admin',$where)->row_array();
				//print_r($admin_data);exit;
				$admindata = array(
							'id'  => $admin_data['id'],
							'name' => $admin_data['name'],
							'email' => $admin_data['email'],
							'image' => $admin_data['profile_pic']						
								);
				
				$this->session->set_userdata('admin_session', $admindata);	
				//echo "<pre>";print_r($this->session->all_userdata());echo "</pre>";		
				redirect('admin/edit_profile');
				
			}else{
			$data['userdetails'] = $this->my_model->check('admin',$where)->row_array();
			//print_r($data);exit;
			$this->load->view('admin/edit_profile',$data);
			}
		}else{
		redirect('admin');
		}
	}
	
	public function check_mobile(){
		$phone = $this->input->post('mobile');		
		$table = $this->input->post('table');		
		$where = array('phone'=>$phone);
		$rows = $this->my_model->check($table,$where)->num_rows();		
		if($rows > 0){
			echo '1';
		}else{
			echo '0';
		}
	}
	
	
	public function check_email(){
		$email = $this->input->post('email');		
		$table = $this->input->post('table');		
		$where = array('email'=>$email);
		$rows = $this->my_model->check($table,$where)->num_rows();		
		if($rows > 0){
			echo '1';
		}else{
			echo '0';
		}
	}
	
	
	public function change_catalyst_status(){		
		$id = $this->input->post('id');
		$status=$this->input->post('status');
		$where = array('cid'=>$id);
		$zero = '0';
		$one = '1';
		if($status == '0'){
		$update = array('cstatus'=>'1');		
		echo '<button type="button" class="btn btn-info btn-xs m-b-10 m-l-5" onClick="change_catalyst_status('. $id . ',' . $one . ')"  >Active</button>';
		}else{
		$update = array('cstatus'=>'0');	
		echo '<button type="button" class="btn btn-warning btn-xs m-b-10 m-l-5" onClick="change_catalyst_status('. $id . ',' . $zero. ')"  >Inactive</button>';
		}
		$this->my_model->update('catalysts',$where,$update);			
	}
	
	public function change_fp_status(){		
		$id = $this->input->post('id');
		$status=$this->input->post('status');
		$where = array('fid'=>$id);
		$zero = '0';
		$one = '1';
		if($status == '0'){
		$update = array('fstatus'=>'1');		
		echo '<button type="button" class="btn btn-info btn-xs m-b-10 m-l-5" onClick="change_fp_status('. $id . ',' . $one . ')"  >Active</button>';
		}else{
		$update = array('fstatus'=>'0');	
		echo '<button type="button" class="btn btn-warning btn-xs m-b-10 m-l-5" onClick="change_fp_status('. $id . ',' . $zero. ')"  >Inactive</button>';
		}
		$this->my_model->update('franchise_partners',$where,$update);	
		
	}
	
	
	public function change_dp_status(){		
		$id = $this->input->post('id');
		$status=$this->input->post('status');
		$where = array('dlvid'=>$id);
		$zero = '0';
		$one = '1';
		if($status == '0'){
		$update = array('dstatus'=>'1');		
		echo '<button type="button" class="btn btn-info btn-xs m-b-10 m-l-5" onClick="change_fp_status('. $id . ',' . $one . ')"  >Active</button>';
		}else{
		$update = array('dstatus'=>'0');	
		echo '<button type="button" class="btn btn-warning btn-xs m-b-10 m-l-5" onClick="change_fp_status('. $id . ',' . $zero. ')"  >Inactive</button>';
		}
		$this->my_model->update('delivery_partners',$where,$update);	
		
	}
	
	
	public function change_dealer_status(){		
		$id = $this->input->post('id');
		$status=$this->input->post('status');
		$where = array('did'=>$id);
		$zero = '0';
		$one = '1';
		if($status == '0'){
		$update = array('dstatus'=>'1');		
		echo '<button type="button" class="btn btn-info btn-xs m-b-10 m-l-5" onClick="change_dealer_status('. $id . ',' . $one . ')"  >Active</button>';
		}else{
		$update = array('dstatus'=>'0');	
		echo '<button type="button" class="btn btn-warning btn-xs m-b-10 m-l-5" onClick="change_dealer_status('. $id . ',' . $zero. ')"  >Inactive</button>';
		}
		$this->my_model->update('dealers',$where,$update);	
		
	}
	
	
	
	
	public function forgot_password(){
		$email = $this->input->post('email');
		$pwd = $this->utility->generate_otp();
		$password =  md5($pwd);
		$where  =array('email'=>$email);
		$rows = $this->my_model->check('admin',$where)->num_rows();
		if($rows > 0){
		$update = array('password'=>$password);
		$res = $this->my_model->update('admin',$where,$update);
		if($res){
			$subject = 'Forgot Password';
			$message = 'Dear Superadmin,your new password is '.$pwd.'';   
			$this->utility->send_email($email,$message,$subject);
			$this->session->set_flashdata('message','pasword_changed');
		}else{
			$this->session->set_flashdata('message','Mobile_not_exists');
		}
		}else{
		$this->session->set_flashdata('message','Invalid email id');
		}
		redirect('admin/index');
	}
	


	
	public function get_location_details(){
		$lat = $this->input->post('lat');
		$lng = $this->input->post('lng');
		$geolocation = $lat.','.$lng;
		$url = 'http://maps.googleapis.com/maps/api/geocode/json?language=en&latlng='.$geolocation.'&sensor=false';
		$file_contents = file_get_contents($url);
		$output = json_decode($file_contents);
		echo $output->results[0]->formatted_address;
		
	}
	

	
	public function delete_user() {	
	$table = $this->input->post('table');
	$id = $this->input->post('id');
		if($this->session->userdata('admin_session')){	
			if($table == 'catalysts'){
				$where = array('cid'=>$id);
			}else if($table == 'franchise_partners'){
				$where = array('fid'=>$id);
			}else if($table == 'dealers'){
				$where = array('did'=>$id);
			}else if($table == 'delivery_partners'){
				$where = array('dlvid'=>$id);
			}		
		//echo $table;print_r($where);exit;
		$res = $this->db->delete($table, $where);
		//echo $this->db->last_query();exit;
		if ($res) {		
		echo '1';
		} else {
		echo '0';
		}
		
		} else {
		redirect("admin/login");
		}
	}
	
	// To Delete Product Category
	public function delete_pc($id) {		
		if($this->session->userdata('admin_session')){			
		$where = array('pcid'=>$id);			
		$cg_details = $this->my_model->check('product_categories',$where)->row();		
		$res = $this->db->delete('product_categories', array('pcid' => $id));	
		$eimg = $cg_details->pc_img;	
		if ($res) {		
		unlink("assets/uploads/products/".$eimg);
		$this->session->set_flashdata("message", "success");
		} else {
		$this->session->set_flashdata("message", "error");
		}
		redirect("/admin/product_categories/");
		} else {
		redirect("admin/login");
		}
	}
	
	
	public function delete_product($id) {		
		if($this->session->userdata('admin_session')){			
		$where = array('pid'=>$id);			
		$cg_details = $this->my_model->check('products',$where)->row();		
		$res = $this->db->delete('products', array('pid' => $id));	
		$eimg = $cg_details->product_image;	
		if ($res) {		
		if($eimg != "no-image.png"){
		unlink("assets/uploads/products/".$eimg);
		}
		$this->session->set_flashdata("message", "success");
		} else {
		$this->session->set_flashdata("message", "error");
		}
		redirect("/admin/products/");
		} else {
		redirect("admin/login");
		}
	}
	
	public function delete_colour_group($id) {		
		if($this->session->userdata('admin_session')){			
		$where = array('cgid'=>$id);			
		$cg_details = $this->my_model->check('colour_groups',$where)->row();		
		$res = $this->db->delete('colour_groups', array('cgid' => $id));	
		$eimg = $cg_details->cg_img;	
		if ($res) {		
		unlink("assets/uploads/colour_groups/".$eimg);
		$this->session->set_flashdata("message", "success");
		} else {
		$this->session->set_flashdata("message", "error");
		}
		redirect("/admin/colour_groups/");
		} else {
		redirect("admin/login");
		}
	}
	
	public function delete_offer($id) {		
		if($this->session->userdata('admin_session')){			
		$where = array('oid'=>$id);			
		$cg_details = $this->my_model->check('offers',$where)->row();		
		$res = $this->db->delete('offers', array('oid' => $id));	
		$eimg = $cg_details->oimage;	
		if ($res) {		
		unlink("assets/uploads/offers/".$eimg);
		$this->session->set_flashdata("message", "success");
		} else {
		$this->session->set_flashdata("message", "error");
		}
		redirect("/admin/offers/");
		} else {
		redirect("admin/login");
		}
	}
	
	public function delete_colour_type($id) {		
		if($this->session->userdata('admin_session')){			
		$where = array('cgid'=>$id);
		$res = $this->db->delete('colour_types', array('ctid' => $id));
		if ($res) {		
		$this->session->set_flashdata("message", "success");
		} else {
		$this->session->set_flashdata("message", "error");
		}
		redirect("/admin/colour_types/");
		} else {
		redirect("admin/login");
		}
	}
	
	
	public function delete_source($id) {		
		if($this->session->userdata('admin_session')){			
		$where = array('sid'=>$id);
		$res = $this->db->delete('source_countries', array('sid' => $id));
		if ($res) {		
		$this->session->set_flashdata("message", "success");
		} else {
		$this->session->set_flashdata("message", "error");
		}
		redirect("/admin/source_countries/");
		} else {
		redirect("admin/login");
		}
	}
	
	
	public function delete_route(){
		$table = $this->input->post('table');
		$rid = $this->input->post('id');
		$res = $this->db->delete('routes',array('id'=>$rid));
		if ($res) {
			$this->session->set_flashdata("message", "success");
		} else {
			$this->session->set_flashdata("message", "error");
		}
	}
	
	public function delete_rider(){
		$table = $this->input->post('table');
		$rid = $this->input->post('id');
		$res = $this->db->delete('riders',array('rid'=>$rid));
		if ($res) {
			$this->session->set_flashdata("message", "success");
		} else {
			$this->session->set_flashdata("message", "error");
		}
	}
	
	public function delete_brand($id) {		
		if($this->session->userdata('admin_session')){	
		$base_url = base_url().'assets/uploads/brands/';
		$where = array('bid'=>$id);
		$brand_details = $this->my_model->check('brands',$where)->row();		
		$eimg = $brand_details->brand_image;		
		$res = $this->db->delete('brands', array('bid' => $id));
		if ($res) {
		unlink("assets/uploads/brands/".$eimg);
		$this->session->set_flashdata("message", "success");
		} else {
		$this->session->set_flashdata("message", "error");
		}
		
		redirect("/admin/brands/");
		} else {
		redirect("admin/login");
		}
	}
	
	
	public function delete_colour($id) {		
		if($this->session->userdata('admin_session')){	
		$base_url = base_url().'assets/uploads/colours/';
		$where = array('cid'=>$id);
		$brand_details = $this->my_model->check('colours',$where)->row();		
		$eimg = $brand_details->colour_image;		
		$res = $this->db->delete('colours', array('cid' => $id));
		
		if ($res) {
			if($eimg != "no-image.png"){
		unlink("assets/uploads/colours/".$eimg);
			}
		$this->session->set_flashdata("message", "success");
		} else {
		$this->session->set_flashdata("message", "error");
		}
		
		redirect("/admin/colours/");
		} else {
		redirect("admin/login");
		}
	}
	

	public function brands(){
		if($this->session->userdata('admin_session')){		
		$data['Brands'] = $this->my_model->get('brands')->result_array();			
		$this->load->view('admin/brands',$data);
		}else{
		redirect('admin');
		}
	}
	
	
		public function brand_action($id=''){
		if($this->session->userdata('admin_session')){	
			if(isset($_POST['submit'])){
				if (!empty($_FILES['brand_image']['name'])) {
				$config['upload_path'] = 'assets/uploads/brands/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPG';
				$config['encrypt_name'] = TRUE;
				$config['max_size'] = 10000;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('brand_image')) {
				$error = array(
				'error' => $this->upload->display_errors()
				);
				$this->session->set_flashdata("succuss", $error['error']);
				print_r($this->upload->display_errors());
				} else {
				$upload_data = $this->upload->data();
				$brand_image = $upload_data['file_name'];
				}
				} else {
				$brand_image = $this->input->post('old_image');
				}
				
				if($id != ''){
				$data = array(
				'brand_name' => $this->input->post('name'),				
				'brand_image' => $brand_image, 
				'description' => strip_tags($this->input->post('description'))
				);
				
				$where	= array('bid'=>$id);	
				$this->my_model->update('brands',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'brand_name' => $this->input->post('name'),				
				'brand_image' => $brand_image, 
				'description' => strip_tags($this->input->post('description')),
				'created_on' => date('Y-m-d')
				);
				
				$this->my_model->save('brands',$data);
				$this->session->set_flashdata('message','added');				
				}
				redirect('admin/brands');
				
			}else{
				if ($id != "") {
				$where = array('bid'=>$id);				
				$data["Brand_data"] = $this->my_model->check('brands',$where)->row_array();				
				$this->load->view("admin/add_brand", $data);
				} else {					
					$data['states'] = $this->my_model->get_states();					
				$this->load->view("admin/add_brand",$data);
				}
			}					
		}else{
		redirect('admin');
		}
	}
	
	
	public function colour_groups(){
		if($this->session->userdata('admin_session')){		
		$data['colour_groups'] = $this->my_model->get('colour_groups')->result_array();			
		$this->load->view('admin/colour_groups',$data);
		}else{
		redirect('admin');
		}
	}
	
	public function colour_group_action($id=''){
		if($this->session->userdata('admin_session')){	
			if(isset($_POST['submit'])){
				if (!empty($_FILES['brand_image']['name'])) {
				$config['upload_path'] = 'assets/uploads/colour_groups/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPG';
				$config['encrypt_name'] = TRUE;
				$config['max_size'] = 10000;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('brand_image')) {
				$error = array(
				'error' => $this->upload->display_errors()
				);
				$this->session->set_flashdata("succuss", $error['error']);
				print_r($this->upload->display_errors());
				} else {
				$upload_data = $this->upload->data();
				$brand_image = $upload_data['file_name'];
				}
				} else {
				$brand_image = $this->input->post('old_image');
				}
				
				if($id != ''){
				$data = array(
				'colour_group_name' => $this->input->post('name'),
				'cg_img' => $brand_image
				);
				
				$where	= array('cgid'=>$id);	
				$this->my_model->update('colour_groups',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'colour_group_name' => $this->input->post('name'),
				'cg_img' => $brand_image,
				'created_on' => date('Y-m-d')
				);
				
				$this->my_model->save('colour_groups',$data);
				$this->session->set_flashdata('message','added');				
				}
				
				redirect('admin/colour_groups');
				
			}else{
				if ($id != "") {
				$where = array('cgid'=>$id);				
				$data["cg_data"] = $this->my_model->check('colour_groups',$where)->row_array();				
				$this->load->view("admin/add_colour_group", $data);
				} else {					
					$data['colour_groups'] = $this->my_model->get('colour_groups')->result_array();					
				$this->load->view("admin/add_colour_group",$data);
				}
			}					
		}else{
		redirect('admin');
		}
	}
	
	
	public function colour_types(){
		if($this->session->userdata('admin_session')){		
		$data['colour_types'] = $this->my_model->get('colour_types')->result_array();			
		$this->load->view('admin/colour_types',$data);
		}else{
		redirect('admin');
		}
	}
	
	public function colour_type_action($id=''){
		if($this->session->userdata('admin_session')){	
			if(isset($_POST['submit'])){
				if (!empty($_FILES['brand_image']['name'])) {
				$config['upload_path'] = 'assets/uploads/colours/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPG';
				$config['encrypt_name'] = TRUE;
				$config['max_size'] = 10000;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('brand_image')) {
				$error = array(
				'error' => $this->upload->display_errors()
				);
				$this->session->set_flashdata("succuss", $error['error']);
				print_r($this->upload->display_errors());
				} else {
				$upload_data = $this->upload->data();
				$brand_image = $upload_data['file_name'];
				}
				} else {
				$brand_image = $this->input->post('old_image');
				}
				
				
				if($id != ''){
				$data = array(
				'colour_type' => $this->input->post('name'),
				'ct_image' => $brand_image
				);
				
				$where	= array('ctid'=>$id);	
				$this->my_model->update('colour_types',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'colour_type' => $this->input->post('name'),
				'ct_image' => $brand_image,
				'created_on'	=> date('Y-m-d')
				);
				
				$this->my_model->save('colour_types',$data);
				$this->session->set_flashdata('message','added');				
				}
				
				redirect('admin/colour_types');
				
			}else{
				if ($id != "") {
				$where = array('ctid'=>$id);				
				$data["cg_data"] = $this->my_model->check('colour_types',$where)->row_array();				
				$this->load->view("admin/add_colour_type", $data);
				} else {					
					$data['colour_types'] = $this->my_model->get('colour_types')->result_array();					
				$this->load->view("admin/add_colour_type",$data);
				}
			}					
		}else{
		redirect('admin');
		}
	}
	
	
	//Product Categories
	public function product_categories(){
		if($this->session->userdata('admin_session')){		
		$data['product_categories'] = $this->my_model->get('product_categories')->result_array();			
		$this->load->view('admin/product_categories',$data);
		}else{
		redirect('admin');
		}
	}
	
	public function pc_action($id=''){
		if($this->session->userdata('admin_session')){	
			if(isset($_POST['submit'])){
				if (!empty($_FILES['brand_image']['name'])) {
				$config['upload_path'] = 'assets/uploads/products/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPG';
				$config['encrypt_name'] = TRUE;
				$config['max_size'] = 10000;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('brand_image')) {
				$error = array(
				'error' => $this->upload->display_errors()
				);
				$this->session->set_flashdata("succuss", $error['error']);
				print_r($this->upload->display_errors());
				} else {
				$upload_data = $this->upload->data();
				$brand_image = $upload_data['file_name'];
				}
				} else {
				$brand_image = $this->input->post('old_image');
				}
				
				if($id != ''){
				$data = array(
				'product_category' => $this->input->post('name'),
				'pc_img' => $brand_image
				);
				
				$where	= array('pcid'=>$id);	
				$this->my_model->update('product_categories',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'product_category' => $this->input->post('name'),
				'pc_img' => $brand_image,
				'created' => date('Y-m-d')
				);
				
				$this->my_model->save('product_categories',$data);
				$this->session->set_flashdata('message','added');				
				}
				
				redirect('admin/product_categories');
				
			}else{
				if ($id != "") {
				$where = array('pcid'=>$id);				
				$data["pc_data"] = $this->my_model->check('product_categories',$where)->row_array();				
				$this->load->view("admin/add_product_category", $data);
				} else {					
					$data['product_categories'] = $this->my_model->get('product_categories')->result_array();					
				$this->load->view("admin/add_product_category",$data);
				}
			}					
		}else{
		redirect('admin');
		}
	}	
	//Product Categories Closed
	
	public function source_countries(){
		if($this->session->userdata('admin_session')){		
		$data['source_countries'] = $this->my_model->get('source_countries')->result_array();			
		$this->load->view('admin/source_countries',$data);
		}else{
		redirect('admin');
		}
	}
	
	public function source_action($id=''){
		if($this->session->userdata('admin_session')){	
			if(isset($_POST['submit'])){
				
				if($id != ''){
				$data = array(
				'source' => $this->input->post('name')
				);
				
				$where	= array('sid'=>$id);	
				$this->my_model->update('source_countries',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'source' => $this->input->post('name'),
				'created_on'	=> date('Y-m-d')
				);
				
				$this->my_model->save('source_countries',$data);
				$this->session->set_flashdata('message','added');				
				}
				
				redirect('admin/source_countries');
				
			}else{
				if ($id != "") {
				$where = array('sid'=>$id);				
				$data["cg_data"] = $this->my_model->check('source_countries',$where)->row_array();				
				$this->load->view("admin/add_source_country", $data);
				} else {					
					$data['source_countries'] = $this->my_model->get('source_countries')->result_array();					
				$this->load->view("admin/add_source_country",$data);
				}
			}					
		}else{
		redirect('admin');
		}
	}
	
	

	
	
	
	
	public function products(){
		if($this->session->userdata('admin_session')){
		$pagination = array();
		$limit_per_page = 10;
		//echo $this->uri->segment(4);exit;         
		$start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$total_records = $this->my_model->count('products');	
			
		$data['products'] = $this->my_model->get_ls('products',$limit_per_page, $start_index)->result_array();
		for($i=0;$i<count($data['products']);$i++){
			$data['products'][$i]['brand'] = $this->my_model->check('brands',array('bid'=>$data['products'][$i]['brand']))->row()->brand_name;
		}
		
		$config['base_url'] = base_url() . 'admin/products';
		$config['total_rows'] = $total_records;
		$config['per_page'] = $limit_per_page;
		$config["uri_segment"] = 3;       
		$config['next_link'] = 'Next';		
		$config['prev_link'] = 'Previous';			
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();
		//print_r($data);exit;
		$this->load->view('admin/products',$data);
		}else{
		redirect('admin');
		}
	}
	
	
	public function products_action($id=''){
		if($this->session->userdata('admin_session')){	
			if(isset($_POST['submit'])){
				if (!empty($_FILES['brand_image']['name'])) {
				$config['upload_path'] = 'assets/uploads/products/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPG';
				$config['encrypt_name'] = TRUE;
				$config['max_size'] = 10000;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('brand_image')) {
				$error = array(
				'error' => $this->upload->display_errors()
				);
				$this->session->set_flashdata("succuss", $error['error']);
				print_r($this->upload->display_errors());
				} else {
				$upload_data = $this->upload->data();
				$colour_image = $upload_data['file_name'];
				}
				} else {
				$colour_image = $this->input->post('old_image');
				}
				
				if (!empty($_FILES['tds_image']['name'])) {
				$config['upload_path'] = 'assets/uploads/products/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc';
				$config['encrypt_name'] = TRUE;
				$config['max_size'] = 10000;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('tds_image')) {
				$error = array(
				'error' => $this->upload->display_errors()
				);
				$this->session->set_flashdata("succuss", $error['error']);
				print_r($this->upload->display_errors());
				} else {
				$upload_data = $this->upload->data();
				$tds_image = $upload_data['file_name'];
				}
				} else {
				$tds_image = $this->input->post('old_tds_image');
				}
				
				if (!empty($_FILES['sds_image']['name'])) {
				$config['upload_path'] = 'assets/uploads/products/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc';
				$config['encrypt_name'] = TRUE;
				$config['max_size'] = 10000;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('sds_image')) {
				$error = array(
				'error' => $this->upload->display_errors()
				);
				$this->session->set_flashdata("succuss", $error['error']);
				print_r($this->upload->display_errors());
				} else {
				$upload_data = $this->upload->data();
				$sds_image = $upload_data['file_name'];
				}
				} else {
				$sds_image = $this->input->post('old_sds_image');
				}
				
				$ov = $this->input->post('suggested_for');
				$suggested_for = implode(",",$ov);
								
				if($id != ''){
				$data = array(
				'product_category' => $this->input->post('category'),
				'product_name' => $this->input->post('name'),
				'brand' => $this->input->post('colour_brand'),
				'product_price' => $this->input->post('price'),
				'description' => $this->input->post('desc'),
				'features' => $this->input->post('features'),
				'benefits' => $this->input->post('benefits'),
				'product_image' => $colour_image,
				'tds' => $tds_image,
				'sds' => $sds_image,
				'uom' => $this->input->post('uom'),
				'pack_size' => $this->input->post('pack_size'),
				'quantity' => $this->input->post('quantity'),
				'suggested_for' => $suggested_for,
				'moq' => $this->input->post('moq')
				);
				
				$where	= array('pid'=>$id);	
				$this->my_model->update('products',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'product_category' => $this->input->post('category'),
				'product_name' => $this->input->post('name'),
				'brand' => $this->input->post('colour_brand'),
				'product_price' => $this->input->post('price'),
				'description' => $this->input->post('desc'),
				'features' => $this->input->post('features'),
				'benefits' => $this->input->post('benefits'),
				'product_image' => $colour_image,
				'tds' => $tds_image,
				'sds' => $sds_image,
				'uom' => $this->input->post('uom'),
				'pack_size' => $this->input->post('pack_size'),
				'quantity' => $this->input->post('quantity'),
				'suggested_for' => $suggested_for,
				'moq' => $this->input->post('moq'),
				'created_on' => date('Y-m-d')
				);
				
				$this->my_model->save('products',$data);
				$this->session->set_flashdata('message','added');				
				}
				//print_r($data);exit;
				redirect('admin/products');
				
			}else{
				if ($id != "") {
				$where = array('pid'=>$id);						
					$data['brands'] = $this->my_model->get('brands')->result_array();									
					$data["product_data"] = $this->my_model->check('products',$where)->row_array();
					$data['products'] = $this->my_model->get_products('products')->result_array();
					$data['product_categories'] = $this->my_model->get('product_categories')->result_array();
				$this->load->view("admin/add_product", $data);
				} else {	
					$data['products'] = $this->my_model->get_products('products')->result_array();				
					$data['brands'] = $this->my_model->get('brands')->result_array();	
					$data['product_categories'] = $this->my_model->get('product_categories')->result_array();					
				$this->load->view("admin/add_product",$data);
				}
			}					
		}else{
		redirect('admin');
		}
	}
	
	
	public function delete_pincode($id) {		
		if($this->session->userdata('admin_session')){			
		$where = array('sid'=>$id);
		$res = $this->db->delete('pincodes', array('pid' => $id));
		if ($res) {		
		$this->session->set_flashdata("message", "deleted");
		} else {
		$this->session->set_flashdata("message", "error");
		}
		redirect("/admin/pincodes/");
		} else {
		redirect("admin/login");
		}
	}
	
	public function pincodes(){
		if($this->session->userdata('admin_session')){
		$pagination = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->my_model->count('pincodes');
		
			$data['pincodes'] = $this->my_model->get_ls('pincodes',$limit_per_page, $start_index)->result_array();	
			$config['base_url'] = base_url() . 'admin/pincodes';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();				
			
		$this->load->view('admin/pincodes',$data);
		}else{
		redirect('admin');
		}
	}
	
	public function pincode_action($id=''){
		if($this->session->userdata('admin_session')){	
			if(isset($_POST['submit'])){
				
				if($id != ''){
				$data = array(
				'pincode' => $this->input->post('pincode')
				);
				
				$where	= array('pid'=>$id);	
				$this->my_model->update('pincodes',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'pincode' => $this->input->post('pincode'),
				'created_on'	=> date('Y-m-d')
				);
				
				$this->my_model->save('pincodes',$data);
				$this->session->set_flashdata('message','added');				
				}
				
				redirect('admin/pincodes');
				
			}else{
				if ($id != "") {
				$where = array('pid'=>$id);				
				$data["cg_data"] = $this->my_model->check('pincodes',$where)->row_array();				
				$this->load->view("admin/add_pincode", $data);
				} else {					
					$data['pincodes'] = $this->my_model->get('pincodes')->result_array();					
				$this->load->view("admin/add_pincode",$data);
				}
			}					
		}else{
		redirect('admin');
		}
	}
	
	
	
	
	
	
	public function delete_dt($id) {		
		if($this->session->userdata('admin_session')){			
		$where = array('dltid'=>$id);
		$res = $this->db->delete('delivery_types', array('dltid' => $id));
		if ($res) {		
		$this->session->set_flashdata("message", "deleted");
		} else {
		$this->session->set_flashdata("message", "error");
		}
		redirect("/admin/delivery_types/");
		} else {
		redirect("admin/login");
		}
	}
	
	public function delivery_types(){
		if($this->session->userdata('admin_session')){		
		$data['delivery_types'] = $this->my_model->get('delivery_types')->result_array();			
		$this->load->view('admin/delivery_types',$data);
		}else{
		redirect('admin');
		}
	}
	
	public function dltype_action($id=''){
		if($this->session->userdata('admin_session')){	
			if(isset($_POST['submit'])){
				
				if($id != ''){
				$data = array(
				'delivery_type' => $this->input->post('delivery_type'),
				'days' => $this->input->post('days'),
				'price' => $this->input->post('price')
				);
				
				$where	= array('dltid'=>$id);	
				$this->my_model->update('delivery_types',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'delivery_type' => $this->input->post('delivery_type'),
				'days' => $this->input->post('days'),
				'price' => $this->input->post('price')
				);
				
				$this->my_model->save('delivery_types',$data);
				$this->session->set_flashdata('message','added');				
				}
				
				redirect('admin/delivery_types');
				
			}else{
				if ($id != "") {
				$where = array('dltid'=>$id);				
				$data["cg_data"] = $this->my_model->check('delivery_types',$where)->row_array();				
				$this->load->view("admin/add_delivery_type", $data);
				} else {					
					$data['delivery_types'] = $this->my_model->get('delivery_types')->result_array();					
				$this->load->view("admin/add_delivery_type",$data);
				}
			}					
		}else{
		redirect('admin');
		}
	}
	
	public function feedbacks(){
		if($this->session->userdata('admin_session')){		
		$data['feedbacks'] = $this->my_model->get('feedback')->result_array();			
		$this->load->view('admin/feedbacks',$data);
		}else{
		redirect('admin');
		}
	}
	
	public function dealers(){
		//echo $this->uri->segment(2);exit;
		if($this->session->userdata('admin_session')){		
		$pagination = array();
        $limit_per_page = 10;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->my_model->count('dealers');
		
			$data['dealers'] = $this->my_model->get_ls('dealers',$limit_per_page, $start_index)->result_array();
			//print_r(($data['dealers']));	exit;
			$config['base_url'] = base_url() . 'admin/dealers';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();	
			
		$this->load->view('admin/dealers',$data);
		}else{
		redirect('admin');
		}
	}
	
	public function dealer_action($id=''){
		if($this->session->userdata('admin_session')){	
			if(isset($_POST['submit'])){
				if (!empty($_FILES['shop_photo']['name'])) {
				$config['upload_path'] = 'assets/uploads/shops/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPG';
				$config['encrypt_name'] = TRUE;
				$config['max_size'] = 10000;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('shop_photo')) {
				$error = array(
				'error' => $this->upload->display_errors()
				);
				$this->session->set_flashdata("succuss", $error['error']);
				print_r($this->upload->display_errors());
				} else {
				$upload_data = $this->upload->data();
				$shop_photo = $upload_data['file_name'];
				}
				} else {
				$shop_photo = $this->input->post('old_shop_image');
				}
							
				if($id != ''){
				$data = array(
				'dealer_name' => $this->input->post('name'),	
				'store_name' => $this->input->post('store_name'),				
				'email' => $this->input->post('email'),		
				'mobile' => $this->input->post('mobile'),		
				'shop_image' => $shop_photo,
				'flat_no' => $this->input->post('flatno'), 
				'state' => $this->input->post('state'), 
				'city' => $this->input->post('city'),
				'colony' => $this->input->post('colony'),
				'pincode' => $this->input->post('pincode'),
				'pan_number' => $this->input->post('pancard'), 
				'gst_number' => $this->input->post('gst'), 
				'cancelled_cheque' => $this->input->post('cc')
				);
				
				$where	= array('did'=>$id);
				$this->my_model->update('dealers',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'dealer_name' => $this->input->post('name'),
				'store_name' => $this->input->post('store_name'),				
				'email' => $this->input->post('email'),		
				'mobile' => $this->input->post('mobile'),						
				'shop_image' => $shop_photo,
				'flat_no' => $this->input->post('flatno'), 
				'state' => $this->input->post('state'), 
				'city' => $this->input->post('city'),
				'colony' => $this->input->post('colony'),
				'pincode' => $this->input->post('pincode'),
				'pan_number' => $this->input->post('pancard'), 
				'gst_number' => $this->input->post('gst'), 
				'cancelled_cheque' => $this->input->post('cc'),
				'password' => base64_encode($this->input->post('password')),  
				'created_on'=>date('Y-m-d')
				);
				
				$this->my_model->save('dealers',$data);
				$this->session->set_flashdata('message','added');				
				}
				redirect('admin/dealers');
				
			}else{
				if ($id != "") {
				$where = array('did'=>$id);
				$data['states'] = $this->my_model->get_states();
				$data['cities'] = $this->my_model->get_cities();
				$data["dealer_data"] = $this->my_model->check('dealers',$where)->row_array();				
				$this->load->view("admin/add_dealer", $data);
				} else {					
					$data['states'] = $this->my_model->get_states();					
				$this->load->view("admin/add_dealer",$data);
				}
			}					
		}else{
		redirect('admin');
		}
	}
	
	
	public function user_search(){
		$data['states'] = $this->my_model->get_states();
		$this->load->view('admin/user_search',$data);
	}
	
	public function search_users(){
		$state = $this->input->post('state');
		$city = $this->input->post('city');
		$table = $this->input->post('table');
		$where = array('state'=>$state,'city'=>$city);
		$data['users'] = $this->my_model->check($table,$where)->result_array();
		$data['user_type'] = $table;
		//print_r($data);exit;
		$this->load->view('admin/user_search_results',$data);
	}
	
	public function reply_feedback(){
		$email = $this->input->post('email');
		$message = $this->input->post('message');
		$res = $this->utility->send_email($email,$message,'Reply To Feedback');
		if($res){
			echo '1';
		}else{
			echo '0';
		}		
	}
	
	
	
	
	public function offers(){
		if($this->session->userdata('admin_session')){		
		$data['Offers'] = $this->my_model->get('offers')->result_array();			
		$this->load->view('admin/offers',$data);
		}else{
		redirect('admin');
		}
	}
	
		public function offer_action($id=''){
		if($this->session->userdata('admin_session')){	
			if(isset($_POST['submit'])){
				if (!empty($_FILES['brand_image']['name'])) {
				$config['upload_path'] = 'assets/uploads/offers/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPG';
				$config['encrypt_name'] = TRUE;
				$config['max_size'] = 10000;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('brand_image')) {
				$error = array(
				'error' => $this->upload->display_errors()
				);
				$this->session->set_flashdata("succuss", $error['error']);
				print_r($this->upload->display_errors());
				} else {
				$upload_data = $this->upload->data();
				$brand_image = $upload_data['file_name'];
				}
				} else {
				$brand_image = $this->input->post('old_image');
				}
				
				$ov = $this->input->post('offer_valid');
				$offerValid = implode(",",$ov);
				
				$op = $this->input->post('products');
				$products = implode(",",$op);
				
				if($id != ''){
				$data = array(
				'offer_name' => $this->input->post('offer_name'),				
				'offer_type' => $this->input->post('offer_type'),				
				'vp' => $this->input->post('vp'),				
				'offer_valid' => $offerValid,				
				'products' => $products,				
				'offer_validity_from' => date('Y-m-d',strtotime($this->input->post('offer_validity_from'))),				
				'offer_validity_to' => date('Y-m-d',strtotime($this->input->post('offer_validity_to'))),				
				'offer_on' => $this->input->post('offer_on'),				
				'oimage' => $brand_image
				);
				
				$where	= array('oid'=>$id);	
				$this->my_model->update('offers',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'offer_name' => $this->input->post('offer_name'),				
				'offer_type' => $this->input->post('offer_type'),				
				'vp' => $this->input->post('vp'),				
				'offer_valid' => $offerValid,	
				'products' => $products,					
				'offer_validity_from' => $this->input->post('offer_validity_from'),				
				'offer_validity_to' => $this->input->post('offer_validity_to'),						
				'offer_on' => $this->input->post('offer_on'),				
				'oimage' => $brand_image,
				'created_on' => date('Y-m-d')
				);
				
$message = 'New offer '.$this->input->post('offer_name').' is added';
$msg = array('message'=>$message);
$title = 'Test Notification';
$dealers = $this->my_model->get('dealers')->result_array();
for($i=0;$i<count($dealers);$i++){
	if($dealers[$i]['device_name'] == 'Android'){
		$token = $dealers[$i]['device_token'];
		$this->my_model->save('app_notifications',array('dealer_id'=>$dealers[$i]['did'],'title'=>'Shopping','msg'=>$message,'date'=>date('Y-m-d')));
		$this->push_notifications->send_notification($token,$msg,$title);			
	}else{
		
	}
}
		
		
				//print_r($data);exit;
				$this->my_model->save('offers',$data);
				$this->session->set_flashdata('message','added');				
				}
				redirect('admin/offers');
				
			}else{
				if ($id != "") {
					$data['dealers'] = $this->my_model->get('dealers')->result_array();
					$data['brands'] = $this->my_model->get('brands')->result_array();
					$data['products'] = $this->my_model->get('products')->result_array();
				$where = array('oid'=>$id);				
				$data["offer_data"] = $this->my_model->check('offers',$where)->row_array();				
				$this->load->view("admin/add_offer", $data);
				} else {
					$data['dealers'] = $this->my_model->get('dealers')->result_array();
					$data['products'] = $this->my_model->get('products')->result_array();
					$data['brands'] = $this->my_model->get('brands')->result_array();
					$data['states'] = $this->my_model->get_states();					
				$this->load->view("admin/add_offer",$data);
				}
			}					
		}else{
		redirect('admin');
		}
	}
	
	
	public function user_details($table,$id){
		if($this->session->userdata('admin_session')){	
		$pagination = array();
		$limit_per_page = 15;
		//echo $this->uri->segment(3);exit;
		$start_index = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$total_records = $this->my_model->count('products');	
			
		$data['products'] = $this->my_model->get_ls('products',$limit_per_page, $start_index)->result_array();
		for($i=0;$i<count($data['products']);$i++){
			$data['products'][$i]['brand'] = $this->my_model->check('brands',array('bid'=>$data['products'][$i]['brand']))->row()->brand_name;
		}
		
		$config['base_url'] = base_url() . 'admin/user_details/'.$table.'/'.$id.'';
		$config['total_rows'] = $total_records;
		$config['per_page'] = $limit_per_page;
		$config["uri_segment"] = 5;       
		$config['next_link'] = 'Next';		
		$config['prev_link'] = 'Previous';			
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();
			if($table == 'catalysts'){
				$where = array('cid'=>$id);
			}else if($table == 'franchise_partners'){
				$where = array('fid'=>$id);
			}else if($table == 'dealers'){
				$where = array('did'=>$id);
			}else if($table == 'delivery_partners'){
				$where = array('dlvid'=>$id);
			}	
			$data['udetails'] = $this->my_model->check($table,$where)->row_array();
			$data['table'] = $table;
			
			$this->load->view('admin/details_view',$data);
		}else{
		redirect('admin');
		}
	}
	
	public function working(){
		$this->load->view('admin/takes_time');
	}
	
	
	public function dealer_orders(){
		if($this->session->userdata('admin_session')){
			$pagination = array();
			$limit_per_page = 10;
			//echo $this->uri->segment(3);exit;
			$start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$total_records = $this->catalyst_model->count_with_condition('orders',array('order_from'=>'dealers'));		
			$data['dorders'] = $this->catalyst_model->condition_and_limit_query('orders',array('order_from'=>'dealers'),$limit_per_page, $start_index)->result_array();			
			for($i=0;$i<count($data['dorders']);$i++){
				$where = "oid=".$data['dorders'][$i]['oid']." and fp_status != '2'";
				$data['dorders'][$i]['assigned_fp'] = $this->catalyst_model->check('fp_assigned_orders',$where)->row()->fp_id;
			}
			$config['base_url'] = base_url() . 'admin/dealer_orders';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();
			//print_r($data);exit;
			$this->load->view('admin/dorders',$data);
		}else{
		redirect('admin');	
		}
	}
	
	
	//For FP Orders
	public function fp_orders(){
		if($this->session->userdata('admin_session')){
			$pagination = array();
			$limit_per_page = 10;
			//echo $this->uri->segment(3);exit;
			$start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$total_records = $this->catalyst_model->count_with_condition('orders',array('order_from'=>'fps'));		
			$data['forders'] = $this->catalyst_model->condition_and_limit_query('orders',array('order_from'=>'fps'),$limit_per_page, $start_index)->result_array();			
			
			$config['base_url'] = base_url() . 'admin/fp_orders';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["links"] = $this->pagination->create_links();
			//print_r($data);exit;
			$this->load->view('admin/forders',$data);
		}else{
		redirect('admin');	
		}
	}
	
	
	public function invoice($order_id){
		$where = array('oid'=>$order_id);			
		$details = $this->my_model->check('orders',$where)->row_array();
		//print_r($details['uid']);exit;
		$idet = str_replace('},', '}&', $details['items']);
		$item_split_details = explode("&",$idet);
		$data['items'] = [];
		for($i=0;$i<count($item_split_details);$i++){
			$item_details[$i] = json_decode($item_split_details[$i],true);
			array_push($data['items'],$item_details[$i]);
		}	
		for($k=0;$k<count($data['items']);$k++){
			$total_items_cost = $data['items'][$k]['count']*$data['items'][$k]['item_cost'];
			$data['total_cost'] += $total_items_cost;
		}
		$data['order_details'] = $details;
		
			if($details['order_from'] == 'dealers'){
			$data['user_name'] = $this->my_model->check('dealers',array('did'=>$details['uid']))->row()->dealer_name;
			$data['user_email'] = $this->my_model->check('dealers',array('did'=>$details['uid']))->row()->email;
			$data['user_mobile'] = $this->my_model->check('dealers',array('did'=>$details['uid']))->row()->mobile;
			}else{
			$data['user_name'] = $this->my_model->check('franchise_partners',array('fid'=>$details['uid']))->row()->dealer_name;
			$data['user_email'] = $this->my_model->check('franchise_partners',array('fid'=>$details['uid']))->row()->email;			
			$data['user_mobile'] = $this->my_model->check('franchise_partners',array('fid'=>$details['uid']))->row()->mobile;			
			}
		
		//print_r($data);exit;
		$this->load->view('admin/invoice',$data);
	}
	
	
	
	public function delete_order() {				
		$id = $this->input->post('id');
		if($this->session->userdata('admin_session')){	
			$where = array('oid'=>$id);
			$res = $this->db->delete('orders', $where);		
			if ($res) {		
			echo '1';
			} else {
			echo '0';
			}
		} else {
		redirect("admin/login");
		}
	}
	
	
	public function get_lat_long(){
		$address = $this->input->post('end_location');
        $prepAddr = str_replace(' ','+',$address);
        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
        $output= json_decode($geocode);
        $latitude = $output->results[0]->geometry->location->lat;
        $longitude = $output->results[0]->geometry->location->lng;		
		$latlngdata = array('lat'=>$latitude,'lng'=>$longitude);
		print_r(json_encode($latlngdata));
	}	 
	
	public function get_eta($lat1, $lat2, $long1, $long2)
	{
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=pl-PL";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    //$dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
    $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

    return array('time' => $time);
	}
	
	
	public function change_route_status(){		
		$id = $this->input->post('id');
		$status=$this->input->post('status');
		$where = array('id'=>$id);
		$zero = '0';
		$one = '1';
		if($status == '0'){
		$update = array('rstatus'=>'1');		
		echo '<button type="button" class="btn btn-info btn-xs m-b-10 m-l-5" onClick="change_route_status('. $id . ',' . $one . ')"  >Active</button>';
		}else{
		$update = array('rstatus'=>'0');	
		echo '<button type="button" class="btn btn-warning btn-xs m-b-10 m-l-5" onClick="change_route_status('. $id . ',' . $zero. ')"  >Inactive</button>';
		}
		$this->my_model->update('routes',$where,$update);			
	}
	
	
	public function change_rider_status(){		
		$id = $this->input->post('id');
		$status=$this->input->post('status');
		$where = array('rid'=>$id);
		$zero = '0';
		$one = '1';
		if($status == '0'){
		$update = array('rstatus'=>'1');		
		echo '<button type="button" class="btn btn-info btn-xs m-b-10 m-l-5" onClick="change_rider_status('. $id . ',' . $one . ')"  >Active</button>';
		}else{
		$update = array('rstatus'=>'0');	
		echo '<button type="button" class="btn btn-warning btn-xs m-b-10 m-l-5" onClick="change_rider_status('. $id . ',' . $zero. ')"  >Inactive</button>';
		}
		$this->my_model->update('riders',$where,$update);			
	}
	
	//Bulk Upload CSV Files for Catalysts,Franchise Partners,Delivery Partners and Dealers
	public function csvuploadfile($table){
        $config['upload_path'] = 'assets/uploads/csv_bulk_upload_files/';
        $config['allowed_types'] = 'csv';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = '1000';
 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
 
        // If upload failed, display error
        if (!$this->upload->do_upload('csv')) {
            $data['error'] = $this->upload->display_errors();
            echo json_encode($data);exit;
        } else {
            $file_data = $this->upload->data();
            $file_path =  base_url().'assets/uploads/csv_bulk_upload_files/'.$file_data['file_name'];
        }
     
        $this->readExcel_skills($file_path,$table);
    }
	public function readExcel_skills($file_path,$table) {
        $this->load->database();
        $this->load->library('csvreader');

        $result = $this->csvreader->parse_file($file_path); 
		//print_r($result);exit;
        $csvData = $result;
        $i=0;                 
        $j=0;
        foreach($csvData as $data)
        {
            $this->db->insert($table,$data);
			  
            $result=($this->db->affected_rows() != 1) ? false : true;
           if($result)
           {
               $j++;
           }else{
               $i++;
           }
               
        }
      $final['success']=$j;
      $final['unsuccess']=$i;
     //echo json_encode($final); exit;        
	  $this->session->set_flashdata("message", "csv_success");
		redirect("/admin/".$table."/");
    }
	
	
	public function update_product_offline_consumption(){
		$product_id = $this->input->post('product_id');
		$offline_consumption = $this->input->post('offline_consumption');
		$res = $this->my_model->update('products',array('pid'=>$product_id),array('offline_consumption'=>$offline_consumption));
		if($res){
			$poc = $this->my_model->check('products',array('pid'=>$product_id))->row();
			$closing = $poc->quantity-($poc->online_consumption+$poc->offline_consumption);
			$details = json_encode(array('status'=>'1','offline_consumption'=>$poc->offline_consumption,'closing'=>$closing),true);
			print_r($details);
		}else{
			$details = json_encode(array('status'=>'0'),true);
			print_r($details);
		}
	}
	
	
	
	
	
	
	// Colours
	public function colours(){
		if($this->session->userdata('admin_session')){	

		$pagination = array();
		$limit_per_page = 10;
		//echo $this->uri->segment(3);exit;
		$start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$total_records = $this->my_model->count('colours');	

		$data['colours'] = $this->my_model->get_ls('colours',$limit_per_page, $start_index)->result_array();
		for($i=0;$i<count($data['colours']);$i++){
			$data['colours'][$i]['colour_type'] = $this->my_model->check('colour_types',array('ctid'=>$data['colours'][$i]['colour_type']))->row()->colour_type;
			$data['colours'][$i]['colour_brand'] = $this->my_model->check('brands',array('bid'=>$data['colours'][$i]['colour_brand']))->row()->brand_name;
			$data['colours'][$i]['colour_group_name'] = $this->my_model->check('colour_groups',array('cgid'=>$data['colours'][$i]['colour_group']))->row()->colour_group_name;
		}
		
		$config['base_url'] = base_url() . 'admin/colours';
		$config['total_rows'] = $total_records;
		$config['per_page'] = $limit_per_page;
		$config["uri_segment"] = 3;       
		$config['next_link'] = 'Next';		
		$config['prev_link'] = 'Previous';			
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();

		$this->load->view('admin/colours',$data);
		}else{
		redirect('admin');
		}
	}
	
	public function colours_action($id=''){
		if($this->session->userdata('admin_session')){	
			if(isset($_POST['submit'])){
				if (!empty($_FILES['brand_image']['name'])) {
				$config['upload_path'] = 'assets/uploads/colours/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPG';
				$config['encrypt_name'] = TRUE;
				$config['max_size'] = 10000;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('brand_image')) {
				$error = array(
				'error' => $this->upload->display_errors()
				);
				$this->session->set_flashdata("succuss", $error['error']);
				print_r($this->upload->display_errors());
				} else {
				$upload_data = $this->upload->data();
				$colour_image = $upload_data['file_name'];
				}
				} else {
				$colour_image = $this->input->post('old_image');
				}
				
		
				
				
				if($id != ''){
				$data = array(
				'colour_code' => $this->input->post('code'),
				'source' => $this->input->post('source'),
				'colour_group' => $this->input->post('colour_group'),
				'colour_name' => $this->input->post('name'),
				'colour_type' => $this->input->post('colour_type'),
				'colour_brand' => $this->input->post('colour_brand'),
				'price' => $this->input->post('price'),
				'description' => $this->input->post('desc'),
				'features' => $this->input->post('features'),
				'benefits' => $this->input->post('benefits'),
				'colour_image' => $colour_image,
				'book_ref' => $this->input->post('book_ref'),
				'card_ref' => $this->input->post('card_ref'),
				'moq' => $this->input->post('moq')
				);
				
				$where	= array('cid'=>$id);	
				$this->my_model->update('colours',$where,$data);
				$this->session->set_flashdata('message','updated');				
				}else{
				$data = array(
				'colour_code' => $this->input->post('code'),
				'source' => $this->input->post('source'),
				'colour_group' => $this->input->post('colour_group'),
				'colour_name' => $this->input->post('name'),
				'colour_type' => $this->input->post('colour_type'),
				'colour_brand' => $this->input->post('colour_brand'),
				'price' => $this->input->post('price'),
				'description' => $this->input->post('desc'),
				'features' => $this->input->post('features'),
				'benefits' => $this->input->post('benefits'),
				'colour_image' => $colour_image,
				'book_ref' => $this->input->post('book_ref'),
				'card_ref' => $this->input->post('card_ref'),
				'moq' => $this->input->post('moq'),
				'created_on' => date('Y-m-d')
				);
				
				$this->my_model->save('colours',$data);
				$this->session->set_flashdata('message','added');				
				}
				//print_r($data);exit;
				redirect('admin/colours');
				
			}else{
				if ($id != "") {
				$where = array('cid'=>$id);	
					$data['sources'] = $this->my_model->get('source_countries')->result_array();					
					$data['groups'] = $this->my_model->get('colour_groups')->result_array();					
					$data['types'] = $this->my_model->get('colour_types')->result_array();					
					$data['brands'] = $this->my_model->get('brands')->result_array();					
					$data['colour_types'] = $this->my_model->get('colour_types')->result_array();					
				$data["colour_data"] = $this->my_model->check('colours',$where)->row_array();				
				$this->load->view("admin/add_colour", $data);
				} else {	
					$data['sources'] = $this->my_model->get('source_countries')->result_array();					
					$data['groups'] = $this->my_model->get('colour_groups')->result_array();					
					$data['types'] = $this->my_model->get('colour_types')->result_array();					
					$data['brands'] = $this->my_model->get('brands')->result_array();					
					$data['colour_types'] = $this->my_model->get('colour_types')->result_array();					
				$this->load->view("admin/add_colour",$data);
				}
			}					
		}else{
		redirect('admin');
		}
	}
	
	public function csv_templates(){
		$data['csvs'] = $this->my_model->get('csv_templates')->result_array();
		$this->load->view('admin/csv_template',$data);
	}
	
	
	public function gst(){
		if($this->session->userdata('admin_session')){	
		$admindata = $this->session->userdata("admin_session");
		$id = $admindata['id'];
		$where	= array('id'=>'1'); 		
		//print_r($where);exit;
			if(isset($_POST['submit'])){
				$data = array(
				'cgst' => $this->input->post('cgst'),
				'sgst' => $this->input->post('sgst')
				);
				$this->my_model->update('gst',$where,$data);
				$this->session->set_flashdata('message','updated');
					
				redirect('admin/gst');
				
			}else{
			$data['gstdetails'] = $this->my_model->check('gst',$where)->row_array();
			//print_r($data);exit;
			$this->load->view('admin/gst',$data);
			}
		}else{
		redirect('admin');
		}
	}
	
	
	
	public function sales(){
		$pagination = array();
		$limit_per_page = 10;
		//echo $this->uri->segment(3);exit;
		$start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$total_records = $this->catalyst_model->count_with_condition('orders',array('order_from'=>'dealers'));		
		$data['dorders'] = $this->catalyst_model->condition_and_limit_query('orders',array('order_from'=>'dealers'),$limit_per_page, $start_index)->result_array();			
		for($i=0;$i<count($data['dorders']);$i++){
		$where = "oid=".$data['dorders'][$i]['oid']." and fp_status != '2'";
		$data['dorders'][$i]['assigned_fp'] = $this->catalyst_model->check('fp_assigned_orders',$where)->row()->fp_id;
		}
		$config['base_url'] = base_url() . 'admin/sales';
		$config['total_rows'] = $total_records;
		$config['per_page'] = $limit_per_page;
		$config["uri_segment"] = 3;       
		$config['next_link'] = 'Next';		
		$config['prev_link'] = 'Previous';			
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();
		
		//Fp Orders
			$fp_pagination = array();
			$limit_per_page = 10;
			//echo $this->uri->segment(3);exit;
			$start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$total_records = $this->catalyst_model->count_with_condition('orders',array('order_from'=>'fps'));		
			$data['forders'] = $this->catalyst_model->condition_and_limit_query('orders',array('order_from'=>'fps'),$limit_per_page, $start_index)->result_array();			
			
			$config['base_url'] = base_url() . 'admin/sales';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;       
			$config['next_link'] = 'Next';		
			$config['prev_link'] = 'Previous';			
            $this->pagination->initialize($config);
			$data["fp_pagination_links"] = $this->pagination->create_links();
		
		$this->load->view('admin/sales',$data);
	}
	
	

	
}
