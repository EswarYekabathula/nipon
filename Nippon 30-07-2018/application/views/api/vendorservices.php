<style>
a {
 text-decoration:none;
}

</style>

<h2 style="text-align:center">HIL Plumber Services</h2>

<h2 style="text-align:center">API-KEY = 891219 </h2>

<h2 style="text-align:center">Base URL = http://testingmadesimple.org/HIL/api/vendor/ </h2>



<h3>1. Registration </h3>
<h2><a href="javascript:">register</a></h2>
Method : POST </br></br>
Parameters: name,,,email,,,mobile,,,password,,,dob(1994-05-04),,,,device_token,,,device_name,,,API-KEY </br></br>
 </br>
 
 
<h3>2. Resend OTP (works for only 3 times) </h3>
<h3><a href="javascript:">resendotp</a></h3>
Method : POST </br></br>
Parameters: mobile,,,API-KEY </br></br>
</br>

 
<h3>3. Verify OTP </h3>
<h3><a href="javascript:">verifyotp</a></h3>
Method : POST </br></br>
Parameters: mobile,,,otp,,,auth_key,,,,,API-KEY </br></br>
</br>


<h3>4. Login </h3>
<h2><a href="javascript:">login</a></h2>
Method : POST </br></br>
Parameters: mobile,,,password,,,lattitude,,,longitude,,,location,,,city,,,API-KEY </br></br>
</br>


<h3>5. Upload Profile </h3>
<h2><a href="javascript:">upload_profile</a></h2>
Method : POST </br></br>
Parameters: user_id,,,profile_picture,,,auth_key,,,API-KEY </br></br>
</br>


<h3>6. Users </h3>
<h2><a href="javascript:">users</a></h2>
Method : GET </br></br>
Parameters: lat,,,lng,,,API-KEY </br></br>
Ex: http://testingmadesimple.org/HIL/api/vendor/users?lat=17.494793&lng=78.399644&API-KEY=891219 </br></br>
</br>



<h3>7. Call History </h3>
<h2><a href="javascript:">call_history</a></h2>
Method : GET </br></br>
Parameters: plumber_id,,,,auth_key,,,,API-KEY </br></br>
Ex: http://testingmadesimple.org/HIL/api/vendor/call_history?plumber_id=13&API-KEY=891219 </br></br>
</br>



<h3>8. Add Appointment </h3>
<h2><a href="javascript:">add_appointment</a></h2>
Method : POST </br></br>
Parameters: plumber_id,,,name,,,mobile,,,date,,,time,,,API-KEY   (Date format 2018-04-18) </br></br>
</br>


<h3>9. Appointments  </h3>
<h2><a href="javascript:">appointments</a></h2>
Method : GET </br></br>
Parameters: plumber_id,,,,auth_key,,,API-KEY </br></br>
Ex: http://testingmadesimple.org/HIL/api/vendor/appointments?plumber_id=13&API-KEY=891219 </br></br>
</br>



<h3>10. Add Address </h3>
<h2><a href="javascript:">add_address</a></h2>
Method : POST </br></br>
Parameters: plumber_id,,,address,,,lattitude,,,longitude,,,open_time,,,close_time,,,auth_key,,,API-KEY    </br></br>
</br>



<h3>11. Call </h3>
<h2><a href="javascript:">call</a></h2>
Method : GET </br></br>
Parameters: user_id,,,,plumber_id,,,,auth_key,,,API-KEY </br></br>
Ex: http://testingmadesimple.org/HIL/api/vendor/call?plumber_id=1&user_id=2&API-KEY=891219 </br></br>
</br>


<h3>12. Upload Aadhar front pic </h3>
<h2><a href="javascript:">upload_aadhar_front</a></h2>
Method : POST </br></br>
Parameters: user_id,,,,aadhar_front,,,,auth_key,,,API-KEY </br></br>
</br>


<h3>13. Upload Aadhar back pic </h3>
<h2><a href="javascript:">upload_aadhar_back</a></h2>
Method : POST </br></br>
Parameters: user_id,,,,aadhar_back,,,,auth_key,,,API-KEY </br></br>
</br>



<h3>14. Edit profile (Name and mobile)</h3>
<h2><a href="javascript:">edit_profile</a></h2>
Method : POST </br></br>
Parameters: plumber_id (pid),,,name,,,mobile,,,auth_key,,,API-KEY </br></br>
</br>


<h3>15. change password</h3>
<h2><a href="javascript:">change_password</a></h2>
Method : POST </br></br>
Parameters: plumber_id(pid),,,oldpassword,,,newpassword,,,auth_key,,,API-KEY </br></br>
</br>


