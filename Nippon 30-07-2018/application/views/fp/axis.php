<?php include('includes/header.php'); 
//print_r($this->session->userdata('order_data'));
?>

<style>
button.qty-change{
    height: 30px;
    width: 30px;
    border-radius: 50%;
    background: #fff;
    border: 1px solid #e3e3e3;
}
.form-control {
     border: 0px solid #ced4da !important; 
}
.form-group {
margin-bottom:0px !important;
    height: 40px !important; 
}

.qty-input{
    height: 30px;
    width: 28px;
    border: 0px !important;
    padding: 5px;
}
.borderless td, .borderless th {
    border: none;
}
.table thead th{
	 border-bottom: none !important;
	
}
</style>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
         <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
          
            <!-- Container fluid  -->
            <div class="container-fluid">
			                <div class="row">
<div class="col-lg-12">
<div class="card" style="height: auto;!important">
<div class="card-body">
<div class="w3-content"><center>
<img class="mySlides" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSocad5xCPGSfTJyK2Syyftglmecg0N018Te6kdKYX_Za1qVhAZ" style="width:15%"></center>
</div>
 
<div class="form-validation" style="margin-left:325px !important">
<form action="<?php echo base_url(); ?>fp/payment_process" method="POST"> 
<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Total Amount </label>
<div class="col-lg-6">
<input type="text" class="form-control"  value="<?php echo $gstDetails['total_cost']; ?>" />
</div>
</div>



<input type="hidden" class="form-control" name="orderId" value="<?php print_r($this->session->userdata('order_data')['order_id']); ?>" />



<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">CGST (<?php echo $gstDetails['cgst']."%"; ?>)</label>
<div class="col-lg-6">
<input type="text" class="form-control"  value="<?php echo $gstDetails['cgst_added_amount']; ?>" />
</div>
</div>


<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">SGST (<?php echo $gstDetails['sgst']."%"; ?>)</label>
<div class="col-lg-6">
<input type="text" class="form-control"  value="<?php echo $gstDetails['sgst_added_amount']; ?>" />
</div>
</div>


<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">GST Amount</label>
<div class="col-lg-6">
<input type="text" class="form-control"  value="<?php echo $gstDetails['total_gst_amount']; ?>" />
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Total Amount With GST</label>
<div class="col-lg-6">
<input type="text" class="form-control" name="amount"  value="<?php echo round($gstDetails['total_with_gst']); ?>" />
</div>
</div>


<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">CustomerID </label>
<div class="col-lg-6">
<input type="text" class="form-control"   name="CustomerId"  value="<?php print_r($user_details['fid']); ?>" />
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">ProductID </label>
<div class="col-lg-6">
<input type="text" class="form-control"   name="product_id" value="<?php print_r($user_details['fid']); ?>" />
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Email ID </label>
<div class="col-lg-6">
<input type="text" class="form-control" name="customer_email" value="<?php print_r($user_details['email']); ?>" />
</div>
</div>


<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Mobile </label>
<div class="col-lg-6">
<input type="text" class="form-control" name="customer_phone" value="<?php print_r($user_details['phone']); ?>" />
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Name </label>
<div class="col-lg-6">
<input type="text" class="form-control"  name="billing_address_first_name"  value="<?php print_r($user_details['name']); ?>" />
</div>
</div>

</br>
<button type='submit' name='submit' class="btn btn-primary">Make Payment</button>

</form>
</div>
</div>
</div>
</div>
</div>
            
				

            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
             <footer class="footer"> &copy; 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->

<!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>




			
</body>
</html>