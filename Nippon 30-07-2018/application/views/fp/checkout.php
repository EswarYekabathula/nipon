<?php include('includes/header.php'); ?>

<style>
button.qty-change{
    height: 30px;
    width: 30px;
    border-radius: 50%;
    background: #fff;
    border: 1px solid #e3e3e3;
}

.qty-input{
    height: 30px;
    width: 28px;
    border: 0px !important;
    padding: 5px;
}
.borderless td, .borderless th {
    border: none;
}
.table thead th{
	 border-bottom: none !important;
	
}
#error-dt-msg{
	color:red;
}
</style>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
         <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Dashboard</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
			<form method="POST" action="<?php echo base_url(); ?>fp/payment">
            <div class="container-fluid">
			
               <div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table borderless ">                                       
                                        <tbody>
										<?php foreach($cdetails as $cval){ ?>
                                            <tr id="removeitem<?php echo $cval['cid']; ?>">
                                                <td><img src="<?php echo $cval['item_image']; ?>" alt="user" class="img-responsive radius" style="width:75px;height:75px"></td>
                                                <td>												
			<span class="text"><?php echo ucwords($cval['item_name']); ?></span>			
			<div class="qty-changer">
				<button type="button" class="quantity-left-minus qty-change" onclick="decrement_value(<?php echo $cval['cid']; ?>,<?php echo $cval['moq']; ?>,<?php echo $cval['item_cost']; ?>);" >-</button>
				<input class="qty-input form-group input-number" type="text" id="quantity<?php echo $cval['cid']; ?>" name="quantity" value="<?php echo $cval['count']; ?>" min="<?php echo $cval['moq']; ?>" max="100">
				<button type="button" class="quantity-right-plus qty-change" onclick="increment_value(<?php echo $cval['cid']; ?>,<?php echo $cval['item_cost']; ?>);" >+</button>
			</div>			
												</td>
												
                                                <td id="item_total_cost<?php echo $cval['cid']; ?>"><i class="fa fa-inr" aria-hidden="true">&nbsp;<?php echo $cval['total_cost']; ?></i></td>
												<td><i class="fa fa-trash btn btn-danger"  onclick="removeCart(<?php echo $cval['cid']; ?>);"></i></td>
                                            </tr>
										<?php } ?>	
                                        </tbody>
                                    </table>
									
                                </div>
                            </div>
                                </div>
                            </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Payment Summary</h4>
                                    <!-- Nav tabs -->
                                    <div class="table-responsive">
                                    <table class="table borderless ">
                                        <tbody>
											<!--<tr>
												<td>Delivery Type</td>
												<td>
												<div class="form-group">
												<select class="form-control" id="delivery_type" name="delivery_type" onchange="update_amount_with_dt();">
												<option value="">Select Delivery Type</option>
												<?php foreach($delivery_types as $dval){ ?>
												<option value="<?php echo $dval['dltid']; ?>"><?php echo $dval['delivery_type']." ".$dval['days']."  Rs".$dval['price']; ?></option>
												<?php } ?>
												</select>
												</div>
												</td>
											</tr> -->
                                            <tr>
                                                <td>Sub Total</td>
                                                <td class="color-primary" id="total_cost"><?php echo '&#x20b9; '.$total_cost; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
				<span id="error-dt-msg"></span>
                <button  type="submit" class="btn btn-primary">Checkout with axis</button>
            </div>
			</form>
            <!-- End Container fluid  -->
            <!-- footer -->
             <footer class="footer"> &copy; 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->

<!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>



<script>
// To Update total amount with delivery type
function update_amount_with_dt(){
	var base_url = '<?php echo base_url(); ?>';
	var dt = $("#delivery_type").val();
	jQuery.ajax({
		type:'POST',
		data:{dt:dt},
		url:base_url+'fp/get_dt_price',
		success:function(data){
			
		}
	}); 
}
</script>

<script>  
function check_dt(){	
	var dt = $("#delivery_type").val();	
	if (dt== "" || dt == "null"){
		$("#error-dt-msg").html('Please select delivery_type');
		return false;
	}else{
		$("#error-dt-msg").html('');
		return true;
	}
}
</script>  
<script>  
function increment_value(id,item_cost){
	var bu = '<?php echo base_url(); ?>';
	var quantity = 0;
	var quantity = parseInt($('#quantity'+id).val());
	$('#quantity'+id).val(quantity + 1);
	var count = quantity + 1;
	jQuery.ajax({
		type:'POST',
		data:{id:id,count:count,item_cost:item_cost},
		url:bu+'fp/update_cart',
		success:function(data){
			var result = JSON.parse(data);
			$('#quantity'+id).val(result['count']);
			$("#item_total_cost"+id).html(result['item_total_cost']);
			$("#total_cost").html(result['total_cost']);
		}
	});
}

function decrement_value(id,dec_val,item_cost){
	var bu = '<?php echo base_url(); ?>';
	var quantity = parseInt($('#quantity'+id).val());
	if(quantity>dec_val){
	$('#quantity'+id).val(quantity - 1);
	var count = quantity - 1;
	jQuery.ajax({
		type:'POST',
		data:{id:id,count:count,item_cost:item_cost},
		url:bu+'fp/update_cart',
		success:function(data){
			var result = JSON.parse(data);
			console.log(result);
			$('#quantity'+id).val(result['count']);
			$("#item_total_cost"+id).html(result['item_total_cost']);
			$("#total_cost").html(result['total_cost']);
		}
	});
	}
}
</script>


			<script>
			function removeCart(cid){
				var base_url = '<?php echo base_url(); ?>';
				var res = confirm('Are you sure you want to remove item from cart ?');
				if(res == true){
					jQuery.ajax({
						type:'POST',
						data:{cart_id:cid},
						url:base_url+'fp/remove_cart',
						success:function(data){
							var result = JSON.parse(data);
							//console.log(result);							
							if(result['status'] = 1){
							$("#removeditem"+cid).hide();
							$("#removeitem"+cid).hide();
							$("#cart_count").html(result['count']);
							$("#total_cost").html(result['total_cost']);
							setTimeout(function() {
		iziToast.success({ message: "Items successfully removed from cart!",   position: "topRight",   zindex:	"99999"		}); }, 100);
							}else{
							setTimeout(function() {
		iziToast.warning({ message: "Please try later!",   position: "topRight",   zindex:	"99999"		}); }, 100);
							}
						}
					});
				}
			}
			</script>
</body>
</html>