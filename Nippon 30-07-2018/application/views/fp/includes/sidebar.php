<div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
<li><a class="" href="<?php echo base_url(); ?>fp" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard </span></a>
<li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-cart-plus"></i><span class="hide-menu">Orders</span></a>
<ul aria-expanded="false" class="collapse">
	<li><a href="<?php echo base_url(); ?>fp/dealer_orders">Dealers Orders</a></li>                                
	<li><a href="<?php echo base_url(); ?>fp/fp_orders">Franchise Partners Orders</a></li>  
</ul>
</li>	
<li><a class="" href="<?php echo base_url(); ?>fp/stocks" aria-expanded="false"><i class="fa fa-line-chart" aria-hidden="true"></i><span class="hide-menu">Stocks </span></a></li>	
<li><a class="" href="<?php echo base_url(); ?>fp/colours" aria-expanded="false"><i class="fa fa-list"></i><span class="hide-menu">Colours </span></a></li>	
<li><a class="" href="<?php echo base_url(); ?>fp/delivery_partners" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu">Dealivery Partners </span></a></li>	
<li><a class="" href="<?php echo base_url(); ?>fp/products" aria-expanded="false"><i class="fa fa-list"></i><span class="hide-menu">Products </span></a></li>	

                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>         