<?php
$fp_id = $this->session->userdata('fp_session')['id'];
$this->load->model('Fp_model','my_model');
$user_id = $this->session->userdata('fp_session')['id'];
?>
<style>
.cart_count{
	margin-top: -10px;
    position: absolute;
    font-size: 15px;
    color: #ef5350;
}
.mailbox .message-center a .mail-contnet {
    padding-left: 25px !important;
}
.fa.pull-right {
    margin-top: -30px !important;
    color: #ef5350 !important;
}
.mailbox .message-center a .mail-contnet .mail-desc {    
    white-space:  unset !important; 
}
</style>
<nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url(); ?>/fp">
                        <!-- Logo icon -->
                        <!--<b><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="homepage" class="dark-logo" /></b>-->
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <!--<span><img src="<?php echo base_url(); ?>assets/images/logo-text.png" alt="homepage" class="dark-logo" /></span>-->
						<b>Xpress Delivery</b>
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <!-- Messages -->
                    
                        <!-- End Messages -->
                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">
					<li class="nav-item dropdown">   
                            <a class="nav-link dropdown-toggle text-muted text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-cart-arrow-down" style="font-size: 20px;"><span class="cart_count" id="cart_count"><?php echo $this->my_model->check('cart',array('uid'=>$user_id,'added_by'=>'FP'))->num_rows(); ?></span></i>
        								<div class="notify"> </div>
        							</a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn">
                                <ul >
                                    <li>
                                        <div class="drop-title">Cart</div>
                                    </li>
                                    <li style="position: relative;width: auto; height:250px; over-y:scroll;">								
                                        <div class="slimScrollDiv" ><div class="message-center" id="cart_list_details">
		<?php 
		$items = $this->my_model->check_cart('cart',array('uid'=>$user_id,'added_by'=>'FP'))->result_array();
			for($i=0;$i<count($items);$i++){		
			
				if($items[$i]['item_type'] == 'Products'){
				$items[$i]['item_name'] = $this->my_model->check('products',array('pid'=>$items[$i]['item_id']))->row()->product_name;
				$item_image = $this->my_model->check('products',array('pid'=>$items[$i]['item_id']))->row()->product_image;
				$items[$i]['item_image'] = base_url().'assets/uploads/products/'.$item_image;
				}else{
				$items[$i]['item_name'] = $this->my_model->check('colours',array('cid'=>$items[$i]['item_id']))->row()->colour_name;
				$item_image = $this->my_model->check('colours',array('cid'=>$items[$i]['item_id']))->row()->colour_image;
				$items[$i]['item_image'] = base_url().'assets/uploads/colours/'.$item_image;	
				}
			}
		?>										
											<?php foreach($items as $ival){ ?>
                                            <a href="#" id="removeditem<?php echo $ival['cid']; ?>">
                                                <img src="<?php echo $ival['item_image']; ?>" style="width:50px;height:50px" />
                                                <div class="mail-contnet">
                                                    <h5><?php echo $ival['item_name']; ?></h5> <span class="mail-desc">Rs. <?php echo $ival['item_cost']; ?></span> 
													<i class="fa fa-trash pull-right" onclick="removefromCart(<?php echo $ival['cid']; ?>);"></i>
                                                </div>												
                                            </a>											
											<?php } ?>
                                          
                                        </div><div class="slimScrollBar" style="background: rgb(220, 220, 220); width: 5px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="<?php echo base_url(); ?>fp/checkout"> <strong>Proceed To Checkout</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
						
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle text-muted text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
							<i class="fa fa-bell" onclick="update_notifications(<?php echo $fp_id; ?>)"></i>
							<div class="notify" id="notify"> 
								<?php 								
								$where = "uid=".$fp_id." and send_to='FP' and view_status='0'";
								$nrows = $this->my_model->count_with_condition('notifications',$where); 
								if($nrows > 0){
								?>
								<span class="heartbit" onclick="update_notifications(<?php echo $fp_id; ?>)"></span> 
								<span class="point" onclick="update_notifications(<?php echo $fp_id; ?>)"></span> 
								<?php } ?>
							</div>
							</a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn" >
                                <ul>
                                    <li>
                                        <div class="drop-title">Notifications</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
											<?php
											$where = "uid=".$fp_id." and send_to='FP'";
											$notifications = $this->my_model->check('notifications',$where)->result_array();
											foreach($notifications as $nval){
											?>
                                            <!-- Message -->
                                            <a href="#">
                                                <div class="btn btn-success btn-circle m-r-10"><i class="ti-calendar"></i></div>
                                                <div class="mail-contnet">
                                                     <span class="mail-desc"><?php echo $nval['message']; ?></span> <span class="time"><?php echo date('d D F Y', strtotime($nval['date'])); ?></span>
                                                </div>
                                            </a>
											<?php } ?>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                       
                        <!-- Profile -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<?php if($this->session->userdata('fp_session')['image'] != ''){ ?>
							<img src="<?php echo base_url(); ?>assets/uploads/users/<?php print_r($this->session->userdata('fp_session')['image']); ?>" alt="user" class="profile-pic" />
							<?php }else{ ?>
							<img src="<?php echo base_url(); ?>assets/uploads/users/default.png" alt="user" class="profile-pic" />
							<?php } ?>							
</a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">
                                    <li><a href="<?php echo base_url(); ?>fp/edit_profile"><i class="ti-user"></i> Profile</a></li>
                                    <!--<li><a href="#"><i class="ti-wallet"></i> Balance</a></li>
                                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                    <li><a href="#"><i class="ti-settings"></i> Setting</a></li>-->
                                    <li><a href="<?php echo base_url(); ?>fp/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
			
			<script>
			function update_notifications(fp_id){
				var base_url = '<?php echo base_url(); ?>';
				var send_to = 'FP';
				jQuery.ajax({
					type:'POST',
					data:{uid:fp_id,send_to:send_to},
					url:base_url+'fp/update_notifications_status',
					success:function(data){
						if(data == 1){
						$("#notify").hide();
						}
					}
				});
			}
			</script>
			
			
			<script>
			function removefromCart(cid){
				var base_url = '<?php echo base_url(); ?>';
				var res = confirm('Are you sure you want to remove item from cart ?');
				if(res == true){
					jQuery.ajax({
						type:'POST',
						data:{cart_id:cid},
						url:base_url+'fp/remove_cart',
						success:function(data){
							var result = JSON.parse(data);
							//console.log(result);							
							if(result['status'] = 1){
							$("#removeditem"+cid).hide();
							$("#cart_count").html(result['count']);
							setTimeout(function() {
		iziToast.success({ message: "Items successfully removed from cart!",   position: "topRight",   zindex:	"99999"		}); }, 100);
							}else{
							setTimeout(function() {
		iziToast.warning({ message: "Please try later!",   position: "topRight",   zindex:	"99999"		}); }, 100);
							}
						}
					});
				}
			}
			</script>
			