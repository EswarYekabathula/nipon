<?php include('includes/header.php'); ?>
<style>
.pagination {
    display: inline-block;
	float:right;
	    margin-top: 10px;
}
.dataTables_info, .dataTables_length {
    display: none !important;
}
div#example23_paginate{
	display:none !important;
}
.pagination a {
    color: black;
	border: 1px solid #ddd; /* Gray */
    float: left;
    padding: 8px 16px;
    text-decoration: none;
}

.pagination  strong {
	color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    background-color: #1976d2;
    color: white;
}
a:not([href]):not([tabindex]) {
    padding: 0px !important;
}
.mtop{
	margin-top:10px !important;
	margin-left:125px !important;
}
</style>
<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
                    <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
<div class="error-page" id="wrapper">
<div class="error-box">
<div class="error-body text-center">
<h1>400</h1>
<h3 class="text-uppercase">Page not found </h3>
<p class="text-muted m-t-30 m-b-30">Please try after some time</p>
<a class="btn btn-info btn-rounded waves-effect waves-light m-b-40" href="index.html">Back to home</a> </div>
<footer class="footer text-center">&copy; 2018 Zebramin.</footer>
</div>
</div>
            <!-- Container fluuid  -->
            <div class="container-fluuid">
			
              
            </div>
            <!-- End Container fluuid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    
	
	



</body>
</html>