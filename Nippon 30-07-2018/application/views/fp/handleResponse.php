<?php include('includes/header.php'); ?>
<?php
	//https://api.juspay.in/order_status?orderId=825010829&merchantId=phptest
	//Get the order Id
	//$orderId = $_GET["order_id"];
	
	echo $orderId = $order_data["order_id"];
	$merchantId = "nippon_test";

	$ch = curl_init('https://axisbank.juspay.in/order_status');
	curl_setopt($ch, CURLOPT_POSTFIELDS ,array('orderId' => $orderId , 'merchantId' => $merchantId ));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    curl_setopt($ch, CURLOPT_USERPWD, 'A18FC27CF1A24FAD8316B015A4F81C68:');

	//get the json response
	$jsonResponse =  json_decode( curl_exec($ch) ); 
	//print_r($jsonResponse);exit;
	//get the information
	$merchantId = $jsonResponse->{'merchantId'};
	$customerId = $jsonResponse->{'customerId'};
	$orderId = $jsonResponse->{'orderId'};
	$status = $jsonResponse->{'status'};
	$statusId = $jsonResponse->{'statusId'};
	$amount = $jsonResponse->{'amount'};
	$refunded = $jsonResponse->{'refunded'};
	$amountRefunded = $jsonResponse->{'amount_refunded'};
	$returnUrl = $jsonResponse->{'return_url'};
	$udf1 = $jsonResponse->{'udf1'};
	$udf2 = $jsonResponse->{'udf2'};
	$udf3 = $jsonResponse->{'udf3'};
	//. . skipping other udf fields
	$txnId = $jsonResponse->{'txnId'};
	$gatewayId = $jsonResponse->{'gatewayId'};
	$bankErrorCode = $jsonResponse->{'bankErrorCode'};
	$bankErrorMessage = $jsonResponse->{'bankErrorMessage'};

?>
<style>
.form-control {
     border: 0px solid #ced4da !important; 
}
.form-group {
margin-bottom:0px !important;
    height: 40px !important; 
}

</style>
<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
                    <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary"><caption><b>Juspay Payment Status</b></caption></h3> </div>
               
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluuid  -->
            <div class="container-fluuid">
			
                <!-- Start Page Content -->
                <div class="row">
                 
<div style="margin: auto;" class="col-lg-6">
<div class="card" style="height: auto;!important">
<div class="card-body">
<div class="form-validation">
<form class="form-valide" action="<?php echo base_url(); ?>admin/edit_profile" method="post" enctype="multipart/form-data">
<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password"><h4>Payment Status : </h4></label>
<div class="col-lg-6">
<p class="form-control"  ><?php echo $status; ?></p>
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password"><h4>Order Id : </h4></label>
<div class="col-lg-6">
<p class="form-control"  ><?php echo $orderId; ?></p>
</div>
</div>



<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password"><h4>Amount : </h4></label>
<div class="col-lg-6">
<p class="form-control"  ><?php echo $amount; ?></p>
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password"><h4>Transaction ID :</h4></label>
<div class="col-lg-6">
<p class="form-control"  ><?php echo $txnId; ?></p>
</div>
</div>
</br>
<a href="<?php echo base_url(); ?>fp/colours" class="btn btn-primary" style="background: #007bff! important;">Continue Shoping</a> 
<a href="<?php echo base_url(); ?>fp" class="btn btn-primary">Home</a>
   

</form>
</div>
</div>
</div>
</div>
					

               
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluuid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    


</body>

</html>