<?php include('includes/header.php'); 
$this->load->model('Fp_model','my_model');
?>
<link rel='stylesheet prefetch' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css'>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<style>
.red{
	color:red;
}
.dataTables_info, .dataTables_length {
    display: none !important;
}
div#example23_paginate{
	display:none !important;
}
.pagination {
    display: inline-block;
	float:right;
	    margin-top: 10px;
}

.pagination a {
    color: black;
	border: 1px solid #ddd; /* Gray */
    float: left;
    padding: 8px 16px;
    text-decoration: none;
}

.pagination  strong {
	color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    background-color: #1976d2;
    color: white;
}
a:not([href]):not([tabindex]) {
    padding: 0px !important;
}

#error-msg{
color: red;
    font-size: 14px;    
    margin-left: 110px;
}
#error-msgaccept{
color: red;
    font-size: 14px;    
    margin-left: 110px;
}
</style>
<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
                    <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Dealer Orders</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dealer Orders</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluuid  -->
            <div class="container-fluuid">
			<?php  if($this->session->flashdata('message') == "success"){ 	 
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Dealer Deleted successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "updated"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Dealer Updated successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "added"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Dealer Added successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}
		?>
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
						
                                <h4 class="card-title">Dealer Orders Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Dealer Name</th>                                                                                                                                                
                                                <th>Delivery Type</th>
												<th>Total Cost</th>												
                                                <th>Assigned Date</th>	
												<th>Delivery Partner</th>														
												<th>FP Status</th>												
												<th>Actions</th>                                    
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Dealer Name</th>   
                                                <th>Delivery Type</th>	
												<th>Total Cost</th>												
                                                <th>Assigned Date</th>	
												<th>Delivery Partner</th>														
                                                <th>FP Status</th>																							
												<th>Actions</th>                                           
                                            </tr>
                                        </tfoot>
<tbody>
	<?php $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3)+1 : 1;
	foreach($dorders as $value){ ?>
		<tr id="row<?php echo $value['id']; ?>">
			<td><?php  echo $start_index++; ?></td>
			<td>
			<?php 
			//print_r($value['oid']);
			$orderDetails = $this->my_model->check('orders',array('oid'=>$value['oid']))->row();
			//print_r($orderDetails->fp_id);
			 echo $this->my_model->check('dealers',array('did'=>$orderDetails->uid))->row()->dealer_name;
			?>
			</td>
			
			
			<td>
			<?php 
			$delivery_type_details =  $this->my_model->check('delivery_types',array('dltid'=>$orderDetails->delivery_type))->row();
			echo $delivery_type_details->delivery_type.' '.('Rs'.$delivery_type_details->price).' '.($delivery_type_details->days);
			?>
			</td>
			
			<td><?php echo $value['total_cost']; ?></td>
			
			<td><?php echo $value['assigned_date']; ?></td>
			
			<td id="assigned_dp_name<?php echo $value['oid'];?>">
			<?php echo $this->my_model->check('delivery_partners',array('dlvid'=>$value['assigned_dp']))->row()->name; ?>
			</td>

		

			<td id="fpstatus<?php echo $value['oid']; ?>">
			<?php if($value['fp_status'] == '1'){ ?>
			<button class="btn btn-primary btn-xs m-b-10 m-l-5">Accepted</button>
			<?php }else if($value['fp_status'] == '2'){ ?>
			<button class="btn btn-danger btn-xs m-b-10 m-l-5">Rejected</button>
			<?php } ?>
			</td>
			
			<td id="assigned_FP<?php echo $value['oid']; ?>">	
			<?php if($value['fp_status'] == '0'){ ?>
			<a href="javascript:accept_order_modal('1',<?php echo $value['oid']; ?>,<?php echo $value['fp_id']; ?>)"  class="btn btn-primary btn-xs m-b-10 m-l-5">Accept</a>
			<a href="javascript:change_order_status('2',<?php echo $value['oid']; ?>,<?php echo $value['fp_id']; ?>)" class="btn btn-danger btn-xs m-b-10 m-l-5">Reject</a>			
			<?php } ?>
			<a href="javascript:order_details(<?php echo $value['oid']; ?>)" class="btn btn-info btn-xs m-b-10 m-l-5">View Details</a>
			<!-- <a href="<?php echo base_url(); ?>fp/invoice/<?php echo $value['oid']; ?>" class="btn btn-primary btn-xs m-b-10 m-l-5">Invoice</a> -->
			
			<?php if($value['assigned_dp'] == '' && $value['fp_status'] != '2' && $value['fp_status'] != '0'){ ?>
			<a href="javascript:nearby_dps(<?php echo $value['oid']; ?>);" class="btn btn-primary btn-xs m-b-10 m-l-5">Assign DP</a>
			<?php } ?>
			
			</td>      
		</tr>
	<?php } ?>
</tbody>  
                                    </table>
                                </div>
								 <?php echo '<div class="pagination">'.$links.'</div>'; ?>
                            </div>
                        </div>
                      
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluuid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    




<div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<!-- Modal Header -->
<div class="modal-header">
<h4 class="modal-title" id="myModalLabel">
Order Details
</h4>
</div>
<!-- Modal Body -->
<div class="modal-body">
<table  class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
<thead>
	<tr>
	<th>S.No</th>
	<th>Item Name</th> 
	<th>Image</th> 
	<th>Count</th> 
	<th>Item Cost</th>	
	</tr>
</thead>
<tbody id="detailsForm">

</tbody>

</table>
</div>
<!-- Modal Footer -->
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>



<div class="modal fade" id="assginDPModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<!-- Modal Header -->
<div class="modal-header">
<h4 class="modal-title" id="myModalLabel">
Assign Delivery Partner
</h4>
</div>
<!-- Modal Body -->
<div class="modal-body">
	<form method="post" action="" enctype="multipart/form-data">
		<div class="row">
          <div class="col-md-4">Select Delivery Partner : </div>
		  <div class="col-md-8">
		  <select class="timepicker form-control" id="Nearby_Dps">
		  <option>Select Delivery Partner</option>
		  </select>
		  </div><br><br><br>
        </div>
		<input  type="hidden" name="order_id" id="order_id" />
        <span id="error-msg"></span>
    </form>
</div>
<!-- Modal Footer -->
<div class="modal-footer">
<button type="button" class="btn btn-primary" onclick="return assign_DP();">Assign</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>




	<div class="modal fade" id="acceptOrder" role="dialog" aria-labelledby="modalLabel" tabindex="-1">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalLabel">Select Tentative Delivery Date</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
			<div class="row">
			<div class="col-md-4">Delivery Date : </div>
			<div class="col-md-8">
			<input type="text" class="form-control" id="delivery_date" name="delivery_date"/>
			</div><br><br><br>
			</div>
			<input type="hidden" id="status" />
			<input type="hidden" id="fp_id" />
			<input type="hidden" id="order_id" />
          </div>
		  <span id="error-msgaccept"></span>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" onclick="accept_order();">Accept</button>
          </div>
        </div>
      </div>
    </div>
<script>
function accept_order_modal(status,order_id,fp_id){
	$("#status").val(status);
	$("#order_id").val(order_id);
	$("#fp_id").val(fp_id);
	$('#acceptOrder').modal('toggle');
}
</script>

<script>
var dateToday = new Date();
var dates = $("#delivery_date").datepicker({
defaultDate: "+1w",
changeMonth: true,   
minDate: dateToday,
onSelect: function(selectedDate) {
var option = this.id == "delivery_date" ? "minDate" : "maxDate",
instance = $(this).data("datepicker"),
date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
dates.not(this).datepicker("option", option, date);
}
});
</script>
  
  
<script>    
function order_details(id){
	var base_url = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{id:id},
		url:base_url+'catalyst/order_details',
		success:function(html){
			$("#detailsForm").html(html);
			$('#detailsModal').modal('toggle');
			//alert(html);
		}
	});
}
</script>


<script>
// To Accept Order
function accept_order(){
	var status = $("#status").val();
	var order_id = $("#order_id").val();
	var fp_id = $("#fp_id").val();
	var dd = document.getElementById("delivery_date").value;
	
	if(dd == "" || dd == "null"){
		$("#error-msgaccept").html('Please select delivery date');
	}else{
		var $str_sta = (status == '1')? 'Accept':'Reject';
		var concern = confirm('Are you sure do you want to '+$str_sta+' this order');
		if (concern == true) {	
		var base_url = '<?php echo base_url(); ?>';
		jQuery.ajax({
			type:'POST',
			data:{status:status,order_id:order_id,fp_id:fp_id,delivery_date:dd},
			url:base_url+'fp/change_fporder_status',        
			success:function(data){
				var result = JSON.parse(data);   
				console.log(result.msg);   
				if(result.status == 'accept'){
					$("#fpstatus"+order_id).html(result.msg);
					$("#assigned_FP"+order_id).html('<a href="javascript:nearby_dps('+order_id+');" class="btn btn-primary btn-xs m-b-10 m-l-5">Assign DP</a><a href="javascript:order_details('+order_id+')" class="btn btn-info btn-xs m-b-10 m-l-5">View Details</a>');
				}else{
					$("#fpstatus"+order_id).html(result.msg);
					$("#assigned_FP"+order_id).html('<a href="javascript:order_details('+order_id+')" class="btn btn-info btn-xs m-b-10 m-l-5">View Details</a>');
				}
			}
		});
		$('#acceptOrder').modal('toggle');
		}else{
			$('#acceptOrder').modal('toggle');
		}
	}
}
</script>

<script>
function change_order_status(status,order_id,fp_id){
	var $str_sta = (status == '1')? 'Accept':'Reject';
	var concern = confirm('Are you sure do you want to '+$str_sta+' this order');
	if (concern == true) {	
	var base_url = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{status:status,order_id:order_id,fp_id:fp_id},
		url:base_url+'fp/change_fporder_status',        
		success:function(data){
			var result = JSON.parse(data);   
			console.log(result);   
			if(result.status == 'accept'){
				$("#fpstatus"+order_id).html(result.msg);
				$("#assigned_FP"+order_id).html('<a href="javascript:nearby_dps('+order_id+');" class="btn btn-primary btn-xs m-b-10 m-l-5">Assign DP</a><a href="javascript:order_details('+order_id+')" class="btn btn-info btn-xs m-b-10 m-l-5">View Details</a>');
			}else{
				$("#fpstatus"+order_id).html(result.msg);
				$("#assigned_FP"+order_id).html('<a href="javascript:order_details('+order_id+')" class="btn btn-info btn-xs m-b-10 m-l-5">View Details</a>');
			}
		}
	});
	}
}
</script>
           

<script>
function nearby_dps(id){
	var base_url = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{id:id},
		url:base_url+'fp/nearby_dps',
		success:function(html){
			$("#Nearby_Dps").html(html);
			$("#order_id").val(id);
			$('#assginDPModal').modal('toggle');
			//alert(html);
		}
	});
}
</script>	

<script>
function assign_DP(id){
	var base_url = '<?php echo base_url(); ?>';
	var dpval = $("#Nearby_Dps").val();
	var order_id = $("#order_id").val();
	if(dpval == "" || dpval == "null"){
		$("#error-msg").html('Please select Delivery partner');
	}else{		
		$("#error-msg").html('');
		jQuery.ajax({
			type:'POST',
			data:{order_id:order_id,dp:dpval},
			url:base_url+'fp/assign_dps',
			success:function(data){
				var fpdata = JSON.parse(data);
				console.log(fpdata);
				if(fpdata.status == 1){
				$('#assginDPModal').modal('hide');
				setTimeout(function() {
		iziToast.success({   message: "Delivery partner assigned successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);
				$("#assigned_FP"+order_id).html('<a href="javascript:order_details('+order_id+')" class="btn btn-primary btn-xs m-b-10 m-l-5">View Details</a>');
				$("#assigned_dp_name"+order_id).html(fpdata.dp_name);
				}else{
					setTimeout(function() {
		iziToast.warning({   message: "Please try later!",   position: "topRight",   zindex:	"99999"		}); }, 100);
				}
			}
		});
	}
}
</script>	 
  

<script>
$('#example23').dataTable({	
$("#example23").dataTable().fnDestroy();	    
});
</script>

</body>

</html>