<?php include('includes/header.php'); ?>
<style>
.pagination {
    display: inline-block;
	float:right;
	    margin-top: 10px;
}
.dataTables_info, .dataTables_length {
    display: none !important;
}
div#example23_paginate{
	display:none !important;
}
.pagination a {
    color: black;
	border: 1px solid #ddd; /* Gray */
    float: left;
    padding: 8px 16px;
    text-decoration: none;
}

.pagination  strong {
	color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    background-color: #1976d2;
    color: white;
}
a:not([href]):not([tabindex]) {
    padding: 0px !important;
}
.dval{
	margin-left:5px !important;
}
.colon{
	margin-left: 15px !important;
}
.label_field{
	width:100px !important;
}
.form-inline label {
    display: block !important;
}
</style>
<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
                    <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Products</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Products</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
			<?php  if($this->session->flashdata('message') == "success"){ 	 
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Product Deleted successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "updated"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Product Updated successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "added"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Product Added successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}
		?>
            <!-- Container fluuid  -->
            <div class="container-fluuid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Products Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Product Name</th> 
                                                <th>Image</th> 
												<th>Brand</th>
												<th>Price</th>
                                                <th>Created On</th> 
												<th>Actions</th>                                           
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Product Name</th> 
                                                <th>Image</th> 
												<th>Brand</th>
												<th>Price</th>
                                                <th>Created On</th> 
												<th>Actions</th>                                            
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php $i=1; foreach($products as $value){ ?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $value['product_name']; ?></td>
												<td><img src="<?php echo base_url(); ?>assets/uploads/products/<?php echo $value['product_image']; ?>" style="width:50px;height:50px" </td>
                                                <td><?php echo $value['brand']; ?></td>
                                                <td><?php echo $value['product_price']; ?></td>
                                                
                                                <td><?php echo $value['created_on']; ?></td>
   

<td id="proceedTocheckout<?php echo $value['pid']; ?>">
<?php 
$fp_id = $this->session->userdata('fp_session')['id'];
$cart_count = $this->my_model->check('cart',array('uid'=>$fp_id,'added_by'=>'FP','item_id'=>$value['pid'],'item_type'=>'Products'))->num_rows();
if($cart_count > 0){ ?>
<a href="javascript:product_details_forCheckout(<?php echo $value['pid']; ?>)" class="btn btn-primary btn-xs m-b-10 m-l-5">View</a>
<?php }else{ ?>
<a href="javascript:product_details(<?php echo $value['pid']; ?>)" class="btn btn-primary btn-xs m-b-10 m-l-5">View</a>
<?php } ?>
</td> 

                                        
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
									<?php echo '<div class="pagination">'.$links.'</div>'; ?>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluuid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
	
	<div class="modal fade" id="detailsModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header"><h4 class="modal-title">Product Details</h4></div> 
		<div class="modal-body">
			<div class="col-md-12" id="detailsForm">
				
			</div>
        </div>
		<div class="modal-footer pdetails" ><button type="button" class="btn btn-info" onclick="addTocart();">Add To Cart</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
      </div>
    </div>
	</div>
	
	
	<!-- Checkout Modal --->
	<div class="modal fade" id="checkoutModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title">Product Details</h4>
        </div>
         <div class="modal-body">
			<div class="col-md-12" id="checkoutForm">
				
			</div>
        </div>
        <div class="modal-footer pcheckout">
          <a href="<?php echo base_url(); ?>fp/checkout" style="color:white"><button type="button" class="btn btn-info" >Proceed To Checkout</button></a>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
	</div>
	
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>

<script>    
function product_details(id){
	var base_url = '<?php echo base_url(); ?>';
	var table = 'products';
	jQuery.ajax({
		type:'POST',
		data:{id:id,table:table},
		url:base_url+'fp/product_details',
		success:function(html){
			$("#detailsForm").html(html);
			$('#detailsModal').modal('toggle');
			//alert(html);
		}
	});
}
</script>


<script>    
function product_details_forCheckout(id){
	var base_url = '<?php echo base_url(); ?>';
	var table = 'products';
	jQuery.ajax({
		type:'POST',
		data:{id:id,table:table},
		url:base_url+'fp/product_details',
		success:function(html){
			$("#checkoutForm").html(html);
			$('#checkoutModal').modal('toggle');
			//alert(html);
		}
	});
}
</script>

<script>
function addTocart(){
	var base_url = '<?php echo base_url(); ?>';
	var item_id = $("#item_id").val();
	
	var item_type = $("#item_type").val();
	var item_cost = $("#item_cost").val();
	var count = $("#item_count").val();		
	jQuery.ajax({
		type:'POST',		
		data:{item_id:item_id,item_type:item_type,item_cost:item_cost,count:count},
		url:base_url+'fp/addtoCart',
		success:function(data){
			$cdetails = JSON.parse(data);
			//console.log($cdetails);    
			$("#cart_count").html($cdetails['cart_count']);
			$('#detailsModal').modal('toggle');
			$("#cart_list_details").html($cdetails['cart_details']);			
			setTimeout(function() {
		iziToast.success({   message: "Items successfully added to cart!",   position: "topRight",   zindex:	"99999"		}); }, 100);
		
		}
	});
	
}
</script>

</body>

</html>