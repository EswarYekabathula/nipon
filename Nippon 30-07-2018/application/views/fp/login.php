<?php include('includes/header.php'); ?>
<style>
label.error{
	color: red;
    font-size: 14px;    
    text-transform: lowercase;
}   
</style>
<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->

    <div id="main-wrapper">

        <div class="unix-login">



            <div class="container-fluid">
<?php  if($this->session->flashdata('message') == "Failed"){ 	 
echo '<script>setTimeout(function() {
iziToast.error({   title: "Oops",   message: "Invalid Details!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
}else if($this->session->flashdata('message') == "Mobile_not_exists"){
echo '<script>setTimeout(function() {
iziToast.error({   message: "Please try later!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
}else if($this->session->flashdata('message') == "pasword_changed"){
echo '<script>setTimeout(function() {
iziToast.success({   message: "Password successfully sent to your email!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
}else if($this->session->flashdata('message') == "Invalid email id"){
echo '<script>setTimeout(function() {
iziToast.warning({   message: "Invalid email id!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
}else if($this->session->flashdata('message') == "Inactive"){
echo '<script>setTimeout(function() {
iziToast.warning({   message: "Your account will be activated once it is approved by admin",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
}
?>
                <div class="row justify-content-center">

<!--<?php
if(isset($_COOKIE['username']) && isset($_COOKIE['password'])) 
{      
       echo $username = get_cookie("fpusername");
       echo $password = get_cookie("fppassword");
} 
else 
{ 
     
} 
?>-->

                    <div class="col-lg-4">
                        <div class="login-content card">
						<img src="<?php echo base_url(); ?>assets/images/xpress_logo.jpeg"  style="width: 150px;margin:auto"/>
                            <div class="login-form">
                                
                                <form id="login_form" method="POST" action="<?php echo base_url(); ?>fp/login">
                                    <div class="form-group">
                                        <label>Email address</label>
   <input type="email" class="form-control" placeholder="Email" name="email" value="<?php if(isset($_COOKIE['fpusername'])){ echo $username = get_cookie("username"); } ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
  <input type="password" class="form-control" placeholder="Password" name="password" value="<?php if(isset($_COOKIE['fppassword'])){ echo $password = get_cookie("password"); } ?>"> 
                                    </div>



<div class="checkbox">
<label>
<input type="checkbox" name="remember_me" <?php if(isset($_COOKIE['fpusername']) && isset($_COOKIE['fppassword'])) { ?>checked <?php } ?>  > Remember Me
</label>
<label class="pull-right">
<a href="javascript:forgotpassword();">Forgotten Password?</a>
</label>
</div>
                                    <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Sign in</button>
                                    <div class="register-link m-t-15 text-center">
                                        
                                    </div>
                                </form>
                                
                                
                                 <form id="forgot_form" method="POST" action="<?php echo base_url(); ?>admin/forgot_password" style="display:none">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class="form-control" placeholder="Enter your email" name="email"  >
                                    </div>
                                    
                                                                       
					<div class="checkbox">
					<label style="display:none">
					<input type="checkbox" name="remember_me"> Remember Me
					</label>
					<label class="pull-right">
					<a href="javascript:login();">Login</a>
					</label>
					</div>



                                    <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Submit</button>
                                    <div class="register-link m-t-15 text-center">
                                        
                                    </div>
                                </form>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
	
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>	
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>

	
	<script>
	$("#login_form").validate({
		rules:{
			email:{
				required:true,
				email:true
			},
			password:'required'
		},
		messages:{
			email:'Please enter email',
			password:'Please enter password'
		}
	});
	
	
	
	$("#forgot_form").validate({
		rules:{
			mobile:{
				required:true
				
			},
		},
		messages:{
			mobile:'Please enter your mobile number',
		}
	});	
	</script>
	
	
	<script>
	function forgotpassword(){
		$("#login_form").hide();
		$("#forgot_form").show();
	}
	function login(){
		$("#login_form").show();
		$("#forgot_form").hide();	
	}
	</script>
	
<script>
function isNumberKey(evt){
              var charCode = (evt.which) ? evt.which : event.keyCode;
              if(charCode != 43 && charCode !=45 && charCode > 31 && (charCode < 48 || charCode > 57))
                 return false;
              else
                 return true;
           }
</script>





<script>
$("#alert").fadeOut(5000);
</script>


</body>
</html>