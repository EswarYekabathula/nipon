<?php  include('includes/header.php'); ?>

<style type="text/css">
#chart{
	width: 100% !important;
	
}
</style>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Dashboard</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">					
					<div class="col-md-3">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-cart-arrow-down f-s-40 color-warning"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2><?php echo $dorders; ?></h2>
                                    <p class="m-b-0">Dealer Orders</p>
                                </div>
                            </div>
                        </div>
                    </div>
					
					
					  <div class="col-md-3">
					  <a href="<?php echo base_url(); ?>fp/colours">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-list f-s-40 color-success"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2><?php echo $colours; ?></h2>
                                    <p class="m-b-0">Colours</p>
                                </div>
                            </div>
                        </div>
						</a>
                    </div>
					
					
					<div class="col-md-3">
					  <a href="<?php echo base_url(); ?>fp/delivery_partners">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-users f-s-40 color-info"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2><?php echo $dp; ?></h2>
                                    <p class="m-b-0">Delivery Partners</p>
                                </div>
                            </div>
                        </div>
					</a>	
                    </div>
					
					
					<div class="col-md-3">
					  <a href="<?php echo base_url(); ?>fp/products">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-product-hunt f-s-40 color-info"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2><?php echo $products; ?></h2>
                                    <p class="m-b-0">Products</p>
                                </div>
                            </div>
                        </div>
						</a>
                    </div>
					
					
                </div>



                <div class="row bg-white m-l-0 m-r-0 box-shadow ">

                    <!-- column 
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"> Chart</h4>
                                <div id="chart"></div>
                            </div>
                        </div>
                    </div>-->
                    <!-- column -->

                    <!-- column -->




                    <!--<div class="col-lg-4" >
                        <div class="card">
                          <div class="card-body browser">
                                <p class="f-w-600">Users <span class="pull-right">85%</span></p>
                                <div class="progress ">
                                    <div role="progressbar" style="width: 85%; height:8px;" class="progress-bar bg-danger wow animated progress-animated"> <span class="sr-only">60% Complete</span> </div>
                                </div>

                                <p class="m-t-30 f-w-600">Plumbers<span class="pull-right">90%</span></p>
                                <div class="progress">
                                    <div role="progressbar" style="width: 90%; height:8px;" class="progress-bar bg-info wow animated progress-animated"> <span class="sr-only">60% Complete</span> </div>
                                </div>

                                <p class="m-t-30 f-w-600">Calls<span class="pull-right">65%</span></p>
                                <div class="progress">
                                    <div role="progressbar" style="width: 65%; height:8px;" class="progress-bar bg-success wow animated progress-animated"> <span class="sr-only">60% Complete</span> </div>
                                </div>

                                <p class="m-t-30 f-w-600">Staff<span class="pull-right">65%</span></p>
                                <div class="progress">
                                    <div role="progressbar" style="width: 65%; height:8px;" class="progress-bar bg-warning wow animated progress-animated"> <span class="sr-only">60% Complete</span> </div>
                                </div>

								
                            </div>
                        </div>
                    </div>-->
                    <!-- column -->
                </div>
                

                


                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
	
	
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
	<script src="https://raw.githubusercontent.com/tinywall/numscroller/master/numscroller-1.0.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->


    <!-- Amchart -->
     <script src="<?php echo base_url(); ?>assets/js/lib/morris-chart/raphael-min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/morris-chart/morris.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/morris-chart/dashboard1-init.js"></script>


	<script src="<?php echo base_url(); ?>assets/js/lib/calendar-2/moment.latest.min.js"></script>
    <!-- scripit init-->
    <script src="<?php echo base_url(); ?>assets/js/lib/calendar-2/semantic.ui.min.js"></script>
    <!-- scripit init-->
    <script src="<?php echo base_url(); ?>assets/js/lib/calendar-2/prism.min.js"></script>
    <!-- scripit init-->
    <script src="<?php echo base_url(); ?>assets/js/lib/calendar-2/pignose.calendar.min.js"></script>
    <!-- scripit init-->
    <script src="<?php echo base_url(); ?>assets/js/lib/calendar-2/pignose.init.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/lib/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/owl-carousel/owl.carousel-init.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
    <!-- scripit init-->

    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>
 <script src="<?php echo base_url(); ?>assets/js/highcharts.js"></script>

		<script type="text/javascript">

Highcharts.chart('chart', {

    title: {
        text: 'ONLINE MEAT DELIVERY'
    },

   

    yAxis: {			
        title: {
            text: 'Number of members registered'
        },
	min: 0,
        max: 100,
        tickInterval: 20,		
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2018
        }
    },

    series: [{
        name: 'Users',
        data: [<?php echo $users_count; ?>],
		color: '#fc6180'
    }, {
        name: 'Delivery  users',
        data: [<?php echo $plumbers_count; ?>],
		color: '#ffb64d'		
    }, {
        name: 'Orders',
        data: [<?php echo $calls_count; ?>],
		color: '#26dad2'		
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
		</script>




</body>

</html>