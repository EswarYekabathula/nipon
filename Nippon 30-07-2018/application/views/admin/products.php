<?php include('includes/header.php'); 
$this->load->model('admin_model','my_model');?>
<style>
.pagination {
    display: inline-block;
	float:right;
	    margin-top: 10px;
}
.dataTables_info, .dataTables_length {
    display: none !important;
}
div#example23_paginate{
	display:none !important;
}
.pagination a {
    color: black;
	border: 1px solid #ddd; /* Gray */
    float: left;
    padding: 8px 16px;
    text-decoration: none;
}

.pagination  strong {
	color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    background-color: #1976d2;
    color: white;
}
a:not([href]):not([tabindex]) {
    padding: 0px !important;
}
#error-msg{
	position: absolute;
    padding: 0px 15px 0px 15px;
    color: red;
    margin-left: 125px;
}
</style>
<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
                    <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Products</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Products</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
			<?php  if($this->session->flashdata('message') == "success"){ 	 
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Product Deleted successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "updated"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Product Updated successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "added"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Product Added successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}
		?>
            <!-- Container fluuid  -->
            <div class="container-fluuid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
							
							<div class="card-header" style=" margin-left: 5px;width: 135px;float: right;background: #1976d2;border-radius: 20px;">
							<h4 class="m-b-0 text-white"><a href="" data-toggle="modal" data-target="#uploadModal" style="color: white;">Upload CSV</a></h4>
							</div>

							<div class="card-header" style="width: 145px;float: right;background: #1976d2;border-radius: 20px;">
                                <h4 class="m-b-0 text-white"><a href="<?php echo base_url();  ?>admin/products_action" style="color: white;">Add Product<a/></h4>
                            </div>
                                <h4 class="card-title">Products Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Product ID</th>
                                                <th>Category</th> 
                                                <th>Product Name</th> 
                                                <th>Image</th> 
												<th>Brand</th>
												<th>Price</th>
												<th>MOQ</th>
                                                <th>Created On</th> 
												<th>Actions</th>                                           
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                               <th>S.No</th>
                                                <th>Product ID</th>
                                                <th>Category</th> 
                                                <th>Product Name</th> 
                                                <th>Image</th> 
												<th>Brand</th>
												<th>Price</th>
												<th>MOQ</th>
                                                <th>Created On</th> 
												<th>Actions</th>                                                
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3)+1 : 1;
										foreach($products as $value){ ?>
                                            <tr>
                                                <td><?php  echo $start_index++; ?></td>
												<td><?php echo $value['pid']; ?></td>
                                                <td><?php echo $this->my_model->check('product_categories',array('pcid'=>$value['product_category']))->row()->product_category; ?></td>
                                                <td><?php echo $value['product_name']; ?></td>
												<td><img src="<?php echo base_url(); ?>assets/uploads/products/<?php echo $value['product_image']; ?>" style="width:50px;height:50px" </td>
                                                <td><?php echo $value['brand']; ?></td>
                                                <td><?php echo $value['product_price']; ?></td>
                                                <td><?php echo $value['moq']; ?></td>
                                                
                                                <td><?php echo $value['created_on']; ?></td>
<td>
<a href="<?php echo base_url(); ?>admin/products_action/<?php echo $value['pid']; ?>" class="btn btn-primary btn-xs m-b-10 m-l-5">Edit</a>
<a href="<?php echo base_url(); ?>admin/delete_product/<?php echo $value['pid']; ?>" class="btn btn-danger btn-xs m-b-10 m-l-5" onclick="return confirm('Are you sure you want to delete?')" >Delete</a>                                              
</td>                                             
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
									<?php echo '<div class="pagination">'.$links.'</div>'; ?>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluuid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    

<script>    
	function change_catalyst_status(id,status) {		
	var r = confirm("Are you sure do you want to change the status");
	if (r == true) {
		jQuery.ajax({
			type:'POST',
			data:{id:id,status:status},			
			url:'change_catalyst_status',
			success:function(data){
			$("#statuschange"+id).html(data);				
			}
        
		});
	} else {

	}	
	}
</script>

    

<div id="uploadModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">       
        <h4 class="modal-title">Bulk Upload Products Data in CSV</h4>              
      </div>
      <div class="modal-body">    
        <!-- Form -->
        <form method='post' action='<?php echo base_url(); ?>admin/csvuploadfile/products' enctype="multipart/form-data">
          Select file : <input type='file' name='csv' id='file' class='form-control' ><br>
		  <span id="error-msg"></span>
          <input type='submit' class='btn btn-info' value='Upload' id='upload' onclick="return checkfile();">
        </form>

        <!-- Preview-->
        <div id='preview'></div>
      </div>
 
    </div>

  </div>
</div>


<script>
function checkfile(){
	var file = $('#file').val();
	if(file  == ""){
		$("#error-msg").html('Please upload csv file');
		return false;
	}else{
		return true;
	}
}
</script>

</body>
</html>