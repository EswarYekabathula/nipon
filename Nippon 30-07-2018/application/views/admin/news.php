<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <!--<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/home/images/favicon.png">-->
    <title> CSU Admin Dashboard </title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/css/helper.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<link href="<?php echo base_url(); ?>assets/css/sweetalert.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/sweetalert.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sweetalert.min.js"></script>


</head>
<style>
.dt-buttons{
	display:none;
}
</style>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
                    <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">News</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">News</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            
            

            <?php  if($this->session->flashdata("message")){ ?>
<div id="alert" class="alert alert-info" style="width:450px;margin-left: 386px;margin-top:28px;position:relative">
Action performed successfully
</div>
<?php } ?>


            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">                    
                        <div class="card">                        
                            <div class="card-body">
                            <div class="card-header" style="width: 125px;float: right;background: #961914;border-radius: 20px;">
                                <h4 class="m-b-0 text-white"><a href="<?php echo base_url();  ?>admin/news_action" style="color: white;">Add News<a/></h4>
                            </div>
	                        
                              
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Title</th>
                                                <th>Author</th>
                                                <th>Created on</th>
                                                <th>Image</th>
                                                <th>Actions</th>                                                
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Title</th>
                                                <th>Author</th>
                                                <th>Created on</th>
                                                <th>Image</th>
                                                <th>Actions</th>                                               
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php $i=1; foreach($news as $value){ ?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $value['title']; ?></td>                                               
                                                <td><?php echo $value['author']; ?></td>
                                                <td><?php echo $value['created']; ?></td>
                                                <td><img src="<?php echo base_url(); ?>assets/uploads/news/<?php echo $value['image']; ?>" style="width:50px;height:50px"></td>
                                               
                                                <td>
<a href="<?php echo base_url(); ?>admin/news_action/<?php echo $value['nid']; ?>" class="btn btn-info btn-xs m-b-10 m-l-5">Edit</a>
                                                <a href="<?php echo base_url(); ?>admin/delete_news/<?php echo $value['nid']; ?>" class="btn btn-danger btn-xs m-b-10 m-l-5" onclick="return confirm('Are you sure you want to delete?')" >Delete</a>
                                                </td>                                                
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    
<script>
$("#alert").fadeOut(5000);
</script>
<script>    
	function change_status(id,status) {	
	var BASE_URL = "<?php echo base_url(); ?>";	
	var r = confirm("Are you sure do you want to change the status");
	if (r == true) {
		jQuery.ajax({
			type:'POST',
			data:{id:id,status:status},			
			url:BASE_URL+'admin/change_status_plumber',
			success:function(data){
			$("#statuschange"+id).html(data);				
			}
        
		});
	} else {

	}	
	}
</script>

</body>

</html>