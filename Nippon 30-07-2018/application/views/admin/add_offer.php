<?php include('includes/header.php'); ?>
<link href="<?php echo base_url(); ?>assets/css/checkbox.css" rel="stylesheet">
<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
<link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css"   rel="stylesheet" type="text/css" />

<link rel='stylesheet prefetch' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css'>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<style>
.rotate{
    -moz-transition: all 2s linear;
    -webkit-transition: all 2s linear;
    transition: all 2s linear;
    float: right !important;   
    margin-top: -50px !important;
    margin-right: 125px !important;
    font-size: 20px !important;
}

.rotate.down{
    -moz-transform:rotate(180deg);
    -webkit-transform:rotate(180deg);
    transform:rotate(180deg);
}
#myMap{
height: 450px !important;
}
label.error{
    color: red;
    font-size: 14px;    
    text-transform: lowercase;
}

#error-msg{
color: red;
    font-size: 14px;    
    margin-left: 250px;
}
span.multiselect-native-select .btn-group {
    width: 100%;
    overflow: visible;
    border: 1px solid #eee;
}
.btn-group, .btn-group-vertical {
    position: relative;
    display: inline-block;
    vertical-align: middle;
}
ul.multiselect-container.dropdown-menu {
    width: 100%;
}
button.multiselect.dropdown-toggle.btn.btn-default {
    width: 100%;
}
.multiselect-container {
		height:400px;
		overflow:auto;
	}
button.multiselect.dropdown-toggle.btn.btn-default {
    text-align: left;
    background: transparent;
	width:100%;
}
ul.multiselect-container.dropdown-menu label.checkbox{
	border:0px !important;
}
span.multiselect-native-select .btn-group{
	width: 100%;
    overflow: visible;
    border: 1px solid #eee;
}
ul.multiselect-container.dropdown-menu {
	width:100%;
}
.dropdown-menu > .active > a {
	background-color:white !important;
}
ul.multiselect-container.dropdown-menu.show{
	margin-top:335px !important;	
}
</style>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
             <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Add/Edit Offer</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Add/Edit Offer</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            
            
 <!-- Container fluid  -->
 
<?php  if($this->session->flashdata("message")){ ?>
<div id="alert" class="alert alert-info" style="width:450px;margin-left: 386px;margin-top:28px;position:relative">
Action performed successfully
</div>
<?php } ?>



           
           
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-validation">
                                    <form class="form-valide" action="<?php echo base_url(); ?>admin/offer_action/<?php echo $offer_data['oid']; ?>" method="post" enctype="multipart/form-data"   id="plumber_form">
									
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-username">Offer Name  <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="offer_name" name="offer_name" placeholder="Enter Offer Name" value='<?php print_r($offer_data['offer_name']); ?>'>
                                            </div>
                                        </div>
										
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-username">Offer Type  <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <select class="form-control" id="offer_type" name="offer_type" onchange="show_other();">
													<option value="">Select Offer Type</option>													
													<option <?php if($offer_data['offer_type'] == "by_value"){ ?> selected <?php } ?> value="by_value">By Value</option>
													<option <?php if($offer_data['offer_type'] == "by_perc"){ ?> selected <?php } ?> value="by_perc">By Percentage</option>
													<!--<option <?php if($offer_data['offer_type'] == "introductory"){ ?> selected <?php } ?> value="introductory">Introductory</option>-->
												</select>
                                            </div>
                                        </div>
										
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-username">Value or Percentage  <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="vp" name="vp" placeholder="Enter Offer Value or Percentage" value='<?php print_r($offer_data['vp']); ?>'>
                                            </div>
											<span style="font-size: 11px; float: right;width: 150px;">if value enter Rs.100/if percentage enter 10%</span>
                                        </div>
										
										
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-username">Offer Valid For <span class="text-danger">*</span>
											</br><span>(All Dealers/Specific Dealer)</span>
											</label>
											
                                            <div class="col-lg-6">
                                                <select class="form-control" id="offer_valid" name="offer_valid[]" multiple="multiple">																					
													<?php foreach($dealers as $dval){ ?>
													<option <?php if(strpos($offer_data['offer_valid'], $dval['did']) !== false){ ?> selected <?php } ?> value="<?php echo $dval['did']; ?>"><?php echo $dval['dealer_name']; ?></option>
													<?php } ?>
												</select>												
                                            </div>
                                        </div>
										
										
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-username">Offer Valid On <span class="text-danger">*</span>
											</br><span>(All Products/Specific Product)</span>
											</label>
											
                                            <div class="col-lg-6">
                                                <select class="form-control" id="products" name="products[]" multiple="multiple">																					
													<?php foreach($products as $pval){ ?>
													<option <?php if(strpos($offer_data['products'], $pval['pid']) !== false){ ?> selected <?php } ?> value="<?php echo $pval['pid']; ?>"><?php echo $pval['product_name']; ?></option>
													<?php } ?>
												</select>												
                                            </div>
                                        </div>
										
										
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-username">Offer Validity  From<span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="offer_validity_from" name="offer_validity_from" value='<?php print_r($offer_data['offer_validity_from']); ?>' placeholder="Offer Validity" autocomplete="off">
                                            </div>
                                        </div>	
										
										
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-username">Offer Validity  To<span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="offer_validity_to" name="offer_validity_to" value='<?php print_r($offer_data['offer_validity_to']); ?>' placeholder="Offer Validity" autocomplete="off">
                                            </div>
                                        </div>
										
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-username">Offer On  <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <select class="form-control" id="offer_on" name="offer_on" >
													<option value="">Select Offer On</option>													
													<option <?php if($offer_data['offer_on'] == "brands"){ ?> selected <?php } ?> value="Brands">Brands</option>
													<option <?php if($offer_data['offer_on'] == "colours"){ ?> selected <?php } ?> value="Colours">Colours</option>
													<option <?php if($offer_data['offer_on'] == "products"){ ?> selected <?php } ?> value="Products">Products</option>
													
												</select>
                                            </div>
                                        </div>
							
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-username"> Image  <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <input type="file" class="form-control" <?php if($offer_data['oid'] == ""){ ?> id="brand_image" <?php   } ?> name="brand_image" >
                                            </div>
                                        </div>
										
										
										<?php if($offer_data['oimage'] != ""){ ?>
										<img src="<?php echo base_url(); ?>assets/uploads/offers/<?php echo $offer_data['oimage']; ?>" style="width:50px;height:50px"/>
										<?php } ?>
										
										
										<span id="error-msg"></span>
										<input type="hidden" id="mobile_validation" name="old_image" value="<?php print_r($offer_data['oimage']); ?>"/> 
										
                                        <div class="form-group row" style="margin-top:10px">
                                            <div class="col-lg-8 ml-auto">
       <button type="submit" class="btn btn-primary" name="submit" onclick="return check_validation();" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> � 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    
<script>
$(".rotate").click(function(){
 $(this).toggleClass("down")  ; 
})

$("#alert").fadeOut(5000);
</script>


<script>
function get_city(){
	var $state = $("#state").val();
	var $BASE_URL = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{'state':$state},
		url: $BASE_URL+'admin/get_cities',
		success:function(data){
			$("#city").html(data);
		}
	});
}
</script>


<script>
function check_validation(){
var offer_name = $("#offer_name").val();
var offer_type = $("#offer_type").val();
var brand_image = $("#brand_image").val();


if(offer_name == "" || offer_name == "null"){
$("#error-msg").html('Fields marked with * are mandatory');
return false;
}else if(offer_type == "" || offer_type == "null"){
$("#error-msg").html('Fields marked with * are mandatory');
return false;
}else if(brand_image == "" || brand_image == "null"){
$("#error-msg").html('Fields marked with * are mandatory');
return false;
}else{
$("#error-msg").html('');
return true;
}
}

</script>


<script type="text/javascript">
$(function () {
	$('#offer_valid').multiselect({
		includeSelectAllOption: true
	});
	$('#btnSelected').click(function () {
		var selected = $("#offer_valid option:selected");
		var message = "";
		selected.each(function () {
			message += $(this).text() + " " + $(this).val() + "\n";
		});                
	});
});

$(function () {
	$('#products').multiselect({
		includeSelectAllOption: true
	});
	$('#btnSelected').click(function () {
		var selected = $("#products option:selected");
		var message = "";
		selected.each(function () {
			message += $(this).text() + " " + $(this).val() + "\n";
		});                
	});
});
</script>


<script>
function isNumberKey(evt){
              var charCode = (evt.which) ? evt.which : event.keyCode;
              if(charCode != 43 && charCode !=45 && charCode > 31 && (charCode < 48 || charCode > 57))
                 return false;
              else
                 return true;
           }
</script>

<script>
var dateToday = new Date();
var dates = $("#offer_validity_from").datepicker({
defaultDate: "+1w",
changeMonth: true,   
minDate: dateToday,
onSelect: function(selectedDate) {
var option = this.id == "offer_validity_from" ? "minDate" : "maxDate",
instance = $(this).data("datepicker"),
date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
dates.not(this).datepicker("option", option, date);
}
});


var dateToday = new Date();
var dates = $("#offer_validity_to").datepicker({
defaultDate: "+1w",
changeMonth: true,   
minDate: dateToday,
onSelect: function(selectedDate) {
var option = this.id == "offer_validity_to" ? "minDate" : "maxDate",
instance = $(this).data("datepicker"),
date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
dates.not(this).datepicker("option", option, date);
}
});
</script>

<script>
      CKEDITOR.replace( 'description' );
</script>

<script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"   type="text/javascript"></script>


</body>
</html>