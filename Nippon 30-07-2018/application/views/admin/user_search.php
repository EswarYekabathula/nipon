<?php include('includes/header.php'); ?>
<link href="<?php echo base_url(); ?>assets/css/checkbox.css" rel="stylesheet">
<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>

<style>
.rotate{
    -moz-transition: all 2s linear;
    -webkit-transition: all 2s linear;
    transition: all 2s linear;
    float: right !important;   
    margin-top: -50px !important;
    margin-right: 125px !important;
    font-size: 20px !important;
}

.rotate.down{
    -moz-transform:rotate(180deg);
    -webkit-transform:rotate(180deg);
    transform:rotate(180deg);
}
#myMap{
height: 450px !important;
}
label.error{
    color: red;
    font-size: 14px;    
    text-transform: lowercase;
}

#error-msg{
color: red;
    font-size: 14px;    
    margin-left: 250px;
}
</style>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
             <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Search Users 
					</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Search Users </li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            
            
 <!-- Container fluid  -->
 
<?php  if($this->session->flashdata("message")){ ?>
<div id="alert" class="alert alert-info" style="width:450px;margin-left: 386px;margin-top:28px;position:relative">
Action performed successfully
</div>
<?php } ?>



           
           
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-validation">
                                    <form class="form-valide">
								
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-username">State  <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <select class="form-control" id="state" name="state" onchange="get_city();">
													<option value="">Select State</option>
													<?php foreach($states as $state_value){ ?>
													<option <?php if($dp_data['state'] == $state_value['State']){ ?> selected <?php } ?>  value="<?php echo $state_value['State']; ?>"><?php echo $state_value['State']; ?></option>
													<?php } ?>
												</select>
                                            </div>
                                        </div>
										
										
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-username">City  <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <select class="form-control" id="city" name="city" >
														<option value="">Select City</option>												
												</select>
                                            </div>
                                        </div>
	
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-username">User Type  <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <select class="form-control" id="utype" name="utype" >
													<option value="">Select User Type</option>
													<option value="catalysts">Catalysts</option>
													<option value="dealers">Dealers</option>
													<option value="franchise_partners">Franchise Partners</option>
													<option value="delivery_partners">Delivery Partners</option>
												</select>
                                            </div>
                                        </div>
	
	
		
										<span id="error-msg"></span>
                                        <div class="form-Type row" style="margin-top:10px">
                                            <div class="col-lg-8 ml-auto">
       <button type="button" class="btn btn-primary" name="button" onclick="return searchUsers();" >search</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
				
				
				<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title" id="title_search"></h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Name</th>                                                                                                
                                                <th>Email</th>
                                                <th>Phone</th>
												<th>Designation</th>
												<th>View</th>                                           
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Name</th>                                                                                                
                                                <th>Email</th>
                                                <th>Phone</th>
												<th>Designation</th>
												<th>View</th>                                           
                                            </tr>
                                        </tfoot>
                                        <tbody id="SearchResults">
                                       
                                        </tbody>
                                    </table>
										<?php echo '<div class="pagination">'.$links.'</div>'; ?>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
				
				
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> � 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    
<script>
$(".rotate").click(function(){
 $(this).toggleClass("down")  ; 
})

$("#alert").fadeOut(5000);
</script>


<script>
function get_city(){
	var $state = $("#state").val();
	var $BASE_URL = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{'state':$state},
		url: $BASE_URL+'admin/get_cities',
		success:function(data){
			$("#city").html(data);
		}
	});
}
</script>


<script>
function check_validation(){
var state = $("#state").val();
var city = $("#city").val();
var utype = $("#utype").val();



if(state == "" || state == "null"){
$("#error-msg").html('Fields marked with * are mandatory');
return false;
}else if(city == "" || city == "null"){
$("#error-msg").html('Fields marked with * are mandatory');
return false;
}else if(utype == "" || utype == "null"){
$("#error-msg").html('Fields marked with * are mandatory');
return false;
}else{
$("#error-msg").html('');
return true;
}
}

</script>



<script>
function searchUsers(){
	var state = $("#state").val();
	var city = $("#city").val();
	var table = $("#utype").val();
	var base_url = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{state:state,city:city,table:table},
		url:base_url+'admin/search_users',
		success:function(data){
			$("#SearchResults").html(data);
		}		
	});
}
</script>





<script>
function isNumberKey(evt){
              var charCode = (evt.which) ? evt.which : event.keyCode;
              if(charCode != 43 && charCode !=45 && charCode > 31 && (charCode < 48 || charCode > 57))
                 return false;
              else
                 return true;
           }
</script>

<script>
      CKEDITOR.replace( 'desc' );
      CKEDITOR.replace( 'benefits' );
      CKEDITOR.replace( 'features' );
</script>

</body>

</html>