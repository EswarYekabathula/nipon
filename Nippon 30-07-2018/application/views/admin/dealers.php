<?php include('includes/header.php'); ?>
<style>
.red{
	color:red;
}
.dataTables_info, .dataTables_length {
    display: none !important;
}
div#example23_paginate{
	display:none !important;
}
.pagination {
    display: inline-block;
	float:right;
	    margin-top: 10px;
}

.pagination a {
    color: black;
	border: 1px solid #ddd; /* Gray */
    float: left;
    padding: 8px 16px;
    text-decoration: none;
}

.pagination  strong {
	color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    background-color: #1976d2;
    color: white;
}
a:not([href]):not([tabindex]) {
    padding: 0px !important;
}
#error-msg{
	position: absolute;
    padding: 0px 15px 0px 15px;
    color: red;
    margin-left: 125px;
}
</style>
<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
                    <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Dealers</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dealers</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluuid  -->
            <div class="container-fluuid">
			<?php  if($this->session->flashdata('message') == "success"){ 	 
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Dealer Deleted successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "updated"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Dealer Updated successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "added"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Dealer Added successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "csv_success"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Delivery partners data uploaded successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}
		?>
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
							<div class="card-header" style=" margin-left: 5px;width: 135px;float: right;background: #1976d2;border-radius: 20px;">
							<h4 class="m-b-0 text-white"><a href="" data-toggle="modal" data-target="#uploadModal" style="color: white;">Upload CSV</a></h4>
							</div>
							<div class="card-header" style="width: 125px;float: right;background: #1976d2;border-radius: 20px;">
                                <h4 class="m-b-0 text-white"><a href="<?php echo base_url();  ?>admin/dealer_action" style="color: white;">Add Dealer<a/></h4>
                            </div>
                                <h4 class="card-title">Dealers Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Dealer ID</th>
                                                <th>Name</th>                                                                                                
                                                <th>Email</th>
                                                <th>Phone</th>												
												<th>Actions</th>                                    
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>S.No</th>
												<th>Dealer ID</th>
                                                <th>Name</th>                                                                                                
                                                <th>Email</th>
                                                <th>Phone</th>												
												<th>Actions</th>                                           
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3)+1 : 1; foreach($dealers as $value){ ?>
                                            <tr id="row<?php echo $value['did']; ?>">
                                                <td><?php  echo $start_index++; ?></td>
                                                <td><?php echo $value['did']; ?></td>
                                                <td><?php echo $value['dealer_name']; ?></td>
                                                <td><?php echo $value['email']; ?></td>
                                                <td><?php echo $value['mobile']; ?></td>
                                              
<td>
<span id="statuschange<?php echo $value['did']; ?>">
<?php if($value['dstatus'] == '1'){ ?>
<button type="button" class="btn btn-info btn-xs m-b-10 m-l-5" onclick="change_dealer_status(<?php echo $value['did']; ?>,'1')">Active</button>
<?php }else{ ?>
<button type="button" class="btn btn-warning btn-xs m-b-10 m-l-5" onclick="change_dealer_status(<?php echo $value['did']; ?>,'0')">Inactive</button>
<?php } ?>
</span>

<a href="<?php echo base_url(); ?>admin/user_details/dealers/<?php echo $value['did']; ?>" class="btn btn-success btn-xs m-b-10 m-l-5">View</a>
<a href="<?php echo base_url(); ?>admin/dealer_action/<?php echo $value['did']; ?>" class="btn btn-primary btn-xs m-b-10 m-l-5">Edit</a>
<a href="javascript:delete_user('dealers','<?php echo $value['did']; ?>')" class="btn btn-danger btn-xs m-b-10 m-l-5"  >Delete</a>                                              

</td>                                             
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
								 <?php echo '<div class="pagination">'.$links.'</div>'; ?>
                            </div>
                        </div>
                      
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluuid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    

<script>    
	function change_dealer_status(id,status) {	
		var bu = '<?php echo base_url(); ?>';
	var r = confirm("Are you sure do you want to change the status");
	if (r == true) {
		jQuery.ajax({
			type:'POST',
			data:{id:id,status:status},			
			url:bu+'admin/change_dealer_status',
			success:function(data){
			$("#statuschange"+id).html(data);				
			}
        
		});
	} else {

	}	
	}
</script>


<script>
function delete_user($table,$id){
	var table = $table;
	var id  = $id;
	var retval = confirm('Are you sure do you want to delete this dealer');
	if( retval == true ){
	var bu = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{table:table,id:id},
		url:bu+'admin/delete_user',
		success:function(data){
			$("#row"+$id).hide();
			setTimeout(function() {
		iziToast.success({   message: "Dealer Deleted successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);
		}
	});
	}
}
</script>

<div id="uploadModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">       
        <h4 class="modal-title">Bulk Upload Dealers Data in CSV</h4>
      </div>
      <div class="modal-body">
        <!-- Form -->
        <form method='post' action='<?php echo base_url(); ?>admin/csvuploadfile/dealers' enctype="multipart/form-data">
          Select file : <input type='file' name='csv' id='file' class='form-control' ><br>
		  <span id="error-msg"></span>
          <input type='submit' class='btn btn-info' value='Upload' id='upload' onclick="return checkfile();">
        </form>

        <!-- Preview-->
        <div id='preview'></div>
      </div>
 
    </div>

  </div>
</div>


<script>
function checkfile(){
	var file = $('#file').val();
	if(file  == ""){
		$("#error-msg").html('Please upload csv file');
		return false;
	}else{
		return true;
	}
}
</script>


<script>
$('#example23').dataTable({	
$("#example23").dataTable().fnDestroy();	    
});
</script>

</body>

</html>