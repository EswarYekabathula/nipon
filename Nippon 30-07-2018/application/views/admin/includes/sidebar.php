<div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
<li>
<a class="" href="<?php echo base_url(); ?>admin" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard </span></a>
</li>

<li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu">Users </span></a>
<ul aria-expanded="false" class="collapse">
	<li><a href="<?php echo base_url(); ?>admin/catalysts">Catalysts</a></li>                                
	<li><a href="<?php echo base_url(); ?>admin/franchise_partners">Franchise Partners</a></li>                                
	<li><a href="<?php echo base_url(); ?>admin/delivery_partners">Delivery Partners</a></li>                                
	<li><a href="<?php echo base_url(); ?>admin/dealers">Dealers</a></li>                                
	
</ul>
</li>	

<li><a class="" href="<?php echo base_url(); ?>admin/brands" aria-expanded="false"><i class="fa fa-file-text-o"></i><span class="hide-menu">Brands </span></a></li>	

<li><a class="" href="<?php echo base_url(); ?>admin/source_countries" aria-expanded="false"><i class="fa fa-globe"></i><span class="hide-menu">Source Countries </span></a></li>	

<li><a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-object-group"></i><span class="hide-menu">Colours Management</span></a>
<ul aria-expanded="false" class="collapse">	
<li><a class="" href="<?php echo base_url(); ?>admin/colour_groups" aria-expanded="false"><span class="hide-menu">Colour Groups </span></a></li>	 
<li><a class="" href="<?php echo base_url(); ?>admin/colour_types" aria-expanded="false"><span class="hide-menu">Colour Types </span></a></li>	
<li><a class="" href="<?php echo base_url(); ?>admin/colours" aria-expanded="false"><span class="hide-menu">Colours </span></a></li>
</ul>
</li>	



<li><a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-product-hunt"></i><span class="hide-menu">Products Management</span></a>
<ul aria-expanded="false" class="collapse">	
<li><a class="" href="<?php echo base_url(); ?>admin/product_categories" aria-expanded="false"><span class="hide-menu">Product Categories </span></a></li>	
<li><a class="" href="<?php echo base_url(); ?>admin/products" aria-expanded="false"><span class="hide-menu">Products </span></a></li>	
</ul>
</li>	


<li><a class="" href="<?php echo base_url(); ?>admin/pincodes" aria-expanded="false"><i class="fa fa-map-marker"></i><span class="hide-menu">Pincodes </span></a></li>	

<li><a class="" href="<?php echo base_url(); ?>admin/delivery_types" aria-expanded="false"><i class="fa fa-truck"></i><span class="hide-menu">Delivery Types </span></a></li>	

<li><a class="" href="<?php echo base_url(); ?>admin/feedbacks" aria-expanded="false"><i class="fa fa-comments-o"></i><span class="hide-menu">Feedbacks</span></a></li>	

<li><a class="" href="<?php echo base_url(); ?>admin/offers" aria-expanded="false"><i class="fa fa-ticket"></i><span class="hide-menu">Offers/Schemes</span></a></li>	

<li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-cart-plus"></i><span class="hide-menu">Orders</span></a>
<ul aria-expanded="false" class="collapse">
	<li><a href="<?php echo base_url(); ?>admin/dealer_orders">Dealers Orders</a></li>                                
	<li><a href="<?php echo base_url(); ?>admin/fp_orders">Franchise Partners Orders</a></li>  
</ul>
</li>	

<li><a class="" href="<?php echo base_url(); ?>admin/gst" aria-expanded="false"><i class="fa fa-pencil"></i><span class="hide-menu">GST</span></a></li>	

<li><a class="" href="<?php echo base_url(); ?>admin/csv_templates" aria-expanded="false"><i class="fa fa-file-excel-o"></i><span class="hide-menu">CSV Templates</span></a></li>	


                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>