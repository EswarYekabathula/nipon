<?php include('includes/header.php'); ?>


<style>
.dataTables_wrapper .dataTables_paginate {
    display: none !important;
}
.form-control {
     border: 0px solid #ced4da !important; 
}
.form-group {
margin-bottom:0px !important;
    height: 40px !important; 
}
.pagination {
    display: inline-block;
	float:right;
	    margin-top: 10px;
}
.dataTables_info, .dataTables_length {
    display: none !important;
}
div#example23_paginate{
	display:none !important;
}
.pagination a {
    color: black;
	border: 1px solid #ddd; /* Gray */
    float: left;
    padding: 8px 16px;
    text-decoration: none;
}

.pagination  strong {
	color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    background-color: #1976d2;
    color: white;
}
a:not([href]):not([tabindex]) {
    padding: 0px !important;
}
</style>


<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
             <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary"> <?php echo ucfirst($table); ?> Details</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin">Home</a></li>
                        <li class="breadcrumb-item active"> <?php echo ucfirst($table); ?> Details</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <?php  if($this->session->flashdata("message")){?>
<div id="alert" class="alert alert-info" style="width:450px;margin-left: 275px;">
Profile updated successfully
</div> 
<?php }?>
            <div class="container-fluid">
			
			




							
					<div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body p-b-0">
                                    <h4 class="card-title"></h4>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs customtab2" role="tablist">
                                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home7" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Dealer Wise</span></a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile7" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Franchise Partner Wise</span></a> </li>                                        
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="home7" role="tabpanel">
                    <div class="card">
                            <div class="card-body">
						
								<h4 class="card-title">Dealer Wise Sales Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Dealer Name</th>                                                                                                
                                                <th>Total Cost</th>
                                                <th>Delivery Type</th>												
                                                <th>Ordered on</th>												
                                                <th>Assigned Franchise Partner</th>												
												<th>Actions</th>                                    
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Dealer Name</th>                                                                                                
                                                <th>Total Cost</th>
                                                <th>Delivery Type</th>												
                                                <th>Ordered on</th>												
                                                <th>Assigned Franchise Partner</th>																							
												<th>Actions</th>                                           
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3)+1 : 1;
										foreach($dorders as $value){ ?>
                                            <tr id="row<?php echo $value['oid']; ?>">
                                                <td><?php  echo $start_index++; ?></td>
                                                <td><?php echo $this->my_model->check('dealers',array('did'=>$value['uid']))->row()->dealer_name; ?></td>
                                                <td><?php echo $value['total_cost']; ?></td>
                                                <td>
												<?php 
												$delivery_type_details =  $this->my_model->check('delivery_types',array('dltid'=>$value['delivery_type']))->row();
												echo $delivery_type_details->delivery_type.' '.('Rs'.$delivery_type_details->price).' '.($delivery_type_details->days);
												?>
												</td>
                                                <td><?php echo date("Y-m-d H:i",strtotime($value['added_on']));	?></td>
                                              
											  <td id="assigned_fp_name<?php echo $value['oid'];?>">
											  <?php echo $this->my_model->check('franchise_partners',array('fid'=>$value['assigned_fp']))->row()->name; ?>
											  </td>
											  
											  
<td id="assigned_FP<?php echo $value['oid']; ?>">
<a href="javascript:order_details(<?php echo $value['oid']; ?>)" class="btn btn-primary btn-xs m-b-10 m-l-5">View Details</a>
<!--<a href="javascript:delete_order(<?php echo $value['oid']; ?>)" class="btn btn-danger btn-xs m-b-10 m-l-5">Delete </a>-->
</td>      
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
								 <?php echo '<div class="pagination">'.$links.'</div>'; ?>
                            </div>
                        </div>
                                        </div>
                                        
										
										<div class="tab-pane  p-20" id="profile7" role="tabpanel">
										<div class="card">
                            <div class="card-body">
						
                                <h4 class="card-title">Franchise Wise Sales Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example25" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
												<th>OrderID</th>
												<th>Transaction ID</th>
                                                <th>Franchise Partner Name</th>                                                                                                
                                                <th>Total Cost</th>                                               											
                                                <th>Ordered on</th>							
												<th>Actions</th>                                        
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>S.No</th>
												<th>OrderID</th>
												<th>Transaction ID</th>
                                                <th>Franchise Partner Name</th>                                                                                                   
                                                <th>Total Cost</th>					
                                                <th>Ordered on</th>																						
												<th>Actions</th>                                           
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3)+1 : 1;
										foreach($forders as $value){ ?>
                                            <tr id="row<?php echo $value['oid']; ?>">
                                                <td><?php  echo $start_index++; ?></td>
												<td><?php echo $value['order_id']; ?></td>
												<td><?php echo $value['transaction_id']; ?></td>
                                                <td><?php echo $this->my_model->check('franchise_partners',array('fid'=>$value['uid']))->row()->name; ?></td>
                                                <td><?php echo $value['total_cost']; ?></td> <td><?php echo date("Y-m-d H:i",strtotime($value['added_on']));	?></td>
                                          
<td id="assigned_FP<?php echo $value['oid']; ?>">

<a href="javascript:order_details(<?php echo $value['oid']; ?>)" class="btn btn-info btn-xs m-b-10 m-l-5">View Details</a>

<!--- After Confirming Order -->
<?php if($value['order_status'] == '1'){ ?>
<!--<a href="<?php echo base_url(); ?>admin/invoice/<?php echo $value['oid']; ?>" class="btn btn-primary btn-xs m-b-10 m-l-5">Invoice</a>--->
<?php if($value['assigned_fp'] == ""){ ?>
<!--<a href="javascript:nearby_fps(<?php echo $value['oid']; ?>)" class="btn btn-primary btn-xs m-b-10 m-l-5">Assign FP</a>-->
<?php } } ?>
</td>      
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
								 <?php echo '<div class="pagination">'.$fp_links.'</div>'; ?>
                            </div>
                        </div>
										
										</div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
				
				
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
<script>
$("#alert").fadeOut(5000);
</script>


<script>
function remove_readonly(product_id){
	$("#offline_consumption"+product_id).removeAttr('readonly');
}
</script>

<script>
function update_offline(product_id){
	var offline_consumption = $("#offline_consumption"+product_id).val();
	var base_url = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{product_id:product_id,offline_consumption:offline_consumption},
		url:base_url+'admin/update_product_offline_consumption',
		success:function(data){
			$details = JSON.parse(data);
			if($details['status'] == '1'){
				$("#offline_consumption"+product_id).val($details['offline_consumption']);
				$("#offline_consumption"+product_id).attr('readonly', true);
				$("#closing"+product_id).html($details['closing']);
				setTimeout(function() {
		iziToast.success({   message: "Offline consumption updated successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);
			}else{
				setTimeout(function() {
		iziToast.warning({   message: "Please try later!",   position: "topRight",   zindex:	"99999"		}); }, 100);
			}
		}
	});
}
</script>

</body>
</html>