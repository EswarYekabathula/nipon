<?php include('includes/header.php'); ?>
<style type="text/css">
#chart{
	width: 100% !important;
}
</style>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Dashboard</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
         
                <div class="row bg-white m-l-0 m-r-0 box-shadow ">
				
                    <!-- column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                
                                <div id="monthly_chart"></div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->

                </div>
                

                


                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->


    <!-- Amchart -->
     <script src="<?php echo base_url(); ?>assets/js/lib/morris-chart/raphael-min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/morris-chart/morris.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/morris-chart/dashboard1-init.js"></script>


	<script src="<?php echo base_url(); ?>assets/js/lib/calendar-2/moment.latest.min.js"></script>
    <!-- scripit init-->
    <script src="<?php echo base_url(); ?>assets/js/lib/calendar-2/semantic.ui.min.js"></script>
    <!-- scripit init-->
    <script src="<?php echo base_url(); ?>assets/js/lib/calendar-2/prism.min.js"></script>
    <!-- scripit init-->
    <script src="<?php echo base_url(); ?>assets/js/lib/calendar-2/pignose.calendar.min.js"></script>
    <!-- scripit init-->
    <script src="<?php echo base_url(); ?>assets/js/lib/calendar-2/pignose.init.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/lib/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/owl-carousel/owl.carousel-init.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
    <!-- scripit init-->

    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>
 <script src="<?php echo base_url(); ?>assets/js/highcharts.js"></script>

 <form class="juspay_inline_form" id="payment_form">
<input type="hidden" class="merchant_id" value="guest"
/>
<input type="hidden" class="order_id" value="guest_ord
er"/>
<div class="card_number_div"></div>
<div class="name_on_card_div"></div>
<div class="card_exp_month_div"></div> - <div class="c
ard_exp_year_div"></div>
<div class="security_code_div"></div>
<input type="checkbox" class="juspay_locker_save"> Sa
ve card information
<input type="hidden" class="redirect" value="true"/>
<input type="radio" class="auth_type" value="" name="a
uth_type"> Verify with Secure Password
<input type="radio" class="auth_type" value="ATMPIN" n
ame="auth_type"> Verify with ATM PIN
<button type="submit" id="common_pay_btn">Make Payment
</button>
</form>


<script type="text/javascript"
src="https://axisbank.juspay.in/pay-v3.js"></script
>
		
		
<script type="text/javascript">
Highcharts.chart('monthly_chart', {
    chart: {
        type: 'line'
    },
    title: {
        text: 'Monthly Average Orders <?php echo date('Y'); ?>'
    },
    subtitle: {
        text: 'Source: Xpress Delivery'
    },
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },
    yAxis: {
        title: {
            text: 'Orders'
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [{
        name: 'Monthly Orders',
        data: [<?php print_r($orderManaly); ?>]
    }]
});
</script>



</body>

</html>