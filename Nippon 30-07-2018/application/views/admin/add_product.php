<?php include('includes/header.php'); ?>
<link href="<?php echo base_url(); ?>assets/css/checkbox.css" rel="stylesheet">
<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>

<link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css"   rel="stylesheet" type="text/css" />

<link rel='stylesheet prefetch' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css'>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<style>  
.rotate{
    -moz-transition: all 2s linear;
    -webkit-transition: all 2s linear;
    transition: all 2s linear;
    float: right !important;   
    margin-top: -50px !important;
    margin-right: 125px !important;
    font-size: 20px !important;
}

.rotate.down{
    -moz-transform:rotate(180deg);
    -webkit-transform:rotate(180deg);
    transform:rotate(180deg);
}

label.error{
    color: red;
    font-size: 14px;    
    text-transform: lowercase;
}

#error-msg{
color: red;
    font-size: 14px;    
    margin-left: 250px;
}
span.multiselect-native-select .btn-group {
    width: 100%;
    overflow: visible;
    border: 1px solid #eee;
}
.btn-group, .btn-group-vertical {
    position: relative;
    display: inline-block;
    vertical-align: middle;
}
ul.multiselect-container.dropdown-menu {
    width: 100%;
}
button.multiselect.dropdown-toggle.btn.btn-default {
    width: 100%;
}
.multiselect-container {
		height:400px;
		overflow:auto;
	}
button.multiselect.dropdown-toggle.btn.btn-default {
    text-align: left;
    background: transparent;
	width:100%;
}
ul.multiselect-container.dropdown-menu label.checkbox{
	border:0px !important;
}
span.multiselect-native-select .btn-group{
	width: 100%;
    overflow: visible;
    border: 1px solid #eee;
}
ul.multiselect-container.dropdown-menu {
	width:100%;
}
.dropdown-menu > .active > a {
	background-color:white !important;
}
ul.multiselect-container.dropdown-menu.show{
	margin-top:220px !important;	
}
</style>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
             <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Add/Edit Product 
					</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Add/Edit Product </li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            
            
 <!-- Container fluid  -->
 
<?php  if($this->session->flashdata("message")){ ?>
<div id="alert" class="alert alert-info" style="width:450px;margin-left: 386px;margin-top:28px;position:relative">
Action performed successfully
</div>
<?php } ?>



           
           
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-validation">
                                    <form class="form-valide" action="<?php echo base_url(); ?>admin/products_action/<?php echo $product_data['pid']; ?>" method="post" enctype="multipart/form-data"   id="plumber_form">
			

	<div class="form-group row">
		<label class="col-lg-4 col-form-label" for="val-username">Category<span class="text-danger">*</span></label>		
		<div class="col-lg-6">
			<select class="form-control" id="category" name="category">	
				<option>Select Product Category</option>
				<?php foreach($product_categories as $cval){ ?>
				<option <?php if(strpos($product_data['product_category'], $cval['pcid']) !== false){ ?> selected <?php } ?> value="<?php echo $cval['pcid']; ?>"><?php echo $cval['product_category']; ?></option>
				<?php } ?>
			</select>												
		</div>
	</div>
	
	<div class="form-group row">
		<label class="col-lg-4 col-form-label" for="val-username">Suggest For <span class="text-danger">*</span></label>		
		<div class="col-lg-6">
			<select class="form-control" id="offer_valid" name="suggested_for[]" multiple="multiple">																					
				<?php foreach($products as $dval){ ?>
				<option <?php if(strpos($product_data['suggested_for'], $dval['pid']) !== false){ ?> selected <?php } ?> value="<?php echo $dval['pid']; ?>"><?php echo $dval['product_name']; ?></option>
				<?php } ?>
			</select>												
		</div>
	</div>							
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Product Name  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="name" name="name" placeholder="Enter Product Name" value='<?php print_r($product_data['product_name']); ?>'>
	</div>
	</div>
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username"> Brand  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<select class="form-control" id="colour_brand" name="colour_brand">
		<option value="">Select Brand</option>
		<?php foreach($brands as $bval){?>
			<option <?php if($bval['bid'] == $product_data['brand']){?> selected <?php } ?>  value="<?php echo $bval['bid']; ?>"><?php echo $bval['brand_name']; ?></option>
		<?php } ?>
	</select>
	</div>
	</div>
	
	<div class="form-group row">
		<label class="col-lg-4 col-form-label" for="val-username"> Image  <span class="text-danger">*</span></label>
		<div class="col-lg-6">
			<input type="file" class="form-control" <?php if($product_data['pid'] == ""){ ?> id="brand_image" <?php   } ?> name="brand_image" >
		</div>
	</div>
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Price  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="price" name="price" placeholder="Enter Product Price" value='<?php print_r($product_data['product_price']); ?>'>
	</div>
	</div>
	
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Description  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<textarea class="form-control" id="desc" name="desc" ><?php print_r($product_data['description']); ?></textarea>
	</div>
	</div>
	
	
	
										
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Features  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<textarea class="form-control" id="features" name="features" ><?php print_r($product_data['features']); ?></textarea>
	</div>
	</div>
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Benefits  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<textarea class="form-control" id="benefits" name="benefits" ><?php print_r($product_data['benefits']); ?></textarea>
	</div>
	</div>
										
	<div class="form-group row">
		<label class="col-lg-4 col-form-label" for="val-username"> TDS  <span class="text-danger">*</span></label>
		<div class="col-lg-6">
			<input type="file" class="form-control" <?php if($product_data['pid'] == ""){ ?> id="tds_image" <?php   } ?> name="tds_image" >
		</div>
	</div>

	<div class="form-group row">
		<label class="col-lg-4 col-form-label" for="val-username"> SDS  <span class="text-danger">*</span></label>
		<div class="col-lg-6">
			<input type="file" class="form-control" <?php if($product_data['pid'] == ""){ ?> id="sds_image" <?php   } ?> name="sds_image" >
		</div>
	</div>	
				
				
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Unit Of Measurement <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="uom" name="uom" placeholder="Enter UOM" value='<?php print_r($product_data['uom']); ?>'>
	</div>
	</div>
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Pack Size <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="pack_size" name="pack_size" placeholder="Enter Pack Size" value='<?php print_r($product_data['pack_size']); ?>'>
	</div>
	</div>
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Quantity  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="quantity" name="quantity" placeholder="Enter Quantity" value='<?php print_r($product_data['quantity']); ?>'>
	</div>
	</div>
	
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Minimum Order Quantity  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="moq" name="moq" placeholder="Enter MOQ" value='<?php print_r($product_data['moq']); ?>'>
	</div>
	</div>
	
	
	
	
	
										<span id="error-msg"></span>
	
	<input type="hidden" name="old_image" value="<?php echo $product_data['product_image']; ?>" />
	<input type="hidden" name="old_tds_image" value="<?php echo $product_data['tds']; ?>" />
	<input type="hidden" name="old_sds_image" value="<?php echo $product_data['sds']; ?>" />
										
                                        <div class="form-Type row" style="margin-top:10px">
                                            <div class="col-lg-8 ml-auto">
       <button type="submit" class="btn btn-primary" name="submit" onclick="return check_validation();" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> � 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    
<script>
$(".rotate").click(function(){
 $(this).toggleClass("down")  ; 
})

$("#alert").fadeOut(5000);
</script>


<script>
function get_city(){
	var $state = $("#state").val();
	var $BASE_URL = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{'state':$state},
		url: $BASE_URL+'admin/get_cities',
		success:function(data){
			$("#city").html(data);
		}
	});
}
</script>


<script>
function check_validation(){
var name = $("#name").val();



if(name == "" || name == "null"){
$("#error-msg").html('Fields marked with * are mandatory');
return false;
}else{
$("#error-msg").html('');
return true;
}
}

</script>


<script type="text/javascript">
$(function () {
	$('#offer_valid').multiselect({
		includeSelectAllOption: true
	});
	$('#btnSelected').click(function () {
		var selected = $("#offer_valid option:selected");
		var message = "";
		selected.each(function () {
			message += $(this).text() + " " + $(this).val() + "\n";
		});                
	});
});
</script>


<script>
function isNumberKey(evt){
              var charCode = (evt.which) ? evt.which : event.keyCode;
              if(charCode != 43 && charCode !=45 && charCode > 31 && (charCode < 48 || charCode > 57))
                 return false;
              else
                 return true;
           }
</script>

<script>
      CKEDITOR.replace( 'desc' );
      CKEDITOR.replace( 'benefits' );
      CKEDITOR.replace( 'features' );
</script>


<script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"   type="text/javascript"></script>

</body>
</html>