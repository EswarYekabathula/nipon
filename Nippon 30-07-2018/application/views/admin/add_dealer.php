<?php include('includes/header.php'); ?>
<link href="<?php echo base_url(); ?>assets/css/checkbox.css" rel="stylesheet">
<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>

<style>
.rotate{
    -moz-transition: all 2s linear;
    -webkit-transition: all 2s linear;
    transition: all 2s linear;
    float: right !important;   
    margin-top: -50px !important;
    margin-right: 125px !important;
    font-size: 20px !important;
}

.rotate.down{
    -moz-transform:rotate(180deg);
    -webkit-transform:rotate(180deg);
    transform:rotate(180deg);
}
#myMap{
height: 450px !important;
}
label.error{
    color: red;
    font-size: 14px;    
    text-transform: lowercase;
}

#error-msg{
color: red;
    font-size: 14px;    
    margin-left: 250px;
}
</style>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
             <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Add/Edit Dealer 
					</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Add/Edit Dealer </li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            
            
 <!-- Container fluid  -->
 
<?php  if($this->session->flashdata("message")){ ?>
<div id="alert" class="alert alert-info" style="width:450px;margin-left: 386px;margin-top:28px;position:relative">
Action performed successfully
</div>
<?php } ?>



           
           
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-validation">
                                    <form class="form-valide" action="<?php echo base_url(); ?>admin/dealer_action/<?php echo $dealer_data['did']; ?>" method="post" enctype="multipart/form-data"   id="plumber_form">
								
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Dealer Name  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="name" name="name" placeholder="Enter Dealer Name" value='<?php print_r($dealer_data['dealer_name']); ?>'>
	</div>
	</div>
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Store Name  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="name" name="store_name" placeholder="Enter Store Name" value='<?php print_r($dealer_data['store_name']); ?>'>
	</div>
	</div>
	
		<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Email  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="email" name="email" placeholder="Enter email" value='<?php print_r($dealer_data['email']); ?>'  onkeyup="return check_email();">
	</div>
	</div>
	
	
		<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Mobile  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Enter mobile" value='<?php print_r($dealer_data['mobile']); ?>' onkeyup="return check_mobile();">
	</div>
	</div>
	
	<div class="form-group row">
		<label class="col-lg-4 col-form-label" for="val-username"> Shop Image  <span class="text-danger">*</span></label>
		<div class="col-lg-6">
			<input type="file" class="form-control" <?php if($dealer_data['did'] == ""){ ?> id="brand_image" <?php   } ?> name="shop_photo" >
		</div>
	</div>
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Flat No  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="flatno" name="flatno" placeholder="Enter Flat No" value='<?php print_r($dealer_data['flat_no']); ?>'>
	</div>
	</div>
	
<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-username">State  <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <select class="form-control" id="state" name="state" onchange="get_city();">
													<option>Select State</option>
													<?php foreach($states as $state_value){ ?>
													<option <?php if($dealer_data['state'] == $state_value['State']){ ?> selected <?php } ?>  value="<?php echo $state_value['State']; ?>"><?php echo $state_value['State']; ?></option>
													<?php } ?>
												</select>
                                            </div>
                                        </div>
										
										
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-username">City  <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <select class="form-control" id="city" name="city" >
													<option>Select City</option>
													<?php if($dealer_data['did'] != ""){ ?>
													<?php foreach($cities as $cval){ ?>
													<option <?php if($dealer_data['city'] == $cval['City']){ ?> selected <?php } ?> value="<?php echo $cval['City']; ?>"><?php echo $cval['City']; ?></option>
													<?php } ?>
													<?php } ?>													
												</select>
                                            </div>
                                        </div>
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Colony  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="colony" name="colony" placeholder="Enter Colony" value='<?php print_r($dealer_data['colony']); ?>'>
	</div>
	</div>
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Pincode  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="pincode" name="pincode" placeholder="Enter Pincode" value='<?php print_r($dealer_data['pincode']); ?>'>
	</div>
	</div>
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Pan card  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="pancard" name="pancard" placeholder="Enter Pancard" value='<?php print_r($dealer_data['pan_number']); ?>'>
	</div>
	</div>
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">GST  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="gst" name="gst" placeholder="Enter GST" value='<?php print_r($dealer_data['gst_number']); ?>'>
	</div>
	</div>
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-username">Cancelled Cheque  <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="cc" name="cc" placeholder="Enter Cancelled Cheque" value='<?php print_r($dealer_data['cancelled_cheque']); ?>'>
	</div>
	</div>
	
		<?php if($dealer_data['did'] == ""){ ?>
										<div class="form-group row">
											<label class="col-lg-4 col-form-label" for="val-password"> Password <span class="text-danger">*</span></label>
											<div class="col-lg-6">
											<input type="text" class="form-control" id="password" name="password" >	
											</div>
										</div>
										<a id="update" href="javascript:password();"><i class="fa fa-refresh rotate"></i></a>
										<?php } ?>
	
	
	
	
										<span id="error-msg"></span>
	
	<input type="hidden" name="old_image" value="<?php echo $dealer_data['Dealer_image']; ?>" />
	<input type="hidden" name="old_tds_image" value="<?php echo $dealer_data['tds']; ?>" />
	<input type="hidden" name="old_sds_image" value="<?php echo $dealer_data['sds']; ?>" />
										
										<input type="hidden" id="mobile_validation" value=""/>
										<input type="hidden" id="email_validation" value=""/>
										
										
                                        <div class="form-Type row" style="margin-top:10px">
                                            <div class="col-lg-8 ml-auto">
       <button type="submit" class="btn btn-primary" name="submit" onclick="return check_validation();" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> � 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    
<script>
$(".rotate").click(function(){
 $(this).toggleClass("down")  ; 
})

$("#alert").fadeOut(5000);
</script>


<script>
function get_city(){
	var $state = $("#state").val();
	var $BASE_URL = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{'state':$state},
		url: $BASE_URL+'admin/get_cities',
		success:function(data){
			$("#city").html(data);
		}
	});
}
</script>


<script>
function check_validation(){
var name = $("#name").val();



if(name == "" || name == "null"){
$("#error-msg").html('Fields marked with * are mandatory');
return false;
}else{
$("#error-msg").html('');
return true;
}
}

</script>





<script>
$(document).ready(function(){
 var text = "";
    var possible = "0123456789";
    for( var i=0; i < 8; i++ ){
        text += possible.charAt(Math.floor(Math.random() * possible.length));    		
	}
	$("#password").val(text);
});
function password(){
	var text = "";
    var possible = "0123456789";
    for( var i=0; i < 8; i++ ){
        text += possible.charAt(Math.floor(Math.random() * possible.length));    		
	}
	$("#password").val(text);
}
</script>



<script>
function check_mobile(){
var BASE_URL = "<?php echo base_url(); ?>";
var mobile = $("#mobile").val();
var table = 'dealers';
jQuery.ajax({
type:'POST',
data:{mobile:mobile,table:table},
url:BASE_URL+'admin/check_mobile',
success:function(data){
if(data == 1){
$("#mobile_validation").val('1');
}else{
$("#mobile_validation").val('0');
}
} 

});
}


function check_email_fp(){
var BASE_URL = "<?php echo base_url(); ?>";
var email = $("#email").val();
var table = 'dealers';
jQuery.ajax({
type:'POST',
data:{email:email,table:table},
url:BASE_URL+'admin/check_email',
success:function(data){
if(data == 1){
$("#email_validation").val('1');
}else{
$("#email_validation").val('0');
}
} 

});
}
</script>




<script>
function isNumberKey(evt){
              var charCode = (evt.which) ? evt.which : event.keyCode;
              if(charCode != 43 && charCode !=45 && charCode > 31 && (charCode < 48 || charCode > 57))
                 return false;
              else
                 return true;
           }
</script>

<script>
      CKEDITOR.replace( 'desc' );
      CKEDITOR.replace( 'benefits' );
      CKEDITOR.replace( 'features' );
</script>

</body>

</html>