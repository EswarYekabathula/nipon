<?php include('includes/header.php'); ?>


<style>
.dataTables_wrapper .dataTables_paginate {
    display: none !important;
}
.form-control {
     border: 0px solid #ced4da !important; 
}
.form-group {
margin-bottom:0px !important;
    height: 40px !important; 
}
.pagination {
    display: inline-block;
	float:right;
	    margin-top: 10px;
}
.dataTables_info, .dataTables_length {
    display: none !important;
}
div#example23_paginate{
	display:none !important;
}
.pagination a {
    color: black;
	border: 1px solid #ddd; /* Gray */
    float: left;
    padding: 8px 16px;
    text-decoration: none;
}

.pagination  strong {
	color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    background-color: #1976d2;
    color: white;
}
a:not([href]):not([tabindex]) {
    padding: 0px !important;
}
</style>


<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
             <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary"> <?php echo ucfirst($table); ?> Details</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin">Home</a></li>
                        <li class="breadcrumb-item active"> <?php echo ucfirst($table); ?> Details</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <?php  if($this->session->flashdata("message")){?>
<div id="alert" class="alert alert-info" style="width:450px;margin-left: 275px;">
Profile updated successfully
</div> 
<?php }?>
            <div class="container-fluid">
			
			<?php if($table != 'dealers'){ ?>
                <!-- Start Page Content -->
                <div class="row">
<div class="col-lg-6">
<div class="card" style="height: auto;!important">
<div class="card-body">
<div class="form-validation">
<form class="form-valide" action="<?php echo base_url(); ?>admin/edit_profile" method="post" enctype="multipart/form-data">
<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Name </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['name']); ?></p>
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Email </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['email']); ?></p>
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Phone Number </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['phone']); ?></p>
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">State </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['state']); ?></p>
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">City </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['city']); ?></p>
</div>
</div>

</form>
</div>
</div>
</div>
</div>
					
<div class="col-lg-6">
<div class="card" style="height: auto;!important">
<div class="card-body">
<div class="form-validation">
<form class="form-valide" action="<?php echo base_url(); ?>admin/edit_profile" method="post" enctype="multipart/form-data">                                        


<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Designation </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['designation']); ?></p>
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Created Date </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['created_date']); ?></p>
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password"> Address </label>
<div class="col-lg-6">
<p   ><?php print_r($udetails['address']); ?></p>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
                </div>
                <?php }else{ ?>
				
				
				<div class="row">
<div class="col-lg-6">
<div class="card" style="height: auto;!important">
<div class="card-body">
<div class="form-validation">
<form class="form-valide" action="<?php echo base_url(); ?>admin/edit_profile" method="post" enctype="multipart/form-data">
<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Name </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['dealer_name']); ?></p>
</div>
</div>


<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Store Name </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['store_name']); ?></p>
</div>
</div>



<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Email </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['email']); ?></p>
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Phone Number </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['mobile']); ?></p>
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Flat No </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['flat_no']); ?></p>
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">City </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['city']); ?></p>
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">State </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['state']); ?></p>
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Colony </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['colony']); ?></p>
</div>
</div>


</form>
</div>
</div>
</div>
</div>
					
<div class="col-lg-6">
<div class="card" style="height: auto;!important">
<div class="card-body">
<div class="form-validation">
<form class="form-valide" action="<?php echo base_url(); ?>admin/edit_profile" method="post" enctype="multipart/form-data">                                        


<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Pincode </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['pincode']); ?></p>
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Created Date </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['created_on']); ?></p>
</div>
</div>



<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Pan Number</label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['pan_number']); ?></p>
</div>
</div>


<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">GST </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['gst_number']); ?></p>
</div>
</div>


<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Cancelled Cheque </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($udetails['cancelled_cheque']); ?></p>
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password"> Profile Image </label>
<div class="col-lg-6">
<?php if($udetails['profile_image'] != ''){?>
<img src="<?php echo base_url(); ?>assets/uploads/dealers/<?php print_r($udetails['profile_image']); ?>" style="width:50px;height:50px" alt="Profile Image">
<?php  } ?>
</div>
</div>

<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password"> Shop Image </label>
<div class="col-lg-6">
<?php if($udetails['shop_image'] != ''){?>
<img src="<?php echo base_url(); ?>assets/uploads/shops/<?php print_r($udetails['shop_image']); ?>" style="width:50px;height:50px" alt="Profile Image">
<?php  } ?>
</div>
</div>

</form>
</div>
</div>
</div>
</div>
                </div>
				
				<?php } ?>





				<?php if($table == 'franchise_partners'){ ?>				
					<div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body p-b-0">
                                    <h4 class="card-title"></h4>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs customtab2" role="tablist">
                                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home7" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Stocks</span></a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile7" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Orders Inprogress</span></a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile8" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Served Orders</span></a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages7" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Rejected Orders</span></a> </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="home7" role="tabpanel">
                    <div class="card">
                            <div class="card-body">
						
                                <h4 class="card-title">Stocks</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="stocks" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Product Name</th>                                                                                                
                                                <th>UOM</th>
                                                <th>Pack Size</th>												
                                                <th>Opening Quantity</th>												
                                                <th>Online Consumption</th>												
                                                <th>Offline Consumption</th>												
												<th>Closing</th>                                      
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Product Name</th>                                                                                                
                                                <th>UOM</th>
                                                <th>Pack Size</th>												
                                                <th>Opening Quantity</th>												
                                                <th>Online Consumption</th>																																	
                                                <th>Offline Consumption</th>																																	
												<th>Closing</th>                                           
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php $start_index = ($this->uri->segment(4)) ? $this->uri->segment(4)+1 : 1;
										foreach($products as $value){ ?>
                                            <tr id="row<?php echo $value['pid']; ?>">
												<td><?php  echo $start_index++; ?></td>
                                                <td><?php echo $value['product_name']; ?></td>
                                                <td><?php echo $value['uom']; ?></td>
                                                <td><?php echo $value['pack_size']; ?></td>
                                                <td><?php echo $value['quantity']; ?></td>
                                                <td><?php echo $value['online_consumption']; ?></td>
                                                <td>
<div class="input-group">
<input type="text" id="offline_consumption<?php echo $value['pid']; ?>" class="form-control" value="<?php echo $value['offline_consumption']; ?>" readonly onclick="remove_readonly(<?php echo $value['pid']; ?>)">
<span class="input-group-btn" onclick="update_offline(<?php echo $value['pid']; ?>);"><button class="btn btn-primary btn-group-right" type="submit"><i class="fa fa-pencil"></i></button></span>
</div>
												
						                        </td>
                                                <td id="closing<?php echo $value['pid']; ?>">
												<?php 
												if($value['online_consumption'] != '0'){ 
												$consumption = $value['online_consumption']+$value['offline_consumption']; 
												echo $value['quantity'] - $consumption;
												}else{
													echo '0';
												}
												?>
												</td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
								 <?php echo '<div class="pagination">'.$links.'</div>'; ?>
                            </div>
                        </div>
                                        </div>
                                        <div class="tab-pane  p-20" id="profile7" role="tabpanel"></div>
                                        <div class="tab-pane p-20" id="profile8" role="tabpanel"></div>
                                        <div class="tab-pane p-20" id="messages7" role="tabpanel"></div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
				<?php } else if($table == 'dealers'){ ?>
					<div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body p-b-0">
                                    <h4 class="card-title"></h4>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs customtab2" role="tablist">
                                        
                                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile8" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Orders Placed</span></a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages7" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Orders Completed</span></a> </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                      
                                      
                                        <div class="tab-pane p-20" id="profile8" role="tabpanel"></div>
                                        <div class="tab-pane p-20" id="messages7" role="tabpanel"></div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
				<?php } ?>
				
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
<script>
$("#alert").fadeOut(5000);
</script>


<script>
function remove_readonly(product_id){
	$("#offline_consumption"+product_id).removeAttr('readonly');
}
</script>

<script>
function update_offline(product_id){
	var offline_consumption = $("#offline_consumption"+product_id).val();
	var base_url = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{product_id:product_id,offline_consumption:offline_consumption},
		url:base_url+'admin/update_product_offline_consumption',
		success:function(data){
			$details = JSON.parse(data);
			if($details['status'] == '1'){
				$("#offline_consumption"+product_id).val($details['offline_consumption']);
				$("#offline_consumption"+product_id).attr('readonly', true);
				$("#closing"+product_id).html($details['closing']);
				setTimeout(function() {
		iziToast.success({   message: "Offline consumption updated successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);
			}else{
				setTimeout(function() {
		iziToast.warning({   message: "Please try later!",   position: "topRight",   zindex:	"99999"		}); }, 100);
			}
		}
	});
}
</script>

</body>
</html>