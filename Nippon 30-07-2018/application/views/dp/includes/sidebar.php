<div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
<li><a class="" href="<?php echo base_url(); ?>dp" aria-expanded="false"><span class="hide-menu">Dashboard </span></a>
<li> <a class="" href="<?php echo base_url(); ?>dp/orders" aria-expanded="false"><i class="fa fa-cart-plus"></i><span class="hide-menu">Orders</span></a></li> 
<li> <a class="" href="<?php echo base_url(); ?>dp/routes" aria-expanded="false"><i class="fa fa-road"></i><span class="hide-menu">Routes</span></a></li>  
<li> <a class="" href="<?php echo base_url(); ?>dp/riders" aria-expanded="false"><i class="fa fa-motorcycle"></i><span class="hide-menu">Riders</span></a></li>  
<li> <a class="" href="<?php echo base_url(); ?>dp/franchise_partners" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu">Franchise Partners</span></a></li>  


                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>         