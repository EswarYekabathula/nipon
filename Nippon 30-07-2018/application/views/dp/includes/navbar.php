<?php
$dp_id = $this->session->userdata('dp_session')['id'];
$this->load->model('dp_model','my_model');
$user_id = $this->session->userdata('dp_session')['id'];
?>
<style>
.cart_count{
	margin-top: -10px;
    position: absolute;
    font-size: 15px;
    color: #ef5350;
}
.mailbox .message-center a .mail-contnet {
    padding-left: 25px !important;
}
.fa.pull-right {
    margin-top: -30px !important;
    color: #ef5350 !important;
}
.mailbox .message-center a .mail-contnet .mail-desc {    
    white-space:  unset !important; 
}
</style>
<nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url(); ?>/dp">
                        <!-- Logo icon -->
                        <!--<b><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="homepage" class="dark-logo" /></b>-->
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <!--<span><img src="<?php echo base_url(); ?>assets/images/logo-text.png" alt="homepage" class="dark-logo" /></span>-->
						<b>Xpress Delivery</b>
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <!-- Messages -->
                    
                        <!-- End Messages -->
                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle text-muted text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
							<i class="fa fa-bell" onclick="update_notifications(<?php echo $dp_id; ?>)"></i>
							<div class="notify" id="notify"> 
								<?php 								
								$where = "uid=".$dp_id." and send_to='DP' and view_status='0'";
								$nrows = $this->my_model->count_with_condition('notifications',$where); 
								if($nrows > 0){
								?>
								<span class="heartbit" onclick="update_notifications(<?php echo $dp_id; ?>)"></span> 
								<span class="point" onclick="update_notifications(<?php echo $dp_id; ?>)"></span> 
								<?php } ?>
							</div>
							</a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn" >
                                <ul>
                                    <li>
                                        <div class="drop-title">Notifications</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
											<?php
											$where = "uid=".$dp_id." and send_to='DP'";
											$notifications = $this->my_model->check('notifications',$where)->result_array();
											foreach($notifications as $nval){
											?>
                                            <!-- Message -->
                                            <a href="#">
                                                <div class="btn btn-success btn-circle m-r-10"><i class="ti-calendar"></i></div>
                                                <div class="mail-contnet">
                                                     <span class="mail-desc"><?php echo $nval['message']; ?></span> <span class="time"><?php echo date('d D F Y', strtotime($nval['date'])); ?></span>
                                                </div>
                                            </a>
											<?php } ?>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                       
                        <!-- Profile -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						
							<img src="<?php echo base_url(); ?>assets/uploads/users/default.png" alt="user" class="profile-pic" /></a>
							
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">
                                    <li><a href="<?php echo base_url(); ?>dp/edit_profile"><i class="ti-user"></i> Profile</a></li>
                                    <!--<li><a href="#"><i class="ti-wallet"></i> Balance</a></li>
                                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                    <li><a href="#"><i class="ti-settings"></i> Setting</a></li>-->
                                    <li><a href="<?php echo base_url(); ?>dp/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
			
			<script>
			function update_notifications(dp_id){
				var base_url = '<?php echo base_url(); ?>';
				var send_to = 'DP';
				jQuery.ajax({
					type:'POST',
					data:{uid:dp_id,send_to:send_to},
					url:base_url+'dp/update_notifications_status',
					success:function(data){
						if(data == 1){
						$("#notify").hide();
						}
					}
				});
			}
			</script>
			