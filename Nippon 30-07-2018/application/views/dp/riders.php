<?php include('includes/header.php'); 
$this->load->model('Dp_model','my_model');?>
<style>
.pagination {
    display: inline-block;
	float:right;
	    margin-top: 10px;
}
.dataTables_info, .dataTables_length {
    display: none !important;
}
div#example23_paginate{
	display:none !important;
}
.pagination a {
    color: black;
	border: 1px solid #ddd; /* Gray */
    float: left;
    padding: 8px 16px;
    text-decoration: none;
}

.pagination  strong {
	color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    background-color: #1976d2;
    color: white;
}
a:not([href]):not([tabindex]) {
    padding: 0px !important;
}
</style>
<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
                    <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Riders</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Riders</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluuid  -->
            <div class="container-fluuid">
			<?php  if($this->session->flashdata('message') == "success"){ 	 
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Rider Deleted successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "updated"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Rider Updated successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "added"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Rider Added successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}
		?>
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
							
							<div class="card-header" style="width: 125px;float: right;background: #1976d2;border-radius: 20px;">
                                <h4 class="m-b-0 text-white"><a href="<?php echo base_url();  ?>dp/rider_action" style="color: white;">Add Rider<a/></h4>
                            </div>
                                <h4 class="card-title">Riders Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Name</th>                                                                                                
                                                <th>Address</th>
                                                <th>Vehicle Type</th>
												<th>Route</th>
												<th>Actions</th>                                           
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Name</th>                                                                                                
                                                <th>Address</th>
                                                <th>Vehicle Type</th>
												<th>Route</th>
												<th>Actions</th>                                         
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3)+1 : 1; foreach($riders as $value){ ?>
                                            <tr id="row<?php echo $value['rid']; ?>">
                                                 <td><?php  echo $start_index++; ?></td>
                                                <td><?php echo $value['rider_name']; ?></td>
                                                <td><?php echo $value['address']; ?></td>
                                                <td><?php echo $value['type_of_vehicle']; ?></td>
                                                <td>
												<?php 
												$route_name =  $this->my_model->check('routes',array('id'=>$value['route']))->row();
												echo $route_name->route_name;?>
												</td>
                                              
<td>
<span id="statuschange<?php echo $value['rid']; ?>">
<?php if($value['rstatus'] == '1'){ ?>
<button type="button" class="btn btn-info btn-xs m-b-10 m-l-5" onclick="change_rider_status(<?php echo $value['rid']; ?>,'1')">Active</button>
<?php }else{ ?>
<button type="button" class="btn btn-warning btn-xs m-b-10 m-l-5" onclick="change_rider_status(<?php echo $value['rid']; ?>,'0')">Inactive</button>
<?php } ?>
</span>
<a href="<?php echo base_url(); ?>dp/rider_action/<?php echo $value['rid']; ?>" class="btn btn-info btn-xs m-b-10 m-l-5"  >Edit</a>
<a href="javascript:delete_rider('riders','<?php echo $value['rid']; ?>')" class="btn btn-danger btn-xs m-b-10 m-l-5"  >Delete</a>
</td>                                             
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
										<?php echo '<div class="pagination">'.$links.'</div>'; ?>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluuid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    

<script>    
	function change_rider_status(id,status) {
	var bu = '<?php echo base_url(); ?>';	
	var r = confirm("Are you sure do you want to change the status");
	if (r == true) {
		jQuery.ajax({
			type:'POST',
			data:{id:id,status:status},			
			url:bu+'admin/change_rider_status',
			success:function(data){
			$("#statuschange"+id).html(data);				
			}
        
		});
	} else {

	}	
	}
</script>

<script>
function delete_rider($table,$id){
	var table = $table;
	var id  = $id;
	var retval = confirm('Are you sure do you want to delete this rider');
	if( retval == true ){
	var bu = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{table:table,id:id},
		url:bu+'admin/delete_rider',
		success:function(data){
			$("#row"+$id).hide();
			setTimeout(function() {
		iziToast.success({   message: "Rider Deleted successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);
		}
	});
	}
}
</script>


<script>
$('#example23').dataTable({	
$("#example23").dataTable().fnDestroy();	    
});
</script>

</body>

</html>