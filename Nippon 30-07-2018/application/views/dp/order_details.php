<?php include('includes/header.php'); 
$this->load->model('dp_model','my_model');?>


<style>
.form-control {
     border: 0px solid #ced4da !important; 
}
.form-group {
margin-bottom:0px !important;
    height: 40px !important; 
}
#map-canvas {
    overflow: hidden !important;
	height:350px !important;
	
}

</style>
<?php 
$rider_details = $this->my_model->check('riders',array('rid'=>$route_details['rider']))->row_array();
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBHNDW193OamX4lJaxKVkuxEeGGwcsdbPs"></script>




<input type="hidden" id="end_lattitude" value="<?php echo $rider_details['lat']; ?>" />
<input type="hidden" id="end_longitude" value="<?php echo $rider_details['lng']; ?>" />

<script>
function mapLocation() {
    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();
    var map;
	
    function initialize() {
        directionsDisplay = new google.maps.DirectionsRenderer();
        var chicago = new google.maps.LatLng(17.437461, 78.448288);
        var mapOptions = {
            draggable: false, zoomControl: false, scrollwheel: false, disableDoubleClickZoom: true
        };
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        directionsDisplay.setMap(map);
		calcRoute();
    }

    function calcRoute() {
		var rider_id = '<?php echo  $rider_details['rid']; ?>';
		var base_url = '<?php echo base_url(); ?>';
        var start = new google.maps.LatLng(17.437461, 78.448288);   


	$.ajax({	
		type: 'POST',
		data:{rider_id:rider_id},
		url: base_url+'dp/get_rider_lat_long',
		success: function (data) { 
		var latlong = JSON.parse(data);
			if (latlong['lattitude'] != null && latlong['longitude'] != null ) {
				var end = new google.maps.LatLng(latlong['lattitude'], latlong['longitude']);       
				$("#end_lattitude").val(latlong['lattitude']);
				$("#end_longitude").val(latlong['longitude']);
			};
		},
	error: function(data) {            
		//console.log(data)
	}
	});
	

        var end = new google.maps.LatLng($("#end_lattitude").val(), $("#end_longitude").val()); 
  
        var bounds = new google.maps.LatLngBounds();
        bounds.extend(start);
        bounds.extend(end);
        map.fitBounds(bounds);
        var request = {
            origin: start,
            destination: end,
            travelMode: google.maps.TravelMode.DRIVING
        };
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                directionsDisplay.setMap(map);
            } else {
                alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
            }
        });
		
		
    }
	
	
	setInterval(calcRoute, 10000);
    google.maps.event.addDomListener(window, 'load', initialize);
}
mapLocation();
</script>












<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
             <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Order Details</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Order Details</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
          
            <div class="container-fluid">
			
<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                 <h4 class="card-title"> Order Details</h4>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Item Name</th>
                                                <th>Image</th>
                                                <th>Count</th>                                                                                                                                                
                                                <th>Item Cost</th>                
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
												<th colspan="4" style="text-align:right">Total Cost</th>
												<th colspan="5" style="float: right;border: 0px;"><?php print_r($total_cost); ?></th>
                                            </tr>
                                        </tfoot>
<tbody>
	<?php $i=1; foreach($items as $ival){?>
<tr>
<td><?php echo $i++; ?></td>
<td><?php echo $ival['item_name']; ?></td>

<td><img src='<?php echo $ival['item_image']; ?>' style="width:50px;height:50px"></td>

<td><?php echo $ival['count']; ?></td>
<td><?php echo $ival['item_cost']; ?></td>
</tr>
<?php } ?>

</tbody>
                                    </table>
                                </div>
								
                            </div>
                        </div>
                      
                    </div>
                </div>

<div class="row">
<div class="col-lg-6">
<div class="card" style="height: auto;!important">
<div class="card-body">
<h4>Route Details</h4>
<?php 
$rdetails = $this->my_model->check('routes',array('id'=>$route_details['route']))->row_array();
?>
<div class="form-validation">
<form class="form-valide" action="<?php echo base_url(); ?>admin/edit_profile" method="post" enctype="multipart/form-data">
<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Route Name </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($rdetails['route_name']); ?></p>
</div>
</div>

<div class=" row">
<label class="col-lg-4 col-form-label" for="val-password">Start Location </label>
<div class="col-lg-6" style="height:auto !important">
<p><?php print_r($rdetails['start_location']); ?></p>
</div>
</div>

<div class=" row">
<label class="col-lg-4 col-form-label" for="val-password">End Location </label>
<div class="col-lg-6" style="height:auto !important">
<p><?php print_r($rdetails['end_location']); ?></p>
</div>
</div>



</form>
</div>
</div>
</div>
</div>

<div class="col-lg-6">
<div class="card" style="height: auto;!important">
<div class="card-body">
<h4>Rider Details</h4>

<div class="form-validation">
<form class="form-valide" action="<?php echo base_url(); ?>admin/edit_profile" method="post" enctype="multipart/form-data">
<div class="form-group row">
<label class="col-lg-4 col-form-label" for="val-password">Rider Name </label>
<div class="col-lg-6">
<p class="form-control"  ><?php print_r($rider_details['rider_name']); ?></p>
</div>
</div>

<div class=" row">
<label class="col-lg-4 col-form-label" for="val-password">Email </label>
<div class="col-lg-6" style="height:auto !important">
<p><?php print_r($rider_details['email']); ?></p>
</div>
</div>

<div class=" row">
<label class="col-lg-4 col-form-label" for="val-password">Address </label>
<div class="col-lg-6" style="height:auto !important">
<p><?php print_r($rider_details['address']); ?></p>
</div>
</div>


<div class=" row">
<label class="col-lg-4 col-form-label" for="val-password">Vehicle Type </label>
<div class="col-lg-6" style="height:auto !important">
<p><?php print_r($rider_details['type_of_vehicle']); ?></p>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
				<div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4>Track Order</h4>
								<div id="map-canvas"></div>
                            </div>
                        </div>
                      
                    </div>
                </div>

				
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
<script>
$("#alert").fadeOut(5000);
</script>
</body>

</html>