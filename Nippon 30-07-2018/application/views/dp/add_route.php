<?php include('includes/header.php'); ?>
<style>
.rotate{
    -moz-transition: all 2s linear;
    -webkit-transition: all 2s linear;
    transition: all 2s linear;
    float: right !important;   
    margin-top: -50px !important;
    margin-right: 125px !important;
    font-size: 20px !important;
}

.rotate.down{
    -moz-transform:rotate(180deg);
    -webkit-transform:rotate(180deg);
    transform:rotate(180deg);
}
#myMap{
height: 250px !important;
}
label.error{
    color: red;
    font-size: 14px;    
    text-transform: lowercase;
}

#error-msg{
color: red;
    font-size: 14px;    
    margin-left: 250px;
}
</style>

<?php if($route_data['id'] == ''){ ?>
<input type="hidden" id="lat" value="17.385044" name="latitude">
<input type="hidden" id="lng" value="78.486671" name="longitude"> 
<?php }else{ ?>
<input type="hidden" id="lat" value="<?php echo $route_data['slatitude'] ?>" name="latitude">
<input type="hidden" id="lng" value="<?php echo $route_data['slongitude'] ?>" name="longitude"> 
<?php } ?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCAs0lgjOYrFXG74z0xQ7evWkZUMQwzATo&callback=initMap"></script>

<link rel='stylesheet prefetch' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css'>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>

		<script type="text/javascript">
	        var lat = $("#lat").val();
			var lng = $("#lng").val();			
            var map;
            var marker;			
            var myLatlng = new google.maps.LatLng(lat,lng);			
            var geocoder = new google.maps.Geocoder();			
            var infowindow = new google.maps.InfoWindow();
            function initialize(){
                var mapOptions = {
                    zoom: 12,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
		       
                map = new google.maps.Map(document.getElementById("myMap"), mapOptions);
                
                marker = new google.maps.Marker({
                    map: map,
                    position: myLatlng,
                    draggable: true 
                });           
                google.maps.event.addListener(marker, 'dragend', function() {

                geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {	
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {                          
                            $('#lat').val(marker.getPosition().lat());
                            $('#lng').val(marker.getPosition().lng());
                        }
                    }
                });
				var lat = marker.getPosition().lat();
				var lng = marker.getPosition().lng();
				callfunction(lat,lng);				
            });            
            }            
            google.maps.event.addDomListener(window, 'load', initialize);
		
        </script>
		
<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
             <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Add Route</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Add Route</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            
            
 <!-- Container fluid  -->
 
<?php  if($this->session->flashdata("message")){ ?>
<div id="alert" class="alert alert-info" style="width:450px;margin-left: 386px;margin-top:28px;position:relative">
Action performed successfully
</div>
<?php } ?>


            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-validation">
                                    <form class="form-valide" action="<?php echo base_url(); ?>dp/route_action/<?php echo $route_data['id'];?>" method="post" enctype="multipart/form-data"   id="plumber_form">



                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-username">Route Name <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="route_name" name="route_name" placeholder="Enter route name.." autocomplete="off" value="<?php echo $route_data['route_name']; ?>">
                                            </div>
                                        </div>
							
	<h5 class="col-lg-8">Start Location </h5>
	<div id="myMap"></div>                                       

	<div class="form-group row" style="margin-top:15px !important">
	<label class="col-lg-4 col-form-label" for="val-password"> Lattitude <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="slattitude" name="slattitude" placeholder="Start Location Lattitude" autocomplete="off" value="<?php echo $route_data['slatitude']; ?>">	
	</div>
	</div>
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-password"> Longitude<span class="text-danger">*</span> </label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="slongitude" name="slongitude"  placeholder="Start Location Longitude" autocomplete="off" value="<?php echo $route_data['slongitude']; ?>"> 	
	</div>
	</div>
	
	
	<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="val-password"> Address <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<textarea  id="slocation" name="start_location" rows="3" cols="67"><?php echo $route_data['start_location']; ?></textarea>	
	</div>
	</div>
	
	<div class="form-group row" style="margin-top:15px !important">
	<label class="col-lg-4 col-form-label" for="val-password"> End Location <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<input type="text" class="form-control" id="end_location" onkeypress="return get_lat_long();" name="end_location"  placeholder="End Location" autocomplete="off" value="<?php echo $route_data['end_location']; ?>">	
	</div>
	</div>
	
	
	
	<input type="hidden" class="form-control" id="end_lattitude" name="end_lattitude" value="<?php echo $route_data['end_latitude']; ?>">		
	<input type="hidden" class="form-control" id="end_longitude" name="end_longitude" value="<?php echo $route_data['end_longitude']; ?>">	
	
	
	
	<div class="form-group row" style="margin-top:15px !important">
	<label class="col-lg-4 col-form-label" for="val-password"> Pickup Location <span class="text-danger">*</span></label>
	<div class="col-lg-6">
	<select class="form-control" id="pickup_location" name="pickup_location">	
		<option value="">Select Franchise Partner</option>
		<?php foreach($fpdata as $fval){ ?>
		<option <?php if($route_data['pickup_location'] == $fval['fid']){ echo 'selected'; ?>  <?php } ?> value="<?php echo $fval['fid']; ?>"><?php echo $fval['name'].' - '.$fval['lat'].' - '.$fval['lng']; ?></option>
		<?php } ?>
	</select>
	</div>
	</div>
	




<span id="error-msg"></span> 
                           
                           
                           <input type="hidden" id="mobile_validation" value=""/>
                           
                                        <div class="form-group row" style="margin-top:10px">
                                            <div class="col-lg-8 ml-auto">
       <button type="submit" class="btn btn-primary" name="submit" onclick="return check_validation();" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer"> � 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
  
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    
<script>
$(".rotate").click(function(){
 $(this).toggleClass("down")  ; 
})

$("#alert").fadeOut(5000);
</script>



<script>
	function callfunction($lat,$lng){		
		var lat = $lat;
		var lng = $lng;
		$("#slattitude").val(lat);
		$("#slongitude").val(lng);
		var BASE_URL = "<?php echo base_url(); ?>";
		jQuery.ajax({
			type:'POST',
			url:BASE_URL+'admin/get_location_details',
			data:{lat:lat,lng:lng},
			success:function(data){						
				$("#slocation").val(data);			
			}
		});
	}
</script>

   
<script>
function get_lat_long(){
var BASE_URL = "<?php echo base_url(); ?>";
var end_location = $("#end_location").val();
jQuery.ajax({
type:'POST',
data:{end_location:end_location},
url:BASE_URL+'admin/get_lat_long',
success:function(data){
var edata = JSON.parse(data);
//console.log(edata);
$("#end_lattitude").val(edata['lat']);
$("#end_longitude").val(edata['lng']);
} 
});
}
</script>
    
<script>
function check_validation(){
var name = $("#route_name").val();
var slattitude = $("#slattitude").val();
var slongitude = $("#slongitude").val();
var end_location = $("#end_location").val();
var pickup_location = $("#pickup_location").val();

if(name == "" || name == "null"){
$("#error-msg").html('Fields marked with * are mandatory');
return false;
}else if(slattitude == "" || slattitude == "null"){
$("#error-msg").html('Fields marked with * are mandatory');
return false;
}else if(slongitude == "" || slongitude == "null"){
$("#error-msg").html('Fields marked with * are mandatory');
return false;
}else if(end_location == "" || end_location == "null"){
$("#error-msg").html('Fields marked with * are mandatory');
return false;
}else if(pickup_location == "" || pickup_location == "null"){
$("#error-msg").html('Fields marked with * are mandatory');
return false;
}else{
$("#error-msg").html('');
return true;
}
}

</script>

</body>

</html>