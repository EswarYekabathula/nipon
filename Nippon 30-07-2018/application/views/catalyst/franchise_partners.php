<?php include('includes/header.php'); ?>
<style>
.pagination {
    display: inline-block;
	float:right;
	    margin-top: 10px;
}
.dataTables_info, .dataTables_length {
    display: none !important;
}
div#example23_paginate{
	display:none !important;
}
.pagination a {
    color: black;
	border: 1px solid #ddd; /* Gray */
    float: left;
    padding: 8px 16px;
    text-decoration: none;
}

.pagination  strong {
	color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    background-color: #1976d2;
    color: white;
}
a:not([href]):not([tabindex]) {
    padding: 0px !important;
}
.mtop{
	margin-top:10px !important;
	margin-left:125px !important;
}
</style>
<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
                    <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Franchise Partners</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Franchise Partners</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluuid  -->
            <div class="container-fluuid">
			<?php  if($this->session->flashdata('message') == "success"){ 	 
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Franchise Partner Deleted successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "updated"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Franchise Partner Updated successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "added"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Franchise Partner Added successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}
		?>
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
							
                                <h4 class="card-title">Franchise Partners Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
												<th>FP ID</th>
                                                <th>Name</th>                                                                                                
                                                <th>Email</th>
                                                <th>Phone</th>
												<th>Designation</th>
												<th>View</th>                                           
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>S.No</th>
												<th>FP ID</th>
                                                <th>Name</th>                                                                                                
                                                <th>Email</th>
                                                <th>Phone</th>
												<th>Designation</th>
												<th>View</th>                                           
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3)+1 : 1; foreach($fp as $value){ ?>
                                            <tr>
                                                <td><?php  echo $start_index++; ?></td>
                                                <td><?php echo $value['fid']; ?></td>
                                                <td><?php echo $value['name']; ?></td>
                                                <td><?php echo $value['email']; ?></td>
                                                <td><?php echo $value['phone']; ?></td>
                                                <td><?php echo $value['designation']; ?></td>
                                              
<td>
<a href="javascript:get_details('<?php echo $value['fid']; ?>','franchise_partners')"  class="btn btn-success btn-xs m-b-10 m-l-5">View</a>
</td>                                             
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
										<?php echo '<div class="pagination">'.$links.'</div>'; ?>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluuid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    
	
<div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<!-- Modal Header -->
<div class="modal-header">
<h4 class="modal-title" id="myModalLabel">
Franchise Partner Details
</h4>
</div>
<!-- Modal Body -->
<div class="modal-body">
<form class="form-horizontal" role="form"  id="detailsForm">


</form>
</div>
<!-- Modal Footer -->
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>

<script>    
function get_details(id,table){
	var base_url = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{id:id,table:table},
		url:base_url+'catalyst/get_details',
		success:function(html){
			$("#detailsForm").html(html);
			$('#detailsModal').modal('toggle');
			//alert(html);
		}
	});
}
</script>

<script>
$('#example23').dataTable({	
$("#example23").dataTable().fnDestroy();	    
});
</script>

</body>

</html>