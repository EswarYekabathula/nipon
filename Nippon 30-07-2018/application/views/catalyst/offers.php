<?php include('includes/header.php'); ?>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
                    <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Offers</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Offers</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluuid  -->
            <div class="container-fluuid">
			<?php  if($this->session->flashdata('message') == "success"){ 	 
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Offer Deleted successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "updated"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Offer Updated successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "added"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Offer Added successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}
		?>
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
							
							<div class="card-header" style="width: 125px;float: right;background: #1976d2;border-radius: 20px;">
                                <h4 class="m-b-0 text-white"><a href="<?php echo base_url();  ?>admin/offer_action" style="color: white;">Add Offer<a/></h4>
                            </div>
                                <h4 class="card-title">Offers Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                       <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Offer Name</th>                                                                                                
                                                <th>Offer Type</th>                                                                                                
                                                                                                                                               
                                                <th>Validity</th>  
                                                <th>Offer Image</th>  
												<th>Actions</th>                                           
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Offer Name</th>                                                                                                
                                                <th>Offer Type</th>                                                                                               
                                                <th>Validity</th>  
												<th>Offer Image</th>  
												<th>Actions</th>                                           
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php $i=1; foreach($Offers as $value){ ?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $value['offer_name']; ?></td>
                                                <td><?php echo $value['offer_type']; ?></td>
                                                <td><?php echo $value['offer_validity']; ?></td>
                                                <td><img src="<?php echo base_url(); ?>assets/uploads/offers/<?php echo $value['oimage']; ?>" style="width:50px;height:50px"</td>
                                               
<td>
<a href="<?php echo base_url(); ?>catalyst/offer_action/<?php echo $value['oid']; ?>" class="btn btn-primary btn-xs m-b-10 m-l-5">Edit</a>
<a href="<?php echo base_url(); ?>catalyst/delete_offer/<?php echo $value['oid']; ?>" class="btn btn-danger btn-xs m-b-10 m-l-5" onclick="return confirm('Are you sure you want to delete?')" >Delete</a>                                              
</td>                                             
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluuid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    

<script>    
	function change_catalyst_status(id,status) {		
	var r = confirm("Are you sure do you want to change the status");
	if (r == true) {
		jQuery.ajax({
			type:'POST',
			data:{id:id,status:status},			
			url:'change_catalyst_status',
			success:function(data){
			$("#statuschange"+id).html(data);				
			}
        
		});
	} else {

	}	
	}
</script>

</body>

</html>