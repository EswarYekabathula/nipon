<?php include('includes/header.php'); 
$this->load->model('Catalyst_model','my_model');
?>
<style>
.red{
	color:red;
}
.dataTables_info, .dataTables_length {
    display: none !important;
}
div#example23_paginate{
	display:none !important;
}
.pagination {
    display: inline-block;
	float:right;
	    margin-top: 10px;
}

.pagination a {
    color: black;
	border: 1px solid #ddd; /* Gray */
    float: left;
    padding: 8px 16px;
    text-decoration: none;
}

.pagination  strong {
	color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    background-color: #1976d2;
    color: white;
}
a:not([href]):not([tabindex]) {
    padding: 0px !important;
}

#error-msg{
color: red;
    font-size: 14px;    
    margin-left: 110px;
}
</style>
<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
                    <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Franchise Partner Orders</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Franchise Partner Orders</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluuid  -->
            <div class="container-fluuid">
			
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
						
                                <h4 class="card-title">Franchise Orders Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
												<th>OrderID</th>
												<th>Transaction ID</th>
                                                <th>Franchise Partner Name</th>                                                                                                
                                                <th>Total Cost</th>                                               											
                                                <th>Ordered on</th>							
												<th>Actions</th>                                        
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>S.No</th>
												<th>OrderID</th>
												<th>Transaction ID</th>
                                                <th>Franchise Partner Name</th>                                                                                                   
                                                <th>Total Cost</th>					
                                                <th>Ordered on</th>																						
												<th>Actions</th>                                           
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3)+1 : 1;
										foreach($forders as $value){ ?>
                                            <tr id="row<?php echo $value['oid']; ?>">
                                                <td><?php  echo $start_index++; ?></td>
												<td><?php echo $value['order_id']; ?></td>
												<td><?php echo $value['transaction_id']; ?></td>
                                                <td><?php echo $this->my_model->check('franchise_partners',array('fid'=>$value['uid']))->row()->name; ?></td>
                                                <td><?php echo $value['total_cost']; ?></td> <td><?php echo date("Y-m-d H:i",strtotime($value['added_on']));	?></td>
                                          
<td id="assigned_FP<?php echo $value['oid']; ?>">
<?php if($value['order_status'] != '1'){ ?>
<a href="javascript:confirm_order(<?php echo $value['oid']; ?>,'1')" class="btn btn-warning btn-xs m-b-10 m-l-5">Confirm</a>
<?php } ?>
<a href="javascript:order_details(<?php echo $value['oid']; ?>)" class="btn btn-info btn-xs m-b-10 m-l-5">View Details</a>

<!--- After Confirming Order -->
<?php if($value['order_status'] == '1'){ ?>
<a href="<?php echo base_url(); ?>catalyst/invoice/<?php echo $value['oid']; ?>" class="btn btn-primary btn-xs m-b-10 m-l-5">Invoice</a>
<?php if($value['assigned_fp'] == ""){ ?>
<!--<a href="javascript:nearby_fps(<?php echo $value['oid']; ?>)" class="btn btn-primary btn-xs m-b-10 m-l-5">Assign FP</a>-->
<?php } } ?>
</td>      
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
								 <?php echo '<div class="pagination">'.$links.'</div>'; ?>
                            </div>
                        </div>
                      
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluuid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    



<div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<!-- Modal Header -->
<div class="modal-header">
<h4 class="modal-title" id="myModalLabel">
Order Details
</h4>
</div>
<!-- Modal Body -->
<div class="modal-body">
<table  class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
<thead>
	<tr>
	<th>S.No</th>
	<th>Item Name</th> 
	<th>Image</th> 
	<th>Count</th> 
	<th>Item Cost</th>	
	</tr>
</thead>
<tbody id="detailsForm">

</tbody>

</table>
</div>
<!-- Modal Footer -->
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>

<script>    
function order_details(id){
	var base_url = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{id:id},
		url:base_url+'catalyst/order_details',
		success:function(html){
			$("#detailsForm").html(html);
			$('#detailsModal').modal('toggle');
			//alert(html);
		}
	});
}
</script>




<div class="modal fade" id="assginFPModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<!-- Modal Header -->
<div class="modal-header">
<h4 class="modal-title" id="myModalLabel">
Assign Franchise Partner
</h4>
</div>
<!-- Modal Body -->
<div class="modal-body">
	<form method="post" action="" enctype="multipart/form-data">
	
		<div class="row">
			<div class="col-md-4">Select Franchise Partner : </div>
			<div class="col-md-8">
			<select class="timepicker form-control" id="Nearby_Fps" onchange="getFpPending();">
			<option>Select Franchise Partner</option>
			</select>
			</div><br><br><br>
        </div>
		
		<div class="row">
			<div class="col-md-4">No.of Orders Pending : </div>
			<div class="col-md-8" id="FP_Pending_orders">
			
			</div><br><br><br>
        </div>
		<input  type="hidden" name="order_id" id="order_id" />
        <span id="error-msg"></span>
    </form>
</div>
<!-- Modal Footer -->
<div class="modal-footer">
<button type="button" class="btn btn-primary" onclick="return assign_FP();">Assign</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>


<script>
function nearby_fps(id){
	var base_url = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{id:id},
		url:base_url+'catalyst/nearby_fps',
		success:function(html){
			$("#Nearby_Fps").html(html);
			$("#order_id").val(id);
			$('#assginFPModal').modal('toggle');
			//alert(html);
		}
	});
}
</script>


<script>
function assign_FP(id){
	var base_url = '<?php echo base_url(); ?>';
	var fpval = $("#Nearby_Fps").val();
	var order_id = $("#order_id").val();
	if(fpval == "" || fpval == "null"){
		$("#error-msg").html('Please select Franchise partner');
	}else{		
		$("#error-msg").html('');
		jQuery.ajax({
			type:'POST',
			data:{order_id:order_id,fp:fpval},
			url:base_url+'catalyst/assign_fps',
			success:function(data){
				var fpdata = JSON.parse(data);
				//console.log(fpdata);
				//console.log(fpdata.fp_name);
				if(fpdata.status == 1){
				$('#assginFPModal').modal('hide');
				setTimeout(function() {
		iziToast.success({   message: "Franchise partner assigned successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);
				$("#assigned_FP"+order_id).html('<a href="javascript:order_details(<?php echo $value['oid']; ?>)" class="btn btn-primary btn-xs m-b-10 m-l-5">View Details</a>');
				$("#assigned_fp_name"+order_id).html(fpdata.fp_name);
				}else{
					setTimeout(function() {
		iziToast.warning({   message: "Please try later!",   position: "topRight",   zindex:	"99999"		}); }, 100);
				}
			}
		});
	}
}
</script>
         
<script>		 
function confirm_order(oid,order_status){
	var bu = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{order_id:oid,order_status:order_status},
		url:bu+'catalyst/confirm_fp_order',
		success:function(data){
			result = JSON.parse(data);
			if(result['status'] == 1){
			setTimeout(function() {
		iziToast.success({   message: "Order confirmed successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);
			$("#assigned_FP"+oid).html(result['msg']);
			}else{		
				setTimeout(function() {
		iziToast.warning({   message: "Please try later!",   position: "topRight",   zindex:	"99999"		}); }, 100);				
			}			
		}
	});
}
</script>		 

<script>
$('#example23').dataTable({	
$("#example23").dataTable().fnDestroy();	    
});
</script>


<script>
function getFpPending(){
	var bu = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		url:bu+'catalyst/getFpPending',
		success:function(data){
			result = JSON.parse(data);			
			$("#FP_Pending_orders").html(result['pending_orders']);					
		}
	});
}
</script>


</body>
</html>