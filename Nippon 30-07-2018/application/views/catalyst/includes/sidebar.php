<div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
<li>
<a class="" href="<?php echo base_url(); ?>catalyst" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard </span></a>
</li>

<li><a href="<?php echo base_url(); ?>catalyst/dealer_requests" aria-expanded="false"><i class="fa fa-hand-o-right"></i><span class="hide-menu">Dealer Requests</span></a></li>                                
<li><a href="<?php echo base_url(); ?>catalyst/dealers" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu">Dealers</span></a></li>                                
<li><a href="<?php echo base_url(); ?>catalyst/franchise_partners" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu">Franchise Partners</span></a></li>                                
<li><a href="<?php echo base_url(); ?>catalyst/delivery_partners" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu">Delivery Partners</span></a></li>                                
<li><a class="" href="<?php echo base_url(); ?>catalyst/feedbacks" aria-expanded="false"><i class="fa fa-comments-o"></i><span class="hide-menu">Feedbacks</span></a></li>	
<li><a class="" href="<?php echo base_url(); ?>catalyst/offers" aria-expanded="false"><i class="fa fa-ticket"></i><span class="hide-menu">Offers/Schemes</span></a></li>	

<li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-cart-plus"></i><span class="hide-menu">Orders</span></a>
<ul aria-expanded="false" class="collapse">
	<li><a href="<?php echo base_url(); ?>catalyst/dealer_orders">Dealers Orders</a></li>                                
	<li><a href="<?php echo base_url(); ?>catalyst/fp_orders">Franchise Partners Orders</a></li>  
</ul>
</li>	
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>