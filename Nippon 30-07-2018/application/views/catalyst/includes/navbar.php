<?php
$cid = $this->session->userdata('catalyst_session')['id'];
$this->load->model('catalyst_model','my_model');
?>
<style>
.mailbox .message-center a .mail-contnet .mail-desc {    
    white-space:  unset !important; 
}
</style>
<nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url(); ?>/catalyst">
                        <!-- Logo icon -->
                        <!--<b><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="homepage" class="dark-logo" /></b>-->
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <!--<span><img src="<?php echo base_url(); ?>assets/images/logo-text.png" alt="homepage" class="dark-logo" /></span>-->
						<b>Xpress Delivery</b>
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <!-- Messages -->
                      
                        <!-- End Messages -->
                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">
					<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle text-muted text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
							<i class="fa fa-bell" onclick="update_notifications(<?php echo $cid; ?>)"></i>
							<div class="notify" id="notify"> 
								<?php 								
								$where = "uid=".$cid." and send_to='Admin' and view_status='0'";
								$nrows = $this->my_model->count_with_condition('notifications',$where); 
								if($nrows > 0){
								?>
								<span class="heartbit" onclick="update_notifications(<?php echo $cid; ?>)"></span> 
								<span class="point" onclick="update_notifications(<?php echo $cid; ?>)"></span> 
								<?php } ?>
							</div>
							</a>
							
													
                            <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn" >
                                <ul>
                                    <li>
                                        <div class="drop-title">Notifications</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
											<?php
											$where = "uid=".$cid." and send_to='Admin'";
											$notifications = $this->my_model->check('notifications',$where)->result_array();
											foreach($notifications as $nval){
											?>
                                            <!-- Message -->
                                            <a href="#">
                                                <div class="btn btn-success btn-circle m-r-10"><i class="ti-calendar"></i></div>
                                                <div class="mail-contnet">
                                                     <span class="mail-desc"><?php echo $nval['message']; ?></span> <span class="time"><?php echo date('d D F Y', strtotime($nval['date'])); ?></span>
                                                </div>
                                            </a>
											<?php } ?>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<?php if($this->session->userdata('catalyst_session')['image'] != ''){ ?>
							<img src="<?php echo base_url(); ?>assets/uploads/users/<?php print_r($this->session->userdata('catalyst_session')['image']); ?>" alt="user" class="profile-pic" />
							<?php }else{ ?>
							<img src="<?php echo base_url(); ?>assets/uploads/users/default.png" alt="user" class="profile-pic" />
							<?php } ?>							
							</a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">
                                    <li><a href="<?php echo base_url(); ?>catalyst/edit_profile"><i class="ti-user"></i> Profile</a></li>
                                    <li><a href="<?php echo base_url(); ?>catalyst/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
			
			
			<script>
			function update_notifications(fp_id){
				var base_url = '<?php echo base_url(); ?>';
				var send_to = 'Admin';
				jQuery.ajax({
					type:'POST',
					data:{uid:fp_id,send_to:send_to},
					url:base_url+'fp/update_notifications_status',
					success:function(data){
						if(data == 1){
						$("#notify").hide();
						}
					}
				});
			}
			</script>