<?php include('includes/header.php'); ?>
<style>
#error-msg{
color: red;
    font-size: 14px;    
    margin-left: 110px;
}
</style>
<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
                    <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Feedbacks</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Feedbacks</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
			<?php  if($this->session->flashdata('message') == "deleted"){ 	 
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Pincode Deleted successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "updated"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Pincode Updated successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "added"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Pincode Added successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}
		?>
            <!-- Container fluuid  -->
            <div class="container-fluuid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
							
							
                                <h4 class="card-title">Feedbacks Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>User Name</th> 
                                                <th>Email</th> 
                                                <th>Mobile</th> 
                                                <th>Query Type</th> 
												<th>Message</th> 
												<th>Actions</th>  												
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>S.No</th>
                                                <th>User Name</th> 
                                                <th>Email</th> 
                                                <th>Mobile</th> 
                                                <th>Query Type</th> 
												<th>Message</th>                                           
												<th>Actions</th>                                           
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        <?php $i=1; foreach($feedbacks as $value){ ?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $value['username']; ?></td>
                                                <td><?php echo $value['email']; ?></td>
                                                <td><?php echo $value['phonenumber']; ?></td>
                                                <td><?php echo $value['querytype']; ?></td>
                                                <td><?php echo $value['message']; ?></td>
<td>
<a href="javascript:sendReply('<?php echo $value['email']; ?>');" class="btn btn-success btn-xs m-b-10 m-l-5"  >Reply</a>                                              

</td>                                             
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
						
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluuid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    

<script>    
	function change_catalyst_status(id,status) {		
	var r = confirm("Are you sure do you want to change the status");
	if (r == true) {
		jQuery.ajax({
			type:'POST',
			data:{id:id,status:status},			
			url:'change_catalyst_status',
			success:function(data){
			$("#statuschange"+id).html(data);				
			}
        
		});
	} else {

	}	
	}
</script>


<!-- Modal -->
<div class="modal fade" id="myModalHorizontal" tabindex="-1" role="dialog" 
aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<!-- Modal Header -->
<div class="modal-header">
<h4 class="modal-title" id="myModalLabel">
Reply to Feedback
</h4>
</div>
<!-- Modal Body -->
<div class="modal-body">
<form class="form-horizontal" role="form">
<div class="row form-group">
<label  class="col-sm-2 control-label"	for="inputEmail3">Email</label>
<div class="col-sm-10">
<input type="email" class="form-control" 	id="send_email" placeholder="Email"/>
</div>
</div>
<div class="row form-group">
<label class="col-sm-2 control-label"	for="inputPassword3" >Message</label>
<div class="col-sm-10">
<textarea  cols="48" rows="5"	id="message" ></textarea>
</div>
</div>
<span id="error-msg"></span>
</form>
</div>
<!-- Modal Footer -->
<div class="modal-footer">
<button type="button" class="btn btn-primary" onclick="send_reply_email();">Reply</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>

<script>
function sendReply($email){
	var email = $email;
	$("#send_email").val(email);
	$('#myModalHorizontal').modal('toggle');
}
</script>

<script>
function send_reply_email(){
	var base_url = '<?php echo base_url(); ?>';
	var email = $("#send_email").val();
	var message = $("#message").val();
	if(message == "" || message == "null"){
		$("#error-msg").html('Please enter message to reply');
	}else{	
		$("#error-msg").html();
		jQuery.ajax({
			type:'POST',
			data:{email:email,message:message},
			url:base_url+'admin/reply_feedback',
			success:function(data){
				if(data == 1){
					setTimeout(function() {
			iziToast.success({   message: "Email sent successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);
			$('#myModalHorizontal').modal('hide');
				}else{
					setTimeout(function() {
			iziToast.warning({   message: "Please try later!",   position: "topRight",   zindex:	"99999"		}); }, 100);
			$('#myModalHorizontal').modal('hide');
				}
			}
		});
	}
}
</script>

</body>
</html>