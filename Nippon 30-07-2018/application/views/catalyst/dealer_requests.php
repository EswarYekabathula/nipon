<?php include('includes/header.php'); ?>
<style>
.red{
	color:red;
}
.dataTables_info, .dataTables_length {
    display: none !important;
}
div#example23_paginate{
	display:none !important;
}
.pagination {
    display: inline-block;
	float:right;
	    margin-top: 10px;
}

.pagination a {
    color: black;
	border: 1px solid #ddd; /* Gray */
    float: left;
    padding: 8px 16px;
    text-decoration: none;
}

.pagination  strong {
	color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    background-color: #1976d2;
    color: white;
}
a:not([href]):not([tabindex]) {
    padding: 0px !important;
}
.mtop{
	margin-top:10px !important;
	margin-left:125px !important;
}
</style>
<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <?php include('includes/navbar.php'); ?>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
                    <?php include('includes/sidebar.php'); ?>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Dealer Requests</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dealer Requests</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluuid  -->
            <div class="container-fluuid">
			<?php  if($this->session->flashdata('message') == "success"){ 	 
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Dealer Deleted successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "updated"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Dealer Updated successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}else if($this->session->flashdata('message') == "added"){
			echo '<script>setTimeout(function() {
		iziToast.success({   message: "Dealer Added successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);</script>';	 
		}
		?>
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
						
                                <h4 class="card-title">Dealer Requests Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Name</th>                                                                                                
                                                <th>Email</th>
                                                <th>Phone</th>												
												<th>Actions</th>                                    
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Name</th>                                                                                                
                                                <th>Email</th>
                                                <th>Phone</th>												
												<th>Actions</th>                                           
                                            </tr>
                                        </tfoot>
                                         <tbody>
                                        <?php $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3)+1 : 1; foreach($dealers as $value){ ?>
                                            <tr id="row<?php echo $value['did']; ?>">
                                                <td><?php  echo $start_index++; ?></td>
                                                <td><?php echo $value['dealer_name']; ?></td>
                                                <td><?php echo $value['email']; ?></td>
                                                <td><?php echo $value['mobile']; ?></td>
                                              
<td id="verifyButton<?php echo $value['did']; ?>">
<span>
<?php if($value['kyc_status'] == '1'){ ?>
<button type="button" class="btn btn-info btn-xs m-b-10 m-l-5">KYC FILLED</button>
<?php }else{ ?>
<button type="button" class="btn btn-warning btn-xs m-b-10 m-l-5" >KYC NOT FILLED</button>
<?php } ?>
</span>


<?php if($value['kyc_status'] == '0'){ ?>
<a href="javascript:get_details('<?php echo $value['did']; ?>','dealers')"  class="btn btn-success btn-xs m-b-10 m-l-5">VERIFY DETAILS</a>
<?php } ?>


</td>                                             
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
								 <?php echo '<div class="pagination">'.$links.'</div>'; ?>
                            </div>
                        </div>
                      
                    </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluuid  -->
            <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved. </footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/custom.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/lib/datatables/datatables-init.js"></script>
    

<div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" 
aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<!-- Modal Header -->
<div class="modal-header">
<h4 class="modal-title" id="myModalLabel">
Verify Details
</h4>
</div>
<!-- Modal Body -->
<div class="modal-body">
<form class="form-horizontal" role="form"  id="detailsForm">


</form>
</div>
<!-- Modal Footer -->
<div class="modal-footer">
<button type="button" class="btn btn-primary" onclick="verify_dealer();">Verify</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>

<script>    
function get_details(id,table){
	var base_url = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{id:id,table:table},
		url:base_url+'catalyst/get_details',
		success:function(html){
			$("#detailsForm").html(html);
			$('#detailsModal').modal('toggle');
			//alert(html);
		}
	});
}
</script>

<script>
function verify_dealer(){
	var permit = confirm('Are you sure do you want to verify this dealer?');
	if( permit == true ){
	var dealer_id = $("#dealer_id").val();
	var base_url = '<?php echo base_url(); ?>';
	jQuery.ajax({
		type:'POST',
		data:{id:dealer_id},
		url:base_url+'catalyst/verify_dealer',
		success:function(data){
			if(data == 1){
				setTimeout(function() {
		iziToast.success({   message: "Dealer Verified successfully!",   position: "topRight",   zindex:	"99999"		}); }, 100);
		$('#detailsModal').modal('hide');
		$("#verifyButton"+dealer_id).html('<button type="button" class="btn btn-info btn-xs m-b-10 m-l-5">KYC FILLED</button>');
			}else{
				setTimeout(function() {
		iziToast.warning({   message: "Please try later!",   position: "topRight",   zindex:	"99999"		}); }, 100);
			}
		}
	});
	}
}
</script>


<script>
$('#example23').dataTable({	
$("#example23").dataTable().fnDestroy();	    
});
</script>

</body>

</html>