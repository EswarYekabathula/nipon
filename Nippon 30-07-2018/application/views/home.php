<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>HIL</title>
	    <meta name="description" content="" />
	    <meta name="keywords" content="" />
	    <meta name="author" content="Themesdesign" />
	    <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/home/images/favicon.png">

	    <!-- google font -->
	    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
	    <!-- Bootstrap core CSS -->
	    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/home/css/bootstrap.min.css" type="text/css">
	    <!--Magnific popup -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/home/css/magnific-popup.css" />
	    <!--Material Icon -->
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/home/css/materialdesignicons.min.css" />
	    <!-- Mobrise  css -->
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/home/css/mobrise.css">
	    <!--Slider-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/home/css/owl.carousel.css"/> 
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/home/css/owl.theme.css"/> 
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/home/css/owl.transitions.css"/>
	    <!-- jquery.fullPage.css -->
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/home/css/jquery.fullPage.css">
	    <!-- Custom  Css -->
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/home/css/style.css"/>
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/home/css/colors/default.css"/>

	</head>
    <body>
    	

           


    	<div class="sticky sticky-dark fixed-top">
	        <!-- Header -->
			<header>
				<nav class="navbar-custom">
					<div class="container nav-position">
						<div class="row">					
								
							<!-- Logo -->
							<a class="navbar-brand logo text-uppercase" href="index-1.html">
				                <img src="<?php echo base_url(); ?>assets/home/images/logo.png" alt="" height="28" />
				            </a>

							
							<!-- Menu button open -->
							<a href="#" class="menu-btn-open">
								<span class="line1"></span>
								<span class="line2"></span>
								<span class="line3"></span>
							</a>
							
						</div>	
					</div>
				</nav>
			</header>
			
 <?php  if($this->session->flashdata("message")){ ?>
<div id="alert" class="alert alert-info" style="width:450px;margin-left: 460px;margin-top:28px;position:relative">
Thank you for contacting us we will get back to you ASAP!
</div>
<?php } ?>

			<!-- Lightbox menu -->
			<div class="menu-lightbox">
				<div class="container nav-position">
					<div class="row">										
						<!-- Menu button close -->
						<a href="#" class="menu-btn-close">
							<span class="line1"></span>
							<span class="line2"></span>
						</a>
					
					</div>
				</div>
				
				<ul class="menu-nav">
					<li><a href="#home">Home</a></li>
					<li><a href="#howitwork">How It Works?</a></li>
					<li><a href="#features">Features</a></li>
					<li><a href="#contact">Contact</a></li>
				</ul>
			</div>
		</div>

        <div id="fullpage">        	
	        <!-- Home -->
	        <section class="section bg-home" id="home">	 
	        	<div class="bg-overlay"></div>       	
	            <div class="home-center">
	                <div class="home-desc-center">
	                    <div class="container">
	                        <div class="row justify-content-denter vertical-content responsive-padding">
	                            <div class="col-lg-8 mt-3">
	                                <div class="">
	                                	<h1 class="text-white home-title">Instafix - your one stop solution for all your plumbing service needs</h1>
	                                	<p class="text-white home-subtitle pt-4">Welcome to INSTAFIX – powered by HIL Limited. Instafix is a one stop solution for all your plumbing
service needs through which you can find plumbers in your area &amp; call them ASAP or set
appointment for the future. Powered by HIL, leading building solutions manufacturers, it is an
endeavour by HIL to bring together consumers &amp; plumbers on one platform to help the consumers
in your plumbing service needs &amp; its free i.e. HIL does not charge any fees or take any commission.
You can directly interact with consumers</p>
	                                	<div class="">
	                                		<a href="#" class="btn btn-custom mt-4 mr-3"> App Store <i class="mdi mdi-apple"></i></a>
	                                		<a href="#" class="btn btn-outline-custom mt-4 mr-3">Google Play <i class="mdi mdi-google-play"></i></a>
	                                	</div>
	                                </div>
	                            </div>
	                            <div class="col-lg-4 mt-3">
	                            	<div>
	                            		<img src="<?php echo base_url(); ?>assets/home/images/iphone.png" alt="" class="img-fluid mx-auto d-block">
	                            	</div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </section>
	        <!-- Home end -->


	        <section class="section bg-services" id="howitwork">
	        	<!-- <div class="bg-overlay"></div> -->
	        	<div class="home-center">
                	<div class="home-desc-center">
			        	<div class="container">
			                <div class="row justify-content-center">
			                    <div class="col-lg-6">
			                        <div class="title text-center text-white">
			                            <h2 class="font-weight-normal">How It Works ?</h2>
			                            <div class="pt-3 pb-3">
				                            <div class="title-box"></div>
											<div class="title-line"></div>
										</div>
			                            <p class=" pt-2">It’s very simple. Just download &amp; install the APP from Google Play Store or Apple App Store. &amp; you
are ready to go. For plumbers, KYC is mandatory.</p>
			                        </div>
			                    </div>
			                </div>
			                <div class="row pt-4 mt-4">
			                	<div class="col-lg-4">
			                		<div class="service-box text-center p-3">
			                			<div class="services-icon">
			                				<span>1</span>
			                			</div>
			                			<div class="services-desc text-white mt-2">
			                				<!--<h4>Lorem Ipsum</h4>-->
			                                <p class="mb-0 mt-3">Download the APP from Google Play Store or Apple App Store if you are using Android based
handset or iPhone respectively.</p>
			                			</div>
			                		</div>
			                	</div>
			                	<div class="col-lg-4">
			                		<div class="service-box text-center p-3">
			                			<div class="services-icon">
			                				<span>2</span>
			                			</div>
			                			<div class="services-desc text-white mt-2">
			                				<!--<h4>Lorem Ipsum</h4>-->
			                                <p class="mb-0 mt-3">Login as the User or Plumber if you are User or Plumber. Users can skip login or registration process
&amp; can directly go to “View Plumbers in your Area”. For Plumbers, KYC is required after registration &amp;
only after the KYC is verified &amp; cleared by HIL, they will be displayed on APP as plumbers after in
their areas.</p>
			                			</div>
			                		</div>
			                	</div>
			                	<div class="col-lg-4">
			                		<div class="service-box text-center p-3">
			                			<div class="services-icon">
			                				<span>3</span>
			                			</div>
			                			<div class="services-desc text-white mt-2">
			                				<!--<h4>Lorem Ipsum</h4>-->
			                                <p class="mb-0 mt-3">User has to call the plumber on the displayed number to negotiate for the service &amp; its
corresponding price &amp; also the time slot. Once the service, its price &amp; time slots are finalized on call,
user can sent the invite on the calendar to plumber or vice versa. Other party has to accept the
invite &amp; VOILA! it’s done.</p>
			                			</div>
			                		</div>
			                	</div>
			                </div>
			            </div>
		            </div>
	            </div>
	        </section>


	        <section class="section bg-features" id="features">
	        	<div class="bg-overlay"></div>
	        	<div class="home-center">
                	<div class="home-desc-center">
			        	<div class="container">
			                <div class="row justify-content-center">
			                    <div class="col-lg-6">
			                        <div class="title text-center text-white">
			                            <h2 class="font-weight-normal">Mobile features</h2>
			                            <div class="pt-3 pb-3">
				                            <div class="title-box"></div>
											<div class="title-line"></div>
										</div>
			                            <p class=" pt-2">Exciting features available in the APP are:</p>
			                        </div>
			                    </div>
			                </div>
							<div class="row vertical-content mt-4 pt-4">
								<div class="col-lg-4">
									<div class="text-white features-box">
										<div class="features-icon">
											<i class="mbri-photo"></i>
										</div>
										<div class="features-content">
											<!--<h5>Lorem Ipsum</h5>-->
											<p>It’s completely free. HIL doesn’t charge any fee or take any commission for this APP. Deal is
between you &amp; plumber directly.</p>
										</div>
									</div>
									<div class="text-white features-box">
										<div class="features-icon">
											<i class="mbri-video-play"></i>
										</div>
										<div class="features-content">
											<!--<h5>Lorem Ipsum</h5>-->
											<p>Consumer can directly book the appointment without any registration or login.</p>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<img src="<?php echo base_url(); ?>assets/home/images/shot-1.png" alt="" class="img-fluid mx-auto d-block">
								</div>
								<div class="col-lg-4">
									<div class="text-white features-box">
										<div class="features-icon">
											<i class="mbri-desktop"></i>
										</div>
										<div class="features-content">
											<!--<h5>Lorem Ipsum</h5>-->
											<p>Available throughout the length &amp; breadth of India.</p>
										</div>
									</div>
									<div class="text-white features-box">
										<div class="features-icon">
											<i class="mbri-website-theme"></i>
										</div>
										<div class="features-content">
											<!--<h5>Lorem Ipsum</h5>-->
											<p>Plumber can directly market themselves through this APP.</p>
										</div>
									</div>
								</div>
							</div>
			            </div>
		            </div>
	            </div>
	        </section>

	        <section class="section bg-contact" id="contact">
	        	<div class="bg-overlay"></div>
	        	<div class="container">
	        		<div class="row justify-content-center">
	                    <div class="col-lg-6">
	                        <div class="title text-center text-white">
	                            <h2 class="font-weight-normal">Contact Us</h2>
	                            <div class="pt-3 pb-3">
		                            <div class="title-box"></div>
									<div class="title-line"></div>
								</div>
	                            <p class=" pt-2">In case you want to contact us with some feedback or suggestion, please feel free to fill the form
below..</p>
	                        </div>
	                    </div>
	                </div>
	                <div class="mt-4 pt-4">
	                    <form class="form-custom text-white" method="post" action="<?php echo base_url(); ?>admin/contact_us">
	                        <div class="row">
	                            <div class="col-lg-6">
	                                <div class="form-group">
	                                    <label for="name">Name</label>
	                                    <input name="name" id="name" type="text" class="form-control" placeholder="Your name..." required>
	                                </div>
	                            </div>
	                            <div class="col-lg-6">
	                                <div class="form-group">
	                                    <label for="email">Email address</label>
	                                    <input name="email" id="email" type="email" class="form-control" placeholder="Your email..." required>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row">	                        	
	                            <div class="col-lg-12">
	                                <div class="form-group">
	                                    <label for="subject">Subject</label>
	                                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Your Subject.."  required />
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-lg-12">
	                                <div class="form-group">
	                                    <label for="comments">Message</label>
	                                    <textarea name="message" id="comments" rows="4" class="form-control" placeholder="Your message..." required></textarea>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-lg-12 text-right">
	                                <input type="submit" class="btn btn-custom" value="Send Message">
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-lg-12">
	                                <div class="text-center mt-5">
	                                	<p class="mb-0">&copy; 2018 HIL</p>
	                                </div>
	                            </div>
	                        </div>
	                    </form>
	                </div>  
	        	</div>
	        </section>
	    </div>



        <!-- JAVASCRIPTS -->
        <script src="<?php echo base_url(); ?>assets/home/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/home/js/popper.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/home/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/home/js/jquery.easing.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/home/js/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/home/js/jquery.fullPage.js"></script>
        <script src="<?php echo base_url(); ?>assets/home/js/scrollspy.min.js"></script>
        <!-- Magnific Popup -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/home/js/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/home/js/owl.carousel.min.js"></script>		
        <script src="<?php echo base_url(); ?>assets/home/js/app.js"></script>    


<script>
$("#alert").fadeOut(8000);
</script>       
    </body>
</html>